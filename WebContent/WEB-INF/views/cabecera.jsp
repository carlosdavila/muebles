<%@page import="org.springframework.context.annotation.Import"%>
<%@ page language="java" pageEncoding="UTF-8" session="true"
	isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>



<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<c:set var = "gPath" scope = "session" value = "${pageContext.servletContext.contextPath}"/>
<script type="text/javascript">

	gPath = '${gPath}';

	</script>
<script type="text/javascript">

	/*
	 * DEFINICION DE OBJETO GLOBAL ATENEA
	 * 	
	 */
	var ERP = {
		//CONFIRMACION
		MSG_CONFIRMACION_GRABADO : '<spring:message code="mensajeconfirmacion.registro" />',
		
		MSG_CONFIRMACION_ELIMINACION : '<spring:message code="mensajeconfirmacion.eliminacion" />',
	
		MSG_CONFIRMACION_ENVIO : '<spring:message code="mensajeconfirmacion.registro.envio" />',
		MSG_CONFIRMACION_RECHAZO : '<spring:message code="mensajeconfirmacion.rechazo" />',
		MSG_CONFIRMACION_AUTOASIGNAR : '<spring:message code="mensajeconfirmacion.autoasignar" />',
		MSG_CONFIRMACION_REGISTRO_VALIDO : '<spring:message code="mensajeconfirmacion.registro.valido" />',
		MSG_CONFIRMACION_REGISTRO_INVALIDO : '<spring:message code="mensajeconfirmacion.registro.invalido" />',

		//REQUEST.
		MSG_ERROR_GENERAL : '<spring:message code="mensajeestatico.error.general" />',
		MSG_GRABADO_EXITO : '<spring:message code="mensajerespuesta.exito.guardo" />',
		MSG_ELIMINACION_EXITO : '<spring:message code="mensajerespuesta.exito.elimino" />',
		MSG_ERROR_RESPONSABLE : '<spring:message code="mensajerespuesta.error.noexisteresponsable" />',

		//VALIDACION.
		MSG_VAL_OBLIGATORIO : '<spring:message code="validacion.comun.mensajeobligatorio" />',
		MSG_VAL_MAXIMO_CARACTERES : '<spring:message code="validacion.comun.mensajemaximotexto" />',
		MSG_VAL_MINIMO_CARACTERES : '<spring:message code="validacion.comun.mensajeminimotexto" />',
		MSG_VAL_MAXIMO_DIGITOS : '<spring:message code="validacion.comun.mensajemaximodigito" />',
		MSG_VAL_MINIMO_DIGITOS : '<spring:message code="validacion.comun.mensajeminimodigito" />',
		MSG_VAL_FORMATO : '<spring:message code="validacion.comun.mensajetipodatotexto" />',
		MSG_VAL_ENTERO : '<spring:message code="validacion.comun.mensajetipodatodigito" />',
		MSG_VAL_DECIMAL : '<spring:message code="validacion.comun.mensajetipodatodecimal" />',
		MSG_VAL_FECHA : '<spring:message code="validacion.comun.mensajetipodatofecha" />',
		MSG_VAL_NOMBRE_VENTA : '<spring:message code="validacion.comun.nombreventa" />',

		//Titulo de dialogos.
		TITULO_CONFIRMACION_REGISTRO : '<spring:message code="titulo.confirmacion.registro" />',
		TITULO_DENEGACION_ACCION : '<spring:message code="titulo.denegacion.accion" />',

		PARAMETRO_ROL_ADMINISTRADOR : '<spring:message code="atenea.rol.administrador"/>',

		//PATRONES VALIDACION.
		PATRON_DECIMAL : /^(\d{1,9}|\d{1,9}\.\d{1,2})$/,
		PATRON_HORA : /^(\d{1,2}):(\d\d)$/
		
	};
</script>
<c:out value=""></c:out>





<link href="${gPath}/resources/js/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<link href="${gPath}/resources/js/bootstrap/css/bootstrap-add.css" rel="stylesheet">	

<link href="${gPath}/resources/css/botones.css" rel="stylesheet">

<link href="${gPath}/resources/js/dataTable/datatable.css" rel="stylesheet">


<link href="${gPath}/resources/css/sweetalert2.min.css" rel="stylesheet">	

<link href="${gPath}/resources/css/w3.css" rel="stylesheet">


<link href="${gPath}/resources/css/font-awesome.min.css" rel="stylesheet">


<link href="${gPath}/resources/css/showLoading.css" rel="stylesheet">
 <script type="text/javascript" src="${gPath}/resources/js/jqueryOld.js"></script>

<!-- Bootstrap core JavaScript -->
<!-- Placed at the end of the document so the pages load faster -->

 <script type="text/javascript" src="${gPath}/resources/js/bootstrap/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="${gPath}/resources/js/bootstrap/js/modal.js"></script>

 <script type="text/javascript" src="${gPath}/resources/js/jquery-validation/dist/jquery.validate.js"></script>

<script type="text/javascript" src="${gPath}/resources/js/comun.js"></script>

<script type="text/javascript" src="${gPath}/resources/js/comunValidation.js"></script>

<script type="text/javascript" src="${gPath}/resources/js/constante.js"></script>

<script type="text/javascript" src="${gPath}/resources/js/jquery.showLoading.min.js"></script>



<script type="text/javascript" src="${gPath}/resources/js/dataTable/datatable.js"></script>


<script type="text/javascript" src="${gPath}/resources/js/dataTable/reload.js"></script>	
	


<script type="text/javascript" src="${gPath}/resources/js/sweetalert2.min.js"></script>	

<script type="text/javascript" src="${gPath}/resources/js/home.js"></script>	

 <script type="text/javascript" src="${gPath}/resources/js/checkout.js"></script>	
 <!-- <script src="https://www.paypalobjects.com/api/checkout.js"></script>-->
</head>

    	<c:if test="${isPc}">
     <style>
         .container{
           padding-right:15px;padding-left:0px!important;margin-right:auto;margin-left:auto
           }
           

           
         </style>
         </c:if>
         
         