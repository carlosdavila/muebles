<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>


<script type="text/javascript" src="${gPath}/resources/js/carrito/gestionCarritoDetalle.js"></script>	
<c:url value='${gPath}/carritoDetalleController/obtenerListaSession'
	var="urlListaFiltrada" />
</head>


    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '907471339401877');
      fbq('track', 'Purchase');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=907471339401877&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->



<button type="button" class="btn btn-info btn-lg" id="btnMyModalCarrito"
	data-toggle="modal" style="visibility: hidden;" data-target="#myModal">Open
	Modal</button>

	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog" >

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="   font-family: 'Roboto'!important;
         font-style: italic!important;">Carrito de Compras</h4>
				</div>
				<div class="modal-body">

<form id="formGestionCarritoDetalle" action="${urlListaFiltrada}"
	method="GET">


	
		<table id="listGestionCarritoDetalle" class="display"
		style="text-align: left;width: 100%;   font-family: 'Roboto'!important;
         font-style: italic!important;">
			<thead>
				<tr>
					<th>PRODUCTO</th>
					<th>DET.</th>
					<th>PRECIO</th>
					<th>CANT.</th>
					<th>TOTAL</th>
					<th></th>
				</tr>
			</thead>
			
			
			<tfoot>
				<tr>
					<th>Total:</th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</tfoot>

		</table>


</form>
	</div>

				<div class="modal-footer">
				 <div id="paypal-button-container"></div>
				  <script>
				  
				  
				  
        paypal.Button.render({
   
            env:  'production', // sandbox | production
   
            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            client: {
            
            	sandbox:    'AWdCpkr9aQPuNmzLWAmAdcJSoYnzfcA-z_0IWXvgq3MRyX42KJ28Q5rIwRv9F9Db12hklqUPY_E8-AcC',
                production: 'AQX5sQrTVmfOjPRJJeBH7V_GPBHlK27oq97ViZ_vO26XaXN2-_jbMkLVMnnZJkmIFYhr1GUOpSr0-3c-'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: function(data, actions) {
            	
            	var table = $('#listGestionCarritoDetalle').DataTable();

            	var data2 = table.rows().data();
            

                	var cadena='{"items":[';
           
            	for (var i = 0; i < data2.length; i++) {
            
            	
          
            		 if(i==(eval(data2.length)-1)){
            			 	cadena=cadena+'{"name": "'+ data2[i].productoBean.productoNombre + 
         	               '","description":"'+ data2[i].productoBean.productoDescripcion+ 
         		           '","quantity":"'+ data2[i].carritoDetalleCantidad+ 
         		           '","price":"'+ data2[i].carritoDetallePrecio+ 
         		           '","currency":"USD"}]}';         
            		 }else{
            			 	cadena=cadena+'{"name": "'+ data2[i].productoBean.productoNombre + 
         	               '","description":"'+ data2[i].productoBean.productoDescripcion+ 
         		           '","quantity":"'+ data2[i].carritoDetalleCantidad+ 
         		           '","price":"'+ data2[i].carritoDetallePrecio+ 
         		           '","currency":"USD"},';
            		 }
            		 
            	}
            	var obj = jQuery.parseJSON( cadena );
            
            
                // Make a call to the REST api to create the payment
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { total: total, currency: 'USD' },
                                
                                item_list: obj
                            }
                        ]
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function(data, actions) {

                // Make a call to the REST api to execute the payment
                return actions.payment.execute().then(function() {
                	
                  //  window.alert('Payment Complete!');
                  //  alert(data.id);
                    guardarCarritoDetalle();
                });
            }

        }, '#paypal-button-container');

    </script>
 	<!-- 
				 		<c:if
				test="${usuario.usuarioPerfil == 'publico' or usuario.usuarioPerfil == 'administrador'}">
				   <input class="btn btn-cargar" type="button" value="ENVIAR" 
					id="btnCarritoGuardar">
			</c:if>
			-->
			<!-- <button type="button" id="btnCarritoCompraCancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
			-->
				<div class="mensajeError" id="errorCarritoVacio" style="text-align: center;">
						<label class="mensajeError">Carrito de Compras vacio</label>
		</div>
			<div class="mensajeError" id="errorGenericoCarrito" style="text-align: center;">
						<label class="mensajeError">Sucedio un error generico</label>
		</div>
			<div class="alert alert-success" id="successCarritoCompra">
                  Se envio satisfactoriamente.
             </div>
		      </div>
			</div>

		</div>
	
	</div>

<script>
	$(document).ready(function() {
	
		$("#btnMyModalCarrito").click();
		configurarGestionCarritoDetalle();

	});
</script>
</html>