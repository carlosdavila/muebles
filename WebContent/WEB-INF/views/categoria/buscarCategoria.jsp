<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>


<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/categoria/buscarCategoria.js' />"></script>
<c:url value='${gPath}/categoriaController/obtenerListaFiltrada'
	var="urlListaFiltrada" />
	

<body>
<h2 class="ico_tit_formulario">Buscar Categoria</h2>
	<div id="content" class="jqGrid">
		<form id="formBuscarCategoria" action="${urlListaFiltrada}"
			method="GET">
			<div class="bs-group">
				<div class="col-md-12">
					<label class="control-label">BUSQUEDA:</label>

					<div class="txt_descr_dato" style="text-align: left; width: 80px">
						<select id="cboColumnaBuscarCategoria" name="columna"
							style="text-align: left; width: 90%">
							<option value="categoriaNombre">Nombre</option>
							<option value="categoriaTipo">Tipo</option>
							<option value="categoriaPadre">Padre</option>
						</select>
					</div>

					<div class="txt_descr_dato" style="text-align: left; width: 100px">
						<input type="text" id="txtValorBuscarCategoria" name="valor"
							style="text-align: left; width: 80px">
					</div>


					<input type="submit" class="btn btn-buscar " value="Buscar">

				</div>

				<div class="col-md-12">
					<table id="listBuscarCategoria" class="table table-bordered"></table>
					<div id="plistBuscarCategoria"></div>
				</div>
				<div class="parrafo"></div>
			</div>
		</form>
	</div>
</body>
<script>
	$(document)
			.ready(
					function() {
				
						configurarBuscarCategoria('<spring:message code="buscarcategoria.paginado"/>');

					});




</script>
</html>