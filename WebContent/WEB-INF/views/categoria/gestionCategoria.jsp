
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>

<script type="text/javascript" src="${gPath}/resources/js/jquery-ui-1.10.3.custom.js"></script>


<link href="${gPath}/resources/css/smoothness/jquery-ui-1.10.3.custom.css" rel="stylesheet">

<script type="text/javascript" src="${gPath}/resources/js/grid/grid.locale-es.js"></script>

<script type="text/javascript" src="${gPath}/resources/js/grid/jquery.jqGrid.js"></script>



<link href="${gPath}/resources/css/grid/ui.jqgrid.css" rel="stylesheet">

<script type="text/javascript" src="${gPath}/resources/js/categoria/gestionCategoria.js"></script>

<script type="text/javascript" src="${gPath}/resources/js/modelo/gestionModelo.js"></script>

<script type="text/javascript" src="${gPath}/resources/js/producto/gestionProducto.js"></script>

<script type="text/javascript" src="${gPath}/resources/js/jQuery-File-Upload-9.8.0/jquery.fileupload.js"></script>





<c:url value='${gPath}/categoriaController/obtenerListaFiltrada'
	var="urlListaFiltrada" />
	
		
		<form id="formGestionCategoriaBuscar" action="${urlListaFiltrada}"
			method="GET">
	
			<h1 >Mantenimiento de Productos</h1>

				
				
				<div class="col-md-6">
			&nbsp;
			</div>
				<div class="parrafo"></div>
				
				<div class="col-md-12">
					<table id="listGestionModelo" class="table table-bordered"></table>
					
				</div>
		
				<div class="parrafo"></div>
					<div class="col-md-2">
				
					<div style="float: right; padding: 12px 12px 0 0">
						<input type="button" class="btn btn-agregar" onclick="mostrarNuevoEnGestionModelo()"
							id="btnGestionModeloNuevo" value="Nuevo Modelo">
					</div>
				</div>
			<div class="col-md-2">
				
					<div style="float: right; padding: 12px 12px 0 0">
								<input type="button" id="btnCancelarMantenimiento" class="btn btn-cancelar" onclick="salirSistema()" value="Salir">
					
					</div>
				</div>
		</form>

<script>
	$(document)
			.ready(
					function() {
						$("#btnBuscarCategoriaProducto").prop('disabled', true);	
			setTimeout(activarPaginaMantenimiento,2000);

					});
	
	
	function activarPaginaMantenimiento(){
		$("#btnBuscarCategoriaProducto").prop('disabled', false);	
		configurarGestionCategoria('<spring:message code="gestioncategoria.paginado"/>') ;
		
		
		//Implementacion con soporte para IE9-
		var urlInsertarAdjunto = gPath + "/comunController/insertarAdjunto";
		cargador = $('#nodoArchivo').fileupload({
			url : urlInsertarAdjunto,
			dataType: 'text',
			autoUpload: false,
			replaceFileInput: false,
			done: function (e, data) {
				try {
					
					var response = jQuery.parseJSON(data.result);
					var listaAdjuntos = response.listaAdjuntos;
					var idAdjunto = response.idAdjunto;
					
					functionGlobal(idAdjunto);
					oAdjuntoTable = $('#tblBandejaListaAdjuntos').dataTable();
    				oAdjuntoTable.fnClearTable();
    				oAdjuntoTable.fnAddData(listaAdjuntos);
    				
    				$('#idNodoComentario').val("");
    				$('#idNodoComentarioValidate').empty();
    				$('#nodoArchivo').wrap('<form>').closest('form').get(0).reset();
    				$('#nodoArchivo').unwrap();
    				$('#fileName').val('');
    				
				} catch(e) {
					mostrarMensajeConAltura('divDialogo', 'Error',
                            CONSTANTE.MENSAJE.ALERTA.TAMANO, 
                            	nodoAdjuntos.mensajes.error);
				}
        	},
        	fail : function (e,data) {
        		mostrarMensajeConAltura('divDialogo', 'Error',
                        CONSTANTE.MENSAJE.ALERTA.TAMANO,
                        	nodoAdjuntos.mensajes.error);
        	}
		}).on('fileuploadadd', function (e, data) {
			try {
				$(this).valid();
			} catch(error) {
				console.log("No se puede imprimir mensaje al validar, pues el elemento no est� en formulario");
			}
			
			excedeTamanioFotoProducto = false;
			var selTipoArchivo = $('#adj_idTipoArchivo').val();
    		if (CONSTANTE.TIPO_ARCHIVO.FOTO_PRODUCTO == selTipoArchivo && data.originalFiles[0]['size'] > 1000000){
	            excedeTamanioFotoProducto = true;
	        }
			
        	data.context = $('<div/>').appendTo('#files');
        	$.each(data.files, function (index, file) {
	        	$("#idNodoAgregar").data(data);
	            if (!index) {
	            	$("#idNodoAgregar").data(data);
	            }
	        }) ;
    	});
		
		
	}
	


</script>
</html>