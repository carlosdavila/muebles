<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/categoria/nuevaCategoria.js' />"></script>
<c:url value='${gPath}/categoriaController/guardar'
	var="urlNuevaCategoriaGuardar" />
<title>Insert title here</title>
</head>
<body>
	<h2 class="ico_tit_formulario">Nuevo Grupo</h2>
	<div id="content">
		<form:form commandName="categoriaBean" id="formNuevaCategoria"
			class="form-horizontal" action="${urlNuevaCategoriaGuardar}"
			method="POST">

			<div class="bs-group bs-group-no-margin">

				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Nombre:</label>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="categoriaNombre" class="form-control vjsrequired"
							style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>
				
				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Tipo</label>
					</div>
				</div>
				
				<div class="col-xs-9">
					<div class="txt_tipo_dato" style="width: 100%">
						<form:input path="categoriaTipo" class="form-control vjsrequired"
							style="width:80%" value="GRUPO" readonly="true" />
					</div>
				</div>
		
			
			
			<form:hidden path="categoriaOrden" class="form-control vjsrequired"
							style="width:80%" />
				
				<div class="parrafo"></div>
			
				<div style="float: right; margin-right: 15px">
					<input class="btn btn-grabar" type="submit" value="GUARDAR"
						id="btnNuevaCategoriaGuardar"> <input
						class="btn btn-default" type="button" value="CANCELAR"
						id="btnNuevaCategoriaCancelar">
				</div>
					<div style="text-align: center;">
				  <p class="mensajeError" id="mensajeErrorCategoria" style="visibility: hidden;"> 
									<label class="mensajeErrorFijo"><spring:message code="mensaje.error.generico"/></label>
				  </p>
				  	</div>
				  				
				<div class="parrafo"></div>
			</div>
		</form:form>
	</div>
</body>
<script>
	$(document).ready(function() {
		configurarNuevaCategoria();
	});
</script>
</html>