<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>


<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/cliente/buscarCliente.js' />"></script>
<c:url value='${gPath}/clienteController/obtenerListaFiltrada'
	var="urlListaFiltrada" />
	

<body>
<h2 class="ico_tit_formulario">Buscar Cliente</h2>
	<div id="content" class="jqGrid">
		<form id="formBuscarCliente" action="${urlListaFiltrada}"
			method="GET">
			<div class="bs-group">
				<div class="col-md-12">
					<label class="control-label">BUSQUEDA:</label>

					<div class="txt_descr_dato" style="text-align: left; width: 80px">
						<select id="cboColumnaBuscarCliente" name="columna"
							style="text-align: left; width: 90%">
							<option value="clienteNombre">Nombre</option>
							<option value="clienteNroDoc">NroDoc</option>
							<option value="clienteTelefono">Teléfono</option>
							<option value="clienteEmail">Email</option>
							<option value="clienteDireccion">Dirección</option>
						</select>
					</div>

					<div class="txt_descr_dato" style="text-align: left; width: 100px">
						<input type="text" id="txtValorBuscarCliente" name="valor"
							style="text-align: left; width: 80px">
					</div>


					<input type="submit" class="btn btn-buscar " value="Buscar">

				</div>

				<div class="col-md-12">
					<table id="listBuscarCliente" class="table table-bordered"></table>
					<div id="plistBuscarCliente"></div>
				</div>
				<div class="parrafo"></div>
			</div>
		</form>
	</div>
</body>
<script>
	$(document)
			.ready(
					function() {
				
						configurarBuscarCliente('<spring:message code="buscarcliente.paginado"/>');

					});




</script>
</html>