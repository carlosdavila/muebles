<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/cliente/editarCliente.js' />"></script>
<c:url value='${gPath}/clienteController/editar'
	var="urlEditarClienteGuardar" />
<title>Insert title here</title>
</head>
<body>
	<h2 class="ico_tit_formulario">Editar Cliente</h2>
	<div id="content">
		<form:form commandName="clienteBean" id="formEditarCliente"
			class="form-horizontal" action="${urlEditarClienteGuardar}"
			method="POST">
			<form:hidden path="clienteId"></form:hidden>
			<div class="bs-group bs-group-no-margin">

				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Nombre:</label>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="clienteNombre" class="form-control vjsrequired"
							style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>
				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Tipo Doc:</label>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:select id="cboEditarClienteTipoDoc"
							path="tipoDocBean.tipoDocId" style="width:80%">
							<form:options items="${listaTipoDoc}" itemValue="tipoDocId"
								itemLabel="tipoDocDesc" />
						</form:select>
					</div>
				</div>
				<div class="parrafo"></div>
				<div class="col-xs-3">

					<div class="txt_descr">
						<label class="control-label">Nro Doc:</label>
					</div>

				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input disabled="true" id="txtEditarClienteNroDoc"
							path="clienteNroDoc" style="width:80%"
							class="form-control vjsrequired vjsnumerico" />
					</div>
				</div>
				<div class="parrafo"></div>
				<div class="col-xs-3">

					<div class="txt_descr">
						<label class="control-label">Telefono:</label>
					</div>

				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="clienteTelefono"
							class="form-control  vjsmaxlength100" style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>
				<div class="col-xs-3">

					<div class="txt_descr" style="width: 100%">
						<label class="control-label">Dirección:</label>
					</div>

				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="clienteDireccion"
							class="form-control  vjsmaxlength100" style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>
				<div class="col-xs-3">

					<div class="txt_descr">
						<label class="control-label">Email:</label>
					</div>

				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:textarea path="clienteEmail"
							class="form-control  vjsmaxlength100" style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>
				<div class="col-xs-3">

					<div class="txt_descr">
						<label class="control-label">Contacto:</label>
					</div>

				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:textarea path="clienteContacto" style="width:80%"
							class="form-control  vjsmaxlength100" />
					</div>
				</div>
				<div class="parrafo"></div>

				<div style="float: right; margin-right: 15px">
					<input class="btn btn-grabar" type="submit" value="GUARDAR"
						id="btnEditarClienteGuardar"> <input
						class="btn btn-default" type="button" value="CANCELAR"
						id="btnEditarClienteCancelar">
				</div>
				<div class="parrafo"></div>
			</div>
		</form:form>
	</div>
</body>
<script>
	$(document).ready(
			function() {
				 configurarEditarCliente();
});

</script>
</html>