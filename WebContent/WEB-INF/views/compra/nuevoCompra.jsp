<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/compra/nuevoCompra.js' />"></script>
<c:url value='${gPath}/compraController/guardar'
	var="urlNuevoCompraGuardar" />
<title>Insert title here</title>
</head>
<body>
	<h2 class="ico_tit_formulario">Nuevo Compra</h2>
	<div id="content">
		<form:form commandName="compraBean" id="formNuevoCompra"
			class="form-horizontal" action="${urlNuevoCompraGuardar}"
			method="POST">

			<div class="bs-group bs-group-no-margin">

				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Codigo:</label>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="compraCodigo" class="form-control vjsrequired"
							style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>
		
				<div class="col-xs-3">

					<div class="txt_descr">
						<label class="control-label">Proveedor:</label>
					</div>

				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input  id="txtCompraProveedor"
							path="proveedorBean.proveedorNombre" style="width:80%"
							 />
					</div>
				</div>
				<div class="parrafo"></div>
				
			</div>
		</form:form>
	</div>
</body>
<script>

$( function() {
	
	$(document).ready(function() {
		configurarNuevoCompra();
	});
	
	
    var cache = {};
    $("#txtCompraProveedor").autocomplete({
      minLength: 2,
      source: function( request, response ) {
        var term = request.term;
        if ( term in cache ) {
          response( cache[ term ] );
          return;
        }
 
        $.getJSON(gPath + '/proveedorController/obtenerBusqueda?proveedor='+ $("#txtCompraProveedor").val(), request, function( data, status, xhr ) {
          cache[ term ] = data;
          response( data );
          alert(data);
        });
      }
    });
  } );



</script>
</html>