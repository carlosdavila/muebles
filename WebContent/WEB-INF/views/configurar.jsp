
<%@page import="org.springframework.context.annotation.Import"%>
<%@ page language="java" pageEncoding="UTF-8" session="true"
	isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Equipments</title>



<body  style="min-width: 80%;margin-left: 2%;margin-right: 2%;font-family: Arial!important;">


 <div class="container" >
<%@ include file="cabecera.jsp" %>
 </div>
  
 <div class="container" id="idMenuPrincipal"  
  >
 <%@ include file="menu.jsp" %>
 </div>

	<div id="spin"></div>

	<div class="container" id="contentMain"  >
	<%@ include file="categoria/gestionCategoria.jsp" %>
	</div>
		<c:if test="${isPc}">
	<div  class="container"  id="foot"  style="background: rgb(70, 163, 54); ">
	<%@ include file="foot.jsp" %>
	</div>
	</c:if>
	<c:if test="${!isPc}">
	<div  class="container"  id="foot"    style="margin-left:30px; background: rgb(70, 163, 54);  ">
	<%@ include file="foot.jsp" %>
	</div>
	</c:if>

</body>

</html>
