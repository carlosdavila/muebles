<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
.TextoNosotros {
	FONT-SIZE: 12px !important;
	COLOR: #000000 !important;
	FONT-FAMILY: Arial !important;
	TEXT-DECORATION: none !important;
	TEXT-ALIGN: justify !important;
}
h6{
font-weight: bold!important;
}
</style>
</head>
<body>

	<div class="container">

		<!-- Left-aligned media object -->
		<div class="media"  >
			
				<c:if test="${isPc}">
			<div
				style="display: table-cell !important; vertical-align: top !important; padding-left: 10px!important;padding-right: 10px!important">

	<h6>NOSOTROS</h6>
	<p class="TextoNosotros">Equipments and Coaching Fitness S.A.C. es una empresa formada en Diciembre del 2011, y que decidi� 
brindar todos sus servicios de asesor�a en gesti�n fitness, venta de m�quinas y equipos cardiovasculares para
gimnasio de la marca IMPULSE, empresa que ya se consolida como la marca l�der en el mercado, sobrepasando
 niveles de conocidas marcas actualmente.<br>
Nuestra empresa est� respalda por la excelente asesor�a y amplia experiencia de Enrique Fern�ndez-Hernani Guti�rrez, 
Gerente General; quien a trav�s de la transformaci�n de su gimnasio SPA ARNOLD  FITNESS CENTER, a llegado
 a consolidar vivencias con el d�a a d�a, adem�s de adquirir mayor informaci�n, preparaci�n y conocimiento, que 
ahora comparte y ense�a a trav�s de Equipments and Coaching Fitness, haciendo de esta manera cumplir tu sue�o 
de tener un gimnasio diferente, funcional y sobre todo productivo, brindando los mejores servicios a tus miembros.</p>
<h6>VISION</h6>
<p class="TextoNosotros">
Ser una empresa l�der en asesor�a y equipamiento para gimnasios en el Per� y Sudam�rica, llegando a todos
los gimnasios del pa�s y ense�arles el logro del �xito.</p>
	<h6>MISION</h6>
	<p class="TextoNosotros">
Transmitir a todos los gimnasios una nueva filosof�a del �xito, y ayudarlos en su gesti�n, crecimiento y
desarrollo personal y empresarial. Nosotros con nuestra experiencia vivencial hemos alcanzado el �xito
con el cual podemos ayudarlos dando nuestro ejemplo. "L�deres con experiencia, formando l�deres a nivel Nacional"</p>
<h6>VALORES</h6>
	<p class="TextoNosotros">
Como una empresa l�der transmitimos de forma espiritual y armoniosa ciertos principios como:
 "El engrandecer al pr�jimo, ayudando a todos los que necesiten nuestro apoyo y d�ndoles el conocimiento que hemos adquirido con el tiempo"
</p>

				
			</div>


			<div
				style="display: table-cell !important; vertical-align: top !important; padding-left: 10px;!important;padding-right: 5px!important;padding-bottom: 18px!important;">
		
				<img src="${gPath}/resources/img/nosotros.jpg" class="media-object"
					style="width: 280px">
			</div>
			
			</c:if>
			
			<c:if test="${!isPc}">
					<div
				style="display: table-cell !important; vertical-align: top !important;">

				<img src="${gPath}/resources/img/nosotros.jpg" class="media-object"
					style="width: 300px">

			<h6>NOSOTROS</h6>
	<p class="TextoNosotros">Equipments and Coaching Fitness S.A.C. es una empresa formada en Diciembre del 2011, y que decidi� 
brindar todos sus servicios de asesor�a en gesti�n fitness, venta de m�quinas y equipos cardiovasculares para
gimnasio de la marca IMPULSE, empresa que ya se consolida como la marca l�der en el mercado, sobrepasando
 niveles de conocidas marcas actualmente.<br>
Nuestra empresa est� respalda por la excelente asesor�a y amplia experiencia de Enrique Fern�ndez-Hernani Guti�rrez, 
Gerente General; quien a trav�s de la transformaci�n de su gimnasio SPA ARNOLD  FITNESS CENTER, a llegado
 a consolidar vivencias con el d�a a d�a, adem�s de adquirir mayor informaci�n, preparaci�n y conocimiento, que 
ahora comparte y ense�a a trav�s de Equipments and Coaching Fitness, haciendo de esta manera cumplir tu sue�o 
de tener un gimnasio diferente, funcional y sobre todo productivo, brindando los mejores servicios a tus miembros.</p>
<h6>VISION</h6>
<p class="TextoNosotros">
Ser una empresa l�der en asesor�a y equipamiento para gimnasios en el Per� y Sudam�rica, llegando a todos
los gimnasios del pa�s y ense�arles el logro del �xito.</p>
	<h6>MISION</h6>
	<p class="TextoNosotros">
Transmitir a todos los gimnasios una nueva filosof�a del �xito, y ayudarlos en su gesti�n, crecimiento y
desarrollo personal y empresarial. Nosotros con nuestra experiencia vivencial hemos alcanzado el �xito
con el cual podemos ayudarlos dando nuestro ejemplo. "L�deres con experiencia, formando l�deres a nivel Nacional"</p>
<h6>VALORES</h6>
	<p class="TextoNosotros">
Como una empresa l�der transmitimos de forma espiritual y armoniosa ciertos principios como:
 "El engrandecer al pr�jimo, ayudando a todos los que necesiten nuestro apoyo y d�ndoles el conocimiento que hemos adquirido con el tiempo"
</p>


			</div>
			</c:if>
			

		</div>
	</div>



</body>
</html>