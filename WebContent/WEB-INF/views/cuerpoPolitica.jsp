<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
.TextoPolitica {
FONT-SIZE: 11px!important;
COLOR: #000000!important; 
FONT-FAMILY: Arial!important; 
TEXT-DECORATION: none!important ; 
TEXT-ALIGN:justify!important;

}
h6{
font-weight: bold!important;
margin-bottom: 0px!important;
}

</style>
</head>
<body>

<div class="container">

  <!-- Left-aligned media object -->
  <div class="media">
  
  <c:if test="${isPc}">
  
    <div  style="display: table-cell!important;vertical-align: top!important;padding-left: 8px;padding-right: 5px;!important;padding-bottom: 8px!important">
     <h5 class="media-heading" style="font-weight: bold!important;">POLITICAS DE PRIVACIDAD</h5>
      <img src="${gPath}/resources/img/politica.jpg" class="media-object" style="width:250px">
    </div>
    <div  style="display: table-cell!important;vertical-align: top!important;padding-right: 10px;padding-top: 10px;">
     
    
 <h6> Informaci�n que es recogida</h6>
   <p class="TextoPolitica">
Nuestro sitio web podr� recoger informaci�n personal, por ejemplo: Nombre, informaci�n de contacto como su direcci�n de correo electr�nica e informaci�n demogr�fica. As� mismo cuando sea necesario podr� ser requerida informaci�n espec�fica para procesar alg�n pedido o realizar una entrega o facturaci�n.
</p>
<h6>Uso de la informaci�n recogida</h6>
  <p class="TextoPolitica">
Nuestro sitio web emplea la informaci�n con el fin de proporcionar el mejor servicio posible, particularmente para mantener un registro de usuarios, de pedidos en caso que aplique, y mejorar nuestros productos y servicios.  Es posible que sean enviados correos electr�nicos peri�dicamente a trav�s de nuestro sitio con ofertas especiales, nuevos productos y otra informaci�n publicitaria que consideremos relevante para usted o que pueda brindarle alg�n beneficio, estos correos electr�nicos ser�n enviados a la direcci�n que usted proporcione y podr�n ser cancelados en cualquier momento.
En est� altamente comprometido para cumplir con el compromiso de mantener su informaci�n segura. Usamos los sistemas m�s avanzados y los actualizamos constantemente para asegurarnos que no exista ning�n acceso no autorizado.
</p>
<h6>Cookies</h6>
 <p class="TextoPolitica">
Una cookie se refiere a un fichero que es enviado con la finalidad de solicitar permiso para almacenarse en su ordenador, al aceptar dicho fichero se crea y la cookie sirve entonces para tener informaci�n respecto al tr�fico web, y tambi�n facilita las futuras visitas a una web recurrente. Otra funci�n que tienen las cookies es que con ellas las webs pueden reconocerte individualmente y por tanto brindarte el mejor servicio personalizado de su web.
Nuestro sitio web emplea las cookies para poder identificar las p�ginas que son visitadas y su frecuencia. Esta informaci�n es empleada �nicamente para an�lisis estad�stico y despu�s la informaci�n se elimina de forma permanente. Usted puede eliminar las cookies en cualquier momento desde su ordenador. Sin embargo, las cookies ayudan a proporcionar un mejor servicio de los sitios web, est�s no dan acceso a informaci�n de su ordenador ni de usted, a menos de que usted as� lo quiera y la proporcione directamente. Usted puede aceptar o negar el uso de cookies, sin embargo, la mayor�a de navegadores aceptan cookies autom�ticamente pues sirve para tener un mejor servicio web. Tambi�n usted puede cambiar la configuraci�n de su ordenador para declinar las cookies. Si se declinan es posible que no pueda utilizar algunos de nuestros servicios.
</p>
<h6>Enlaces a Terceros</h6>
 <p class="TextoPolitica">
Este sitio web pudiera contener en laces a otros sitios que pudieran ser de su inter�s. Una vez que usted de clic en estos enlaces y abandone nuestra p�gina, ya no tenemos control sobre al sitio al que es redirigido y por lo tanto no somos responsables de los t�rminos o privacidad ni de la protecci�n de sus datos en esos otros sitios terceros. Dichos sitios est�n sujetos a sus propias pol�ticas de privacidad por lo cual es recomendable que los consulte para confirmar que usted est� de acuerdo con estas.
</p>
<h6>Control de su informaci�n personal</h6>
 <p class="TextoPolitica">
En cualquier momento usted puede restringir la recopilaci�n o el uso de la informaci�n personal que es proporcionada a nuestro sitio web.  Cada vez que se le solicite rellenar un formulario, como el de alta de usuario, puede marcar o desmarcar la opci�n de recibir informaci�n por correo electr�nico.  En caso de que haya marcado la opci�n de recibir nuestro bolet�n o publicidad usted puede cancelarla en cualquier momento.
Esta compa��a no vender�, ceder� ni distribuir� la informaci�n personal que es recopilada sin su consentimiento, salvo que sea requerido por un juez con un Poder judicial.
  </p>
  
    </div>
    </c:if>
    
    
    
  <c:if test="${!isPc}">
    <div  style="display: table-cell!important;vertical-align: top!important;padding-right: 10px;!important">
     <h4 class="media-heading">POLITICAS DE PRIVACIDAD</h4>
      <img src="${gPath}/resources/img/politica.jpg" class="media-object" style="width:300px">
   
    
 <h6> Informaci�n que es recogida</h6>
   <p class="TextoPolitica">
Nuestro sitio web podr� recoger informaci�n personal, por ejemplo: Nombre, informaci�n de contacto como su direcci�n de correo electr�nica e informaci�n demogr�fica. As� mismo cuando sea necesario podr� ser requerida informaci�n espec�fica para procesar alg�n pedido o realizar una entrega o facturaci�n.
</p>
<h6>Uso de la informaci�n recogida</h6>
  <p class="TextoPolitica">
Nuestro sitio web emplea la informaci�n con el fin de proporcionar el mejor servicio posible, particularmente para mantener un registro de usuarios, de pedidos en caso que aplique, y mejorar nuestros productos y servicios.  Es posible que sean enviados correos electr�nicos peri�dicamente a trav�s de nuestro sitio con ofertas especiales, nuevos productos y otra informaci�n publicitaria que consideremos relevante para usted o que pueda brindarle alg�n beneficio, estos correos electr�nicos ser�n enviados a la direcci�n que usted proporcione y podr�n ser cancelados en cualquier momento.
En est� altamente comprometido para cumplir con el compromiso de mantener su informaci�n segura. Usamos los sistemas m�s avanzados y los actualizamos constantemente para asegurarnos que no exista ning�n acceso no autorizado.
</p>
<h6>Cookies</h6>
 <p class="TextoPolitica">
Una cookie se refiere a un fichero que es enviado con la finalidad de solicitar permiso para almacenarse en su ordenador, al aceptar dicho fichero se crea y la cookie sirve entonces para tener informaci�n respecto al tr�fico web, y tambi�n facilita las futuras visitas a una web recurrente. Otra funci�n que tienen las cookies es que con ellas las webs pueden reconocerte individualmente y por tanto brindarte el mejor servicio personalizado de su web.
Nuestro sitio web emplea las cookies para poder identificar las p�ginas que son visitadas y su frecuencia. Esta informaci�n es empleada �nicamente para an�lisis estad�stico y despu�s la informaci�n se elimina de forma permanente. Usted puede eliminar las cookies en cualquier momento desde su ordenador. Sin embargo, las cookies ayudan a proporcionar un mejor servicio de los sitios web, est�s no dan acceso a informaci�n de su ordenador ni de usted, a menos de que usted as� lo quiera y la proporcione directamente. Usted puede aceptar o negar el uso de cookies, sin embargo, la mayor�a de navegadores aceptan cookies autom�ticamente pues sirve para tener un mejor servicio web. Tambi�n usted puede cambiar la configuraci�n de su ordenador para declinar las cookies. Si se declinan es posible que no pueda utilizar algunos de nuestros servicios.
</p>
<h6>Enlaces a Terceros</h6>
 <p class="TextoPolitica">
Este sitio web pudiera contener en laces a otros sitios que pudieran ser de su inter�s. Una vez que usted de clic en estos enlaces y abandone nuestra p�gina, ya no tenemos control sobre al sitio al que es redirigido y por lo tanto no somos responsables de los t�rminos o privacidad ni de la protecci�n de sus datos en esos otros sitios terceros. Dichos sitios est�n sujetos a sus propias pol�ticas de privacidad por lo cual es recomendable que los consulte para confirmar que usted est� de acuerdo con estas.
</p>
<h6>Control de su informaci�n personal</h6>
 <p class="TextoPolitica">
En cualquier momento usted puede restringir la recopilaci�n o el uso de la informaci�n personal que es proporcionada a nuestro sitio web.  Cada vez que se le solicite rellenar un formulario, como el de alta de usuario, puede marcar o desmarcar la opci�n de recibir informaci�n por correo electr�nico.  En caso de que haya marcado la opci�n de recibir nuestro bolet�n o publicidad usted puede cancelarla en cualquier momento.
Esta compa��a no vender�, ceder� ni distribuir� la informaci�n personal que es recopilada sin su consentimiento, salvo que sea requerido por un juez con un Poder judicial.
  </p>
  
    </div>
    </c:if>  
    
    
    
  </div>
</div>
  


</body>
</html>