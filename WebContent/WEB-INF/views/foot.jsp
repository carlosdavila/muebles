<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>


<style>

/************************************************************
*************************Footer******************************
*************************************************************/


.footer1 {
	
	padding-top: 40px;
	padding-right: 0;
	padding-bottom: 20px;
	padding-left: 0; /*	border-top-width: 4px;
	border-top-style: solid;
	border-top-color: #003;*/
}

.title-widget {
	color: #898989;
	font-size: 20px;
	font-weight: 300;
	line-height: 1;
	position: relative;
	text-transform: uppercase;
	font-family: 'Fjalla One', sans-serif;
	margin-top: 0;
	margin-right: 0;
	margin-bottom: 25px;
	margin-left: 0;
	padding-left: 28px;
}

.title-widget::before {
	background-color: #ea5644;
	content: "";
	height: 22px;
	left: 0px;
	position: absolute;
	top: -2px;
	width: 5px;
}

.widget_nav_menu ul {
	list-style: outside none none;
	padding-left: 0;
}

.widget_archive ul li {
	background-color: rgba(0, 0, 0, 0.3);
	content: "";
	height: 3px;
	left: 0;
	position: absolute;
	top: 7px;
	width: 3px;
}

.widget_nav_menu ul li {
	font-size: 13px;
	font-weight: 700;
	line-height: 20px;
	position: relative;
	text-transform: uppercase;
	border-bottom: 1px solid rgba(0, 0, 0, 0.05);
	margin-bottom: 7px;
	padding-bottom: 7px;
	width: 95%;
}

.title-median {
	color: #636363;
	font-size: 20px;
	line-height: 20px;
	margin: 0 0 15px;
	text-transform: uppercase;
	font-family: 'Fjalla One', sans-serif;
}

.footerp p {
	font-family: 'Gudea', sans-serif;
}

#social:hover {
	-webkit-transform: scale(1.1);
	-moz-transform: scale(1.1);
	-o-transform: scale(1.1);
}

#social {
	-webkit-transform: scale(0.8);
	/* Browser Variations: */
	-moz-transform: scale(0.8);
	-o-transform: scale(0.8);
	-webkit-transition-duration: 0.5s;
	-moz-transition-duration: 0.5s;
	-o-transition-duration: 0.5s;
}
/* 
    Only Needed in Multi-Coloured Variation 
                                               */
.social-fb:hover {
	color: #3B5998;
}

.social-tw:hover {
	color: #4099FF;
}

.social-gp:hover {
	color: #d34836;
}

.social-em:hover {
	color: #f39c12;
}

.nomargin {
	margin: 0px;
	padding: 0px;
}

.footer-bottom {
	background-color: #15224f;
	min-height: 30px;
	width: 100%;
}

.copyright {
	color: #fff;
	line-height: 30px;
	min-height: 30px;
	padding: 7px 0;
}

.design {
	color: #ffffff;

	font-size: 17
}

.design a {
	color: #ffffff;
	
}
.design a:hover, a:focus { 
	color: blue!important;
	
}


/************************************************************
*************************Footer******************************
*************************************************************/
</style>
<body>
	<div id="idPopup"></div>




	<div class="container" style="padding-right: 0" >

		<c:if test="${isPc}">


			<div class="navbar navbar-inverse navbar-static-top">



				<div class="col-xs-6 design" style="background: rgb(70, 163, 54);padding-left: 12%; padding-bottom: 1%;padding-top: 1%;line-height: 160%!important">
                
					<a    href="mailto:gerencia.equipmentperu@outlook.com">
						<span class="fa fa-envelope-o fa-1x"></span>
					www.peru-impulsefitness.com
					</a> 
					<br>
					<a href="mailto:gerencia.equipmentperu@outlook.com" >
						<span class="fa fa-envelope-o fa-1x"></span>
						www.eng-impulsefitness.com
					</a><br>
					 <a href="https://www.google.com.pe/maps/place/Equipments+and+coaching+fitness/@-12.1743833,-77.0105707,17z/data=!3m1!4b1!4m5!3m4!1s0x9105b9d53186d253:0x578f84cfdbc3acf6!8m2!3d-12.1743833!4d-77.008382"    > <span class="fa fa-map-marker fa-1x"></span>
						Av.Guardia Peruana 305 Matellini-Chorrillos
					</a>

				</div>


				<div class="col-xs-6  design" style="background: rgb(70, 163, 54);padding-left: 11%; padding-bottom: 1%;padding-top: 1%;line-height: 160%!important">
					
					 <a href=""> <span
						class="fa fa-facebook-square fa-1x"></span> Equipments and
						Coaching Fitness
					</a><br>
					 <a href=""> <span
						class="fa fa-facebook-square fa-1x"></span> Impulse Heach
						Teach-Peru
					</a><br>
<a href="" > <span class="fa fa-phone fa-1x"></span>
						7194349, <span class="fa fa-mobile fa-1x"></span> 999 331 801, <span class="fa fa-mobile fa-1x"></span> 961 758 905
					</a>


				</div>
			</div>
		</c:if>
		<c:if test="${!isPc}">



			<div class="navbar navbar-inverse navbar-static-top">



				<div class="col-xs-12"  style="background: rgb(70, 163, 54)">

					<a href="mailto:gerencia.equipmentperu@outlook.com" class="btn">
						<span class="fa fa-envelope-o fa-1x"></span>
						www.peru-impulsefitness.com
					</a> <a href="mailto:gerencia.equipmentperu@outlook.com" class="btn">
						<span class="fa fa-envelope-o fa-1x"></span>
						www.eng-impulsefitness.com
					</a> <a href="https://www.google.com.pe/maps/place/Equipments+and+coaching+fitness/@-12.1743833,-77.0105707,17z/data=!3m1!4b1!4m5!3m4!1s0x9105b9d53186d253:0x578f84cfdbc3acf6!8m2!3d-12.1743833!4d-77.008382" class="btn"> <span class="fa fa-map-marker fa-1x"></span>
						Av.Guardia Peruana 305 Matellini-Chorrillos
					</a> <a href="" class="btn"> <span class="fa fa-phone fa-1x"></span>
						7194349, <span class="fa fa-mobile fa-1x"></span> 999 331 801, <span class="fa fa-mobile fa-1x"></span> 961 758 905
					</a> <a href="" class="btn"> <span
						class="fa fa-facebook-square fa-1x"></span> Equipments and
						Coaching Fitness
					</a> <a href="" class="btn"> <span
						class="fa fa-facebook-square fa-1x"></span> Impulse Heach
						Teach-Peru
					</a>

				</div>

			</div>











		</c:if>

	</div>







	<form id="formSalirSistema" method="get">

		<input type="hidden" id="txtModeloId" name="modeloId"> <input
			type="hidden" id="txtValor" name="valor"> <input
			type="hidden" id="txtPagina" name="pagina"> <input
			type="hidden" id="txtCategoriaId" name="categoriaId">
	</form>
	<script type="text/javascript">
		$(document).ready(function() {

			gActivoLoading = CONSTANTE.ESTADO.INACTIVO;
			generarLoading();

			configurarMenu();

		});

		var contador = 1;
	</script>
</body>
</html>