
<%@page import="org.springframework.context.annotation.Import"%>
<%@ page language="java" pageEncoding="UTF-8" session="true"
	isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Equipments</title>
</head>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '907471339401877');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=907471339401877&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

<body  style="min-width: 80%;margin-left: 2%;margin-right: 2%;font-family: Arial!important;">


 <div class="container" >
<%@ include file="cabecera.jsp" %>
 </div>
  
 <div class="container" id="idMenuPrincipal"  
 >
 
 
 
 <%@ include file="menu.jsp" %>
 </div>

	<div id="spin"></div>

	<div class="container" id="contentMain" >
	<%@ include file="imagenes.jsp" %>
	</div>
		<c:if test="${isPc}">
	<div  class="container"  id="foot"  >
	<%@ include file="foot.jsp" %>
	</div>
	</c:if>
	<c:if test="${!isPc}">
	<div  class="container"  id="foot"    style="margin-left:30px; background: rgb(70, 163, 54);  ">
	<%@ include file="foot.jsp" %>
	</div>
	</c:if>

</body>



</html>
