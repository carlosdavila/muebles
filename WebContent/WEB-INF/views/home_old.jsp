
<%@ page language="java" pageEncoding="UTF-8" session="true"
	isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>



<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<script type="text/javascript">
	gPath = '<c:if test="${empty gPath}">${pageContext.servletContext.contextPath}</c:if>';

	</script>



<script type="text/javascript">


	/*
	 * DEFINICION DE OBJETO GLOBAL ATENEA
	 * 	
	 */
	var ERP = {
		//CONFIRMACION
		MSG_CONFIRMACION_GRABADO : '<spring:message code="mensajeconfirmacion.registro" />',
		
		MSG_CONFIRMACION_ELIMINACION : '<spring:message code="mensajeconfirmacion.eliminacion" />',
	
		MSG_CONFIRMACION_ENVIO : '<spring:message code="mensajeconfirmacion.registro.envio" />',
		MSG_CONFIRMACION_RECHAZO : '<spring:message code="mensajeconfirmacion.rechazo" />',
		MSG_CONFIRMACION_AUTOASIGNAR : '<spring:message code="mensajeconfirmacion.autoasignar" />',
		MSG_CONFIRMACION_REGISTRO_VALIDO : '<spring:message code="mensajeconfirmacion.registro.valido" />',
		MSG_CONFIRMACION_REGISTRO_INVALIDO : '<spring:message code="mensajeconfirmacion.registro.invalido" />',

		//REQUEST.
		MSG_ERROR_GENERAL : '<spring:message code="mensajeestatico.error.general" />',
		MSG_GRABADO_EXITO : '<spring:message code="mensajerespuesta.exito.guardo" />',
		MSG_ELIMINACION_EXITO : '<spring:message code="mensajerespuesta.exito.elimino" />',
		MSG_ERROR_RESPONSABLE : '<spring:message code="mensajerespuesta.error.noexisteresponsable" />',

		//VALIDACION.
		MSG_VAL_OBLIGATORIO : '<spring:message code="validacion.comun.mensajeobligatorio" />',
		MSG_VAL_MAXIMO_CARACTERES : '<spring:message code="validacion.comun.mensajemaximotexto" />',
		MSG_VAL_MINIMO_CARACTERES : '<spring:message code="validacion.comun.mensajeminimotexto" />',
		MSG_VAL_MAXIMO_DIGITOS : '<spring:message code="validacion.comun.mensajemaximodigito" />',
		MSG_VAL_MINIMO_DIGITOS : '<spring:message code="validacion.comun.mensajeminimodigito" />',
		MSG_VAL_FORMATO : '<spring:message code="validacion.comun.mensajetipodatotexto" />',
		MSG_VAL_ENTERO : '<spring:message code="validacion.comun.mensajetipodatodigito" />',
		MSG_VAL_DECIMAL : '<spring:message code="validacion.comun.mensajetipodatodecimal" />',
		MSG_VAL_FECHA : '<spring:message code="validacion.comun.mensajetipodatofecha" />',
		MSG_VAL_NOMBRE_VENTA : '<spring:message code="validacion.comun.nombreventa" />',

		//Titulo de dialogos.
		TITULO_CONFIRMACION_REGISTRO : '<spring:message code="titulo.confirmacion.registro" />',
		TITULO_DENEGACION_ACCION : '<spring:message code="titulo.denegacion.accion" />',

		PARAMETRO_ROL_ADMINISTRADOR : '<spring:message code="atenea.rol.administrador"/>',

		//PATRONES VALIDACION.
		PATRON_DECIMAL : /^(\d{1,9}|\d{1,9}\.\d{1,2})$/,
		PATRON_HORA : /^(\d{1,2}):(\d\d)$/
		
	};
</script>



<link href="resources/css/botones.css" rel="stylesheet">

<link href="resources/js/bootstrap/css/bootstrap.min.css" rel="stylesheet">




<!-- SmartMenus jQuery Bootstrap Addon CSS -->

<link href="resources/css/showLoading.css" rel="stylesheet">
 <script type="text/javascript" src="resources/js/jqueryOld.js"></script>


<!-- Bootstrap core JavaScript -->
<!-- Placed at the end of the document so the pages load faster -->

 <script type="text/javascript" src="resources/js/bootstrap/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="resources/js/bootstrap/js/modal.js"></script>

 <script type="text/javascript" src="resources/js/jquery-validation/dist/jquery.validate.js"></script>

<script type="text/javascript" src="resources/js/jquery-validation/dist/additional-methods.js"></script>

<script type="text/javascript" src="resources/js/jquery-validation/localization/messages_es.js"></script>

<script type="text/javascript" src="resources/js/comun.js"></script>

<script type="text/javascript" src="resources/js/comunValidation.js"></script>

<script type="text/javascript" src="resources/js/constante.js"></script>

<script type="text/javascript" src="resources/js/jquery.showLoading.min.js"></script>

<link href="resources/css/w3.css" rel="stylesheet">

<script type="text/javascript" src="resources/js/dataTable/datatable.js"></script>

<link href="resources/js/dataTable/datatable.css" rel="stylesheet">

<script type="text/javascript" src="resources/js/dataTable/reload.js"></script>	
	
<link href="resources/js/bootstrap/css/bootstrap-add.css" rel="stylesheet">	

<link href="resources/css/sweetalert2.min.css" rel="stylesheet">	

<script type="text/javascript" src="resources/js/sweetalert2.min.js"></script>	



</head>
 
<body  style="min-width: 80%;   font-family:   SourceHanSansCN !important;margin-left: 2%;margin-right: 2%">

  
 <div class="container" id="idMenuPrincipal"></div>


	<div id="spin"></div>

	<div class="container" id="contentMain"></div>
     <div id="idPopup"></div>
    <footer class="container-fluid text-center">
  <p style="color: gray;">Derechos reservados Equipments 2017</p>
</footer>

<form id="formSalirSistema"></form>
</body>

<script type="text/javascript">




var flagUsuarioActivo=false;
var flagCambiarClave=false;

	$(document).ready(function(){
		
		 flagUsuarioActivo='${mostrarMensajeUsuarioActivo}';
		 flagCambiarClave='${mostrarCambiarClave}';
		
		gActivoLoading = CONSTANTE.ESTADO.INACTIVO;
		generarLoading();
		

	setTimeout(mostrarMenuPrincipal,1000);
	
	})	;

var contador = 1;

	function mostrarMenuPrincipal(){
		
		urlBandeja = '${gPath}/mostrarMenu';
		$("#idMenuPrincipal").load(gPath + urlBandeja);
	}


	
	function mostrarCarritoDetalle(){
	
		var urlBandeja = '/carritoDetalleController/mostrarGestionCarritoDetalle';
		$("#idPopup").load( gPath+urlBandeja);
	}
			

	
	function mostrarLogin(){
		
		var urlBandeja = '/usuarioController/mostrarLogin';
		$("#idPopup").load( gPath+urlBandeja);
	}
	
    function mostrarPaginaImagen(){
		
    	urlBandeja = '${gPath}/imagen';
    	$("#contentMain").load(gPath + urlBandeja);
	}

 function mostrarMenu(){
		
	    urlBandeja = '${gPath}/menu';
	    $("#divMenu").load(gPath + urlBandeja);
	}
 
 
 function mostrarCambiarClave(){
		
	    urlBandeja = '${gPath}/usuarioController/mostrarCambiarClave';
	    $("#idPopup").load(gPath + urlBandeja);
	}
 
 function mostrarBienvenido(){
		
	    urlBandeja = '${gPath}/usuarioController/mostrarBienvenido';
	    $("#idPopup").load(gPath + urlBandeja);
	}
 
	function mostrarProductosSlider(modeloId,valor,pagina)
	{

	var urlBandeja = '${gPath}/productoController/listarProducto?modeloId='+modeloId+'&valor='+valor+'&pagina='+pagina;
	$("#contentMain").load( gPath+urlBandeja);
	$('.navbar-collapse').collapse('hide');
	}
	
	

	
	function mostrarProductosSliderPorBuscar(input)
	{
     var txtProducto=$("#"+input).val();
    
     mostrarProductosSlider(0,txtProducto,1);
	
	}

 
	function salirSistema() {
		
		$.ajax({
			type : 'post',
			url : 'usuarioController/salir',
			
			success : function(data) {
			
				setTimeout(mostrarMenuPrincipal,150);
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				
			}
		});
	}
	
    function mostrarPaginaMantenimiento(){
		
    	urlBandeja = '${gPath}/categoriaController/mostrarGestionCategoria';
    	$("#contentMain").load(gPath + urlBandeja);
	}
    
    
    function obtenerTotal(){
    	
    	$
    	.ajax({
    		type : 'post',
    		url : gPath+'/carritoDetalleController/obtenerTotal',
    		success : function(data) {
    			 
    						 $('#spanCarritoTotal').html("$"+ data);
    				         $('#spanCarritoTotal1').html("$"+ data);
    		
    		}
    	});
    	
    }
    
    function configurarImagenMobil(){
    	

		 
		  $('#escenario img').each(function(){
			  
			  if ($(this).hasClass('imgProducto')){
		var idImg=$(this).attr("id");

		var divContenedorImg=$("#"+idImg+"_div_conte");
		var divImg=$("#"+idImg+"_div");
		var widthDivImgConte=divContenedorImg.width();
		var widthDivImg=divImg.width();
		
		var left=(widthDivImgConte-widthDivImg)/2;
		
		if(left>0){
	
	
			divImg.css( {
				"margin-left":left+'px'
				} );
		
		}else{
			
			divImg.css( {
				"margin-left":'0px',
				 width :widthDivImgConte+'px'
				} );
			 jQuery(this).css( {
			        width : '100%',
			        height : 'auto'
			      } );
			
		}
			  var heightOld = jQuery(this).height();
		      var widthOld = jQuery(this).width();
		  //  var new_width = 100; //nuevo tamaño
		    var new_height = 258; //nuevo tamaño
		    
		    
		    if (heightOld > new_height){
			
			      var calculo = Math.round((100*new_height)/ heightOld); //porcentaje
			      var new_width = Math.round((widthOld*calculo)/100);
			      jQuery(this).css( {
			        width : new_width+'px',
			        height : new_height+'px'
			      } );
			    }
			  }
		    
		  });
}
    function configurarImagenPC(){
    	
		 
		  $('#escenario img').each(function(){
			  
			  if ($(this).hasClass('imgProducto')){
		var idImg=$(this).attr("id");
		var divContenedorImg=$("#"+idImg+"_div_conte");
		var divImg=$("#"+idImg+"_div");
		var widthDivImgConte=divContenedorImg.width();
		var widthDivImg=divImg.width();
		var left=(widthDivImgConte-widthDivImg)/2;
		if(left>0){
		divImg.css( {
			"margin-left":left+'px'
			} );
		}
			  var heightOld = jQuery(this).height();
		      var widthOld = jQuery(this).width();
		  //  var new_width = 100; //nuevo tamaño
		    var new_height = 428; //nuevo tamaño
		    
		    
		    if (heightOld > new_height){
			
			      var calculo = Math.round((100*new_height)/ heightOld); //porcentaje
			      var new_width = Math.round((widthOld*calculo)/100);
			      jQuery(this).css( {
			        width : new_width+'px',
			        height : new_height+'px'
			      } );
			    }
			  }
		    
		  });
	
}
</script>



   



</html>
