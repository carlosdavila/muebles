
<%@ page language="java" pageEncoding="UTF-8" session="true"
	isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>


<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<script type="text/javascript">
	gPath = '${gPath}<c:if test="${empty gPath}">${pageContext.servletContext.contextPath}</c:if>';


	
	</script>



<script type="text/javascript">


	/*
	 * DEFINICION DE OBJETO GLOBAL ATENEA
	 * 	
	 */
	var ERP = {
		//CONFIRMACION
		MSG_CONFIRMACION_GRABADO : '<spring:message code="mensajeconfirmacion.registro" />',
		MSG_CONFIRMACION_ELIMINACION : '<spring:message code="mensajeconfirmacion.eliminacion" />',
		MSG_CONFIRMACION_ENVIO : '<spring:message code="mensajeconfirmacion.registro.envio" />',
		MSG_CONFIRMACION_RECHAZO : '<spring:message code="mensajeconfirmacion.rechazo" />',
		MSG_CONFIRMACION_AUTOASIGNAR : '<spring:message code="mensajeconfirmacion.autoasignar" />',
		MSG_CONFIRMACION_REGISTRO_VALIDO : '<spring:message code="mensajeconfirmacion.registro.valido" />',
		MSG_CONFIRMACION_REGISTRO_INVALIDO : '<spring:message code="mensajeconfirmacion.registro.invalido" />',

		//REQUEST.
		MSG_ERROR_GENERAL : '<spring:message code="mensajeestatico.error.general" />',
		MSG_GRABADO_EXITO : '<spring:message code="mensajerespuesta.exito.guardo" />',
		MSG_ELIMINACION_EXITO : '<spring:message code="mensajerespuesta.exito.elimino" />',
		MSG_ERROR_RESPONSABLE : '<spring:message code="mensajerespuesta.error.noexisteresponsable" />',

		//VALIDACION.
		MSG_VAL_OBLIGATORIO : '<spring:message code="validacion.comun.mensajeobligatorio" />',
		MSG_VAL_MAXIMO_CARACTERES : '<spring:message code="validacion.comun.mensajemaximotexto" />',
		MSG_VAL_MINIMO_CARACTERES : '<spring:message code="validacion.comun.mensajeminimotexto" />',
		MSG_VAL_MAXIMO_DIGITOS : '<spring:message code="validacion.comun.mensajemaximodigito" />',
		MSG_VAL_MINIMO_DIGITOS : '<spring:message code="validacion.comun.mensajeminimodigito" />',
		MSG_VAL_FORMATO : '<spring:message code="validacion.comun.mensajetipodatotexto" />',
		MSG_VAL_ENTERO : '<spring:message code="validacion.comun.mensajetipodatodigito" />',
		MSG_VAL_DECIMAL : '<spring:message code="validacion.comun.mensajetipodatodecimal" />',
		MSG_VAL_FECHA : '<spring:message code="validacion.comun.mensajetipodatofecha" />',
		MSG_VAL_NOMBRE_VENTA : '<spring:message code="validacion.comun.nombreventa" />',

		//Titulo de dialogos.
		TITULO_CONFIRMACION_REGISTRO : '<spring:message code="titulo.confirmacion.registro" />',
		TITULO_DENEGACION_ACCION : '<spring:message code="titulo.denegacion.accion" />',

		PARAMETRO_ROL_ADMINISTRADOR : '<spring:message code="atenea.rol.administrador"/>',

		//PATRONES VALIDACION.
		PATRON_DECIMAL : /^(\d{1,9}|\d{1,9}\.\d{1,2})$/,
		PATRON_HORA : /^(\d{1,2}):(\d\d)$/
	};
</script>



<link href="<c:url value='${gPath}/resources/css/botones.css' />"
	rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link
	href="<c:url value='${gPath}/resources/js/bootstrap/css/bootstrap.min.css' />"
	rel="stylesheet">

<link
	href="<c:url value='${gPath}/resources/js/bootstrap/css/dropdowns-enhancement.css' />"
	rel="stylesheet">


<!-- SmartMenus jQuery Bootstrap Addon CSS -->

<link
	href="<c:url value='${gPath}/resources/css/smoothness/jquery-ui-1.10.3.custom.css' />"
	rel="stylesheet" />
<link href="<c:url value='${gPath}/resources/css/showLoading.css' />"
	rel="stylesheet" />


<link href="<c:url value='${gPath}/resources/css/common.css' />"
	rel="stylesheet" />

<link href="<c:url value='${gPath}/resources/css/menu/layer.css' />"
	rel="stylesheet" />
<link href="<c:url value='${gPath}/resources/css/style.css' />"
	rel="stylesheet" />



<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/jquery-1.9.1.js' />"></script>


<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/grid/grid.locale-es.js' />"></script>
<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/grid/jquery.jqGrid.js' />"></script>

<link rel="stylesheet" type="text/css" media="screen"
	href="<c:url value='${gPath}/resources/css/grid/ui.jqgrid.css'/>" />
<link rel="stylesheet" type="text/css" media="screen"
	href="<c:url value='${gPath}/resources/css/grid/ui.multiselect.css'/>" />

<link rel="stylesheet" type="text/css" media="screen"
	href="<c:url value='${gPath}/resources/fonts.css'/>" />

<link rel="stylesheet" type="text/css" media="screen"
	href="<c:url value='${gPath}/resources/estilos.css'/>" />

<!-- Bootstrap core JavaScript -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/bootstrap/js/bootstrap.min.js' />"></script>

<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/jquery-validation/dist/jquery.validate.js' />"></script>
<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/jquery-validation/dist/additional-methods.js' />"></script>
<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/jquery-validation/localization/messages_es.js' />"></script>

<!-- Placed after of the "bootstrap.min.js" -->
<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/moment-2.8.3.js' />"></script>
<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/jquery-ui-1.10.3.custom.js' />"></script>




<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/comun.js' />"></script>
<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/comunValidation.js' />"></script>

<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/constante.js' />"></script>

<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/jquery.babypaunch.spinner.min.js' />"></script>

<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/hiddenLoading.js' />"></script>


<link
	href="<c:url value='${gPath}/resources/css/carrito.css' />"
	rel="stylesheet">


<link
	href="<c:url value='${gPath}/resources/css/carrito2.css' />"
	rel="stylesheet">
<link
	href="<c:url value='${gPath}/resources/js/bootstrap/css/bootstrap-add.css' />"
	rel="stylesheet">


<link
	href="<c:url value='${gPath}/resources/css/prueba.css' />"
	rel="stylesheet">

</head>
<body style="min-width: 100%;">

<!-- empezo cambio -->


<header class="header">
		
			<!--'start_frame_cache_authorize'-->           
			 <div class="authorize hidden-print">
            <a href="#" class="login hidden-sm hidden-xs" data-toggle="modal" data-target="#authModal">
                <span class="login__label text-uppercase">iniciar sesión</span>
                <span class="login__icon"><img src="resources/img/sessionChico.png" alt="">
                </span>
            </a>
            
             <a href="#" class="login hidden-sm hidden-xs" data-toggle="modal" data-target="#authModal">
                <span class="login__label text-uppercase">Cart</span>
                <span class="login__icon"><img src="resources/img/carritoGrande.png" alt="">
                </span>
            </a>
            
            
             <a href="#" class="login hidden-sm hidden-xs" data-toggle="modal" data-target="#authModal">
                <span class="login__label text-uppercase">Cart</span>
                <span class="login__icon"><img src="resources/img/escudo.png" alt="">
                </span>
            </a>
            
		
	          </div>


		</div>
		<div class="row" >
						<div class="col-sm-12 col-md-3 print-logo">
									<div class="logo">
				
					<img src="resources/img/escudo.png" alt="" class="logo__img">
									</div>
			</div>
			
			<!-- Switching languages begin -->
			<div id="lang_block" class="col-sm-3 static-block hidden-print mobile-align">
								<!--'start_frame_cache_lang_dynamic_area'-->					<ul class="lang">
													<li class="lang__item">
								<a href="" class="no_click current lang__link lang__link--es">WEB PRINCIPAL DE SALTER - Fitness, Professional, Gimnasios, Gimnàs, Cardio, Gym, Wellness, Training, Equipment, Spinning, Deporte, Esport.</a>
							</li>
													<li class="lang__item">
								<a href="/ca/" class="lang__link lang__link--ca">WEB PRINCIPAL DE SALTER - Fitness, Professional, Gimnasios, Gimnàs, Cardio, Gym, Wellness, Training, Equipment, Spinning, Deporte, Esport.</a>
							</li>
													<li class="lang__item">
								<a href="/en/" class="lang__link lang__link--en">WEB PRINCIPAL DE SALTER - Fitness, Professional, Gimnasios, Gimnàs, Cardio, Gym, Wellness, Training, Equipment, Spinning, Deporte, Esport.</a>
							</li>
													<li class="lang__item">
								<a href="/fr/" class="lang__link lang__link--fr">WEB PRINCIPAL DE SALTER - Fitness, Professional, Gimnasios, Gimnàs, Cardio, Gym, Wellness, Training, Equipment, Spinning, Deporte, Esport.</a>
							</li>
											</ul>
									<!--'end_frame_cache_lang_dynamic_area'-->			</div>
			<!-- Switching languages end -->

			<div class="col-sm-6 static-block hidden-print mobile-align">
				<!--'start_frame_cache_search'-->	
				<div id="search">

		<form method="get" action="/es/gimnasio/" class="form form--special form--search search-mobile">
			<fieldset class="line-mobile">
				<input id="title-search-input" type="search" name="q" placeholder="BUSCADOR DE PRODUCTOS" class="form__input" value="" autocomplete="off">
				<button type="submit" class="form__button" name="s">irse</button>
			</fieldset>
		</form>

	</div>
		
		</div>
	
		</div>



		<div class="hidden-print relative-element">

		</div>
	</header>


<!-- termino cambio -->




	<div class="container" >
		<nav class="navbar navbar-default" >

			<div class="navbar-header" style="background:black">
			
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-ex1-collapse">
					
					<span class="sr-only">Desplegar navegación</span>

					 <span class="icon-bar"></span>
					 <span class="icon-bar"></span>
					 <span class="icon-bar"></span>
				</button>
				<div class="visible-sm visible-xs nav-mobile" id="mobile_navigation">
	
		              <span class="trigger lang-trigger"><img src="resources/img/sessionChico.png" alt="Login"></span>

	
              </div>
			
			</div>


			<div  id="menu_erp"  class="collapse navbar-collapse navbar-ex1-collapse"  style="width: 100%;text-align: left">

				<ul class="nav navbar-nav">
					<li><a id='idImagen' href='#'>Home</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Product<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<c:forEach items="${listaCategoria}" var="categoria">
								<li class="dropdown dropdown-submenu" ><a   style="font-size: 15px!important;" href="#" class="dropdown-toggle"
								 data-toggle="dropdown">&nbsp;${categoria.categoriaNombre}<b class="caret"></b></a>
									<ul class="dropdown-menu">
									
										<c:forEach items="${categoria.listaModelo}" var="modelo">
											<li><a style="font-size: 13px!important;" onclick="mostrarProductosSlider(${modelo.modeloId})" href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${modelo.modeloNombre}</a></li>
										
										</c:forEach>
									
								
								</ul>
                              </li>
                     
                              
                              
							</c:forEach>
						
						</ul>
						
						
					</li>

					<li><a id='idCases' href='#'>Cases</a></li>
					<li><a id='idServices' href='#'>Service & Support</a></li>
					<li><a id='idAbout' href='#'>About Us</a></li>
					<li><a id='idCategoria' href='#'>Mantenimiento</a></li>

					<li>
				</ul>
			</div>
		</nav>
	</div>



	<!--Basic example-->

	<div id="spin"></div>
	


	<div id="contentMain" style="text-align: center">

	
	</div>

	<form:form name="formSalir" id="formSalir" method="POST"
		action="${urlSalir}">
	</form:form>
	
</body>


<script type="text/javascript">



(function($){
	$(document).ready(function(){
		$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
			event.preventDefault(); 
			event.stopPropagation(); 
			$(this).parent().siblings().removeClass('open');
			$(this).parent().toggleClass('open');
		});
	});
})(jQuery);

var contador = 1;
 
			
$("#spin").spinner({       color: "black"         , 
	background: "rgba(255,255,255,0.5)"         ,
	html: "<i class='fa fa-repeat' style='color: gray;'></i>"    
	
	});

	urlBandeja = '${gPath}/imagen';
	$("#contentMain").load(gPath + urlBandeja);
		

	$("#menu_erp")
			.on(
					'click',
					'a',
					function() {

						var idSubMenuItem = "";

						var urlBandeja = "";
						idSubMenuItem = (this.id);

						switch (idSubMenuItem) {
						case 'rrrrrr':
							urlBandeja = '${gPath}/clienteController/mostrarGestionCliente';
							break;
						case 'rrrrr':
							urlBandeja = '${gPath}/insumoController/mostrarGestionInsumo';
							break;
						case 'rrrr':
							urlBandeja = '${gPath}/encuestaController/bandejaEncuesta';
							break;
						case 'rrr':
							urlBandeja = '${gPath}/examenController/mostrarGestionExamen';
							break;
						case 'ee':
							urlBandeja = '${gPath}/parametroController/mostrarGestionParametro';
							break;
						case 'ee':
							urlBandeja = '${gPath}/pedidoController/mostrarGestionPedido';
							break;
						
						case 'eee':
							urlBandeja = '${gPath}/compraController/mostrarGestionCompra';
							break;
						case 'idCategoria':
							urlBandeja = '${gPath}/categoriaController/mostrarGestionCategoria';
							break;
						case 'idImagen':
							urlBandeja = '${gPath}/imagen';
							break;
						default:
							urlBandeja = '';
						}

						if (urlBandeja != '') {
							$("#contentMain").load(gPath + urlBandeja);
						}
					});
			
	function mostrarProductosSlider(modeloId)
	{
	
	var urlBandeja = '${gPath}/productoController/listarProducto?modeloId='+modeloId;
	$("#contentMain").load( gPath+urlBandeja);
	
	}
			
</script>


</html>
