<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<c:url value='${gPath}/insumoController/editarStock'
	var="urlEditarInsumoGuardar" />
</head>
<body>
<h2 class="ico_tit_formulario">Agregar Stock</h2>
<div id="content">
		<form:form commandName="insumoBean" id="formAgregarStock"
			class="form-horizontal" action="${urlEditarInsumoGuardar}"
			method="POST">
			<form:hidden path="insumoId"></form:hidden>
			<div class="bs-group bs-group-no-margin">
                <div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Nombre:</label>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="insumoNombre" readonly="true"
							class="form-control  vjsmaxlength100 vjsrequired"
							style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>
			   <div class="col-xs-3">
                    <div class="txt_descr">
						<label class="control-label">Stock:</label>
					</div>
                </div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
					<input type="text"   value="${insumoBean.insumoStock}" id="txtStockInicial"readonly="readonly"
								class="form-control vjsrequired vjsminvalue0 vjsmaxvalue1000000 vjsdecimal"
							style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>
			   <div class="col-xs-3">
                    <div class="txt_descr">
						<label class="control-label">Entrada de Insumo:</label>
					</div>
                </div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
					<input type="text" id="txtEntradaInsumo" name="txtEntradaInsumo"
								class="form-control vjsrequired vjsminvalue0 vjsmaxvalue1000000 vjsdecimal"
							style="width:80%" />
					</div>
				</div>
							<div class="parrafo"></div>
			   <div class="col-xs-3">
                    <div class="txt_descr">
						<label class="control-label">Stock Final:</label>
					</div>
                </div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="insumoStock" readonly="true"
								class="form-control vjsrequired vjsminvalue0 vjsmaxvalue1000000 vjsdecimal"
							style="width:80%" />
					</div>
				</div>
			<div class="parrafo"></div>

				<div style="float: right; margin-right: 15px">
					<input class="btn btn-grabar" type="submit" value="GUARDAR"
						id="btnAgregarStockGuardar"> <input
						class="btn btn-default" type="button" value="CANCELAR"
						id="btnAgregarStockCancelar">
				</div>
				<div class="parrafo"></div>
				</div>
				</form:form>
				</div>
</body>

<script>
$(document).ready(function() {
	vincularValidacionEstilosJs("formAgregarStock",
			validarGuardarAgregarStock);
	$("#txtEntradaInsumo").keyup(function() {
		$('#insumoStock').val(eval(eval($('#txtStockInicial').val())+ eval($('#txtEntradaInsumo').val())).toFixed(2));
	});
	$("#btnAgregarStockCancelar").click(function() {
		$("#divDialogAgregarStock").dialog("close");
	});

});

function validarGuardarAgregarStock() {
	var aceptar$ = mostrarMensajeConfirmacion(ERP.MSG_CONFIRMACION_GRABADO,
			'Guardar Nuevo Stock');
	aceptar$.click(function() {
		guardarAgregarStock();
	});
};

function guardarAgregarStock() {
	var form = $('#formAgregarStock');
	$.ajax({
		type : form.attr('method'),
		url : form.attr('action'),
		data : form.serialize(),
		success : function(data) {
			$("#divDialogAgregarStock").dialog("close");
			buscarEnGestionInsumo();
		}
	});
};

</script>
</html>