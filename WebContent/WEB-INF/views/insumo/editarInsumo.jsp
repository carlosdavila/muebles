<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/insumo/editarInsumo.js' />"></script>

<c:url value='${gPath}/insumoController/editar'
	var="urlEditarInsumoGuardar" />



<title>Insert title here</title>
</head>
<body>
<h2 class="ico_tit_formulario">Editar Insumo</h2>
	<div id="content">
<form:form commandName="insumoBean" id="formEditarInsumo"
			class="form-horizontal" action="${urlEditarInsumoGuardar}"
			method="POST">
<form:hidden path="insumoId"/>
			<div class="bs-group bs-group-no-margin">
<h2>Datos de Insumo</h2>
				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Nombre:</label>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="insumoNombre"
							class="form-control  vjsmaxlength100 vjsrequired"
							style="width:80%" />
					</div>
				</div>
			
			
			
			
						<div class="parrafo"></div>
			
				
			
				<div class="col-xs-3">

					<div class="txt_descr" style="width: 100%">
						<label class="control-label">UM:</label>
					</div>

				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="insumoUM"
							class="form-control vjsmaxlength20 vjsrequired" style="width:80%" />
					</div>
				</div>
				
				<div class="parrafo"></div>
				<div class="col-xs-3">
                    <div class="txt_descr">
						<label class="control-label">Stock:</label>
					</div>
                </div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="insumoStock"
							class="form-control  vjsrequired vjsminvalue0 vjsmaxvalue1000000 vjsdecimal"
							style="width:80%" />
					</div>
				</div>
				
				<div class="parrafo"></div>
					<div class="col-xs-3">

					<div class="txt_descr">
						<label class="control-label">Precio Soles:</label>
					</div>

				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="insumoPrecioSoles"
							class="form-control vjsrequired vjsminvalue0 vjsmaxvalue1000000 vjsdecimal"
							style="width:80%" />
					</div>
				</div>
				
				
				<div class="parrafo"></div>
				

				<div style="float: right; margin-right: 15px">
					<input class="btn btn-grabar" type="submit" value="GUARDAR"
						id="btnEditarInsumoGuardar"> <input
						class="btn btn-default" type="button" value="CANCELAR"
						id="btnEditarInsumoCancelar">
				</div>
				<div class="parrafo"></div>
			</div>
		</form:form>

	</div>
</body>
<script>
	$(document).ready(function() {

		configurarEditarInsumo();

	});
</script>
</html>