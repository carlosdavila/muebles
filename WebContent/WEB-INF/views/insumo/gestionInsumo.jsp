<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>

<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/insumo/gestionInsumo.js' />"></script>

<c:url value='${gPath}/insumoController/obtenerListaFiltrada'
	var="urlListaFiltrada" />

<body>
<h1 >Gestion de Insumos</h1>
	<div id="content" class="jqGrid">

		<form id="formGestionInsumoBuscar" action="${urlListaFiltrada}"
			method="POST">
			<div class="bs-group">
				<div class="col-md-12">
					<label class="control-label">BUSQUEDA:</label>

					<div class="txt_descr_dato" style="text-align: left; width: 80px">
						<select id="cboGestionInsumoColumna" name="columna"
							style="text-align: left; width: 90%">
							<option value="insumoNombre">Nombre</option>
							
						</select>
					</div>
					<div class="txt_descr_dato" style="text-align: left; width: 100px">
						<input type="text" id="txtGestionInsumoBuscar" name="valor"
							style="text-align: left; width: 80px">
					</div>


					<input type="submit" class="btn btn-buscar " value="Buscar">

				</div>

				<div class="col-md-12">
					<div style="float: right; padding: 12px 12px 0 0">
						<input type="button" class="btn btn-agregar"
							id="btnGestionInsumoNuevo" value="Nuevo Insumo">
					</div>
				</div>
				<div class="parrafo"></div>
				<div class="col-md-12">
					<table id="listGestionInsumo" class="table table-bordered"></table>
					<div id="plistGestionInsumo"></div>
				</div>
				<div class="parrafo"></div>
			</div>
		</form>
	</div>
</body>
<script>
TC_DOLAR = '${tc}';
	$(document)
			.ready(
					function() {
						configurarGestionInsumo('<spring:message code="gestioninsumo.paginado"/>') ;		

					});

	
</script>
</html>