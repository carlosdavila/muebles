<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:url value='${gPath}/usuarioController/validarLogin' var="urlValidarLogin" />
 <html > 
 <title>SISTEMA DE ALMACEN</title>
   <head>
        <link rel="stylesheet" type="text/css" href="/Erp/resources/css/login/css/demo.css" />
        <link rel="stylesheet" type="text/css" href="/Erp/resources/css/login/css/style.css" />
		<link rel="stylesheet" type="text/css" href="/Erp/resources/css/login/css/animate-custom.css" />
				<link rel="stylesheet" type="text/css" href="/Erp/resources/css/comun.css" />
		<link href="<c:url value='${gPath}/resources/js/bootstrap/css/bootstrap.min.css' />" rel="stylesheet">
    </head>

    <body>

        <div class="container">
            <!-- Codrops top bar -->
            <div class="codrops-top">
               
                <span class="right">
                    <a >
                        <strong>SISTEMA DE ALMACEN</strong>
                    </a>
                </span>
                <div class="clr"></div>
            </div><!--/ Codrops top bar -->
            <header>
                <h1>SISTEMA DE ALMACEN<span></h1>
		
            </header>
            <section>				
                <div id="container_demo" class="codrops-top" >
          
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                        <div id="login" class="animate form">
                          <form:form commandName="usuario" method="POST"  action="${urlValidarLogin}">
                                <h1>Login</h1> 
                                <p> 
                                    <label for="username" class="uname" data-icon="u" >  Usuario </label>
                       
									<form:input path="usuarioUser" required="required" type="text" placeholder="usuario"/>
                                </p>
                                <p> 
                                    <label for="password" class="youpasswd" data-icon="p"> Clave </label>
									<form:password path="usuarioClave"  required="required"  placeholder="eg. X8df!90EO"/>
                                 
                                </p>
                                <p class="mensajeErrorFijo"> 
									<label class="mensajeErrorFijo">${mensajeError}</label>
								</p>
                                <p class="login button"> 
                                    <input type="submit" value="Login" /> 
								</p>
                            
                          </form:form>
                        </div>

						
                    </div>
                </div>  
            </section>
        </div>
     
     
  
    </body>
          
</html>