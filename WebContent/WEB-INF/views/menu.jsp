<%@ page language="java" pageEncoding="UTF-8" session="true"
	isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:url value='${gPath}/productoController/listarProducto'
	var="urlListarProducto" />

<style>
.thumb-dropdown .dropdown-menu>li {
	position: relative;
}

.thumb-dropdown .dropdown-menu>li>a .thumbnail {
	position: absolute;
	left: 100%;
	top: -10px;
	display: none;
	width: 350px;
	height: auto;
	margin-left: 5px;
}

.thumb-dropdown .dropdown-menu>li>a:hover .thumbnail {
	display: block;
}

.button-carrito {
	position: relative;
	float: right;
	padding: 9px 10px;
	margin-top: 8px;
	margin-right: 15px;
	margin-bottom: 8px;
	background-color: transparent;
	border: 1px solid transparent;
	border-radius: 4px;
}

.button-carrito:hover {
	position: relative;
	float: right;
	padding: 9px 10px;
	margin-top: 8px;
	margin-right: 15px;
	margin-bottom: 8px;
	background-color: #ffffff;
	border: 1px solid #ffffff;
	border-radius: 4px;
}

.button-carrito:active {
	position: relative;
	float: right;
	padding: 9px 10px;
	margin-top: 8px;
	margin-right: 15px;
	margin-bottom: 8px;
	background-color: transparent;
	border: 1px solid transparent;
	border-radius: 4px;
}

.btn-buscar_productos {
	background: url(${gPath}/resources/img/ico_btn_buscar.png) center left #75A538
		!important;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#888888',
		endColorstr='#8c8c8c', GradientType=0) !important;
	filter: progid:DXImageTransform.Microsoft.gradient(enabled=false)
		!important;
	background-repeat: no-repeat !important;
	border-color: #858585 !important;
	border: 1px solid #858585 !important;
	color: #FFF;
	box-shadow: inset 0 1px 0 rgba(255, 255, 255, .15), 0 1px 1px
		rgba(0, 0, 0, .075);
	padding: 5px 8px 5px 20px;
	background-color: transparent;
	border: 1px solid transparent;
	border-radius: 4px;
}

.btn-buscar_productos:hover {
	background-color: #ffffff;
	border-color: #ffffff !important;
	border: 1px solid #ffffff !important;
	border-radius: 4px;
}
</style>

<!-- cabecera -->

<c:if test="${!isPc}">



<div   class="container"
	style=" text-align: center;">


		<div class="col-xs-12" style=" display: table;  ">

	
			<div class="visible-sm visible-xs" style=" display: table-cell;">
			
					
						<img src="${gPath}/resources/img/escudo.jpg" width="250px;" height="123px">
					
				
	
			</div>
		</div>

	

	<div class="parrafo"></div>

</div>


<!-- Menu -->


<div class="navbar-wrapper">
	<div class="container">

		<div class="navbar navbar-inverse navbar-static-top" style="margin-right: -15px;">
			<div class="container">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

					<div class="visible-sm visible-xs"
						style="text-align: right !important;">
						<a href="#"> <span id="spanCarritoTotal" class="badge">$0</span></a>
						<button type="button" class="button-carrito"
							onclick="mostrarCarritoDetalle()"
							style="height: 45px; width: 50px; background-image: url('${gPath}/resources/img/carrito_x.jpg');">
						</button>


					</div>

					<a style="hover:" class="navbar-brand" href="#"></a>
				</div>


				<div id="menu_erp" class="collapse navbar-collapse ">

					<div class="visible-sm visible-xs"
						style="text-align: right !important;">
						<c:if test="${!isPc}">
							<form:form id="formListarProductos" class="form-horizontal"
								action="${urlListarProducto}" method="GET">
								<div class="input-group">
									<input type="text" id="txtBuscarProducto"
										style="min-height: 3.5em" class="form-control vjsrequired"
										placeholder="Buscar..."> <span class="input-group-btn">

										<input class="btn-buscar_productos" style="height: 42px;"
										type="submit" onclick="mostrarProductosSliderPorBuscar()"
										value="&nbsp;"> 
									</span>
								</div>
							</form:form>
						</c:if>
					</div>


					<ul class="nav navbar-nav">
						<li><a id='idImagen' href='#'>PRINCIPAL</a></li>
						<li><a id='idNosotros' href='#'>NOSOTROS</a></li>
						

		
		<li><a onclick=" mostrarModelosSlider(1,1)"  href='#'>PRODUCTOS</a></li>
		
					    <li><a id='idAccesorios' href='#'>ACCESORIOS</a></li>
				
						<li><a id='idPoliticas' href='#'>POLITICAS Y SEGURIDAD</a></li>
				     
						<c:if test="${ empty usuarioBean and isPc}">

							<li><a  href="#"
								onclick="mostrarLogin()">LOGIN </a></li>

						</c:if>
				
						<c:if test="${not empty usuarioBean }">
						<li><a id='idConfigurar' href='#'>CONFIGURAR</a></li>
						<li><a style="margin-right: 54%" onclick="salirSistema()" href='#'>SALIR</a></li>
                       </c:if>
					</ul>
				</div>
			</div>
		</div>

	</div>
</div>



</c:if>






















<c:if test="${isPc}">




<div   class="container"
	style=" text-align: center;">
	
		
		<div class="col-xs-2">

			
		<div class="visible-sm hidden-xs">	
			<div style="margin-left: 25%;">
						<img src="${gPath}/resources/img/escudo.jpg" width="250px;" height="123px">
					</div>

	</div>		
			
	</div>
	
		<div class="col-xs-5">
			<div class="visible-sm hidden-xs">
				<div style="margin-left: 45%;">
							<div id="myCarouselCabecera" class="carousel slide" data-ride="carousel"  style="width: 270px!important;margin-top: 5%;">


  <div class="carousel-inner" style="width: 250px!important;height: 100px">
      <!-- 
    <div class="item active"   style="width: 250px!important;">
     <img src="${gPath}/resources/img/ab_coaster_x.png"  width="250px;" height="auto" >
    </div>

    <div class="item"   style="width: 250px!important;">
       <img src="${gPath}/resources/img/tecnofitness_x.jpg"  width="250px;" height="auto" >
    </div>

    <div class="item"   style="width: 250px!important;">
          <img src="${gPath}/resources/img/impulse2016_x.jpg"  width="250px;" height="auto">
    </div>
    -->
  </div>

</div>
			</div>	
				
				
				</div>
	 </div>
	
		

		
		
		
	<c:if test="${isPc}">
		<div class="col-xs-2" style="margin-top: 32px">
			<div class="visible-sm hidden-xs">


				<form:form id="formListarProductos" class="form-horizontal"
					action="${urlListarProducto}" method="GET">
					<div class="input-group">
						<div  style="margin-left: 20%;">
							<input type="text" id="txtBuscarProducto" value="${valor}"
								class="form-control vjsrequired" style="min-height: 3.5em"
								placeholder="Buscar...">
						</div>
						<span class="input-group-btn"> <input
							class="btn-buscar_productos" style="height: 42px;" type="submit"
							onclick="mostrarProductosSliderPorBuscar()" value="">
						</span>
					</div>

				</form:form>

			</div>
		</div>
	
	
	<div class="col-xs-2" style="margin-top: 20px; margin-left: 10px">
		<div class="visible-sm hidden-xs">
		<!--  	<a href="#"> <span id="spanCarritoTotal1" class="badge">$0</span></a>-->
			
			<button type="button" class="button-carrito"
				
				style="height: 45px; width: 50px; background-image: url('${gPath}/resources/img/carrito_x.jpg');">
			</button>
<!--<img src="${gPath}/resources/img/paypal_x.jpg" width="150px"  data-toggle="tooltip" title="Page con PAYPAL(Visa,MasterCard,Maestro,American Express)">-->

		</div>
	</div>

	</c:if>



	<div class="parrafo"></div>

</div>


<!-- Menu -->


<div class="navbar-wrapper">
	<div class="container">

		<div class="navbar navbar-inverse navbar-static-top" style="margin-right: -15px;">
			<div class="container" style=" 
			font-size:15px;
  align-items: center;
  display: flex;
  justify-content: center;">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

					<div class="visible-sm visible-xs"
						style="text-align: right !important;">
						<a href="#"> <span id="spanCarritoTotal" class="badge">$0</span></a>
						<button type="button" class="button-carrito"
							onclick="mostrarCarritoDetalle()"
							style="height: 45px; width: 50px; background-image: url('${gPath}/resources/img/carrito_x.jpg');">
						</button>


					</div>

					<a style="hover:" class="navbar-brand" href="#"></a>
				</div>


				<div id="menu_erp" class="collapse navbar-collapse ">

					<div class="visible-sm visible-xs"
						style="text-align: right !important;">
			
					</div>

					<ul class="nav navbar-nav">
						<li><a id='idImagen' href='#'>PRINCIPAL</a></li>
						<li><a id='idNosotros' href='#'>NOSOTROS</a></li>
						
	
	<!--  
				<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown">PRODUCTOS<b class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<c:forEach items="${listaCategoria}" var="categoria">
									<li class="dropdown dropdown-submenu"><a onclick=" mostrarModelosSlider(${categoria.categoriaId},1)"
										style="font-size: 15px !important;" href="#"
										class="dropdown-toggle" data-toggle="dropdown">&nbsp;&nbsp;&nbsp;${categoria.categoriaNombre}</a>
			                         </li>
								</c:forEach>
							</ul></li>
		-->
					    <li><a  onclick=" mostrarModelosSlider(1,1)"href='#'>PRODUCTOS</a></li>  
		
						<li><a id='idPoliticas' href='#'>POLITICAS Y SEGURIDAD</a></li>
				   
							<li><a  href="#"
								onclick="mostrarLogin()">LOGIN </a></li>

				
						<c:if test="${not empty usuarioBean }">
						<li><a id='idConfigurar' href='#'>CONFIGURAR</a></li>
						<li><a style="margin-right: 54%" onclick="salirSistema()" href='#'>SALIR</a></li>
                       </c:if>
					</ul>
				</div>
			</div>
		</div>

	</div>
</div>



</c:if>


