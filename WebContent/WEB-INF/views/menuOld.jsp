<%@ page language="java" pageEncoding="UTF-8" session="true"
	isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<style>
.thumb-dropdown .dropdown-menu>li {
	position: relative;
}

.thumb-dropdown .dropdown-menu>li>a .thumbnail {
	position: absolute;
	left: 100%;
	top: -10px;
	display: none;
	width: 350px;
	height: auto;
	margin-left: 5px;
}

.thumb-dropdown .dropdown-menu>li>a:hover .thumbnail {
	display: block;
}

.button-carrito {
	position: relative;
	float: right;
	padding: 9px 10px;
	margin-top: 8px;
	margin-right: 15px;
	margin-bottom: 8px;
	background-color: transparent;
	border: 1px solid transparent;
	border-radius: 4px;
}

.button-carrito:hover {
	position: relative;
	float: right;
	padding: 9px 10px;
	margin-top: 8px;
	margin-right: 15px;
	margin-bottom: 8px;
	background-color: #ffffff;
	border: 1px solid #ffffff;
	border-radius: 4px;
}

.button-carrito:active {
	position: relative;
	float: right;
	padding: 9px 10px;
	margin-top: 8px;
	margin-right: 15px;
	margin-bottom: 8px;
	background-color: transparent;
	border: 1px solid transparent;
	border-radius: 4px;
}

.btn-buscar_productos {
	background: url(resources/img/ico_btn_buscar.png) center left #75A538
		!important;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#888888',
		endColorstr='#8c8c8c', GradientType=0) !important;
	filter: progid:DXImageTransform.Microsoft.gradient(enabled=false)
		!important;
	background-repeat: no-repeat !important;
	border-color: #858585 !important;
	border: 1px solid #858585 !important;
	color: #FFF;
	box-shadow: inset 0 1px 0 rgba(255, 255, 255, .15), 0 1px 1px
		rgba(0, 0, 0, .075);
	padding: 5px 8px 5px 20px;
	background-color: transparent;
	border: 1px solid transparent;
	border-radius: 4px;
}

.btn-buscar_productos:hover {
	background-color: #ffffff;
	border-color: #ffffff !important;
	border: 1px solid #ffffff !important;
	border-radius: 4px;
}
</style>

<!-- cabecera -->


<div id="ck_header" class="header clearfix"
	style="background: #ffffff !important; text-align: center">
	<div class="col-xs-6">
		<div style="margin-left: 60%;">
			<img src="resources/img/escudo.png" width="230px;" height="80px">
		</div>
	</div>

	<div class="col-xs-2" style="margin-top: 32px">
		<div class="visible-sm hidden-xs">
			<div class="input-group">
				<div>
					<input type="text" id="txtBuscarProducto" class="form-control" placeholder="Buscar...">
				</div>
				<span class="input-group-btn">
					<button class="btn-buscar_productos"  
						onclick="mostrarProductosSliderPorBuscar('txtBuscarProducto')" type="button"
						style="height: 42px;">&nbsp;</button>
				</span>
			</div>

		</div>
	</div>

	<div class="col-xs-1" style="margin-top: 20px; margin-left: 10px">
		<div class="visible-sm hidden-xs">
			<button type="button" class="button-carrito"
				onclick="mostrarCarritoDetalle()"
				style="height: 45px; width: 50px; background-image: url('resources/img/carrito.jpg');">
			</button>

		</div>
	</div>
	<div class="col-xs-1 " style="margin-top: 20px">
		<div class="visible-sm hidden-xs">
			<button type="button" class="button-carrito" onclick="mostrarLogin()"
				style="height: 45px; width: 50px; background-image: url('resources/img/login.jpg');">
			</button>
		</div>
	</div>

	<div class="col-xs-1 " style="margin-top: 20px">
		<div class="visible-sm hidden-xs"></div>
	</div>

	<div class="col-xs-12">

		<div id="divNombreUsuario" class="nombre_usuario">${usuario.usuarioNombre}</div>
</div>
	<div class="parrafo"></div>
</div>


<!-- Menu -->


<div class="navbar-wrapper">
	<div class="container">

		<div class="navbar navbar-inverse navbar-static-top">
			<div class="container">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

					<div class="visible-sm visible-xs"
						style="text-align: right !important;">
						<button type="button" class="button-carrito"
							onclick="mostrarCarritoDetalle()"
							style="height: 45px; width: 50px; background-image: url('resources/img/carrito.jpg');">
						</button>

						<button type="button" class="button-carrito"
							onclick="mostrarLogin()"
							style="height: 45px; width: 50px; background-image: url('resources/img/login.jpg');">
						</button>
					</div>

					<a style="hover:" class="navbar-brand" href="#"></a>
				</div>

			
				<div id="menu_erp" class="navbar-collapse collapse">
	         
	         <div class="visible-sm visible-xs"
						style="text-align: right !important;">
	          <form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" id="txtBuscarProductoMobil" class="form-control"
							placeholder="Buscar..."> <span class="input-group-btn">
							<button class="btn-buscar_productos"  
						onclick="mostrarProductosSliderPorBuscar('txtBuscarProductoMobil')" type="button"
						style="height: 42px;">&nbsp;</button>
						</span>
					</div>
				</form>
				</div>

					<ul class="nav navbar-nav">
						<li><a id='idImagen' href='#'>HOME</a></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown">PRODUCT<b class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<c:forEach items="${listaCategoria}" var="categoria">
									<li class="dropdown dropdown-submenu"><a
										style="font-size: 15px !important;" href="#"
										class="dropdown-toggle" data-toggle="dropdown">&nbsp;&nbsp;&nbsp;${categoria.categoriaNombre}<b
											class="caret"></b></a>
										<ul class="dropdown-menu">

											<c:forEach items="${categoria.listaModelo}" var="modelo">
												<li><a style="font-size: 12px !important;"
													onclick="mostrarProductosSlider(${modelo.modeloId})">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${modelo.modeloNombre}</a></li>
											</c:forEach>
										</ul></li>
								</c:forEach>
							</ul></li>

						<li><a id='idCases' href='#'>CASES</a></li>
						<li><a id='idServices' href='#'>SERVICE & SUPPORT</a></li>
						<li><a id='idAbout' href='#'>ABOUT US</a></li>
						<c:if test="${usuario.usuarioPerfil == 'administrador'}">
							<li><a id='idCategoria' href='#'>MANTENIMIENTO</a></li>
						</c:if>
						<li><a onclick="salirSistema()" href='#'>SALIR</a></li>

						<li>
					</ul>


				</div>
			</div>
		</div>

	</div>
</div>













<script type="text/javascript">

$(document).ready(function() {
	$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
		event.preventDefault(); 
		event.stopPropagation(); 
		$(this).parent().siblings().removeClass('open');
		$(this).parent().toggleClass('open');
	});
		$("#menu_erp")
		.on(
				'click',
				'a',
				function() {
					
					var idSubMenuItem = "";

					var urlBandeja = "";
					idSubMenuItem = (this.id);

					switch (idSubMenuItem) {
					case 'idLogin':
						urlBandeja = '${gPath}/usuarioController/mostrarLogin';
						break;
				
					case 'rrrrr':
						urlBandeja = '${gPath}/insumoController/mostrarGestionInsumo';
						break;
					case 'rrrr':
						urlBandeja = '${gPath}/encuestaController/bandejaEncuesta';
						break;
					case 'rrr':
						urlBandeja = '${gPath}/examenController/mostrarGestionExamen';
						break;
					case 'ee':
						urlBandeja = '${gPath}/parametroController/mostrarGestionParametro';
						break;
					case 'ee':
						urlBandeja = '${gPath}/pedidoController/mostrarGestionPedido';
						break;
					
					case 'eee':
						urlBandeja = '${gPath}/compraController/mostrarGestionCompra';
						break;
					case 'idCategoria':
						urlBandeja = '${gPath}/categoriaController/mostrarGestionCategoria';
						break;
					case 'idImagen':
						urlBandeja = '${gPath}/imagen';
						break;
					default:
						urlBandeja = '';
					}

					if (urlBandeja != '') {
						$("#contentMain").load(gPath + urlBandeja);
					}
				});

		setTimeout(mostrarPaginaImagen,100);
		

		
});
	
	</script>



