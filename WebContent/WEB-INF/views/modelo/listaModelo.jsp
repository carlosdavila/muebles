<%@ page language="java" pageEncoding="UTF-8" session="true"
	isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<style>

.fr-grid {
	margin: 20px 0 0 0;
	padding: 0;
	list-style: none;
	display: block;
	text-align: center;
	width: 100%;
}

.fr-grid:after,
.fr-item:before {
	content: '';
    display: table;
}

.fr-grid:after {
	clear: both;
}

.fr-grid li {
	width: 300px;
	height: 200px;
	display: inline-block;
	margin: 20px;
}




.fr-item {
	width: 100%;
	height: 100%;
	position: relative;
	cursor: default;
	box-shadow: 
		inset 0 0 0 16px rgba(206,206,206,0.8),
		0 1px 2px rgba(0,0,0,0.1);
		
	-webkit-transition: all 0.4s ease-in-out;
	-moz-transition: all 0.4s ease-in-out;
	-o-transition: all 0.4s ease-in-out;
	-ms-transition: all 0.4s ease-in-out;
	transition: all 0.4s ease-in-out;
}



.fr-info {
	position: absolute;
	background: rgba(105, 217, 26, 0.8);
	width: inherit;
	height: inherit;
	opacity: 0;
	
	-webkit-transition: all 0.4s ease-in-out;
	-moz-transition: all 0.4s ease-in-out;
	-o-transition: all 0.4s ease-in-out;
	-ms-transition: all 0.4s ease-in-out;
	transition: all 0.4s ease-in-out;
	
	-webkit-transform: scale(0);
	-moz-transform: scale(0);
	-o-transform: scale(0);
	-ms-transform: scale(0);
	transform: scale(0);
	
	-webkit-backface-visibility: hidden;

}

.fr-info h3 {
	color: #fff;
	text-transform: uppercase;
	letter-spacing: 2px;
	font-size: 22px;
	margin: 0 30px;
	padding: 45px 0 0 0;
	height: 140px;
	   font-family: 'Roboto'!important;
         font-style: italic!important;
	text-shadow: 
		0 0 1px #fff, 
		0 1px 2px rgba(0,0,0,0.3);
}

.fr-info p {
	 color: #fff; 
	 padding: 10px 5px; 
	   font-family: 'Roboto'!important;
         font-style: italic!important;
	 margin: 0 30px; 
	 font-size: 12px; 
	 border-top: 1px solid rgba(255,255,255,0.5); 
	 opacity: 0; */
	-webkit-transition: all 1s ease-in-out 0.4s; 
	-moz-transition: all 1s ease-in-out 0.4s; 
	-o-transition: all 1s ease-in-out 0.4s; 
	! -ms-transition: all 1s ease-in-out 0.4s; 
	! transition: all 1s ease-in-out 0.4s; 
}

.fr-info p a {
	display: block;
	color: #fff; 
	color: rgba(255,255,255,0.7); 
	font-style: normal; 
	font-weight: 700; 
	text-transform: uppercase;
	font-size: 9px;
	letter-spacing: 1px;
	padding-top: 4px;
	   font-family: 'Roboto'!important;
         font-style: italic!important;
}

.fr-info p a:hover {
	color: #fff222;
	color: rgba(255,242,34, 0.8);
}

.fr-item:hover {
	box-shadow: 
		inset 0 0 0 1px rgba(255,255,255,0.1),
		0 1px 2px rgba(0,0,0,0.1);
}
.fr-item:hover .fr-info {
	-webkit-transform: scale(1);
	-moz-transform: scale(1);
	-o-transform: scale(1);
	-ms-transform: scale(1);
	transform: scale(1);
	opacity: 1;
}

.fr-item:hover .fr-info p {
	opacity: 1;
}

.claseAnchoLargoModelo {
	width: 300px!important;
    height:200px!important;
    cursor: pointer!important;
   
    background-size: 300px 200px;
    background-repeat: no-repeat;
}

</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>

<div class="container">

<h2 style="font-size: 30px;!important;  font-family: 'Roboto'!important;font-style: italic!important;">Catálogo</h2>
<!--  
	<c:if test="${total ne 1}">

	<div style="margin-left: 15px">
		<ul class="pagination">
			<c:if test="${pagina ne 1}">
				<li><a href="#"
					onclick="mostrarModelosSlider('${categoriaId}','${pagina-1}')">Previous</a></li>
			</c:if>

			<c:forEach var="i" begin="1" end="${total}">

				<c:if test="${i eq pagina}">
					<li class="active"><a href="#"
						onclick="mostrarModelosSlider('${categoriaId}',${i})">${i}</a></li>

				</c:if>
				<c:if test="${i ne pagina}">
					<li><a href="#"
						onclick="mostrarModelosSlider('${categoriaId}',${i})">${i}</a></li>

				</c:if>


			</c:forEach>
			<c:if test="${pagina ne total}">
				<li><a href="#"
					onclick="mostrarModelosSlider('${categoriaId}','${pagina+1}')">Next</a></li>
			</c:if>

		</ul>
	</div>

</c:if>
-->
<div class="row main">
<ul class="fr-grid" >
	<c:forEach items="${listaModelo}" var="modelo">
		
			<li>
		
			<div id="divImgModelo_${modelo.modeloId}" class="fr-item claseAnchoLargoModelo" 
			style="cursor: pointer!important;" onclick="mostrarProductosSlider(${modelo.modeloId},null,1)">
			<div class="fr-info">
			<h3>${modelo.modeloNombre}</h3>
			<p>
			<a>Impulse</a>
			</p>
			</div>
			</div>
			</li>
				
				<script>
				cargarImagenModelo("divImgModelo_${modelo.modeloId}","${modelo.modeloImagen}");
                </script>
                
	
	</c:forEach>
		</ul>
	</div>
	</div>
	
	
	<!-- 
	<c:if test="${total ne 1}">

	<div style="margin-left: 15px">
		<ul class="pagination">
			<c:if test="${pagina ne 1}">
				<li><a href="#"
					onclick="mostrarModelosSlider('${categoriaId}','${pagina-1}')">Previous</a></li>
			</c:if>

			<c:forEach var="i" begin="1" end="${total}">

				<c:if test="${i eq pagina}">
					<li class="active"><a href="#"
						onclick="mostrarModelosSlider('${categoriaId}',${i})">${i}</a></li>

				</c:if>
				<c:if test="${i ne pagina}">
					<li><a href="#"
	                    onclick="mostrarModelosSlider(${categoriaId},${i})">${i}</a></li>

				</c:if>


			</c:forEach>
			<c:if test="${pagina ne total}">
				<li><a href="#"
					onclick="mostrarModelosSlider('${categoriaId}','${pagina+1}')">Next</a></li>
			</c:if>

		</ul>
	</div>

</c:if>
	-->
	
</body>

</html>