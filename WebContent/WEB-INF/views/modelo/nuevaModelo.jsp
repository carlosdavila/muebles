<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html>

<head>
	
	 <script type="text/javascript" src="${gPath}/resources/js/modelo/nuevaModelo.js"></script>
	
<c:url value='${gPath}/modeloController/guardar'
	var="urlNuevaModeloGuardar" />

</head>
<body>
	
	<div id="content">

		<div class="bs-group bs-group-no-margin">
        <div class="parrafo"></div>
      
      		<div class="col-xs-3">
				<div class="txt_descr">
					<label class="control-label">Imagenes <span
						class="label-alert"> (*)</span></label>
				</div>
			</div>
			<div class="col-xs-9">
			</div>
      
      <div class="parrafo"></div>
			<div class="parrafo"></div>
		<!--  
        <div class="col-xs-6">
                <div ><label>Imagen Portada Producto </label></div>
                <div>
				<img id="guardarModeloImg0" style="width: 45px; height: 50px">
				</div>
				 <div id="divEliminarModelo0"><a onclick="eliminarImagenModelo(0)">Eliminar</a></div>
		</div>
	    -->	
	    <div class="col-xs-6">
                <div ><label>Imagen Catalogo (300x200)</label></div>
                <div>
				<img id="guardarModeloImg0" style="width: 45px; height: 50px">
				</div>
				 <div id="divEliminarModelo0"><a onclick="eliminarImagenModelo(0)">Eliminar</a></div>
		</div>
			 

			<div class="parrafo"></div>


			<div class="col-xs-12">
				<div class="contenedor-cargar-archivo" style="visibility: hidden;">
					<input type="text" id="fileName" style="visibility: hidden;">
				</div>
				<div id="divBotonCargar">

					<input id="fileInputButton" type="button" value="Cargar"
						class="btn-cargar"  />
                    <input type="file" name="file"
						id="fileupload" class="file_input_hidden"
						onchange="	var path = this.value;
							var fileName = path.match(/[^\/\\]+$/);  
						document.getElementById('fileName').value = fileName;
						var tamanio = document.getElementById('fileupload').files[0].size;
						if(tamanio>'<spring:message code="tamanio.imagen.modelo"/>'){
						alert('El peso de la imagen es mayor a <spring:message code="tamanio.imagen.modelo.kb"/>');
						return false;
						}" />
					
				</div>
			</div>
			

			<div class="parrafo"></div>
		<form:form commandName="modeloBean" id="formNuevaModelo"
			class="form-horizontal" action="${urlNuevaModeloGuardar}"
			method="POST">

			<div class="bs-group bs-group-no-margin">
			<!--  
		<div class="col-xs-3">
						<div class="txt_descr">
							<label class="control-label">Categoria:</label>
						</div>
					</div>
					<div class="col-xs-9">
						<div class="txt_descr_dato" style="width: 100%">
							<form:select 
								path="categoriaBean.categoriaId" style="width:80%">
								<form:options items="${listaCategoria}" itemValue="categoriaId"
									itemLabel="categoriaNombre" />
							</form:select>
						</div>
					</div>
					-->
					<div class="parrafo"></div>
				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Nombre:</label>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="modeloNombre" class="form-control vjsrequired"
							style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>
				
				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Tipo</label>
					</div>
				</div>
				
				<div class="col-xs-9">
					<div class="txt_tipo_dato" style="width: 100%">
						<form:input path="modeloTipo" class="form-control vjsrequired"
							style="width:80%" value="MODELO" readonly="true" />
					</div>
				</div>
		
			
				<form:hidden path="modeloOrden" class="form-control vjsrequired"
							style="width:80%"  readonly="true"/>
				
				<div class="parrafo"></div>


				<div style="float: right; margin-right: 15px">
					<input class="btn btn-grabar" type="submit" value="GUARDAR"
						id="btnNuevaModeloGuardar"> <input
						class="btn btn-default" type="button" value="CANCELAR"
						id="btnNuevaModeloCancelar">
				</div>
					<div style="text-align: center;">
				  <p class="mensajeError" id="mensajeErrorModelo" style="visibility: hidden;"> 
									<label class="mensajeErrorFijo"><spring:message code="mensaje.error.generico"/></label>
				  </p>
				  	</div>
				  				
				<div class="parrafo"></div>
			</div>
		</form:form>
	</div>
	</div>
</body>
<script>
	$(document).ready(function() {
		configurarNuevaModelo();
		reiniciarImagenesModelo();
		var urlInsertarAdjunto = gPath
				+ "/modeloController/insertarAdjunto";

		$('#fileupload').fileupload({
			url : urlInsertarAdjunto,
			dataType : 'json',
			success : function(data) {
				var  tamanio= data.length;
				$('#divBotonCargar').show();
				if(eval(tamanio)==1){
					$('#divBotonCargar').hide();
				}
				reiniciarImagenesModelo();
				cargarImagenesModelo(data);
			},  error: function (e, data) {
		           alert("Error en carga de archivo");
	        }
		}).prop('disabled', !$.support.fileInput).parent().addClass(
				$.support.fileInput ? undefined : 'disabled');
		
	});
	
	function cargarImagenesModelo(data) {
		
		$.each(data, function(i, item) {
			
			cargarImagenProducto("guardarModeloImg" + i, item.nombre);
			$('#divEliminarModelo'+i).show();
			
		});
		
	}
	
	
	function eliminarImagenModelo(numeroImagen){
		
		$.ajax({
			url : gPath+'/modeloController/eliminarImagen?numeroImagen='
			+ numeroImagen,
			type : 'get',
			dataType : 'json',
			success : function(data) {
				$('#divBotonCargar').show();
				reiniciarImagenesModelo();
				cargarImagenesModelo(data);
			}
		});
	}
	
	
	
	function reiniciarImagenesModelo(){
		
		
		$('#guardarModeloImg0').attr("src", gPath+"/resources/img/default.jpg");
	//	$('#guardarModeloImg1').attr("src", gPath+"/resources/img/default.jpg");
				
	
		$('#divEliminarModelo0').hide();
	//	$('#divEliminarModelo1').hide();
	
	}
</script>
</html>