<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/parametro/editarParametro.js' />"></script>
<c:url value='${gPath}/parametroController/editar'
	var="urlEditarParametroGuardar" />
<title>Insert title here</title>
</head>
<body>
	<div id="content">
	<h2 class="ico_tit_formulario">Editar Parametro</h2>
		<form:form commandName="parametroBean" id="formEditarParametro"
			class="form-horizontal" action="${urlEditarParametroGuardar}"
			method="POST">
			<form:hidden path="parametroId"></form:hidden>
			<form:hidden path="parametroNombre"></form:hidden>
			<div class="bs-group bs-group-no-margin">

				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">${parametroBean.parametroNombre}:</label>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="parametroValor" class="form-control vjsrequired"
							style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>


				<div style="float: right; margin-right: 15px">
					<input class="btn btn-grabar" type="submit" value="GUARDAR"
						id="btnEditarParametroGuardar"> <input
						class="btn btn-default" type="button" value="CANCELAR"
						id="btnEditarParametroCancelar">
				</div>
				<div class="parrafo"></div>
			</div>
		</form:form>
	</div>
</body>
<script>
	$(document).ready(function() {

		configurarEditarParametro();

	});
</script>
</html>