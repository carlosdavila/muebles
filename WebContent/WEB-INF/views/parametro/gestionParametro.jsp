<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/parametro/gestionParametro.js' />"></script>
<c:url value='${gPath}/parametroController/obtenerListaFiltrada'
	var="urlListaFiltrada" />

<body>
<h1 >Gestion de Parametros</h1>
	<div id="content" class="jqGrid">
		<form id="formGestionParametroBuscar" action="${urlListaFiltrada}"
			method="POST">
			<div class="bs-group">
				<div class="col-md-12">


					<input type="submit" class="btn btn-buscar " value="Buscar">

				</div>
				<div class="col-md-12">
					<table id="listGestionParametro" class="table table-bordered"></table>

				</div>
				<div class="parrafo"></div>
			</div>
		</form>
	</div>
</body>
<script>
	$(document)
			.ready(
					function() {
						
						configurarGestionParametro();
				});

	
</script>
</html>