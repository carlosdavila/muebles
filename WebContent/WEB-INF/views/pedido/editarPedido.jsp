<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<html>

<head>

<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/pedido/editarPedido.js' />"></script>

<c:url value='${gPath}/pedidoController/editar'
	var="urlEditarPedidoGuardar" />
<title>Insert title here</title>
</head>
<body>
	<h2 class="ico_tit_formulario">Editar Pedido</h2>
	<div id="content" class="jqGrid">
		<form:form commandName="pedidoBean" id="formEditarPedido"
			class="form-horizontal" action="${urlEditarPedidoGuardar}"
			method="POST">
			<form:hidden path="pedidoId" />
			<form:hidden path="clienteBean.clienteId" id="txtClienteIdEnPedido" />
			<form:hidden path="listProductoId" />
			<form:hidden path="pedidoOrdenArchivo" />
			<form:hidden path="pedidoIgv"/>
			<div class="bs-group bs-group-no-margin">

				<h2 class="h2-no-margin">Datos del Cliente</h2>
				<div class="col-md-3">
					<div class="txt_descr">
						<label class="control-label">Nombre:</label>
					</div>
				</div>

				<div class="col-md-6">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input id="txtClienteNombreEnPedido" 
							path="clienteBean.clienteNombre" class="form-control vjsrequired"
							style="width:80%" disabled="true" />
					</div>
				</div>
				<div class="col-md-3">
					<div style="float: right; padding: 8px 12px 0 0">
						<input type="button" class="btn btn-buscar"
							id="btnBuscarClienteEnPedido" value="Buscar Cliente">
					</div>
				</div>
				<div class="parrafo"></div>
				<div class="col-md-3">
					<div class="txt_descr">
						<label class="control-label">Tipo Doc:</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input id="txtTipoDocEnPedido"
							path="clienteBean.tipoDocBean.tipoDocDesc"
							class="form-control vjsrequired" disabled="true"
							style="width:80%" />
					</div>
				</div>

				<div class="col-md-3">
					<div class="txt_descr">
						<label class="control-label">Nro Doc:</label>
					</div>
				</div>

				<div class="col-md-3">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input id="txtNroDocEnPedido"
							path="clienteBean.clienteNroDoc" class="form-control vjsrequired"
							style="width:80%" disabled="true" />
					</div>
				</div>
				<div class="parrafo"></div>
				<div class="col-md-3">
					<div class="txt_descr">
						<label class="control-label">Direccion:</label>
					</div>
				</div>

				<div class="col-md-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input id="txtDireccionEnPedido"
							path="clienteBean.clienteDireccion"
							class="form-control vjsrequired" disabled="true"
							style="width:80%" />
					</div>
				</div>

				<div class="parrafo"></div>
				<div class="col-md-3">
					<div class="txt_descr">
						<label class="control-label">Teléfono:</label>
					</div>
				</div>

				<div class="col-md-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input id="txtTelefonoEnPedido"
							path="clienteBean.clienteTelefono" class="form-control"
							disabled="true" style="width:80%" />
					</div>
				</div>

				<div class="parrafo"></div>
				<h2>Lista de Productos</h2>
				<div class="col-md-9"></div>
				<div class="col-md-3">
					<div style="float: right; padding: 12px 12px 0 0">
						<input type="button" class="btn btn-agregar"
							id="btnAgregarProductoEnPedido" value="Agregar Producto">
					</div>
				</div>
				<div class="col-md-12">
					<table id="listPedidoDetalle" class="table table-bordered">

					</table>
				</div>
				<div class="parrafo"></div>
				<h2>Datos del Pedido</h2>
				<div class="col-md-3">
					<div class="txt_descr">
						<label class="control-label">Pedido:</label>
					</div>
				</div>
				<div class="col-md-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="pedidoNombre" class="form-control vjsrequired"
							style="width:80%" />
					</div>
				</div>

				<div class="col-md-4">

					<div class="txt_descr">
						<label class="control-label">Precio Bruto:</label>
					</div>

					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="pedidoPrecioBruto" readonly="true"
							class="form-control vjsrequired" style="width:80%" />
					</div>
				</div>

				<div class="col-md-4">
					<div class="txt_descr">
						<label class="control-label">PRECIO IGV:</label>
					</div>

					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="pedidoPrecioIgv" class="form-control vjsrequired"
							readonly="true" style="width:80%" />
					</div>
				</div>


				<div class="col-md-4">
					<div class="txt_descr">
						<label class="control-label">Precio Neto:</label>
					</div>

					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="pedidoPrecioNeto" readonly="true"
							class="form-control vjsrequired" style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>
				<h2>Datos de Orden</h2>
				<div class="col-md-4">
					<div class="txt_descr">
						<label class="control-label">Nro de Orden:</label>
					</div>
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="pedidoNroOrden" class="form-control "
							style="width:80%" />
					</div>
				</div>

				<div class="col-md-4">
					<div class="txt_descr">
						<label class="control-label">Fecha de Orden:</label>
					</div>
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="pedidoFechaOrdenRequest"
							class="form-control  vjsfecha" style="width:85%" />
					</div>
				</div>

				<div class="col-md-4">

					<div style="float: left;">
						<div class="contenedor-cargar-archivo">
							<label class="control-label">Archivo </label> <input type="text"
								id="fileName" class="file_input_textbox form-control"
								readonly="readonly">
						</div>

						<div class="file_input_div contenedor-boton-cargar">
							<input id="fileInputButton" type="button" value="..."
								class="file_input_button" /> <input type="file" name="file"
								id="nodoArchivo" class="file_input_hidden"
								onchange="	var path = this.value;
							var fileName = path.match(/[^\/\\]+$/);  
						document.getElementById('fileName').value = fileName;" />
						</div>

					</div>

				</div>

				<div class="parrafo"></div>
				<h2>Datos de Factura</h2>
				<div class="col-md-4">
					<div class="txt_descr">
						<label class="control-label">Nro de Factura:</label>
					</div>
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="pedidoNroFactura" class="form-control"
							style="width:80%" />
					</div>
				</div>

				<div class="col-md-4">
					<div class="txt_descr">
						<label class="control-label">Fecha de Factura:</label>
					</div>
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="pedidoFechaFacturaRequest"
							class="form-control  vjsfecha" style="width:85%" />
					</div>
				</div>
					
				<div class="parrafo"></div>
				
						<h2>Datos de Guia</h2>
				<div class="col-md-4">
					<div class="txt_descr">
						<label class="control-label">Nro de Guia:</label>
					</div>
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="pedidoNroGuia" class="form-control"
							style="width:80%" />
					</div>
				</div>

				<div class="col-md-4">
					<div class="txt_descr">
						<label class="control-label">Fecha de Guia:</label>
					</div>
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="pedidoFechaGuiaRequest"
							class="form-control  vjsfecha" style="width:85%" />
					</div>
		
				</div>
				<div class="parrafo"></div>
				<div style="float: right; margin-right: 15px">
					<input class="btn btn-grabar" type="submit" value="GUARDAR"
						id="btnGuardarEditarPedido"> <input
						class="btn btn-default" type="button" value="CANCELAR"
						id="btnCancelarEditarPedido">

				</div>
				<div class="parrafo"></div>


				</div>
		</form:form>
	</div>
</body>
<script>
	lastsel = -1;
	$(document).ready(function() {
		configurarEditarPedido('${pedidoBean.pedidoId}');
	});
</script>
</html>