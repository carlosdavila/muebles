<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>

<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/pedido/gestionPedido.js' />"></script>
	<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/jquery.ajaxfileupload.js' />"></script>

<c:url value='${gPath}/pedidoController/obtenerListaFiltrada'
	var="urlListaFiltrada" />


<body>
<h1 >Gestion de Pedidos</h1>
	<div id="content" class="jqGrid">
		<form id="formGestionPedidoBuscar" action="${urlListaFiltrada}"
			method="POST">
					
		<div class="bs-group">
				<div class="col-md-12">
					<label class="control-label">BUSQUEDA:</label>

					<div class="txt_descr_dato" style="text-align: left; width: 80px">
						<select id="cboGestionPedidoColumna" name="columna"
							style="text-align: left; width: 100% ;padding:6px 6px!important">
							<option value="pedidoNombre">Pedido</option>
							<option value="clienteBean.clienteNombre">Cliente</option>
							<option value="pedidoNroOrden">Nro Orden</option>
							<option value="pedidoFechaOrden">Fecha Orden</option>
							<option value="pedidoNroFactura">Nro Factura</option>
							<option value="pedidoFechaFactura">Fecha Factura</option>
							<option value="pedidoNroGuia">Nro Guia</option>
							<option value="pedidoFechaGuia">Fecha Guia</option>
						</select>
					</div>
           
					<div  id="divValorGestionPedido" class="txt_descr_dato" style="text-align: left; width: 100px;">
						  <input type="text" id="txtGestionPedidoBuscar" name="valor"
							style="text-align: left; width: 80px ;padding:6px 6px!important">
					</div>

                    <div id="divFechaInicialGestionPedido" class="txt_descr_dato" style="text-align: left; width: 100px">
							<label class="control-label">F.INICIO:</label><input type="text" id="fechaInicio" name="fechaInicio" class="form-control  vjsfecha error"
							style="text-align: left; width: 80px">
					</div>
					    <div id="divFechaFinalGestionPedido" class="txt_descr_dato" style="text-align: left; width: 100px">
						<label class="control-label">F.FIN:</label><input type="text" id="fechaFinal" name="fechaFinal" class="form-control  vjsfecha error"
							style="text-align: left; width: 80px">
					</div>
                        
					<input type="submit" class="btn btn-buscar " value="Buscar">

				</div>

				<div class="col-md-12">
					<div style="float: right; padding: 12px 12px 0 0">
						<input type="button" class="btn btn-agregar"
							id="btnGestionPedidoNuevo" value="Nuevo Pedido">
					</div>
				</div>
				<div class="parrafo"></div>
				<div class="col-md-12">
					<table id="listGestionPedido" class="table table-bordered"></table>
					<div id="plistGestionPedido"></div>
				</div>
				<div class="parrafo"></div>
			</div>
		</form>
	</div>
	    <form id="formExportarfactura" name="formExportarfactura" method="POST">
	       <input type="hidden" name="pedidoId" id="pedidoIdExportarFactura" >
       </form>
       <form id="formExportarGuia" name="formExportarGuia" method="POST">
	       <input type="hidden" name="pedidoId" id="pedidoIdExportarGuia" >
       </form>
       	  <form id="formExportarOrden" name="formExportarOrden" method="POST">
	          <input type="hidden" name="pedidoId" id="pedidoIdExportarOrden" >
         </form>
</body>
<script>
	$(document)
			.ready(
					function() {
						configurarGestionPedido('<spring:message code="gestionpedido.paginado"/>');
					});


</script>
</html>