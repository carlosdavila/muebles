<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html>

<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/pregunta/gestionPregunta.js' />"></script>

<c:url value='${gPath}/preguntaController/obtenerListaFiltrada'
	var="urlListaFiltrada" />

<body>
<h1 >Gestion de Pregunta</h1>
	<div id="content" class="jqGrid">
		<form id="formGestionPreguntaBuscar" action="${urlListaFiltrada}"
			method="GET">
			<div class="bs-group">
				<div class="col-md-12">
                  <input id="idExamen" type="hidden" value="${examenBean.examenId}">
                 <input type="text" value="${examenBean.examenNombre}">
				</div>
			
          <div class="parrafo"></div>
				<div class="col-md-12">
					<div style="float: right; padding: 12px 12px 0 0">
						<input type="button" class="btn btn-agregar"
							id="btnGestionPreguntaNuevo" value="Nueva Pregunta">
					</div>
				</div>
				<div class="parrafo"></div>
				<div class="col-md-12">
					<table id="listGestionPregunta" class="table table-bordered"></table>
					<div id="plistGestionPregunta"></div>
				</div>
				<div class="parrafo"></div>
			</div>
		</form>
	</div>
</body>
<script>
	$(document)
			.ready(
					function() {
				
						configurarGestionPregunta() ;
				

					});


</script>
</html>