<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>


<script type="text/javascript"
	src="<c:url value='${gPath}/resources/js/producto/buscarProducto.js' />"></script>
<c:url value='${gPath}/productoController/obtenerListaFiltrada'
	var="urlListaFiltrada" />
	

<body>
<h2 class="ico_tit_formulario">Buscar Producto</h2>
	<div id="content" class="jqGrid">
		<form id="formBuscarProducto" action="${urlListaFiltrada}"
			method="GET">
			<div class="bs-group">
				<div class="col-md-12">
					<label class="control-label">BUSQUEDA:</label>

					<div class="txt_descr_dato" style="text-align: left; width: 80px">
						<select id="cboColumnaBuscarProducto" name="columna"
							style="text-align: left; width: 90%">
							<option value="productoNombre">Nombre</option>
							<option value="productoTipo">Tipo</option>
							<option value="productoPadre">Padre</option>
						</select>
					</div>

					<div class="txt_descr_dato" style="text-align: left; width: 100px">
						<input type="text" id="txtValorBuscarProducto" name="valor"
							style="text-align: left; width: 80px">
					</div>


					<input type="submit" class="btn btn-buscar " value="Buscar">

				</div>

				<div class="col-md-12">
					<table id="listBuscarProducto" class="table table-bordered"></table>
					<div id="plistBuscarProducto"></div>
				</div>
				<div class="parrafo"></div>
			</div>
		</form>
	</div>
</body>
<script>
	$(document)
			.ready(
					function() {
				
						configurarBuscarProducto('<spring:message code="buscarproducto.paginado"/>');

					});




</script>
</html>