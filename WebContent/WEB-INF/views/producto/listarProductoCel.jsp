
<%@ page language="java" pageEncoding="UTF-8" session="true"
	isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<style>

/* The Modal (background) */
.modalProducto {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 9000; /* Sit on top */
	padding-top: 100px; /* Location of the box */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	overflow: auto; /* Enable scroll if needed */
	background-color: rgb(0, 0, 0); /* Fallback color */
	background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modalProducto-content {
	margin: auto;
	display: block;
	width: 80%;
	max-width: 700px;
}

/* Caption of Modal Image */
.captionText {
	margin: auto;
	display: block;
	width: 80%;
	max-width: 700px;
	text-align: center;
	color: #ccc;
	padding: 10px 0;
	height: 150px;
}

/* Add Animation */
.modalProducto-content, #caption {
	-webkit-animation-name: zoom;
	-webkit-animation-duration: 0.6s;
	animation-name: zoom;
	animation-duration: 0.6s;
}

@
-webkit-keyframes zoom {
	from {-webkit-transform: scale(0)
}

to {
	-webkit-transform: scale(1)
}

}
@
keyframes zoom {
	from {transform: scale(0)
}

to {
	transform: scale(1)
}

}

/* The Close Button */
.closeProducto {
	position: absolute;
	top: 15px;
	right: 35px;
	color: #f1f1f1;
	font-size: 40px;
	font-weight: bold;
	transition: 0.3s;
}

.closeProducto:hover, .closeProducto:focus {
	color: #bbb;
	text-decoration: none;
	cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px) {
	.modalProducto-content {
		width: 100%;
	}
}

.claseAnchoLargo {
	width: 300px !important;
	height: 300px !important;
	cursor: pointer !important;
	background: url(resources/img/imagenGrande.jpg);
	background-size: 300px 300px;
	background-repeat: no-repeat;
}

.claseAnchoLargo2 {
	width: 300px !important;
	height: 300px !important;
	cursor: pointer !important;
	background-repeat: no-repeat;
}

#imagenEspecificacion {
	height: auto;
	position: relative;
}

#imagenEspecificacion>div {
	position: relative;
	color: green !important;
	font-size: 13px !important;
	padding: 0px 0px;
	margin-bottom: -12px;
	margin-left: 0px;
}
</style>
<!-- 
<c:if test="${not empty modeloBean}">
	<div style="margin-left: 15px">
		<img style="width: 100%; height: auto;" id="imgModeloCabecera">
	</div>

	<script>
cargarImagenProducto("imgModeloCabecera","${modeloBean.modeloImagen}");
</script>

</c:if>
-->

<c:if test="${total ne 1}">

	<div style="margin-left: 15px">
		<ul class="pagination">
			<c:if test="${pagina ne 1}">
				<li><a href="#"
					onclick="mostrarProductosSlider(${modeloId},'${valor}','${pagina-1}')">Previous</a></li>
			</c:if>

			<c:forEach var="i" begin="1" end="${total}">

				<c:if test="${i eq pagina}">
					<li class="active"><a href="#"
						onclick="mostrarProductosSlider(${modeloId},'${valor}',${i})">${i}</a></li>

				</c:if>
				<c:if test="${i ne pagina}">
					<li><a href="#"
						onclick="mostrarProductosSlider(${modeloId},'${valor}',${i})">${i}</a></li>

				</c:if>


			</c:forEach>
			<c:if test="${pagina ne total}">
				<li><a href="#"
					onclick="mostrarProductosSlider(${modeloId},'${valor}','${pagina+1}')">Next</a></li>
			</c:if>

		</ul>
	</div>

</c:if>

<section>


	<div id="escenario" class="w3-row-padding">


		<c:forEach items="${listaProducto}" var="producto">


				<div class="w3-third"
					style="margin-bottom: 5px !important; margin-top: 5px !important">

						<div class="w3-card-4"
							style="width: 310px !important; height: 480px !important;">

					<div
						style="margin-left: 14px; width: 100%; height: 55px; color: red; font-size: 25px;text-align: center!important;">
						${producto.productoNombre}</div>


			







					<div id="myCarousel${producto.productoId}" class="carousel slide"
						data-ride="carousel" style="margin-left: 5px; margin-right: 5px">

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<div id="producto1_${producto.productoId}"
								class="item active claseAnchoLargo">
								<a href="#"></a>
								<!--  
							<a href="#" >
								<img    class="imgProducto" 
									alt="Producto:${producto.productoNombre},Precio:S/. ${producto.productoPreVenta}"
									id="producto1_${producto.productoId}"
									style="width:100%;height:auto; ">
									</a>
									-->
								<script>
								cargarImagenModelo("producto1_${producto.productoId}","${producto.productoImagen}");
                                </script>


							</div>
							<c:if test="${not empty  producto.productoImagen1}">
								<div id="producto2_${producto.productoId}"
									class="item claseAnchoLargo">

									<script>
									cargarImagenModelo("producto2_${producto.productoId}","${producto.productoImagen1}");
                                </script>

								</div>
							</c:if>
							<c:if test="${not empty  producto.productoImagen2}">
								<div id="producto3_${producto.productoId}"
									class="item claseAnchoLargo">

									<script>
									cargarImagenModelo("producto3_${producto.productoId}","${producto.productoImagen2}");
                                </script>

								</div>

							</c:if>

							<c:if test="${not empty  producto.productoImagen3}">
								<div id="producto4_${producto.productoId}"
									class="item claseAnchoLargo">

									<script>
									cargarImagenModelo("producto4_${producto.productoId}","${producto.productoImagen3}");
                                </script>

								</div>

							</c:if>

							<c:if test="${not empty  producto.productoImagen4}">
								<div id="producto5_${producto.productoId}"
									class="item claseAnchoLargo">

									<script>
									cargarImagenModelo("producto5_${producto.productoId}","${producto.productoImagen4}");
                                </script>

								</div>

							</c:if>

						</div>

						<!-- Left and right controls -->
						<a class="left carousel-control"
							style="background-image: linear-gradient(to right, rgba(0, 0, 0, 0) 0, rgba(0, 0, 0, 0) 100%) !important"
							href="#myCarousel${producto.productoId}" data-slide="prev"> <span
							class="glyphicon glyphicon-chevron-left"
							style="top: 100% !important; background: #75A53F !important"></span>
							<span class="sr-only">Previous</span>
						</a> <a class="right carousel-control"
							style="background-image: linear-gradient(to right, rgba(0, 0, 0, 0) 0, rgba(0, 0, 0, 0) 100%) !important"
							href="#myCarousel${producto.productoId}" data-slide="next"> <span
							class="glyphicon glyphicon-chevron-right"
							style="top: 100% !important; left: 25% !important; background: #75A53F !important"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
					<c:if test="${producto.tamanioGrande}">
						<div
							style="margin-left: 13%; margin-right: 13%; text-align: center; font-size: 12px !important; color: red !important;">${producto.productoDescripcion}</div>
					</c:if>
					<c:if test="${!producto.tamanioGrande}">
						<div
							style="margin-left: 13%; margin-right: 13%; text-align: center; font-size: 15px !important; color: red !important;">${producto.productoDescripcion}</div>
					</c:if>


					<div class="w3-container w3-center">
						<p>
						<h1
							style="text-align: center; font-size: 16px !important; color: red !important; text-shadow: 1px 1px 9px #000000 !important; background-color: transparent !important;">PRECIO REFERENCIAL POR M²:
							${producto.productoPreVenta}</h1>
<!--  
						<p>
							<input class="btn-agregar" type="button" value="Add to Cart"
								onclick="agregarCarrito(${producto.productoId},1)">
						</p>
-->
						<p>&nbsp;</p>


					</div>




				</div>
			</div>



			<div id="myModal_${producto.productoId}" class="modalProducto">

				<!-- The Close Button -->
				<span class="closeProducto"
					onclick="document.getElementById('myModal_${producto.productoId}').style.display='none'">&times;</span>

				<!-- Modal Content (The Image) -->
				<img class="modalProducto-content"
					id="img_producto_${producto.productoId}">

				<!-- Modal Caption (Image Text) -->
				<div class="captionText" id="caption_${producto.productoId}"></div>
			</div>
			<style>
#producto_







 







${
producto







 







.productoId








	
				







}
:hover {
	opacity: 0.7;
}
</style>

			<script>
						// Get the modal
						var modal = document.getElementById('myModal_${producto.productoId}');
						
						// Get the image and insert it inside the modal - use its "alt" text as a caption
						var img1 = document.getElementById("producto1_${producto.productoId}");
						var modalImg = document.getElementById("img_producto_${producto.productoId}");
						var captionText = document.getElementById("caption_${producto.productoId}");
					
						img1.onclick = function(){
						
						    modal.style.display = "block";
						 var lUrl  = gPath+'/productoController/cargarImagen'+'?imagen=${producto.productoImagen}'  ;
					     modalImg.src = lUrl;
						//    captionText.innerHTML = this.alt;
						}
						</script>
			<c:if test="${not empty  producto.productoImagen1}">
				<script>					
						var img2 = document.getElementById("producto2_${producto.productoId}");
						var modalImg = document.getElementById("img_producto_${producto.productoId}");
						var captionText = document.getElementById("caption_${producto.productoId}");
					
						img2.onclick = function(){
							
						    modal.style.display = "block";
							 var lUrl  = gPath+'/productoController/cargarImagen'+'?imagen=${producto.productoImagen1}'  ;
						     modalImg.src = lUrl;
						}
						</script>
			</c:if>


			<c:if test="${not empty  producto.productoImagen2}">
				<script>					
						var img3 = document.getElementById("producto3_${producto.productoId}");
						var modalImg = document.getElementById("img_producto_${producto.productoId}");
						var captionText = document.getElementById("caption_${producto.productoId}");
						
						img3.onclick = function(){
							
						    modal.style.display = "block";
							 var lUrl  = gPath+'/productoController/cargarImagen'+'?imagen=${producto.productoImagen2}'  ;
						     modalImg.src = lUrl;
						}
						</script>
			</c:if>



			<c:if test="${not empty  producto.productoImagen3}">
				<script>					
						var img4 = document.getElementById("producto4_${producto.productoId}");
						var modalImg = document.getElementById("img_producto_${producto.productoId}");
						var captionText = document.getElementById("caption_${producto.productoId}");
						
						img4.onclick = function(){
							
						    modal.style.display = "block";
							 var lUrl  = gPath+'/productoController/cargarImagen'+'?imagen=${producto.productoImagen3}'  ;
						     modalImg.src = lUrl;
						}
						</script>
			</c:if>


			<c:if test="${not empty  producto.productoImagen4}">
				<script>					
						var img5 = document.getElementById("producto5_${producto.productoId}");
						var modalImg = document.getElementById("img_producto_${producto.productoId}");
						var captionText = document.getElementById("caption_${producto.productoId}");
						
						img5.onclick = function(){
							
						    modal.style.display = "block";
							 var lUrl  = gPath+'/productoController/cargarImagen'+'?imagen=${producto.productoImagen4}'  ;
						     modalImg.src = lUrl;
						}
						</script>
			</c:if>





			<script>	
						// Get the <span> element that closes the modal
						var span = document.getElementsByClassName("closeProducto")[0];
						
						// When the user clicks on <span> (x), close the modal
						span.onclick = function() { 
						    modal.style.display = "none";
						}
						
						</script>
		</c:forEach>




	</div>

</section>


<c:if test="${total ne 1}">

	<div style="margin-left: 15px">
		<ul class="pagination">
			<c:if test="${pagina ne 1}">
				<li><a href="#"
					onclick="mostrarProductosSlider(${modeloId},'${valor}','${pagina-1}')">Previous</a></li>
			</c:if>

			<c:forEach var="i" begin="1" end="${total}">

				<c:if test="${i eq pagina}">
					<li class="active"><a href="#"
						onclick="mostrarProductosSlider(${modeloId},'${valor}',${i})">${i}</a></li>

				</c:if>
				<c:if test="${i ne pagina}">
					<li><a href="#"
						onclick="mostrarProductosSlider(${modeloId},'${valor}',${i})">${i}</a></li>

				</c:if>


			</c:forEach>
			<c:if test="${pagina ne total}">
				<li><a href="#"
					onclick="mostrarProductosSlider(${modeloId},'${valor}','${pagina+1}')">Next</a></li>
			</c:if>

		</ul>
	</div>

</c:if>






