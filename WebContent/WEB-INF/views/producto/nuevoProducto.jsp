<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html>

<head>

	
			 <script type="text/javascript" src="${gPath}/resources/js/producto/nuevoProducto.js"></script>
	
<c:url value='${gPath}/productoController/guardar'
	var="urlNuevoProductoGuardar" />

</head>
<body>

	<div id="content">

		<div class="bs-group bs-group-no-margin">
			<div class="parrafo"></div>

			<div class="col-xs-3">
				<div class="txt_descr">
					<label class="control-label">Imagenes(500x500) <span
						class="label-alert"> (*)</span></label>
				</div>
			</div>
			<div class="col-xs-9"></div>

			<div class="parrafo"></div>
			<div class="parrafo"></div>

			<div class="col-xs-2">
				<div>
					<img id="guardarProductoImg0" style="width: 45px; height: 50px">
				</div>
				<div id="divEliminarProducto0">
					<a onclick="eliminarImagen(0)">Eliminar</a>
				</div>
			</div>

			<div class="col-xs-2">
				<div>
					<img id="guardarProductoImg1" style="width: 45px; height: 50px">
				</div>
				<div id="divEliminarProducto1">
					<a onclick="eliminarImagen(1)">Eliminar</a>
				</div>
			</div>


			<div class="col-xs-2">
				<div>
					<img id="guardarProductoImg2" style="width: 45px; height: 50px">
				</div>
				<div id="divEliminarProducto2">
					<a onclick="eliminarImagen(2)">Eliminar</a>
				</div>
			</div>


			<div class="col-xs-2">
				<div>
					<img id="guardarProductoImg3" style="width: 45px; height: 50px">
				</div>
				<div id="divEliminarProducto3">
					<a onclick="eliminarImagen(3)">Eliminar</a>
				</div>
			</div>


			<div class="col-xs-2">
				<div>
					<img id="guardarProductoImg4" style="width: 45px; height: 50px">
				</div>
				<div id="divEliminarProducto4">
					<a onclick="eliminarImagen(4)">Eliminar</a>
				</div>
			</div>



			<div class="parrafo"></div>


			<div class="col-xs-12">
				<div class="contenedor-cargar-archivo" style="visibility: hidden;">
					<input type="text" id="fileName" style="visibility: hidden;">
				</div>
				<div id="divBotonCargar">

					<input id="fileInputButton" type="button" value="Cargar"
						class="btn-cargar" /> <input type="file" name="file"
						id="fileupload" class="file_input_hidden"
						onchange="	var path = this.value;
							var fileName = path.match(/[^\/\\]+$/);  
						document.getElementById('fileName').value = fileName;
						var tamanio = document.getElementById('fileupload').files[0].size;
						if(tamanio>'<spring:message code="tamanio.imagen.producto"/>'){
						alert('El peso de la imagen es mayor a <spring:message code="tamanio.imagen.producto.kb"/>');
						return false;
						}" />

				</div>
			</div>


			<div class="parrafo"></div>



			<form:form commandName="productoBean" id="formNuevoProducto"
				class="form-horizontal" action="${urlNuevoProductoGuardar}"
				method="POST">

				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Nombre:</label>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="productoNombre" class="form-control vjsrequired"
							style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>
				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Descripcion:</label>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="productoDescripcion"
							class="form-control vjsrequired" style="width:80%" />
					</div>
				</div>
				<div class="parrafo"></div>

				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">PRECIO:</label>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="productoPreVenta"
							class="form-control vjsrequired vjsdecimal" style="width:80%" />
					</div>
				</div>



				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Tipo</label>
					</div>
				</div>

				<div class="col-xs-9">
					<div class="txt_tipo_dato" style="width: 100%">

						<form:input path="productoTipo" class="form-control vjsrequired"
							style="width:80%" value="PRODUCTO" readonly="true" />
					</div>
				</div>

				<form:hidden path="productoOrden" />

				<form:hidden path="modeloBean.modeloId"
					value="${modeloBean.modeloId}" />


				<div class="parrafo"></div>
				<!--  
				<div class="col-xs-6">Especificaciones:</div>
				<div class="col-xs-6">
					<input class="btn btn-default" type="button" value="AGREGAR"
						onclick="agregarEspecificacion()">
				</div>
				-->
				
				
<!--  
				<div id="content"
					style="width: 100%; height: 220px; overflow: scroll;">

					<div class="bs-group bs-group-no-margin">

						<div class="parrafo"></div>
						<c:forEach var="i" begin="1" end="35">


							<div class="parrafo"></div>
							<div id="especificacion${i}">
								<div class="col-xs-3">
									<div class="txt_descr">
										<label class="control-label"> <form:select
												class="dropdown" path="productoEspecificacion${i}"
												style="width:80%">

												<form:options items="${listaEspecificacion}"
													itemValue="productoEspecificacionId"
													itemLabel="productoEspecificacionNombre" />

											</form:select>
										</label>
									</div>
								</div>
								<div class="col-xs-8">
									<div class="txt_descr_dato" style="width: 100%">
										<form:input path="productoEspecificacionValor${i}"
											class="form-control " style="width:80%" />
									</div>
								</div>

								<div class="col-xs-1">
									<a><img onclick="eliminarEspecificacion(${i})"
										src="${gPath}/resources/img/eliminar.png"></a>
								</div>

							</div>
							<div class="parrafo"></div>

							<script>	$('#especificacion${i}').hide();</script>
						</c:forEach>

					</div>
				</div>
 
-->


				<div style="float: right; margin-right: 15px">
					<input class="btn btn-grabar" type="submit" value="GUARDAR"
						id="btnNuevoProductoGuardar"> <input
						class="btn btn-default" type="button" value="CANCELAR"
						id="btnNuevoProductoCancelar">
				</div>
				<div style="text-align: center;">
					<p class="mensajeError" id="mensajeErrorProducto"
						style="visibility: hidden;">
						<label class="mensajeErrorFijo"><spring:message
								code="mensaje.error.generico" /></label>
					</p>
				</div>

				<div class="parrafo"></div>
			</form:form>
		</div>

	</div>
</body>
<script>

var alementosEspecificacion=1;
	$(document).ready(
			function() {
				alementosEspecificacion=1;
				reiniciarImagenes();
				configurarNuevoProducto();

				var urlInsertarAdjunto = gPath
						+ "/productoController/insertarAdjunto";

				$('#fileupload').fileupload({
					url : urlInsertarAdjunto,
					dataType : 'json',
					success : function(data) {
						var  tamanio= data.length;
						$('#divBotonCargar').show();
						if(eval(tamanio)==5){
							$('#divBotonCargar').hide();
						}
						reiniciarImagenes();
						cargarImagenes(data);
					},  error: function (e, data) {
			           alert("Error en carga de archivo");
			        }
				}).prop('disabled', !$.support.fileInput).parent().addClass(
						$.support.fileInput ? undefined : 'disabled');

			});

	function cargarImagenes(data) {
	
		$.each(data, function(i, item) {
			
			cargarImagenProducto("guardarProductoImg" + i, item.nombre);
			$('#divEliminarProducto'+i).show();
			
		});
		
	}
	
	function eliminarImagen(numeroImagen){
		
		$.ajax({
			url : gPath+'/productoController/eliminarImagen?numeroImagen='
			+ numeroImagen,
			type : 'get',
			dataType : 'json',
			success : function(data) {
				$('#divBotonCargar').show();
				reiniciarImagenes();
				cargarImagenes(data);
			}
		});
	}
	
	
	function reiniciarImagenes(){

		
		
		$('#guardarProductoImg0').attr("src", gPath+"/resources/img/default.jpg");
		$('#guardarProductoImg1').attr("src", gPath+"/resources/img/default.jpg");
		$('#guardarProductoImg2').attr("src", gPath+"/resources/img/default.jpg");
		$('#guardarProductoImg3').attr("src", gPath+"/resources/img/default.jpg");
		$('#guardarProductoImg4').attr("src", gPath+"/resources/img/default.jpg");
		
		
	
		$('#divEliminarProducto0').hide();
		$('#divEliminarProducto1').hide();
		$('#divEliminarProducto2').hide();
		$('#divEliminarProducto3').hide();
		$('#divEliminarProducto4').hide();
	
	}
	
	
	
	function agregarEspecificacion(){
		
		if(alementosEspecificacion>34){
			alert("No se pueden agregar mas especificaciones");
		}else{
		
		if(alementosEspecificacion==1){
		$('#especificacion'+alementosEspecificacion).show();
		alementosEspecificacion++;
		}else{
			if($('#productoEspecificacion'+(eval(alementosEspecificacion)-1)).val()==""){
				alert("Falta seleccionar el tipo de especificacion");
			}else	if($('#productoEspecificacionValor'+(eval(alementosEspecificacion)-1)).val()==""){
				alert("Falta agregar datos de especificacion");
			}else{
				$('#especificacion'+alementosEspecificacion).show();
				alementosEspecificacion++;	
			}
		
		}
		}
	}
	
	
	function eliminarEspecificacion(codigo){
		$('#especificacion'+codigo).hide();
		$('#productoEspecificacion'+codigo).val(null);
		$('#productoEspecificacionValor'+codigo).val(null);
		alementosEspecificacion--;
	}
	

	
</script>
</html>