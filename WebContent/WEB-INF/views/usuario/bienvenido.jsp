<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>

<head>
<button type="button" class="btn btn-info btn-lg" id="btnMyModalBienvenido"
	data-toggle="modal" style="visibility: hidden;" data-target="#myModal">Open
	Modal</button>

<c:url value='${gPath}/usuarioController/recuperarClave'
	var="urlRecuperarClave" />
<title>Insert title here</title>
</head>
<form:form commandName="usuario" id="formRecuperarClave"
	action="${urlRecuperarClave}" method="post">
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Bienvenido ${usuario.usuarioNombre}</h4>
				</div>


				<div class="modal-body">

					<div class="content">

					
					

					</div>

				</div>



			</div>
		</div>
	</div>
</form:form>
<script>
	$(document).ready(function() {
		$("#btnMyModalBienvenido").click();
	
	});


</script>
</html>