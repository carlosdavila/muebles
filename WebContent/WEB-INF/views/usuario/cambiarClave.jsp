<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>


<head>
<button type="button" class="btn btn-info btn-lg" id="btnMyModalCambiarClave"
	data-toggle="modal" style="visibility: hidden;" data-target="#myModal">Open
	Modal</button>

<c:url value='${gPath}/usuarioController/cambiarClave'
	var="urlCambiarClaveGuardar" />
<title>Insert title here</title>
</head>
<form:form commandName="usuario" id="formCambiarClave"
	action="${urlCambiarClaveGuardar}" method="post">
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Cambiar Clave</h4>
				</div>


				<div class="modal-body">

					<div class="content">


						<div class="col-xs-3">

							<div class="txt_descr">
								<label class="control-label">Nueva Clave:</label>
							</div>

						</div>
						<div class="col-xs-9">
							<div class="txt_descr_dato" style="width: 100%">
								<form:password path="usuarioClaveNueva"
									class="form-control vjsrequired" style="width:80%" />
							</div>
						</div>


						<div class="parrafo"></div>
						<div class="col-xs-3">

							<div class="txt_descr">
								<label class="control-label">Repetir Clave:</label>
							</div>

						</div>
						<div class="col-xs-9">
							<div class="txt_descr_dato" style="width: 100%">
								<input type="password" id="txtRepetirClave"
									class="form-control vjsrequired">
							</div>
						</div>

						<div class="modal-footer">

							<div style="float: right; margin-right: 15px">
								<input class="btn btn-grabar" type="submit" value="GUARDAR"
									id="btnNuevoUsuarioGuardar"> 
							</div>


						</div>
						<div class="modal-footer">
							<div class="alert alert-success" id="successCambiarClave">
								Se restablecio la clave satisfactoriamente.</div>
							<div class="alert alert-danger" id="errorClave"
								style="text-align: center;">La clave no coincide.</div>
								<div class="alert alert-danger" id="errorCambiarClave"
								style="text-align: center;">Sucedio un error genérico.</div>
						</div>

					</div>

				</div>



			</div>
		</div>
	</div>
</form:form>
<script>
	$(document).ready(function() {
		$("#errorClave").hide();
		$("#successCambiarClave").hide();
		$("#errorCambiarClave").hide();
		vincularValidacionEstilosJs("formCambiarClave", guardarCambiarClave);
		$("#btnMyModalCambiarClave").click();
	});

	function guardarCambiarClave() {
		$("#errorClave").hide();
		$("#successCambiarClave").hide();
		if ($("#txtRepetirClave").val() == $("#usuarioClaveNueva").val()) {

			var form = $('#formCambiarClave');
			$.ajax({
				type : form.attr('method'),
				url : form.attr('action'),
				data : form.serialize(),
				success : function(data) {
					$("#successCambiarClave").show();
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {

					$("#errorCambiarClave").show();
				}
			});

		} else {
			$("#errorClave").show();
			$("#txtRepetirClave").val("");
			$("#usuarioClaveNueva").val("");
		}
	}
</script>
</html>