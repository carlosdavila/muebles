<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<c:url value='${gPath}/usuarioController/guardarClave'
	var="urlIngresarClave" />
<title>Insert title here</title>
</head>
<body>
	<h2 class="ico_tit_formulario">Ingresar Clave</h2>
	<div id="content">
		<form:form commandName="usuarioBean" id="formIngresarClave"
			class="form-horizontal" action="${urlIngresarClave}"
			method="POST">

			<div class="bs-group bs-group-no-margin">

				<div class="col-xs-3">
					<div class="txt_descr">
						<label class="control-label">Nueva Clave:</label>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
						<form:input path="usuarioClave" class="form-control vjsrequired"
							style="width:80%" />
					</div>
				</div>
				
				<div class="parrafo"></div>
				<div class="col-xs-3">

					<div class="txt_descr">
						<label class="control-label">Repetir Clave:</label>
					</div>

				</div>
				<div class="col-xs-9">
					<div class="txt_descr_dato" style="width: 100%">
					<input type= "text" style="width:80%" id="inputRepetirClave">
					
					</div>
				</div>
				<div class="parrafo"></div>
				
            <p class="mensajeErrorFijo"> 
				<label class="mensajeErrorFijo">${mensajeError}</label>
			</p>
				<div style="float: right; margin-right: 15px">
					<input class="btn btn-grabar" type="submit" value="GUARDAR"
						id="btnIngresarClaveGuardar"> <input
						class="btn btn-default" type="button" value="CANCELAR"
						id="btnIngresarClaveCancelar">
				</div>
				<div class="parrafo"></div>
			</div>
		</form:form>
	</div>
</body>
<script>
	$(document).ready(function() {
		vincularValidacionEstilosJs("formIngresarClave",
				validarIngresarClave);
	});
	
	function validarIngresarClave(){
		
	}

	function guardarIngresarClave() {
		var form = $('#formIngresarClave');
		$
				.ajax({
					type : form.attr('method'),
					url : form.attr('action'),
					data : form.serialize(),
					success : function(data) {
				
					}
				});
	}
</script>
</html>