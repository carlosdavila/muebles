<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>


<c:url value='${gPath}/usuarioController/validarLogin'
	var="urlValidarlogin" />
</head>
<button type="button" class="btn btn-info btn-lg" id="btnMyModalLogin"
	data-toggle="modal" style="visibility: hidden;" data-target="#myModal">Open
	Modal</button>

<form:form commandName="usuario" method="post" id="formLogin"
	action="${urlValidarlogin}">
	<form:hidden path="usuarioEstado" />

	<!-- Modal -->


	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">LOGGIN</h4>
				</div>
				<div class="modal-body">
					<div class="content">

						<div class="parrafo"></div>
						<div class="col-xs-3">
							<label for="usrnm">Usuario:</label>
						</div>
						<div class="col-xs-3">

							<form:input path="usuarioUser" style="text-transform:unset!important;width:120px!important;" class="form-control"  placeholder="Usuario" required="required" type="text" />
						</div>
						<div class="col-xs-6"></div>
						<div class="parrafo"></div>
						<div class="col-xs-3">
							<label for="pswd">Password:</label>
						</div>
						<div class="col-xs-3">
							<form:input path="usuarioClave" style="text-transform:unset!important;width:120px!important;"  class="form-control" placeholder="Password" required="required"
								type="password" />
						</div>
						<div class="col-xs-6"></div>
						<div class="parrafo"></div>

					</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-default" value="Loggin"
						id="Ingresar">
				  
				</div>
				<div class="modal-footer">
				<!-- 
				 <a  onclick="cerrarPopupAntesdeCrearUsuario()" style="text-decoration: underline;">Nuevo Usuario</a>
				 <a onclick="cerrarPopupAntesdeRecuperarClave()" style="text-decoration: underline;">Recuperar Clave</a>
			    -->
				  <div class="alert alert-danger" id="errorGenericoLogin"
						style="text-align: center;">
						Error genérico.
					</div>
					<div class="alert alert-danger" id="errorUsuarioLogin"
						style="text-align: center;">
						Usuario o clave incorrecta
					</div>
					<div class="alert alert-danger" id="errorUsuarioDesactivo"
						style="text-align: center;">
						Usuario desactivado
					</div>
				</div>
			</div>

		</div>
	</div>




</form:form>
<script>
	$(document).ready(function() {
		$("#errorGenericoLogin").hide();
		$("#errorUsuarioLogin").hide();
		$("#errorUsuarioDesactivo").hide();

		$("#btnMyModalLogin").click();

		vincularValidacionEstilosJs("formLogin", login);

	});

	function login() {
		var form = $('#formLogin');
		$.ajax({
			type : form.attr('method'),
			url : gPath+'/usuarioController/validarLogin',
			data : form.serialize(),
			success : function(data) {
				if (data == 1) {
					$('#myModal').modal('hide');
					$('#formSalirSistema').attr('action', gPath);
					$("#formSalirSistema").submit();
				} else if (data == 2) {
					$("#errorUsuarioLogin").show();
				} else if (data == 0) {
					$("#errorUsuarioDesactivo").show();
				}

			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {

				$("#errorGenericoLogin").show();
			}
		});
	}

	function inicio() {
		$('#myModal').modal('hide');
		//var formInicio = $('#idFormInicio');
		//formInicio.submit();
		setTimeout(mostrarMenuPrincipal,400);
		//setTimeout(mostrarPaginaImagen,50);
	
		
	}
	
	function cerrarPopupAntesdeCrearUsuario(){
		$('#myModal').modal('hide');
		setTimeout(loginCrearUsuario,400);
	}
	
	function loginCrearUsuario(){
		
		var urlBandeja = '/usuarioController/mostrarNuevoUsuario';
		$("#idPopup").load( gPath+urlBandeja);
	}
	
	
	function cerrarPopupAntesdeRecuperarClave(){
		$('#myModal').modal('hide');
		setTimeout(mostrarRecuperarClave,400);
	}
	function mostrarRecuperarClave(){
		
		var urlBandeja = '/usuarioController/mostrarRecuperarClave';
		$("#idPopup").load( gPath+urlBandeja);
	}
	
	
</script>