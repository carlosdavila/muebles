<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>


<c:url value='${gPath}/usuarioController/validarLogin'
	var="urlValidarlogin" />
</head>

<form:form commandName="usuario" method="post"
	action="${urlValidarlogin}">

	<div class="content">


		<h3>Login</h3>
	<div class="parrafo"></div>
		<div class="col-xs-3">
			<label for="usrnm">E-mail:</label>
		</div>
		<div class="col-xs-3">

			<form:input path="usuarioUser" required="required" type="text"
				placeholder="E-mail" />
		</div>
		<div class="col-xs-6"></div>
			<div class="parrafo"></div>
		<div class="col-xs-3">
			<label for="pswd">Password:</label>
		</div>
		<div class="col-xs-3">
			<form:input path="usuarioClave" required="required" type="password"
				placeholder="Clave" />
		</div>
		<div class="col-xs-6"></div>
			<div class="parrafo"></div>
		<div class="col-xs-3"></div>
		<div class="col-xs-3">
			<input type="submit" value="Log in">
		</div>
		<div class="col-xs-6"></div>
	</div>



</form:form>