<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>

<head>
<button type="button" class="btn btn-info btn-lg" id="btnMyModalNuevoUsuario"
	data-toggle="modal" style="visibility: hidden;" data-target="#myModal">Open
	Modal</button>

<c:url value='${gPath}/usuarioController/guardar'
	var="urlNuevoUsuarioGuardar" />
<title>Insert title here</title>
</head>
<form:form commandName="usuario" id="formNuevoUsuario"
	action="${urlNuevoUsuarioGuardar}" method="post">
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Nuevo Usuario</h4>
				</div>


				<div class="modal-body">

					<div class="content">


						<div class="col-xs-3">
							<div class="txt_descr">
								<label class="control-label">Nombre:</label>
							</div>
						</div>
						<div class="col-xs-9">
							<div class="txt_descr_dato" style="width: 100%">
								<form:input path="usuarioNombre"
									class="form-control vjsrequired" style="width:80%" />
							</div>
						</div>

						<div class="parrafo"></div>
						<div class="col-xs-3">

							<div class="txt_descr">
								<label class="control-label">Email:</label>
							</div>

						</div>
						<div class="col-xs-9">
							<div class="txt_descr_dato" style="width: 100%">
								<form:input path="usuarioUser" class="form-control vjsrequired"
									style="width:80%" />
							</div>
						</div>


						<div class="parrafo"></div>
						<div class="col-xs-3">

							<div class="txt_descr">
								<label class="control-label">Clave:</label>
							</div>

						</div>
						<div class="col-xs-9">
							<div class="txt_descr_dato" style="width: 100%">
								<form:password path="usuarioClave"
									class="form-control vjsrequired" style="width:80%" />
							</div>
						</div>


						<div class="parrafo"></div>
						<div class="col-xs-3">

							<div class="txt_descr">
								<label class="control-label">Repetir Clave:</label>
							</div>

						</div>
						<div class="col-xs-9">
							<div class="txt_descr_dato" style="width: 100%">
								<input type="password" id="txtRepetirClave"
									class="form-control vjsrequired">
							</div>
						</div>

						<div class="modal-footer">

							<div style="float: right; margin-right: 15px">
								<input class="btn btn-grabar" type="submit" value="GUARDAR"
									id="btnNuevoUsuarioGuardar"> 
							</div>


						</div>
						<div class="modal-footer">
							<div class="alert alert-success" id="successNuevoUsuario">
								Se envio un Email,para activar su cuenta satisfactoriamente.</div>
							<div class="alert alert-danger" id="errorClave"
								style="text-align: center;">La clave no coincide.</div>
								<div class="alert alert-danger" id="errorNuevoUsuario"
								style="text-align: center;">Sucedio un error genérico.</div>
									<div class="alert alert-danger" id="errorUsuarioExiste"
								style="text-align: center;">Usuario ya existente.</div>
						</div>

					</div>

				</div>



			</div>
		</div>
	</div>
</form:form>
<script>
	$(document).ready(function() {
		$("#errorClave").hide();
		$("#successNuevoUsuario").hide();
		$("#errorUsuarioExiste").hide();
		$("#errorNuevoUsuario").hide();
		vincularValidacionEstilosJs("formNuevoUsuario", guardarNuevoUsuario);
		$("#btnMyModalNuevoUsuario").click();
	});

	function guardarNuevoUsuario() {
		$("#errorClave").hide();
		$("#errorUsuarioExiste").hide();
		$("#successNuevoUsuario").hide();
		if ($("#txtRepetirClave").val() == $("#usuarioClave").val()) {

			var form = $('#formNuevoUsuario');
			$.ajax({
				type : form.attr('method'),
				url : form.attr('action'),
				data : form.serialize(),
				success : function(data) {
					if(data=="0"){
					$("#successNuevoUsuario").show();
					}else{
						$("#errorUsuarioExiste").show();
					}
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {

					$("#errorNuevoUsuario").show();
				}
			});

		} else {
			$("#errorClave").show();
			$("#txtRepetirClave").val("");
			$("#usuarioClave").val("");
		}
	}
</script>
</html>