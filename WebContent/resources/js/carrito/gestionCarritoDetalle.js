function configurarGestionCarritoDetalle() {
	
	
	$("#errorCarritoVacio").hide();
	$("#errorGenericoCarrito").hide();
	$("#successCarritoCompra").hide();

	$("#btnCarritoGuardar").click(function() {
		guardarCarritoDetalle();
	});

	$('#listGestionCarritoDetalle')
			.DataTable(
					{

						"responsive" : true,
						"bPaginate" : false,
						"bFilter" : false,
						"bInfo" : false,
						"bLengthChange" : false,
						"processing" : true,
						"ajax" : {
							"url" : gPath
									+ '/carritoDetalleController/obtenerListaSession',
							"type" : 'GET'
						},
						"columns" : [ {
							'data' : "productoBean.productoNombre"
						}, {
							'data' : "productoBean.productoDescripcion"
						}, {
							'data' : "carritoDetallePrecio"
						}, {
							'data' : "carritoDetalleCantidad"
						}, {
							'data' : "carritoDetalleTotal"
						}, {
							'data' : "carritoDetalleId"
						}

						],
						fixedHeader : {
							header : true,
							footer : true
						},
						"columnDefs" : [
                
								{
									"targets" : 2,
									"data" : null,
									"render" : function(data, type, row) { // Devuelve
																			// el
																			// contenido
																			// personalizado
										return '<span>$'+ row.carritoDetallePrecio+'</span>';
									}
								},
							
								{
									"targets" : 3,
									"data" : null,
									"render" : function(data, type, row) { // Devuelve
																			// el
																			// contenido
																			// personalizado
										// alert(row.carritoDetalleId);
										var cadena = '<select onchange="guardarCantidadCarrito(this.value,'
												+ row.carritoDetalleId + ')">'
										for (var i = 1; i < 31; i++) {
											if (eval(data) == eval(i)) {
												cadena = cadena
														+ '<option selected value="'
														+ i + '">' + i
														+ '</option>';
											} else {
												cadena = cadena
														+ '<option value="' + i
														+ '">' + i
														+ '</option>';
											}

										}
										cadena = cadena + '</select>';

										return cadena;
									}
								},
								{
									"targets" : 4,
									"data" : null,
									"render" : function(data, type, row) { // Devuelve
																			// el
																			// contenido
																			// personalizado
										return '<span>$'+ row.carritoDetalleTotal+'</span>';
									}
								},
								{
									"targets" : 5,
									"data" : null,
									"render" : function(data, type, full) { // Devuelve
																			// el
																			// contenido
																			// personalizado
										return '<a onclick="eliminarEnGestionCarritoDetalle('
												+ data
												+ ')"><img src="'
												+ gPath
												+ '/resources/img/eliminar.png"></a>';
									}
								} ],

						"footerCallback" : function(row, data, start, end,
								display) {

							var api = this.api(), data;

							// Remove the formatting to get integer data for
							// summation
							var intVal = function(i) {
								return typeof i === 'string' ? i.replace(
										/[\$,]/g, '') * 1
										: typeof i === 'number' ? i : 0;
							};

							// Total over all pages
							
							total = api.column(4).data().reduce(function(a, b) {
								return intVal(a) + intVal(b);
							}, 0);
							
							total = Math.round(total * 100) / 100;
						
							$(api.column(0).footer()).html('Total : $'+total);
							$(api.column(1).footer()).html();
							$(api.column(2).footer()).html();
							$(api.column(3).footer()).html();
							$(api.column(4).footer()).html();
							$(api.column(5).footer()).html();
							 $('#spanCarritoTotal').html("$"+ total);
					         $('#spanCarritoTotal1').html("$"+ total);
						}

					});

	//setTimeout(gestionCarritoDetalleCargarImagenes, 1000);
}
function buscarEnGestionCarritoDetalle() {

	var table = $("#listGestionCarritoDetalle").dataTable();

	table.fnReloadAjax();
//	setTimeout(gestionCarritoDetalleCargarImagenes, 200);
}

function eliminarEnGestionCarritoDetalle(carritoDetalleId) {
	$.ajax({
		type : 'post',
		url : gPath + '/carritoDetalleController/eliminar?carritoDetalleId='
				+ carritoDetalleId,
		success : function(mensaje) {
			if (mensaje != "") {
				mostrarMensajeError(mensaje);
			}

			buscarEnGestionCarritoDetalle();
		}
	});
}

function gestionCarritoDetalleCargarImagenes() {

	var table = $('#listGestionCarritoDetalle').DataTable();

	var data = table.rows().data();
	for (var i = 0; i < data.length; i++) {
		var imagen = data[i].productoBean.productoImagen;
		var id = data[i].carritoDetalleId;
		cargarImagenProducto("carritoProducto_" + id, imagen);
	}

}

function guardarCarritoDetalle() {

	var table = $('#listGestionCarritoDetalle').DataTable();
	var data = table.rows().data();
	if (data.length > 0) {
		$.ajax({
			type : 'post',
			url : gPath + '/carritoDetalleController/enviarCorreo',
			success : function(data) {
				buscarEnGestionCarritoDetalle();
				$("#successCarritoCompra").show();

			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {

				$("#errorGenericoCarrito").show();
			}
		});
	} else {
		$("#errorCarritoVacio").show();
	}
}

function guardarCantidadCarrito(cantidad, carritoDetalleId) {

	$.ajax({
		type : 'post',
		url : gPath
				+ '/carritoDetalleController/cambiarCantidad?carritoDetalleId='
				+ carritoDetalleId + '&cantidad=' + cantidad,
		success : function(mensaje) {
			buscarEnGestionCarritoDetalle();
		}
	});

}
