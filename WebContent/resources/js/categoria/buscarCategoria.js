function configurarBuscarCategoria(paginado) {

	vincularValidacionEstilosJs("formBuscarCategoria", buscarEnBuscarCliente);

	$("#listBuscarCategoria")
			.jqGrid(
					{
						mtype : "GET",
						datatype : "json",
						url : gPath + '/categoriaController/obtenerListaFiltrada',
						height : 300,
						rowNum : 180,
						colNames : [ 'Id', 'NOMBRE', 'TIPO', 'PADRE',
								'ORDEN','ACCION'],
						colModel : [
								{
									name : 'categoriaId',
									index : 'categoriad',
									width : 70,
									sorttype : "int",
									editable : false,
									hidden : true,
									sortable : false
								},
								{
									name : 'categoriaNombre',
									index : 'categoriaNombre',
									width : 150,
									editable : false,
									sortable : true
								},
								{
									name : 'categoriaTipo',
									index : 'categoriaTipo',
									width : 90,
									editable : false,
									hidden : false,
									sortable : true
								},

								{
									name : 'categoriaPadre',
									index : 'categoriaPadre',
									width : 100,
									editable : false,
									hidden : false,
									sortable : true
								},

								{
									name : 'categoriaOrden',
									index : 'categoriaOrden',
									width : 150,
									editable : false,
									hidden : true,
									sortable : true
								},
							
								{
									name : '',
									index : '',
									width : 80,
									editable : true,
									formatter : function radio(cellValue,
											option, rowObject) {
										var cadena = "";

										cadena = '<input type="radio" onclick="name="radio" id="checkAgregarCliente_('
												+ option.rowId
												+ ')" name="radio" id="checkAgregarCliente_'

												+ '" />';

										return cadena;

									}
								}

						],
						pager : "#plistGestionCategoria",
						rowNum : paginado,
						viewrecords : true,
						grouping : false,
						gridComplete : function() {
						},
						caption : '',
						beforeRequest : function() {
							responsive_jqgrid($(".jqGrid"));
						}
					});

	$("#listBuscarCategoria").jqGrid('navGrid', "#plistBuscarCategoria", {
		edit : false,
		add : false,
		del : false,
		search : false,
		refresh : false
	});
}

function buscarEnBuscarCategoria() {
	$("#listBuscarCliente").jqGrid('clearGridData');
	$("#listBuscarCliente").jqGrid(
			'setGridParam',
			{
				url : gPath + '/categoriaController/obtenerListaFiltrada'
						+ '?valor=' + $("#txtValorBuscarCategoria").val()
						+ '&columna=' + $("#cboColumnaBuscarCategoria").val()
			});
	$("#listBuscarCategoria").trigger('reloadGrid');

}

