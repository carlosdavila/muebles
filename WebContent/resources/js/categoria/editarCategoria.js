function configurarEditarCategoria() {

	
	vincularValidacionEstilosJs("formEditarCategoria",
			validarGuardarEditarCategoria);



	$("#btnEditarCategoriaCancelar").click(function() {
		$("#divDialogEditarCategoria").dialog("close");
	});
}


function guardarEditarCategoria() {
	
	var form = $('#formEditarCategoria');
	$.ajax({
		type : form.attr('method'),
		url : form.attr('action'),
		data : form.serialize(),
		success : function(data) {
			
			$("#divDialogEditarCategoria").dialog("close");
			buscarEnGestionCategoria();
		},
		   error: function(xhr, status, error){
			   document.getElementById("mensajeErrorCategoria").style.visibility = 'visible';
	        }
	});
}

function validarGuardarEditarCategoria() {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Edicion registro de grupo');
	aceptar$.click(function() {
		guardarEditarCategoria();
	});
}


