
var idsCategoriaInicial ;

var idsModeloInicial ;
function configurarGestionCategoria(paginado) {



	    
	$("#listGestionModelo")
	.jqGrid(
			{
				mtype : "GET",
				datatype : "json",
				url : gPath + '/modeloController/obtenerListaFiltrada' ,
				height : 170,
				rowNum : 180,


										colNames : [ 'Id', 'CATEGORIA','MODELO',
												'TIPO','ORDEN',
												'ACCIONES' ],
										colModel : [
												{
													name : 'modeloId',
													index : 'modeloId',
													width : 0,
													sorttype : "int",
													editable : false,
													hidden : true,
													sortable : false
												},
												{
													name : 'categoriaBean.categoriaNombre',
													index : 'categoriaBean.categoriaNombre',
													width : 250,
													editable : false,
													sortable : false,
													hidden : true
												},
												{
													name : 'modeloNombre',
													index : 'modeloNombre',
													width : 250,
													editable : false,
													sortable : false
												},
												{
													name : 'modeloTipo',
													index : 'modeloTipo',
													width : 120,
													editable : false,
													sortable : false,
													hidden:true
												},
											
										
												{
													name : 'modeloOrden',
													index : 'modeloOrden',
													width : 0,
													editable : false,
													sortable : false,
													hidden:true
												},
												{
													name : '',
													index : '',
													width : 150,
													editable : true,
													formatter : function radio(
															cellValue,
															option,
															rowObject) {

														return '<a  title="Editar Modelo" onclick="mostrarEditarEnGestionModelo('
																+ rowObject.modeloId
																+ ')"><span class="glyphicon glyphicon-pencil"></span></a>'
																+'<span>   </span>'
																+ '<a   title="Eliminar Modelo"  onclick="validarEliminarEnGestionModelo('
																+ rowObject.modeloId+ ')"><span class="glyphicon glyphicon-remove"></span></a>'
																+'<span>   </span>'
																+ '<a   title="Nuevo Producto"  onclick="mostrarNuevoEnGestionProducto('
																+ rowObject.modeloId
																+ ')"><span class="glyphicon glyphicon-file"></span></a>'
																;

													}
												}

										],

										rowNum : 300,
										viewrecords : false,
										grouping : false,
										gridComplete : function() {
											//idsModeloInicial = jQuery("#" + subgrid_table_id).jqGrid('getDataIDs');
										},
										caption : '',
										beforeRequest : function() {
											responsive_jqgrid($(".jqGrid"));
										},
										subGrid : true,
										subGridRowExpanded : function(subgrid_id, row_id) {
										   // var tableModelo= "#tableModelo_"+categoriaId + "_t";
											var myGrid = $("#listGestionModelo");
											
											var modeloId = myGrid.jqGrid('getCell', row_id,
													'modeloId');
                                           
											var subgrid_table_id;
											subgrid_table_id = "tableProducto_"+modeloId + "_t";
											
											$("#" + subgrid_id).html(
													"<table id='" + subgrid_table_id
															+ "' class='scroll'></table>");
											$("#" + subgrid_table_id)
													.jqGrid(
															{

																mtype : "GET",
																datatype : "json",
						                                        url : gPath
																		+ '/productoController/obtenerListaFiltrada?modeloId='+modeloId,
																height : 200,
																rowNum : 300,

																colNames : [ 'Id', 'PRODUCTO',
																		'TIPO','MODELOID','ORDEN',
																		'ACCIONES' ],
																colModel : [
																		{
																			name : 'productoId',
																			index : 'productoId',
																			width : 0,
																			sorttype : "int",
																			editable : false,
																			hidden : true,
																			sortable : false
																		},
																		{
																			name : 'productoNombre',
																			index : 'productoNombre',
																			width : 300,
																			editable : false,
																			sortable : false
																		},
																		{
																			name : 'productoTipo',
																			index : 'productoTipo',
																			width : 90,
																			editable : false,
																			sortable : false,
																			hidden:true
																		},
																	
																		{
																			name : 'modeloBean.modeloId',
																			index : 'modeloBean.modeloId',
																			width : 0,
																			editable : false,
																			sortable : false,
																			hidden:true
																		},
																		{
																			name : 'productoOrden',
																			index : 'productoOrden',
																			width : 0,
																			editable : false,
																			sortable : false,
																			hidden:true
																		},
																		{
																			name : '',
																			index : '',
																			width : 150,
																			editable : true,
																			formatter : function radio(
																					cellValue,
																					option,
																					rowObject) {

																				return '<a    title="Editar Producto"  onclick="mostrarEditarEnGestionProducto('
																						+ rowObject.productoId
																						+ ')"><span class="glyphicon glyphicon-pencil"></span></a>'
																						+'<span>   </span>'
																						+ '<a    title="Eliminar Producto"  onclick="validarEliminarEnGestionProducto('
																						+ rowObject.productoId+","+rowObject.modeloBean.modeloId
																						+ ')"><span class="glyphicon glyphicon-remove"></span></a>'
																					
																						;

																			}
																		}

																],

																rowNum : 300,
																viewrecords : false,
																grouping : false,
																gridComplete : function() {

																}

															});

										//	jQuery("#" + subgrid_table_id).sortableRows(); 
										//	jQuery("#" + subgrid_table_id).jqGrid('gridDnD');
											
								
										}

									});


$("#listGestionModelo").jqGrid('navGrid',
	"#plistGestionModelo", {
		edit : false,
		add : false,
		del : false,
		search : false,
		refresh : false
	});


//jQuery("#listGestionModelo").sortableRows(); 
//jQuery("#listGestionModelo").jqGrid('gridDnD');


$("#listGestionCategoria").droppable({

	   drop: function( evento, ui ) {
		
		  setTimeout(categoriaMoverFilas, 300); 
	   }
	});
 


}


function categoriaMoverFilas() {
	
	var flag=	isDiferenteOrdenCategorias();
  
	if(eval(flag)){

		var idCategorias=	obtenerIdsCategoriasOrdenas();

	   $("#spin").show();
	
	$.ajax({
				type : 'get',
				url : gPath+'/categoriaController/mover?idDiferentes='
						+ idCategorias,
				success : function() {
					   $("#spin").hide();
					buscarEnGestionCategoria();
				}
			});

	}
}



function isDiferenteOrdenCategorias(){
   
    var flag=false;
	var ids = jQuery("#listGestionCategoria").jqGrid('getDataIDs');
		
		for (var i = 0; i < ids.length; i++) 
		{
			 
			  var rowId = ids[i];
			  var rowInicial=idsCategoriaInicial[i];
			  
			  var rowData = jQuery('#listGestionCategoria').jqGrid ('getRowData', rowId);
			  var rowDataInicial = jQuery('#listGestionCategoria').jqGrid ('getRowData', rowInicial);
				if(rowDataInicial.categoriaId!= rowData.categoriaId){
					flag=true;
				}
			
		}

	return flag;
	
}


function obtenerIdsCategoriasOrdenas(){
	
	 var idCategorias= new Array();

    
	var ids = jQuery("#listGestionCategoria").jqGrid('getDataIDs');

		for (var i = 0; i < ids.length; i++) 
		{
			
			  var rowId = ids[i];
			
			  var rowData = jQuery('#listGestionCategoria').jqGrid ('getRowData', rowId);
			
			  idCategorias[i]=rowData.categoriaId;
			
		}
return idCategorias;
}





function buscarEnGestionCategoria() {

	$("#listGestionCategoria").jqGrid('clearGridData');
	$("#listGestionCategoria").jqGrid(
			'setGridParam',
			{
				url :  gPath + '/categoriaController/obtenerListaFiltrada'  
			});
	$("#listGestionCategoria").trigger('reloadGrid');

}

function mostrarNuevoEnGestionCategoria() {

	var lUrl = gPath + '/categoriaController/mostrarNuevaCategoria';
	var titulo = "Nuevo grupo";
	mostrarMensajeConHtmlAltura('divDialogNuevaCategoria', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}





function mostrarEditarEnGestionCategoria(categoriaId) {

	var lUrl = gPath + '/categoriaController/mostrarEditarCategoria?categoriaId='
			+ categoriaId;
	var titulo = "Editar grupo";
	mostrarMensajeConHtmlAltura('divDialogEditarCategoria', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function validarEliminarEnGestionCategoria(categoriaId) {
	
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_ELIMINACION, 'Elimina grupo');
	aceptar$.click(function() {
		eliminarEnGestionCategoria(categoriaId);
	});
};

function eliminarEnGestionCategoria(categoriaId) {
	$
			.ajax({
				type : 'get',
				url : gPath+'/categoriaController/eliminar?categoriaId='
						+ categoriaId,
				success : function(mensaje) {
					
					  if(mensaje!=""){
						  mostrarMensajeError(mensaje);
		                 }
					buscarEnGestionCategoria();
				}
			});
}


