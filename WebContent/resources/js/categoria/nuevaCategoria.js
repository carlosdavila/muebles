function configurarNuevaCategoria() {

	vincularValidacionEstilosJs("formNuevaCategoria",
			validarGuardarNuevaCategoria);


	$("#btnNuevaCategoriaCancelar").click(function() {
		$("#divDialogNuevaCategoria").dialog("close");
	});
}


function validarGuardarNuevaCategoria() {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Guardar registro de grupo');
	aceptar$.click(function() {
		guardarNuevaCategoria();
	});
}


function guardarNuevaCategoria() {
	var form = $('#formNuevaCategoria');
	$
			.ajax({
				type : form.attr('method'),
				url : form.attr('action'),
				data : form.serialize(),
				success : function(data) {
				
					$("#divDialogNuevaCategoria").dialog(
							"close");
					buscarEnGestionCategoria();
				},
			   error: function(xhr, status, error){
				   document.getElementById("mensajeErrorCategoria").style.visibility = 'visible';
				
		        }
			});
}