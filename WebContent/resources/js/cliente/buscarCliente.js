function configurarBuscarCliente(paginado) {

	vincularValidacionEstilosJs("formBuscarCliente", buscarEnBuscarCliente);

	$("#listBuscarCliente")
			.jqGrid(
					{
						mtype : "GET",
						datatype : "json",
						url : gPath + '/clienteController/obtenerListaFiltrada',
						height : 300,
						rowNum : 180,
						colNames : [ 'Id', 'NOMBRE', 'TIPO DOC', 'NRO DOC',
								'TELEFONO', 'DIRECCION', 'ACCION' ],
						colModel : [
								{
									name : 'clienteId',
									index : 'clienteId',
									width : 70,
									sorttype : "int",
									editable : false,
									hidden : true,
									sortable : false
								},
								{
									name : 'clienteNombre',
									index : 'clienteNombre',
									width : 150,
									editable : false,
									sortable : true
								},
								{
									name : 'tipoDocBean.tipoDocDesc',
									index : 'tipoDocBean.tipoDocDesc',
									width : 90,
									editable : false,
									hidden : false,
									sortable : true
								},

								{
									name : 'clienteNroDoc',
									index : 'clienteNroDoc',
									width : 100,
									editable : false,
									hidden : false,
									sortable : true
								},

								{
									name : 'clienteTelefono',
									index : 'clienteTelefono',
									width : 150,
									editable : false,
									hidden : true,
									sortable : true
								},
								{
									name : 'clienteDireccion',
									index : 'clienteDireccion',
									width : 150,
									editable : false,
									sortable : true
								},
								{
									name : '',
									index : '',
									width : 80,
									editable : true,
									formatter : function radio(cellValue,
											option, rowObject) {
										var cadena = "";

										cadena = '<input type="radio" onclick="enviarClienteAproducto('
												+ option.rowId
												+ ')" name="radio" id="checkAgregarCliente_'

												+ '" />';

										return cadena;

									}
								}

						],
						pager : "#plistGestionCliente",
						rowNum : paginado,
						viewrecords : true,
						grouping : false,
						gridComplete : function() {
						},
						caption : '',
						beforeRequest : function() {
							responsive_jqgrid($(".jqGrid"));
						}
					});

	$("#listBuscarCliente").jqGrid('navGrid', "#plistBuscarCliente", {
		edit : false,
		add : false,
		del : false,
		search : false,
		refresh : false
	});
}

function buscarEnBuscarCliente() {
	$("#listBuscarCliente").jqGrid('clearGridData');
	$("#listBuscarCliente").jqGrid(
			'setGridParam',
			{
				url : gPath + '/clienteController/obtenerListaFiltrada'
						+ '?valor=' + $("#txtValorBuscarCliente").val()
						+ '&columna=' + $("#cboColumnaBuscarCliente").val()
			});
	$("#listBuscarCliente").trigger('reloadGrid');

}

function enviarClienteAproducto(rowId) {

	var myGrid = $('#listBuscarCliente');
	var clienteId = myGrid.jqGrid('getCell', rowId, 'clienteId');
    var clienteNombre = myGrid.jqGrid('getCell', rowId, 'clienteNombre');
	var tipoDoc = myGrid.jqGrid('getCell', rowId, 'tipoDocBean.tipoDocDesc');
	var nroDoc = myGrid.jqGrid('getCell', rowId, 'clienteNroDoc');
	var direccion = myGrid.jqGrid('getCell', rowId, 'clienteDireccion');
	var telefono = myGrid.jqGrid('getCell', rowId, 'clienteTelefono');
	$("#txtClienteIdEnPedido").val(clienteId);
	$("#txtClienteNombreEnPedido").val(clienteNombre);
	$("#txtTipoDocEnPedido").val(tipoDoc);
	$("#txtNroDocEnPedido").val(nroDoc);
	$("#txtDireccionEnPedido").val(direccion);
	$("#txtTelefonoEnPedido").val(telefono);
}
