function configurarEditarCliente() {

	changeTipoDocEnGestionCliente();
	vincularValidacionEstilosJs("formEditarCliente",
			validarGuardarEditarCliente);

	$("#cboEditarClienteTipoDoc").change(function() {
		changeTipoDocEnGestionCliente();
	});

	$("#btnEditarClienteCancelar").click(function() {
		$("#divDialogEditarCliente").dialog("close");
	});
}

function changeTipoDocEnGestionCliente() {
	if ($("#cboEditarClienteTipoDoc").val() != "") {
		$("#txtEditarClienteNroDoc").attr("disabled", false);
	} else {
		$("#txtEditarClienteNroDoc").val("");
		$("#txtEditarClienteNroDoc").attr("disabled", true);
	}
}

function guardarEditarCliente() {
	var form = $('#formEditarCliente');
	$.ajax({
		type : form.attr('method'),
		url : form.attr('action'),
		data : form.serialize(),
		success : function(data) {
			$("#divDialogEditarCliente").dialog("close");
			buscarEnGestionCliente();
		}
	});
}

function validarGuardarEditarCliente() {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Edicion Registro de Cliente');
	aceptar$.click(function() {
		guardarEditarCliente();
	});
}


