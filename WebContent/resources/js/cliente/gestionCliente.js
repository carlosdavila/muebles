function configurarGestionCliente(paginado) {

	$('#btnGestionClienteNuevo').click(function(event) {
		mostrarNuevoEnGestionCliente();
	});

	vincularValidacionEstilosJs("formGestionClienteBuscar",
			buscarEnGestionCliente);
	
	$("#listGestionCliente")
	.jqGrid(
			{
				mtype : "GET",
				datatype : "json",
				url : '${urlListaFiltrada}',
				height : 300,
				rowNum : 180,
				
				colNames : [ 'Id', 'NOMBRE',
						'TIPO DOC', 'NRO DOC',
						'TELEFONO', 'EMAIL',
						'DIRECCION', 'ACCIONES' ],
				colModel : [
						{
							name : 'clienteId',
							index : 'clienteId',
							width : 70,
							sorttype : "int",
							editable : false,
							hidden : true,
							sortable : false
						},
						{
							name : 'clienteNombre',
							index : 'clienteNombre',
							width : 220,
							editable : false,
							sortable : true
						},
						{
							name : 'tipoDocBean.tipoDocDesc',
							index : 'tipoDocBean.tipoDocDesc',
							width : 90,
							editable : false,
							hidden : false,
							sortable : true
						},

						{
							name : 'clienteNroDoc',
							index : 'clienteNroDoc',
							width : 100,
							editable : false,
							hidden : false,
							sortable : true
						},
						{
							name : 'clienteTelefono',
							index : 'clienteTelefono',
							width : 100,
							editable : false,
							sortable : true
						},
						{
							name : 'clienteEmail',
							index : 'clienteEmail',
							width : 150,
							editable : false,
							sortable : true
						},

						{
							name : 'clienteDireccion',
							index : 'clienteDireccion',
							width : 150,
							editable : false,
							sortable : true
						},

						{
							name : 'clienteId',
							index : 'clienteId',
							width : 150,
							editable : true,
							formatter : function radio(
									cellValue,
									option,
									rowObject) {

								return '<a onclick="mostrarEditarEnGestionCliente('
										+ rowObject.clienteId
										+ ')"><img src="'+gPath+'/resources/img/editar.png"></a>'
										+'<span>   </span>'
										+ '<a onclick="validarEliminarEnGestionCliente('
										+ rowObject.clienteId
										+ ')"><img src="'+gPath+'/resources/img/eliminar.png"></a>';

							}
						}

				],
				pager : "#plistGestionCliente",
				rowNum : paginado,
				viewrecords : true,
				grouping : false,
				gridComplete : function() {
				},
				caption : '',
				beforeRequest : function() {
					responsive_jqgrid($(".jqGrid"));
				}
			});

$("#listGestionCliente").jqGrid('navGrid',
	"#plistGestionCliente", {
		edit : false,
		add : false,
		del : false,
		search : false,
		refresh : false
	});
}
function buscarEnGestionCliente() {
	$("#listGestionCliente").jqGrid('clearGridData');
	$("#listGestionCliente").jqGrid(
			'setGridParam',
			{
				url :  gPath + '/clienteController/obtenerListaFiltrada' + '?valorCoincidencia='
						+ $("#txtGestionClienteBuscar").val() 
			});
	$("#listGestionCliente").trigger('reloadGrid');

}

function mostrarNuevoEnGestionCliente() {

	var lUrl = gPath + '/clienteController/mostrarNuevoCliente';
	var titulo = "Nuevo Cliente";
	mostrarMensajeConHtmlAltura('divDialogNuevoCliente', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function mostrarEditarEnGestionCliente(clienteId) {

	var lUrl = gPath + '/clienteController/mostrarEditarCliente?clienteId='
			+ clienteId;
	var titulo = "Editar Cliente";
	mostrarMensajeConHtmlAltura('divDialogEditarCliente', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function validarEliminarEnGestionCliente(clienteId) {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_ELIMINACION, 'Elimina Cliente');
	aceptar$.click(function() {
		eliminarEnGestionCliente(clienteId);
	});
};

function eliminarEnGestionCliente(clienteId) {
	$
			.ajax({
				type : 'post',
				url : gPath+'/clienteController/eliminar?clienteId='
						+ clienteId,
				success : function(mensaje) {
					  if(mensaje!=""){
						  mostrarMensajeError(mensaje);
		                 }
					buscarEnGestionCliente();
				}
			});
}