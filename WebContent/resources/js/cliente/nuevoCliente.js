function configurarNuevoCliente() {

	vincularValidacionEstilosJs("formNuevoCliente",
			validarGuardarNuevoCliente);

	$("#cboNuevoClienteTipoDoc")
			.change(
					function() {
						if ($("#cboNuevoClienteTipoDoc")
								.val() != "") {
							$("#txtNuevoClienteNroDoc")
									.attr("disabled", false);
						} else {
							$("#txtNuevoClienteNroDoc")
									.val("");
							$("#txtNuevoClienteNroDoc")
									.attr("disabled", true);
						}
					});

	$("#btnNuevoClienteCancelar").click(function() {
		$("#divDialogNuevoCliente").dialog("close");
	});
}


function validarGuardarNuevoCliente() {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Guardar Registro de Cliente');
	aceptar$.click(function() {
		guardarNuevoCliente();
	});
}


function guardarNuevoCliente() {
	var form = $('#formNuevoCliente');
	$
			.ajax({
				type : form.attr('method'),
				url : form.attr('action'),
				data : form.serialize(),
				success : function(data) {
					$("#divDialogNuevoCliente").dialog(
							"close");
					buscarEnGestionCliente();
				}
			});
}