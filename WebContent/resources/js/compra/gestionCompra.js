function configurarGestionCompra(paginado) {

	$('#btnGestionCompraNuevo').click(function(event) {
		mostrarNuevoEnGestionCompra();
	});

	vincularValidacionEstilosJs("formGestionCompraBuscar",
			buscarEnGestionCompra);
	
	$("#listGestionCompra")
	.jqGrid(
			{
				mtype : "GET",
				datatype : "json",
				url : '${urlListaFiltrada}',
				height : 300,
				rowNum : 180,
				
				colNames : [ 'Id', 'CODIGO',
						'PROVEEDOR', 'ACCIONES' ],
				colModel : [
						{
							name : 'compraId',
							index : 'compraId',
							width : 70,
							sorttype : "int",
							editable : false,
							hidden : true,
							sortable : false
						},
						{
							name : 'compraCodigo',
							index : 'compraCodigo',
							width : 220,
							editable : false,
							sortable : true
						},
						{
							name : 'proveedorBean.proveedorNombre',
							index : 'proveedorBean.proveedorNombre',
							width : 90,
							editable : false,
							hidden : false,
							sortable : true
						},

						{
							name : 'compraId',
							index : 'compraId',
							width : 150,
							editable : true,
							formatter : function radio(
									cellValue,
									option,
									rowObject) {

								return '<a onclick="mostrarEditarEnGestionCompra('
										+ rowObject.compraId
										+ ')"><img src="'+gPath+'/resources/img/editar.png"></a>'
										+'<span>   </span>'
										+ '<a onclick="validarEliminarEnGestionCompra('
										+ rowObject.compraId
										+ ')"><img src="'+gPath+'/resources/img/eliminar.png"></a>';

							}
						}

				],
				pager : "#plistGestionCompra",
				rowNum : paginado,
				viewrecords : true,
				grouping : false,
				gridComplete : function() {
				},
				caption : '',
				beforeRequest : function() {
					responsive_jqgrid($(".jqGrid"));
				}
			});

$("#listGestionCompra").jqGrid('navGrid',
	"#plistGestionCompra", {
		edit : false,
		add : false,
		del : false,
		search : false,
		refresh : false
	});
}
function buscarEnGestionCompra() {
	$("#listGestionCompra").jqGrid('clearGridData');
	$("#listGestionCompra").jqGrid(
			'setGridParam',
			{
				url :  gPath + '/compraController/obtenerListaFiltrada' + '?valor='
						+ $("#txtGestionCompraBuscar").val() + '&columna='
						+ $("#cboGestionCompraColumna").val()
			});
	$("#listGestionCompra").trigger('reloadGrid');

}

function mostrarNuevoEnGestionCompra() {

	var lUrl = gPath + '/compraController/mostrarNuevoCompra';
	var titulo = "Nuevo Compra";
	mostrarMensajeConHtmlAltura('divDialogNuevoCompra', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function mostrarEditarEnGestionCompra(compraId) {

	var lUrl = gPath + '/compraController/mostrarEditarCompra?compraId='
			+ compraId;
	var titulo = "Editar Compra";
	mostrarMensajeConHtmlAltura('divDialogEditarCompra', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function validarEliminarEnGestionCompra(compraId) {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_ELIMINACION, 'Elimina Compra');
	aceptar$.click(function() {
		eliminarEnGestionCompra(compraId);
	});
};

function eliminarEnGestionCompra(compraId) {
	$
			.ajax({
				type : 'get',
				url : gPath+'/compraController/eliminar?compraId='
						+ compraId,
				success : function(mensaje) {
					  if(mensaje!=""){
						  mostrarMensajeError(mensaje);
		                 }
					buscarEnGestionCompra();
				}
			});
}