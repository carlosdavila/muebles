function configurarNuevoCompra() {

	vincularValidacionEstilosJs("formNuevoCompra",
			validarGuardarNuevoCompra);


	$("#btnNuevoCompraCancelar").click(function() {
		$("#divDialogNuevoCompra").dialog("close");
	});
}


function validarGuardarNuevoCompra() {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Guardar Registro de Compra');
	aceptar$.click(function() {
		guardarNuevoCompra();
	});
}


function guardarNuevoCompra() {
	var form = $('#formNuevoCompra');
	$
			.ajax({
				type : form.attr('method'),
				url : form.attr('action'),
				data : form.serialize(),
				success : function(data) {
					$("#divDialogNuevoCompra").dialog(
							"close");
					buscarEnGestionCompra();
				}
			});
}