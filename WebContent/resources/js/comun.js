if (typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, '');
	};
}

(function($) {
	$.fn.goTo = function() {
		$('html, body').animate({
			scrollTop : $(this).offset().top + 'px'
		}, 'fast');
		return this;
	};
})(jQuery);

function mensaje(lIdDiv,lTitulo,lTamano,lLista,lListaBoton,lAlturaAuto, lAnchoAuto){
	var lHeight = null;
	var lWidth = null;
	if(lAlturaAuto){
		lHeight = Math.round(($(window).height()*(lTamano-10))/100);
	}else{
		lHeight='auto';
	}
		
	if(lAnchoAuto){
		lWidth = Math.round(($(window).width()*lTamano)/100);
	}else{
		lWidth = 'auto';
	}	
	
	if($('body').find('#'+lIdDiv).length>0){
		$("#"+lIdDiv).remove();
	}
	
	//CAN'T REMOVE THIS STYLE TAG WITHOUT LOSING THE CONTENT
	var lDialogo = $(
			"<div id='"+lIdDiv+"'>"+
			"<div id='"+lIdDiv+"Contenido'>"+
			"</div></div>");
	
	lDialogo.dialog({
		dialogClass: 'fixed-dialog',
		title: lTitulo,
		modal : true,
		position: { my: "center", at: "center", of: window },		
		width : lWidth,
		height:lHeight,
		resizable: false,
		closeText: "cerrar",
		open : function (event){
			
			$(this).find('#'+lIdDiv+'Contenido').empty();
			
			var isTypeList = false;
			var vignette = '';
			if(lLista instanceof Array){
				isTypeList = true;
				if(lLista.length > 1){
					vignette = '* ';
				}
			}
			
			var tabla = "<div id=\""+lIdDiv+"Tabla\" >";
			tabla += "<p class=\"parrafoMensaje\" >";
			
			if(isTypeList){
				$.each(lLista, function(i, item) {
					tabla += getFilaDeMensaje(item, vignette);
				});
			} else {
				tabla += lLista;
			}			
			
			tabla += "</p>";
			tabla += "</div>";
			tabla += "<div class=\"parrafo\" ></div>";
			var estiloBoton = "";
			
			if(lListaBoton != null){ 
				if(lListaBoton.length == 1){
					estiloBoton = "popup1boton";
				}else{
					estiloBoton = "popup2botones";
				}
			}
			
			tabla += "<div id=\""+lIdDiv+"TablaBoton\" class=\"" + estiloBoton + "\" >";
			if(lListaBoton != null){
				tabla += getFilaDeBoton(lListaBoton);
			}
			tabla += "</div>";
			tabla += "<div class=\"parrafo\" ></div>";
			$(this).find('#'+lIdDiv+'Contenido').html(tabla);
		},
		close: function() {
			lDialogo.dialog('destroy');
		}
		
	});
}

function mostrarMensaje(lIdDiv, lTitulo, lTamano, lLista) {
	mensaje(lIdDiv, lTitulo, lTamano, lLista, null, true, true);
}

function mostrarMensajeConAltura(lIdDiv, lTitulo, lTamano, lLista) {
	mensaje(lIdDiv, lTitulo, lTamano, lLista, null, false, true);
}

function mostrarMensajeConBoton(lIdDiv, lTitulo, lTamano, lLista, lListaBoton) {
	mensaje(lIdDiv, lTitulo, lTamano, lLista, lListaBoton, true, true);
}

function mostrarMensajeConBotonAltura(lIdDiv, lTitulo, lTamano, lLista,
		lListaBoton) {
	mensaje(lIdDiv, lTitulo, lTamano, lLista, lListaBoton, false, true);
}

function mostrarMensajeConBotonera(lIdDiv, lTitulo, lTamano, lLista,
		lListaBoton) {
	mensaje(lIdDiv, lTitulo, lTamano, lLista, lListaBoton, true, true);
}

function mostrarMensajeConAlturaBotonera(lIdDiv, lTitulo, lTamano, lLista,
		lListaBoton) {
	mensaje(lIdDiv, lTitulo, lTamano, lLista, lListaBoton, false, true);
}

function getFilaDeMensaje(lMensaje, caracter) {
	var lScript = null;
	lScript = caracter + lMensaje + "<br/>";
	return lScript;
}

function getFilaDeBoton(lListaBoton) {
	var lScript = "";
	$.each(lListaBoton, function(i, item) {
		lScript += "<div>";
		lScript += getBoton(item.id, item.value, item.onclick);
		lScript += "</div>";
	});
	return lScript;
}

function getBoton(lId, lValue, lOnclick) {
	var lScript = "<input type=\"button\"";
	lScript += "id=\"" + lId + "\"";
	lValue.toUpperCase() == "CANCELAR" ? lScript += "class=\"btn btn-default btn-left\"" :
		lScript += "class=\"btn btn-warning btn-left\"";
	lScript += "value=\"" + lValue + "\"";
	lScript += "onclick=\"" + lOnclick + "\"";
	lScript += ">";
	return lScript;
}

function obtenerMensajeConHtml(lIdDiv, lTitulo, lUriHtml, 
		lTamanoAncho, lTamanoAlto) {
	
	var lHeight = 'auto';
	if (validarValor(lTamanoAlto)) {
		lHeight = Math.round(($(window).height() * lTamanoAlto) / 100);
	}

	var lWidth = 'auto';
	if (validarValor(lTamanoAncho)) {
		lWidth = Math.round(($(window).width() * lTamanoAncho) / 100);
	}

	if ($('body').find('#' + lIdDiv).length > 0) {
		$("#" + lIdDiv).remove();
	}

	var lDialogo = $("<div id='" + lIdDiv + "'><div id='" + lIdDiv
			+ "Contenido'></div></div>");
	
	lDialogo.dialog({
		title 		: lTitulo,
		modal 		: true,
		resizable 	: true,
		height 		: lHeight,
		width 		: lWidth,
		closeText 	: "cerrar",
		open : function(event) {
		
			var horizontal = ($(window).width() - lWidth) / 2;
			var vertical = ($(window).width() - lWidth) / 4;
			$(event.target).parent().css('position', 'fixed');
			$(event.target).parent().css('z-index', '2000');
			$(event.target).parent().css('top', vertical+'px');
	        $(event.target).parent().css('left', horizontal+'px');
			
			$(this).find('#' + lIdDiv + 'Contenido').load(lUriHtml);
		},
		close: function() {
			lDialogo.dialog('destroy');
		}
	});
}

function mostrarMensajeConHtml(lIdDiv, lTitulo, lTamano, lUriHtml) {
	obtenerMensajeConHtml(lIdDiv, lTitulo, lUriHtml, lTamano, lTamano);
}

function mostrarMensajeConHtmlInicial(lIdDiv, lTitulo, ancho,altura, lUriHtml) {
	obtenerMensajeConHtml(lIdDiv, lTitulo, lUriHtml, ancho, altura);
}


function mostrarMensajeConHtmlAltura(lIdDiv, lTitulo, lTamanoAncho, lUriHtml) {
	obtenerMensajeConHtml(lIdDiv, lTitulo, lUriHtml, lTamanoAncho);
}

function bloquearPasteCampo(lIdCampo) {
	$(lIdCampo).on('paste', function(e) {
		e.preventDefault();
	});
}

function estaValorEnArray(lArray, pValor) {
	var lEsta = false;
	for ( var lKey in lArray) {
		if (lArray[lKey] == pValor) {
			lEsta = true;
			break;
		}
	}
	return lEsta;
}

function estaKeyEnArray(lArray, pKey) {
	var lEsta = false;
	for ( var lKey in lArray) {
		if (lKey == pKey) {
			lEsta = true;
			break;
		}
	}
	return lEsta;
}

function esFormatoHora(lStr) {
	var lExpresion = /^\d{1,2}:\d{1,2}$/;
	return lExpresion.test(lStr);
}

function esFormatoFecha(lStr) {
	var lExpresion = /^(\d{1,2})[- \/](\d{1,2})[- \/](\d{4})$/;
	return lExpresion.test(lStr);
}

function esFormatoFechaHora(lStr) {
	var lExpresion = /^(\d{1,2})[- \/](\d{1,2})[- \/](\d{4})\s\d{1,2}:\d{1,2}$/;
	return lExpresion.test(lStr);
}

function esFormatoArray(lStr) {
	var lExpresion = /^(\w+)\[(\w+)\]/;
	return lExpresion.test(lStr);
}

function getMilisegundoDeFecha(lStrFecha) {
	var lArray = null;
	var lTempFecha = null;
	var lMiliseconds = null;
	if (lStrFecha.indexOf('/') != -1) {
		lArray = lStrFecha.split("/");
	} else if (lStrFecha.indexOf('-') != -1) {
		lArray = lStrFecha.split("-");
	}
	if (lArray != null) {
		lTempFecha = lArray[1] + "/" + lArray[0] + "/" + lArray[2];
		lTempFecha = new Date(lTempFecha);
		lMiliseconds = lTempFecha.getTime();
	}
	return lMiliseconds;
}

function getMilisegundoDeFechaHora(lStrFecha) {
	var lArray = null;
	var lTempFecha = null;
	var lMiliseconds = null;
	var lArrayTime = null;
	
	var obj = lStrFecha.split(" ");
	var oDate = obj[0];
	var oTime = obj[1];
	
	if (oDate.indexOf('/') != -1) {
		lArray = oDate.split("/");
	} else if (oDate.indexOf('-') != -1) {
		lArray = oDate.split("-");
	} if (oTime.indexOf(':') != -1) {
		lArrayTime = oTime.split(":");
	}
	
	if (lArray != null && lArrayTime != null) {
		lTempFecha = lArray[1] + "/" + lArray[0] + "/" + lArray[2];
		lTempFecha = new Date(lTempFecha);
		lTempFecha.setHours(lArrayTime[0], lArrayTime[1], 0);
		lMiliseconds = lTempFecha.getTime();
	}
	
	return lMiliseconds;
}

function getMilisegundoDeHora(lStrHora){
	var lTempFecha = null;
	var lMiliseconds = null;
	var lArrayTime = null;
	
	var oTime = lStrHora;
	if (oTime.indexOf(':') != -1) {
		lArrayTime = oTime.split(":");
	}
	
	if (lArrayTime != null) {
		lTempFecha = new Date();
		lTempFecha.setHours(lArrayTime[0], lArrayTime[1], 0);
		lMiliseconds = lTempFecha.getTime();
	}
	
	return lMiliseconds;
}

function getFechaDeMilisegundo(lMilisegundo, lStrSeparador) {
	var lStrFecha = '';
	var lFecha = new Date(1 * lMilisegundo);
	var lDia = lFecha.getDate();
	var lMes = lFecha.getMonth() + 1;
	var lAno = lFecha.getFullYear();
	lStrFecha = (lDia <= 9 ? '0' + lDia : lDia) + lStrSeparador
			+ (lMes <= 9 ? '0' + lMes : lMes) + lStrSeparador + lAno;
	return lStrFecha;
}

function getValorPorObjetoYAtributo(item, strAtributo) {
	var lValor = '';
	var lArray = null;
	var lObjeto = null;
	var i = 0;
	lArray = strAtributo.split('.');
	if (lArray.length > 1) {
		lObjeto = item;
		for (i = 0; i < lArray.length - 1; i++) {
			if (lObjeto != null) {
				lObjeto = lObjeto[lArray[i]];
			}
		}
		if (lObjeto != null) {
			lValor = lObjeto[lArray[i]];
		}
	} else {
		lValor = item[strAtributo];
	}
	if (lValor == null) {
		lValor = '';
	}
	return lValor;
}

function Objeto() {
	this.esLista = false;
	this.arrayObjeto = new Array();
	this.getArrayObjeto = getArrayObjeto;
	this.getJson = getJson;
}

function getArrayObjeto(lArrayFiltro) {
	var lThis = this;
	var lArray = null;
	for ( var atributo in lThis) {
		if (typeof lThis[atributo] != 'function'
				&& !(lThis[atributo] instanceof Array) && atributo != 'esLista') {
			if (atributo.indexOf('.') != -1) {
				if (lArrayFiltro != null) {
					if (estaValorEnArray(lArrayFiltro, atributo)) {
						lArray = atributo.split('.');
						setObjeto(lThis, lArray, 0, lThis[atributo]);
					}
				} else {
					lArray = atributo.split('.');
					setObjeto(lThis, lArray, 0, lThis[atributo]);
				}
			}
		}
	}
	return this.arrayObjeto;
}

function setObjeto(pObjeto, lArray, lIndice, valor) {
	var lObjeto = null;
	var lObjetoNested = null;
	var lKeyDeArray = null;
	var lArrayAtributo = null;
	var lIndiceObjeto = null;
	if (esFormatoArray(lArray[lIndice])) {
		lKeyDeArray = lArray[lIndice];
		lArrayAtributo = lKeyDeArray.split('[');
		lKeyDeArray = lArrayAtributo[0];
		lArrayAtributo = lArrayAtributo[1].split(']');
		lIndiceObjeto = lArrayAtributo[0];
		if (estaKeyEnArray(pObjeto.arrayObjeto, lKeyDeArray)) {
			lObjeto = pObjeto.arrayObjeto[lKeyDeArray];
		} else {
			lObjeto = new Objeto();
			lObjeto.esLista = true;
			pObjeto.arrayObjeto[lKeyDeArray] = lObjeto;
		}
		if (lObjeto.arrayObjeto[lIndiceObjeto] == undefined) {
			lObjetoNested = new Objeto();
			lObjeto.arrayObjeto[lIndiceObjeto] = lObjetoNested;
		} else {
			lObjetoNested = lObjeto.arrayObjeto[lIndiceObjeto];
		}
		lObjeto = lObjetoNested;
	} else {
		lKeyDeArray = lArray[lIndice];
		if (estaKeyEnArray(pObjeto.arrayObjeto, lKeyDeArray)) {
			lObjeto = pObjeto.arrayObjeto[lKeyDeArray];
		} else {
			lObjeto = new Objeto();
			pObjeto.arrayObjeto[lKeyDeArray] = lObjeto;
		}
	}
	if ((lArray.length > 2) && (lIndice < lArray.length - 2)) {
		setObjeto(lObjeto, lArray, lIndice + 1, valor);
	} else {
		lObjeto[lArray[lIndice + 1]] = valor;
	}
}

function getJson(lArrayFiltro) {
	var lStr = '';
	var lObjeto = this;
	lStr = getJsonPorObjeto(lObjeto, lArrayFiltro, true);
	return lStr;
}

function getJsonPorObjeto(lObjeto, lArrayFiltro, pPintaKey) {
	var lStr = '';
	var lStrObjeto = '';
	var lLoopObjeto = null;
	var lArray = null;
	var lPintaKey = true;
	for ( var atributo in lObjeto) {
		if (typeof lObjeto[atributo] != 'function'
				&& !(lObjeto[atributo] instanceof Array)
				&& atributo != 'esLista' && atributo.indexOf('.') == -1) {
			if (lArrayFiltro != null) {
				if (estaValorEnArray(lArrayFiltro, atributo)) {
					lStr = concatenarAtributo(lObjeto, lStr, atributo);
				}
			} else {
				lStr = concatenarAtributo(lObjeto, lStr, atributo);
			}
		}
	}
	if (lStr != '') {
		lStr = lStr.substring(0, lStr.length - 1);
	}

	lArray = lObjeto.getArrayObjeto(lArrayFiltro);
	for ( var lKey in lArray) {
		lLoopObjeto = lArray[lKey];
		if (pPintaKey) {
			lStrObjeto += '"' + lKey + '":';
		}
		if (lLoopObjeto.esLista) {
			lStrObjeto += '[';
			lPintaKey = false;
		} else {
			lStrObjeto += '{';
		}
		lStrObjeto += getJsonPorObjeto(lLoopObjeto, null, lPintaKey);
		if (lLoopObjeto.esLista) {
			lStrObjeto += '],';
		} else {
			lStrObjeto += '},';
		}
	}
	if (lStrObjeto != '') {
		lStrObjeto = lStrObjeto.substring(0, lStrObjeto.length - 1);
		if (lStr != '') {
			lStr += ',' + lStrObjeto;
		} else {
			lStr += lStrObjeto;
		}
	}
	return lStr;
}

function concatenarAtributo(lObjeto, lStr, atributo) {
	var lStrJson = null;
	var lLoopValor = null;
	lLoopValor = lObjeto[atributo];
	lStrJson = lStr;
	if (lLoopValor == '') {
		lStrJson += '"' + atributo + '":null,';
	} else {
		if (esFormatoFecha(lLoopValor)) {
			lStrJson += '"' + atributo + '":'
					+ getMilisegundoDeFecha(lLoopValor) + ',';
		} else if (esFormatoFechaHora(lLoopValor)) {
			lStrJson += '"' + atributo + '":'
			+ getMilisegundoDeFechaHora(lLoopValor) + ',';
		}  else if (esFormatoHora(lLoopValor)) {
			lStrJson += '"' + atributo + '":'
			+ getMilisegundoDeHora(lLoopValor) + ',';
		} else {
			lStrJson += '"' + atributo + '":"' + lLoopValor.replace(/"/g,"\\\"") + '",';
		}
	}
	return lStrJson;
}

/*******************************************************************************
 * frankayala: A continuaci�n la especificaci�n de cada par�metro de entrada de
 * esta funci�n:
 * 
 * lId: Id del contenedor dentro del cual se leeran todos los atributos de tipo
 * "input" para posteriormente poder mapearlas para el envio.
 * 
 * obj: Puede ocurrir en algunos casos que deseemos mandar los datos de m�s de
 * un objeto contenedor, por ejemplo 2 formularios, para este caso en primera
 * instancia se crea un nuevo "Objeto" pero luego debe hacerse referencia a este
 * mismo objeto y sobre este a�adir los dem�s campos del otro formulario, es por
 * eso la existencia de esta variable.
 * 
 * lCampo: Tambi�n puede ocurrir que por ejemplo necesitemos encapsular los
 * campos de cierto grupo de formularios bajo un nombre de atributo en comun,
 * por ejemplo, los campos de 2 formularios deber�n mapearse bajo el atributo
 * padre 'productoBean' y existe un tercer formulario que se mapeara bajo el
 * otro atributo padre 'costoProductoBean', de esta manera podemos trabajar
 * bajo varias entidades "Bean", bajo un mismo proceso Ajax.
 * 
 ******************************************************************************/

function getObjetoPorId(lId, obj, lCampo) {

	var lLista = $('#' + lId).find('*').filter(":input");
	var lListaRadio = new Array();
	var lNombreRadio = null;
	var lControl = null;
	var lObjeto = null;
	var lControlValor = null;
	var lControlName = null;
	var lCampoPadre = '';
	
	if(validarValor(lCampo)){
		lCampoPadre = lCampo+'.';
	}
	
	if(validarValor(obj)){
		lObjeto = obj;
	}else{
		lObjeto = new Objeto();
	}

	lLista.each(function(i) {
		lControl = $(this);

		if (lControl.is(':radio')) {
			lNombreRadio = lControl.context.name;
			if (!estaValorEnArray(lListaRadio, lNombreRadio)) {
				lListaRadio.push(lNombreRadio);
			}
		} else if (lControl.is(':checkbox')) {
			if (lControl.is(':checked')) {
				lObjeto[lCampoPadre+lControl.attr('name')] = lControl.val();
			} else {
				lObjeto[lCampoPadre+lControl.attr('name')] = '';
			}
		} else if (lControl.is(':text') && validarValor(lControl.attr('name'))) {
			
			if (lControl.attr('id').indexOf('txtHtml') == 0) {
				lObjeto[lCampoPadre+lControl.attr('name')] = $.trim(lControl.val());
			} else {
				lObjeto[lCampoPadre+lControl.attr('name')] = $.trim(lControl.val()
						.toUpperCase());
			}
			
		} else if (lControl.context.type == 'hidden' && validarValor(lControl.attr('name')) && 
				validarValor(lControl.attr('id'))) {
			
			lObjeto[lCampoPadre+lControl.attr('name')] = $.trim(lControl.val().toUpperCase());
			
		} else if (lControl.context.type == 'textarea' && validarValor(lControl.attr('name')) && 
				validarValor(lControl.attr('id'))) {
			
			if (lControl.attr('id').indexOf('txtHtml') == 0) {
				lObjeto[lCampoPadre+lControl.attr('name')] = $.trim(lControl.val());
			} else {
				lObjeto[lCampoPadre+lControl.attr('name')] = $.trim(lControl.val()
						.toUpperCase());
			}
			
		} else if (lControl.context.type == 'select-one') {
			if (lControl.val() == null) {
				lObjeto[lCampoPadre+lControl.attr('name')] = '';
			} else {
				lObjeto[lCampoPadre+lControl.attr('name')] = $.trim(lControl.val()
						.toUpperCase());
			}
		} else if (lControl.context.type == 'select-multiple') {
			if (lControl.val() == null) {
				lObjeto[lCampoPadre+lControl.attr('name')] = '';
			} else {
				var lValue = '';
				var lArray = lControl.val();
				for(var i=0;i<lArray.length;i++){
					lValue = lValue+lArray[i]+',';
				}
				lValue = lValue.substring(0, lValue.length-1);
				lObjeto[lCampoPadre+lControl.attr('name')] = lValue;
			}
		}
	});
	$.each(lListaRadio, function(i, item) {
		// lLista = $('#'+lId).find('*').filter('[name='+item+']');
		lLista = $('#' + lId).find('*').filter(':radio');
		lControlValor = '';
		lControlName = item;
		lLista.each(function(i) {
			lControl = $(this);
			if (lControl.context.name == item) {
				if (lControl.is(':checked')) {
					lControlValor = lControl.val();
					lControlName = lControl.context.name;
				}
			}
		});
		lObjeto[lCampoPadre+lControlName] = lControlValor;
	});
	return lObjeto;
}

function stringify() {
	var lObjeto = new stringifyProxy();
	var lStr = lObjeto.stringifyCore.apply(lObjeto, arguments);
	return lStr;
}

function stringifyProxy() {
	this.stringifyCore = stringifyCore;
	this.stringifySingle = stringifySingle;
	this.stringifyFilter = stringifyFilter;
}

function stringifyCore() {
	var lStr = '';
	if (arguments.length == 1) {
		lStr = this.stringifySingle.apply(this, arguments);
	} else if (arguments.length == 2) {
		lStr = this.stringifyFilter.apply(this, arguments);
	}
	if (lStr != '') {
		lStr = '{' + lStr + '}';
	} else {
		lStr = '{}';
	}
	return lStr;
}

function stringifySingle(lObjeto) {
	return lObjeto.getJson(null);
}

function stringifyFilter(lObjeto, lArray) {
	return lObjeto.getJson(lArray);
}

function cambiarTamanoImagenCalendario() {
	$("img.ui-datepicker-trigger").addClass("img_calendario");
}

function agregarTitleBotonesPaginacion(){
	$('.first').attr('title', 'Primero');
	$('.previous').attr('title', 'Anterior');
	$('.next').attr('title', 'Pr\u00F3ximo');
	$('.last').attr('title', '\u00DAltimo');
}

function mostrarPopupConfirmacion(lMensajeConfirmacion) {
	var listaMensaje = new Array();
	var listaBoton = new Array();
	var lBoton = new Object();
	lBoton.id = "btnPopupAceptarMensaje";
	lBoton.value = "ACEPTAR";
	lBoton.onclick = "$('#divDialogoPopupConfirmacion').dialog('close');";
	listaBoton.push(lBoton);
	listaMensaje.push(lMensajeConfirmacion);
	mostrarMensajeConBoton('divDialogoPopupConfirmacion', 'Mensaje', 30,
			listaMensaje, listaBoton);
}

function mostrarPopupConfirmacionConAltura(lMensajeConfirmacion) {
	var listaBoton = new Array();
	var lBoton = new Object();
	lBoton.id = "btnPopupAceptarMensaje";
	lBoton.value = "ACEPTAR";
	lBoton.onclick = "$('#divDialogoPopupConfirmacion').dialog('close');";
	listaBoton.push(lBoton);
	
	var listaMensaje = new Array();
	if(lMensajeConfirmacion instanceof Array){
		listaMensaje = lMensajeConfirmacion;
	} else {
		listaMensaje.push(lMensajeConfirmacion);
	}
	
	mostrarMensajeConBotonAltura('divDialogoPopupConfirmacion', 'Mensaje', 30,
			listaMensaje, listaBoton);
}

function mostrarMensajeModalSimple(mensaje,titulo) {
	var listaBoton = new Array();
	var lBoton = new Object();
	lBoton.id = "btnModalAceptarMensaje";
	lBoton.value = "ACEPTAR";
	lBoton.onclick = "$('#divDialogoPopupConfirmacion').dialog('close');";
	listaBoton.push(lBoton);
	
	var listaMensaje = new Array();
	if(mensaje instanceof Array){
		listaMensaje = mensaje;
	} else {
		listaMensaje.push(mensaje);
	}
	
	mostrarMensajeConBotonAltura('divDialogoPopupConfirmacion', titulo, 30,
			listaMensaje, listaBoton);
}

/*******************************************************************************
 * frankayala: Agregue que ahora este m�todo que carga las opciones del combo, 
 * ahora haga una funci�n equivalente a lo de un Map que con el identificador
 * escogido como "valor", almacene en una variable contenedora todo el objeto
 * obtenido, y esto es retornado de manera que luego podamos acceder a las dem�s 
 * variables de nuestra lista obteniendolas con el valor del combo seleccionado.
 * A modo de ejemplo una vez obtenido el objeto por el Identificador se puede
 * hacer lo siguiente:
 * 
 * var objeto = mapObject[valorCombo];
 * var valorEsperado = getValorPorObjetoYAtributo(objeto, 'otroCampoDelObjeto');
 * 
 * Actualizaci�n: Ahora permite recibir una variable que si tiene valor ser� 
 * inyectada como primera opci�n, resultando asi la "opci�n por defecto".
 * 
 ******************************************************************************/
function cargarOpcionDeLista(idCampo, lista, valor, descripcion, opcionPorDefecto) {

	var valObject;
	var objSelect = '';
	var mapObject = {};
	
	if(validarValor(opcionPorDefecto)){
		objSelect += "<option value>" + opcionPorDefecto + "</option>";
	}
	
	$.each(lista, function(i, item) {
		valObject = getValorPorObjetoYAtributo(item, valor);
		mapObject[valObject] = item;
		objSelect += "<option value=\"" + valObject + "\" >"
				+ getValorPorObjetoYAtributo(item, descripcion) + "</option>";
	});

	$('#' + idCampo).html(objSelect);
	return mapObject;
}

function cargarSeleccionDeListaMultiple(idCampo, valores){
	if(validarValor(valores)){
		$.each(valores.split(","), function(i,e){
			$('#'+idCampo+" option[value='" + e + "']").attr("selected", true);
		});
	}
}

function agregarReglasValidacion(itemValidacion, lIdcampoActual,
		lIdcampoComparacion) {
	if (itemValidacion != null) {
		$("#" + lIdcampoActual).rules(
				"add",
				getValidacionCampo(itemValidacion, lIdcampoActual,
						lIdcampoComparacion));
	}
}

function getValidacionCampo(itemValidacion, lIdcampoActual, lIdcampoComparacion) {

	var objetoValidacion = {
		messages : {}
	};

	$("#" + lIdcampoActual).rules("remove");
	$("#" + lIdcampoActual).removeClass("tipoNumeroDecimal");
	$("#" + lIdcampoActual).removeClass("tipoNumeroEntero");
	$("#" + lIdcampoActual).removeClass("tipoAlfanumerico");
	$("#" + lIdcampoActual).removeClass("tipoTexto");
	$("#" + lIdcampoActual).removeClass("tipoLetra");

	if (itemValidacion.tipoDato != null) {
		if (itemValidacion.tipoDato == 'digito') {
			
			objetoValidacion.digits = true;
			objetoValidacion.messages.digits = itemValidacion.mensajeTipoDato;
			$("#" + lIdcampoActual).addClass("tipoNumeroEntero");
			
		} else if (itemValidacion.tipoDato == 'decimal') {
			
			objetoValidacion.number = true;
			objetoValidacion.messages.number = itemValidacion.mensajeTipoDato;
			$("#" + lIdcampoActual).addClass("tipoNumeroDecimal");
			
		} else if (itemValidacion.tipoDato == 'texto') {
			
			objetoValidacion.texto = true;
			objetoValidacion.messages.texto = itemValidacion.mensajeTipoDato;
			$("#" + lIdcampoActual).addClass("tipoTexto");
			
		} else if (itemValidacion.tipoDato == 'alfanumerico') {
			
			objetoValidacion.alfanumerico = true;
			objetoValidacion.messages.alfanumerico = itemValidacion.mensajeTipoDato;
			$("#" + lIdcampoActual).addClass("tipoAlfanumerico");
			
		}  else if (itemValidacion.tipoDato == 'letra') {
			
			objetoValidacion.letra = true;
			objetoValidacion.messages.letra = itemValidacion.mensajeTipoDato;
			$("#" + lIdcampoActual).addClass("tipoLetra");
			
		} else if (itemValidacion.tipoDato == 'fecha') {
			objetoValidacion.fecha = true;
			objetoValidacion.messages.fecha = itemValidacion.mensajeTipoDato;
		} else if (itemValidacion.tipoDato == 'fechaHora') {
			objetoValidacion.fechaHora = true;
			objetoValidacion.messages.fechaHora = itemValidacion.mensajeTipoDato;
		} else if (itemValidacion.tipoDato == 'hora') {
			objetoValidacion.hora = true;
			objetoValidacion.messages.hora = itemValidacion.mensajeTipoDato;
		} else if (itemValidacion.tipoDato == 'email') {
			objetoValidacion.email = true;
			objetoValidacion.messages.email = itemValidacion.mensajeTipoDato;
		}
	}

	if (itemValidacion.tamanioMaximo != null) {
		objetoValidacion.maxlength = itemValidacion.tamanioMaximo;
		objetoValidacion.messages.maxlength = itemValidacion.mensajeMaximo;
	}

	if (itemValidacion.tamanioExacto != null) {
		objetoValidacion.exactlength = itemValidacion.tamanioExacto;
		objetoValidacion.messages.exactlength = itemValidacion.mensajeExacto;
	}

	if (itemValidacion.formato != null) {
		objetoValidacion.formato = itemValidacion.formato;
		objetoValidacion.messages.formato = itemValidacion.mensajeFormato;
	}

	if (itemValidacion.valorMinimo != null) {
		objetoValidacion.min = itemValidacion.valorMinimo;
		objetoValidacion.messages.min = itemValidacion.mensajeValorMinimo;
	}

	if (itemValidacion.valorMaximo != null) {
		objetoValidacion.max = itemValidacion.valorMaximo;
		objetoValidacion.messages.max = itemValidacion.mensajeValorMaximo;
	}

	if (itemValidacion.mensajeObligatorio != null) {
		objetoValidacion.required = true;
		objetoValidacion.messages.required = itemValidacion.mensajeObligatorio;
	}

	if (itemValidacion.mensajeComparacion != null) {
		objetoValidacion.igualA = '#' + lIdcampoComparacion;
		objetoValidacion.messages.igualA = itemValidacion.mensajeComparacion;
	}

	if (itemValidacion.mensajeComparacionDiferente != null) {
		objetoValidacion.diferenteA = '#' + lIdcampoComparacion;
		objetoValidacion.messages.diferenteA = itemValidacion.mensajeComparacionDiferente;
	}

	if (itemValidacion.clasegrupo1obligatorio != null) {
		objetoValidacion.obligatorio_de_grupo = [ 1,
				itemValidacion.clasegrupo1obligatorio ];
		objetoValidacion.messages.obligatorio_de_grupo = '';
	}

	if (itemValidacion.mensajeComparacionMayor != null) {
		objetoValidacion.greaterThan = '#' + lIdcampoComparacion;
		objetoValidacion.messages.greaterThan = itemValidacion.mensajeComparacionMayor;
	}

	if (itemValidacion.mensajeComparacionMenor != null) {
		objetoValidacion.lessThan = '#' + lIdcampoComparacion;
		objetoValidacion.messages.lessThan = itemValidacion.mensajeComparacionMenor;
	}
	
	if (itemValidacion.mensajeComparacionFechaHoraMayor != null) {
		objetoValidacion.timeGreaterThan = '#' + lIdcampoComparacion;
		objetoValidacion.messages.timeGreaterThan = itemValidacion.mensajeComparacionFechaHoraMayor;
	}

	if (itemValidacion.mensajeComparacionFechaHoraMenor != null) {
		objetoValidacion.timeLessThan = '#' + lIdcampoComparacion;
		objetoValidacion.messages.timeLessThan = itemValidacion.mensajeComparacionFechaHoraMenor;
	}

	if (itemValidacion.mensajeObligatorioPorCombo != null) {
		objetoValidacion.required = function(element) {
			return ($('#' + lIdcampoComparacion).val() != '');
		};
		objetoValidacion.messages.required = itemValidacion.mensajeObligatorioPorCombo;
	}

	return objetoValidacion;
}

$(document).ajaxError(
		function(event, jqxhr, settings, exception) {
			$("body").hideLoading();
			var lObjetoRespuesta = $.parseJSON(jqxhr.responseText);
			mostrarMensajeError(lObjetoRespuesta.mensajeRespuesta);
		});

function convertirValorNullAVacio(lValorServidor) {
	return lValorServidor != null ? lValorServidor : '';
}

function convertirMayusculaCampo(lValorCampo) {
	if (lValorCampo != null) {
		lValorCampo = $.trim(lValorCampo.toUpperCase());
	}

	return lValorCampo;
}

function generarLoading() {
	$(document).ajaxStart(function() {
		if (gActivoLoading != 1) {
			
			$("body").showLoading();
			
		}
	});

	$(document).ajaxStop(function() {
		if (gActivoLoading != 1) {
			$("body").hideLoading();
			
		} else {
			gActivoLoading = '';
		}
	});
}



function inicializarAjax() {
	$.ajaxSetup({
		cache : false
	});
}

inicializarAjax();

function removerValidacion(campoValidador) {
	var divMensajeError = document.getElementById(campoValidador);
	if (divMensajeError != null && divMensajeError != undefined
			&& divMensajeError.hasChildNodes()) {
		divMensajeError.removeChild(divMensajeError.lastChild);
	}
}

function toUpperValidate(obj) {
	if(obj != undefined && obj != null && 
			typeof obj == 'string'){
		return obj.toUpperCase();
	} else {
		return obj;
	}
}

function validarValor(obj) {
	return obj != undefined && obj != null;
}

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

function FP_SoloNumerosSinPunto(event) {

	//Permitir
	if (event.keyCode == 46 || event.keyCode == 8  || 		// permite: delete, backspace 
		event.keyCode == 9  || event.keyCode == 27 || 		// permite: tab, escape
		event.keyCode == 13 || 								// permite: enter
		(event.keyCode == 65 && event.ctrlKey == true) ||	// permite: Ctrl+A
		(event.keyCode == 86 && event.ctrlKey == true) ||	// permite: Ctrl+V
		(event.keyCode == 67 && event.ctrlKey == true) ||	// permite: Ctrl+C
		(event.keyCode == 88 && event.ctrlKey == true) ||	// permite: Ctrl+X
		(event.keyCode >= 35 && event.keyCode <= 40)){ 		// permite: end, home, left, up, right, down
		return;
	} else {
		
		// No permitir
		if ((event.keyCode < 48 || event.keyCode > 57)  && 		// permitidos: 0,1,2,3,4,5,6,7,8,9
		    (event.keyCode < 96 || event.keyCode > 105)) {		// permitidos: 0,1,2,3,4,5,6,7,8,9
				event.preventDefault();
		}
	}
}

function FP_SoloNumerosConPunto(event) {
	
	//Permitir
	if (event.keyCode == 46   || event.keyCode == 8  || 		// permite: delete, backspace 
		event.keyCode == 9    || event.keyCode == 27 || 		// permite: tab, escape
		event.keyCode == 13   || 								// permite: enter
		(event.keyCode == 65  && event.ctrlKey == true) ||		// permite: Ctrl+A
		(event.keyCode == 86  && event.ctrlKey == true) ||		// permite: Ctrl+V
		(event.keyCode == 67  && event.ctrlKey == true) ||		// permite: Ctrl+C
		(event.keyCode == 88  && event.ctrlKey == true) ||		// permite: Ctrl+X
		(event.keyCode >= 35  && event.keyCode <= 40)   || 		// permite: end, home, left, up, right, down
		(event.keyCode == 190 || event.keyCode == 110)){		// permite: "."
		return;
	} else {
		
		// No permitir
		if ((event.keyCode < 48 || event.keyCode > 57)  && 		// permitidos: 0,1,2,3,4,5,6,7,8,9
		    (event.keyCode < 96 || event.keyCode > 105)) {		// permitidos: 0,1,2,3,4,5,6,7,8,9
				event.preventDefault();
		}
	}
}

function FP_SoloLetrasNumerosyEspeciales(event) {
	
	//Permitir
	if (event.keyCode == 46 || event.keyCode == 8  || 			// permite: delete, backspace 
		event.keyCode == 9  || event.keyCode == 27 || 			// permite: tab, escape
		event.keyCode == 13 || event.keyCode == 32 ||			// permite: enter, space
		(event.keyCode == 65 && event.ctrlKey == true) ||		// permite: Ctrl+A
		(event.keyCode == 86 && event.ctrlKey == true) ||		// permite: Ctrl+V
		(event.keyCode == 67 && event.ctrlKey == true) ||		// permite: Ctrl+C
		(event.keyCode == 88 && event.ctrlKey == true) ||		// permite: Ctrl+X
		(event.keyCode >= 35 && event.keyCode <= 40)   || 		// permite: end, home, left, up, right, down
		(event.keyCode == 187 || event.keyCode == 107) ||		// permite: "+", "+"
		(event.keyCode == 109 || event.keyCode == 189) ||		// permite: "-", "-"
		(event.keyCode == 55  && event.shiftKey == true) ||		// permite: "/"
		(event.keyCode == 189 && event.shiftKey == true)) {		// permite: "_"
		return;
	} else {
		
		// No permitir
		if ((event.keyCode < 48 || event.keyCode > 57)  && 		// permitidos: 0,1,2,3,4,5,6,7,8,9
		    (event.keyCode < 65 || event.keyCode > 90)  &&		// permitidos: A,B,C,D,E,F...X,Y,Z
		    (event.keyCode < 96 || event.keyCode > 105) &&		// permitidos: 0,1,2,3,4,5,6,7,8,9
		    (event.keyCode != 192)) {							// permitidos: �
				event.preventDefault();
		}
	}
}

function FP_SoloLetrasyNumeros(event) {
	
	//Permitir
	if (event.keyCode == 46 || event.keyCode == 8  || 		// permite: delete, backspace 
		event.keyCode == 9  || event.keyCode == 27 || 		// permite: tab, escape
		event.keyCode == 13 || event.keyCode == 32 ||		// permite: enter, space
		(event.keyCode == 65 && event.ctrlKey == true) ||	// permite: Ctrl+A
		(event.keyCode == 86 && event.ctrlKey == true) ||	// permite: Ctrl+V
		(event.keyCode == 67 && event.ctrlKey == true) ||	// permite: Ctrl+C
		(event.keyCode == 88 && event.ctrlKey == true) ||	// permite: Ctrl+X
		(event.keyCode >= 35 && event.keyCode <= 40)){ 		// permite: end, home, left, up, right, down
		return;
	} else {
		
		// No permitir
		if ((event.keyCode < 48 || event.keyCode > 57)  && 		// permitidos: 0,1,2,3,4,5,6,7,8,9
		    (event.keyCode < 65 || event.keyCode > 90)  &&		// permitidos: A,B,C,D,E,F...X,Y,Z
		    (event.keyCode < 96 || event.keyCode > 105) &&		// permitidos: 0,1,2,3,4,5,6,7,8,9
		    (event.keyCode != 192)) {							// permitidos: �
				event.preventDefault();
		}
	}
}

function FP_SoloLetras(event) {

	//Permitir
	if (event.keyCode == 46 || event.keyCode == 8  || 		// permite: delete, backspace 
		event.keyCode == 9  || event.keyCode == 27 || 		// permite: tab, escape
		event.keyCode == 13 || event.keyCode == 32 ||		// permite: enter, space
		(event.keyCode == 65 && event.ctrlKey == true) ||	// permite: Ctrl+A
		(event.keyCode == 86 && event.ctrlKey == true) ||	// permite: Ctrl+V
		(event.keyCode == 67 && event.ctrlKey == true) ||	// permite: Ctrl+C
		(event.keyCode == 88 && event.ctrlKey == true) ||	// permite: Ctrl+X
		(event.keyCode >= 35 && event.keyCode <= 40)){ 		// permite: end, home, left, up, right, down
		return;
	} else {
		
		// No permitir
		if ((event.keyCode < 65 || event.keyCode > 90)  &&		// permitidos: A,B,C,D,E,F...X,Y,Z
		    (event.keyCode != 192)) {							// permitidos: �
				event.preventDefault();
		}
	}
}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

/*******************************************************************************
 * frankayala: Obtiene los datos del registro seleccionado adicionando el indice 
 * de la fila.
 ******************************************************************************/
function fnGetSelected(oTable) {
	var obj = oTable.$('tr.selected');
	if (obj.length > 0) {
		var oData = oTable.fnGetData(obj[0]);
		oData.rowIndex = obj[0]._DT_RowIndex;
		return oData;
	} else {
		return null;
	}
}

/*******************************************************************************
 * frankayala: Obtiene el numero de p�gina actual en la que nos encontramos en 
 * el datatable
 ******************************************************************************/
function fnGetCurrentPage(oTable) {
	return Math.ceil(oTable.fnSettings()._iDisplayStart
			/ oTable.fnSettings()._iDisplayLength) + 1;
}

/*******************************************************************************
 * frankayala: Obtiene la cantidad de registros que se muestran por p�gina en el 
 * datatable
 ******************************************************************************/
function fnGetDisplayLength(oTable) {
	return oTable.fnSettings()._iDisplayLength;
}

/*******************************************************************************
 * rmunoz: Intercambia 2 filas de una tabla.
 * Los registros deben poseer el atributo "orden", caso contrario
 * el intercambio procede sin garantizar grabado en la bd.
 * Advertencia: Requiere usar funci�n fnRowCallback, en lugar de fnCreatedRow
 *******************************************************************************/
function intercambiarFilas(oTable, filaA, filaB) {
	var dataA = oTable.fnGetData(filaA);
	var dataB = oTable.fnGetData(filaB);
	var tmp = dataA;
	dataA = dataB;
	dataB = tmp;
	tmp = dataA.orden;
	dataA.orden = dataB.orden;
	dataB.orden = tmp;
	oTable.fnUpdate(dataA,filaA);
	oTable.fnUpdate(dataB,filaB);
	
	//Se mueve a pagina
	var registrosxPagina = fnGetDisplayLength(oTable);
	var paginaObjetivo = parseInt(filaB/registrosxPagina);
	oTable.fnPageChange(paginaObjetivo);
	
	//Actualizando la nueva seleccion
	oTable.data('dataSeleccionada',dataB);
	oTable.data('indiceSeleccionado',filaB);
	$(oTable.fnGetNodes(filaB)).addClass("selected");
	$(oTable.fnGetNodes(filaA)).removeClass("selected");

}

/********************************************************************************
 * rmunoz: Habilita evento click para seleccionar fila, la data de la fila 
 * seleccionada y el indice de su posicion actual en la tabla son
 * adjuntados a la tabla de modo que se puede recuperar:
 * 		- referenciaAtabla.data('dataSeleccionada')
 * 		- referenciaAtabla.data('indiceSeleccionado')
 * 
 */
function habilitarSeleccionFila(oTable) {
	oTable.on('page',function(){
		oTable.find('tbody tr').toggleClass('selected',false);
		oTable.removeData('dataSeleccionada');
		oTable.removeData('indiceSeleccionado');
	});
	oTable.find('tbody').on( 'click', 'tr', function () {
		if(oTable.data('indiceSeleccionado') == oTable.fnGetPosition(this)) {
			$(this).toggleClass('selected',false);
			oTable.removeData('dataSeleccionada');
			oTable.removeData('indiceSeleccionado');
		} else {
			$(this).toggleClass('selected',true);
			$(this).siblings().toggleClass('selected',false);
			oTable.data('dataSeleccionada',oTable.fnGetData($(this)[0]));
			oTable.data('indiceSeleccionado',oTable.fnGetPosition(this));
		}
    } );
	
}

/********************************************************************************
 * RORMES: Iniciar el pesta�a adjuntos. Se puede pasar una funci�n par llamar
 * al terminar de cargar el jsp de adjuntos si deseas.
 *******************************************************************************/
function iniciarFormularioAdjuntos(keyTablaSistema, idRegistro, 
	targetid, esHabilitado, customFunction) {
	
	eliminarDivExistentePorClase("adjuntoArchivos");
	var param = '';
	if (validarValor(esHabilitado)){
		param = "&esHabilitado=" + esHabilitado;
	}
	
	var url = gPath + '/comunController/iniciarVistaAdjuntos?' + "keyTablaSistema="
			+ keyTablaSistema + "&idRegistro=" + idRegistro  + param;
	$('#' + targetid).load(url, function(data) {
		if (customFunction != undefined && customFunction != null)
			functionGlobal = customFunction;
	});

}

function iniciarFormularioComentarios(keyTablaSistema, idRegistro, 
	targetid, esHabilitado, customFunction) {
	  eliminarDivExistentePorClase("vistaComentario");
	var param = '';
	if (validarValor(esHabilitado)){
		param = "&esHabilitado=" + esHabilitado;
	}
	
	var url = gPath + '/comunController/iniciarVistaComentarios?' + "keyTablaSistema="
			+ keyTablaSistema + "&idRegistro=" + idRegistro  + param;
	$('#' + targetid).load(url, function(data) {
		if (customFunction != undefined && customFunction != null)
			customFunction();
	});
}

function iniciarVistaHistorial(keyTablaSistema, idRegistro, targetid, customFunction) {
  eliminarDivExistentePorClase("vistaHistorial");
	var url = gPath + '/comunController/iniciarVistaHistorial?' + "keyTablaSistema="
	+ keyTablaSistema + "&idRegistro=" + idRegistro;
	$('#' + targetid).load(url, function(data) {
		if (customFunction != undefined && customFunction != null)
			customFunction();
	});
}

function iniciarFormularioCostoProducto(targetid, params, customFunction){
	var url = gPath + '/costoProductoController/iniciarVistaCostoProducto' + params;
	$('#' + targetid).load(url, function(data) {
		functionGlobalValid = function(param){};
		if (customFunction != undefined && customFunction != null)
			customFunction();
	});
}

function iniciarFormularioMaterialProducto(targetid, params, customFunction){
	var url = gPath + '/materialProductoController/iniciarVistaMaterialProducto' + params;
	$('#' + targetid).load(url, function(data) {
		if (customFunction != undefined && customFunction != null)
			customFunction();
	});
}

function iniciarFormularioSeguimiento(targetid, params, customFunction){
	var url = gPath + '/seguimientoProductoController/iniciarVistaSeguimientoProducto' + params;
	$('#' + targetid).load(url, function(data) {
		if (customFunction != undefined && customFunction != null)
			customFunction();
	});	
}

/***********************************************************************************
 * frankayala: Permite almacenar en variables globales los IDs del seleccionado Tab.
 ***********************************************************************************/
function establecerTabSeleccionado(obj, customFunction){
	
	gTabSelected = obj;
	gTabBodySelected = obj.replace("liTab", "tab");
	$('.nav-tabs a[href="#'+gTabBodySelected+'"]').tab("show");
	
	if(validarValor(customFunction)){
		customFunction();
	}
}

function cambiarTabSeleccionado(newTabSelected){
	
	if($('div.contenido-scroll').length != 0) {
		$('div.contenido-scroll').scrollTop(0);
	} else {
		$('body').scrollTop(0);
	}
	
	var newTabBodySelected = newTabSelected.replace("liTab", "tab");
	$('.nav-tabs a[href="#'+newTabBodySelected+'"]').tab("show");
	gTabBodySelected = newTabBodySelected;
	gTabSelected = newTabSelected;	
}

function seleccionarTab(newTabSelected){
	if (gTabSelected != newTabSelected) {
		cambiarTabSeleccionado(newTabSelected);
	}
}

/***********************************************************************************
 * hpalacios: enable/disabled todos los elementos de un div.
 * opcion: true->enable  false->disabled
 * habilitaDeshabilitaElementosDiv('#tabDatosGenerales',false);
 ***********************************************************************************/
function habilitaDeshabilitaElementosDiv(idDiv, opcion) {
	$(idDiv).find('input, textarea, select, button').each(function() {
		$(this).attr('disabled', !opcion);
	});	
}

function validarComparacionFechas(lIdcampoActual, lIdcampoComparacion){
	if($('#'+lIdcampoActual).valid() && $('#'+lIdcampoComparacion).val() == '') {
		$('#'+lIdcampoComparacion).val($('#'+lIdcampoActual).val());
	}
	$('#'+lIdcampoComparacion).valid();
}

/***********************************************************************************
 * rormes: Permite codificar y descodificar  una cadena remplazando elementos 
 * selecionados con los codigos html.  
 ***********************************************************************************/
function  htmlEncode(str) {
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}

function  htmlDecode(str) {
    return String(str)
            .replace(/&amp;/g, '&')
            .replace(/&quot;/g, '\"')
            .replace(/&#39;/g, '\'')
            .replace(/&lt;/g, '<')
            .replace(/&gt;/g, '>');
}

/**********************************************************************
* Permite ordenar los elemenos seleccionados en una lista multiple.
*
***********************************************************************/
function ordenarListaMultiple(idLista) {
	var seleccionados = $("#"+idLista+" option:selected");
	var noSeleccionados = new Array();
	$("#"+idLista+" option").each(function (index,option){
		if (jQuery.inArray(option, seleccionados) === -1) {
			noSeleccionados.push(option);
		}
	});

	var ordenado = $.merge(seleccionados, noSeleccionados);

	$("#"+idLista+"").empty();

	$.each(ordenado, function(index, item) {
	  $("#"+idLista).append(item);
	});
}

/**********************************************************************
 * frankayala: M�todos que nos permitir�n controlar los momentos en que
 * se pueda modificar el valor del "campo" es cuesti�n, ejecutando la
 * "funci�n personalizada" que establezcamos para este escenario.
 **********************************************************************/
function configurarControlDelCambio(idCampo, funcionPersonalizada){
	
	$('#'+idCampo).keyup(function(event) {
		delay(function(){
			funcionPersonalizada();
		}, 400);
	});
	
	$('#'+idCampo).bind('paste', function() {
		clearTimeout(0);
		setTimeout(function() {
			$('#'+idCampo).keyup();
        }, 100);
    });
}

/**********************************************************************
 * frankayala: Con esta configuraci�n ya se podr�a visualizar los tabs.
 **********************************************************************/
function configurarTabs() {
	$('a[data-toggle="tab"]').each(function() {
		$(this).click(function(){
			$('.nav-tabs a[href="'+$(this).attr('href')+'"]').tab("show");
		});
	});
}

/**********************************************************************
 * rmunoz: Muestra mensaje de error.(Sin botones).
 * @EstandarAtenea
 **********************************************************************/
function mostrarMensajeError(mensaje) {
	mostrarMensajeConAltura('divDialogo', 'Error',
			CONSTANTE.MENSAJE.ALERTA.TAMANO, mensaje);
}

/**********************************************************************
 * rmunoz: Muestra mensaje informativo.(Con un boton de Aceptar).
 * La funcion opcional permite a�adir una acci�n tras cerrar el dialog.
 * @EstandarAtenea
 **********************************************************************/
function mostrarMensajeInformativo(mensaje,fnAccionPosteriorAlCierre) {
	mostrarPopupConfirmacionConAltura(mensaje);
	if(fnAccionPosteriorAlCierre) {
		$('#btnPopupAceptarMensaje').click(fnAccionPosteriorAlCierre);
	}
}

/**********************************************************************
 * rmunoz: Muestra mensaje de exito.(Con un boton de Aceptar).
 * La funcion opcional permite a�adir una acci�n tras cerrar el dialog.
 * @EstandarAtenea
 **********************************************************************/
function mostrarMensajeExito(mensaje,fnAccionPosteriorAlCierre) {
	mostrarPopupConfirmacionConAltura(mensaje);
	if(fnAccionPosteriorAlCierre) {
		$('#btnPopupAceptarMensaje').click(fnAccionPosteriorAlCierre);
	}
}

/**********************************************************************
 * rmunoz: Muestra mensaje de advertencia.(Con un boton de Aceptar).
 * La funcion opcional permite a�adir una acci�n tras cerrar el dialog.
 * @EstandarAtenea
 **********************************************************************/
function mostrarMensajeAdvertencia(mensaje,fnAccionPosteriorAlCierre) {
	mostrarPopupConfirmacionConAltura(mensaje);
	if(fnAccionPosteriorAlCierre) {
		$('#btnPopupAceptarMensaje').click(fnAccionPosteriorAlCierre);
	}
}

/**********************************************************************
 * rmunoz: Muestra mensaje de confirmacion.(Con boton Aceptar y Cancelar).
 * El boton ACEPTAR es devuelto, a fin de establecerle una funci�n personalizada.
 * El bot�n CANCELAR solo cierra este dialogo.
 * @EstandarAtenea
 **********************************************************************/
function mostrarMensajeConfirmacion(mensaje,titulo) {
	var listaBoton = new Array();
	var lBoton = new Object();
	lBoton.id ='btnAceptarMensaje';
	lBoton.value ='ACEPTAR';
	lBoton.onclick = "$( '#jqDialogConfirmacion').dialog( 'destroy')";
	listaBoton.push(lBoton);
	lBoton = new Object();
	lBoton.id ='btnCancelarMensaje';
	lBoton.value ='CANCELAR';
	lBoton.onclick = "$( '#jqDialogConfirmacion').dialog( 'destroy')";
	listaBoton.push(lBoton);			
	mostrarMensajeConAlturaBotonera('jqDialogConfirmacion', titulo,
	CONSTANTE.MENSAJE.CONFIRMACION.TAMANO, mensaje, 
	listaBoton);
	
	$botonAceptar = $('#btnAceptarMensaje','#jqDialogConfirmacion');
	return $botonAceptar;
}

/**********************************************************************
 * rmunoz: Muestra mensaje al denegar la acci�n solicitada.
 * La denegaci�n implica que la acci�n no se ejecutar�. Por ello solo
 * se cierra el dialog al aceptar la condici�n de restricci�n.
 * @EstandarAtenea
 **********************************************************************/
function mostrarMensajeDenegacion(mensaje) {
	mostrarMensajeModalSimple(mensaje,ERP.TITULO_DENEGACION_ACCION);
}


/***
 * rmunoz: Plugin para selectores multiples.
 * 
 */
$.fn.ateneaMultipleList = function(){
	return this.each(function(){
		var this$ = $(this);
		this$.not(" .selecter_callback,  .selecter_test").selecter();
		$(" select.selecter_callback").selecter({
			callback: selectCallback
		});

		// Disabled
		$(" .toggle_selecter").on("click", toggleEnabled);
		if (!$(" .selecter_disabled").is(":disabled")) {
			$(" .toggle_selecter").text("Disable");
		} else {
			$(" .toggle_selecter").text("Enable");
		}

		// Disabled Option
		$(" .toggle_selecter_option").on("click", toggleEnabledOption);
		if (!$(" .selecter_disabled_option option").eq(0).is(":disabled")) {
			$(" .toggle_selecter_option").text("Disable Option 'One'");
		} else {
			$(" .toggle_selecter_option").text("Enable Option 'One'");
		}
		//Fix mensajes de validaci�n.
		$('<div class="mensajeError" style="color:red">').appendTo(this$.parent());
		this$.change(function(){this$.valid();});
	});
}; 

function selectCallback(value, index) {
	$(" .callback_output").prepend("<p>VALUE: " + value + ", INDEX: " + index + "</p>");
}
function toggleEnabled(e) {
	e.preventDefault();

	if ($(" .selecter_disabled").is(":disabled")) {
		$(" .selecter_disabled").selecter("enable");
		$(" .toggle_selecter").text("Disable");
	} else {
		$(" .selecter_disabled").selecter("disable");
		$(" .toggle_selecter").text("Enable");
	}
}

function toggleEnabledOption(e) {
	e.preventDefault();
	if ($(" .selecter_disabled_option option").eq(0).is(":disabled")) {
		$(" .selecter_disabled_option").selecter("enable", "1");
		$(" .toggle_selecter_option").text("Disable Option 'One'");
	} else {
		$(" .selecter_disabled_option").selecter("disable", "1");
		$(" .toggle_selecter_option").text("Enable Option 'One'");
	}
}

/**
 * rmunoz: Soporte delay en keyUp
 */
var delay = (function(){
	  var timer = 0;
	  return function(callback, ms){
	    clearTimeout (timer);
	    timer = setTimeout(callback, ms);
	  };
})();

/**
 * Crea un div que reemplaza temporalmente el contenido 
 * expuesto por la vista principal Main.(la cual queda oculta, y se protegen los id's de sus formularios).
 * El div carga el contenido obtenido desde la url.
 * Para salir de esta vista, llamar a:
 * reestablecerDivMain();
 */
function suplantarDivMain(url) {
	
	$('input,textarea,select,form,table').attr('id', function() {
		$(this).data('id', $(this).attr('id'));
		return $(this).attr('id') + '_main'; 
	});
	
	var clon = $('<div>');
	clon.addClass('contenedorTemporal').insertAfter($('#contentMain')).load(url);
	$('#contentMain').hide();
	clon.data("fnCancelar",function(){
		clon.remove();
	});
}

/**
 * Reestablece el contenido ocultado en el div Main.
 * La copia que reemplazo el Main es removida por completo.
 */
function reestablecerDivMain(url) {
	if($('.contenedorTemporal').length > 0) {
		
		$('.contenedorTemporal').data('fnCancelar')();
		$('input,textarea,select,form,table').attr('id', function() { 
			return $(this).data('id');
		});
		
		if (url) {
			$('#contentMain').load(url);
		}
		
		$('#contentMain').show();
	}
}

/**
 * Permite actualizar la data externamente, 
 * 
 */
function habilitarEdicionExterna(elemento,fnActualizarData) {
	elemento.bind('seActualizoExternamente', function(event,coordenada){
		fnActualizarData(coordenada);
	});
}

/**
 * Ejecuta consulta ajax bajo par�metros est�ndar.
 * 	method : String POST o GET.
 * 	url : relativa o absoluta.
 * 	params : objeto a enviar como parametro
 * 	fnCallback : funcion ejecutada tras respuesta de �xito del servidor.
 * 	return objeto request ajax (xhr).
 */
function ejecutarConsultaAjax(method,url, params, fnCallback) {
	return $.ajax({
		type : method,
		url : url,
		data : params,
		success : fnCallback
	});
}

/**********************************************************************
 * frivas: retorna los checkbox seleccionados.
 **********************************************************************/

  function obtenerCheckBoxMarcados(nombreObjeto){
    var checked = [];
	$("input[name='"+nombreObjeto+"']:checked").each(function() {
		checked.push(parseInt($(this).val()));
	});
   return checked;
  }

/**********************************************************************
 * frivas: recarga la grilla por nombre.
 **********************************************************************/
function cargarGrillaPorNombre(table,data){
		    table.fnClearTable();
			table.fnAddData(data);
			table.fnDraw();
			table.fnPageChange('first');
}

/**********************************************************************
 * frivas: retorna true o false,dependiendo si al menos 1 radio button esta seleccionado.
   nota:Los radioButton del formulario tiene que tener un value asignado y tener el mismo nombre.
 **********************************************************************/

function validarSeleccionRadioButton(nombre,form){
	var valor=$('input[name='+nombre+']:checked', '#'+form).val();
	return validarValor(valor);
}

/**********************************************************************
 * fayala: M�todo que nos permitir� establecer mensajes de errores
 * est�ticos, es decir fuera de las reglas de validaci�n del Jquery
 * Validation, estos servir�n mayormente para objetivos como toda una 
 * secci�n de Divs, o una Bandeja por ejemplo.
 * 
 * idObjetivo: Es el ID del elemento que queremos evaluar para mostrar
 * el mensaje de error encima de est� secci�n.
 * 
 * mensaje: Es el mensaje de error a mostrar.
 **********************************************************************/
function establecerMensajeError(idObjetivo, mensaje, elemPosicion){
	limpiarMensajeError(idObjetivo);
	var divError = $('<div class="mensajeError">');
	var labelError = $('<label class="error" id="'+idObjetivo+'_error">');
	
	labelError.html(mensaje);
	divError.append(labelError);
	divError.insertBefore($('#'+idObjetivo));
	labelError.data("fnLimpiar", function(){
		divError.remove();
	});
	
	var container = $('div.contenido-scroll');
	if($('div.contenido-scroll').length == 0) {
		container = $('body');
	}
	
	var isDefault = true;
	var scrollTo  = $('#'+idObjetivo+'_error');
	if (validarValor(elemPosicion)) {
		if (elemPosicion == 'top') {
			isDefault = false;
			container.scrollTop(0);
		} else if ($('#'+elemPosicion).length > 0) {
			scrollTo = $('#'+elemPosicion);
		}
	}
	
	if (isDefault) {
//		container.animate({
//		    scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
//		});
		
		container.scrollTop(
		    scrollTo.offset().top - container.offset().top + container.scrollTop()
		);
	}
}

/*************************************************************************
 * fayala: M�todo que nos permitir� eliminar el mensaje de error que 
 * hayamos establecido anteriormente en alguna validaci�n, haciendo
 * uso del ID que asignamos para su creaci�n.
 * 
 * idObjetivo: Es el ID del elemento que hemos evaluado del cual se hace
 * uso de su ID m�s una concatenaci�n para formar el ID del mensaje error. 
 *************************************************************************/
function limpiarMensajeError(idObjetivo){
	if($('#'+idObjetivo+'_error').length > 0) {
		$('#'+idObjetivo+'_error').data('fnLimpiar')();
	}
}

function eliminarDivExistentePorClase(nombre){
	$('.'+nombre).each(function(key, element){
		$(element).remove();
	});
}

function eliminarDialogExistentePorId(idDialog){
	$('#'+idDialog).each(function(key, element){
		$(element).dialog('destroy');
	});
}

function cargarPerfil(id){
	$("#idPerfil").val(id);
	$("#formHome").submit();
}


function responsive_jqgrid(jqgrid) {
	jqgrid.find('.ui-jqgrid').addClass('clear-margin span12').css('width',
			'');
	jqgrid.find('.ui-jqgrid-view').addClass('clear-margin span12').css(
			'width', '');
	jqgrid.find('.ui-jqgrid-view > div').eq(1).addClass(
			'clear-margin span12').css('width', '').css('min-height', '0');
	jqgrid.find('.ui-jqgrid-view > div').eq(2).addClass(
			'clear-margin span12').css('width', '').css('min-height', '0');
	jqgrid.find('.ui-jqgrid-sdiv').addClass('clear-margin span12').css(
			'width', '');
	jqgrid.find('.ui-jqgrid-pager').addClass('clear-margin span12').css(
			'width', '');
}


function eliminarRow(grilla,selRowId){
	 $('#'+grilla).jqGrid('delRowData', selRowId);
}

function calcularPrecioSoles(precioDolar,tc){
	
	return eval(eval(precioDolar)*eval(tc)).toFixed(2);
}


function obtenerUltimoIdGrid(grid){
	var idsLista = $("#"+grid).jqGrid('getDataIDs');
	var id=0;
	if(idsLista.length>0){
       id=idsLista[(eval(idsLista.length)-1)];
	}

	return  id;
}



function replicar(obj1,obj2){
	$("#"+obj2).val(obj1.value);
}




function cargarImagenProducto(input,imagen){


	var param = '?'+new Date().getTime();
	if(imagen != null){
		param += '&imagen=' + imagen;
	}
	
	var lUrl  = gPath+'/productoController/cargarImagen'+param;
	$('#'+input).attr("src", lUrl).on("load", function() {

	});
}




