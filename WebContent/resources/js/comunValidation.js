// Inicio - Reglas de Validacion propias

jQuery.validator.addMethod("obligatorio_de_grupo", function(value, element,
		options) {
	var numberRequired = options[0];
	var selector = options[1];
	var fields = $(selector, element.form);
	var filled_fields = fields.filter(function() {
		// it's more clear to compare with empty string
		return $(this).val() != "";
	});
	var empty_fields = fields.not(filled_fields);
	// we will mark only first empty field as invalid
	if (filled_fields.length < numberRequired && empty_fields[0] == element) {
		return false;
	}
	return true;
}, jQuery.format("Please fill at least {0} of these fields."));

jQuery.validator.addMethod("exactlength", function(value, element, param) {
	return this.optional(element) || value.length == param;
}, jQuery.format("Por favor ingrese {0} d�gitos"));

jQuery.validator.addMethod("maximoEntero", function(value, element, param) {
	return this.optional(element) || parseInt(value) <= param;
}, jQuery.format("Por favor ingrese {0} d�gitos"));

jQuery.validator.addMethod("letra", function(value, element) {
	return this.optional(element) || /^[a-z\u00F1\s]+$/i.test(value);
}, "Letras y espacio solo permitidos");

jQuery.validator.addMethod("alfanumerico", function(value, element) {
	return this.optional(element) || /^[a-z0-9\u00F1\s]+$/i.test(value);
}, "Letras, numero y espacio solo permitidos");

jQuery.validator.addMethod("texto", function(value, element) {
	return this.optional(element) || /^[a-z0-9\u00F1\/_+\s-]+$/i.test(value);
}, "Letras, numero, espacio y los siguientes caracteres '+, -, _, /' son permitidos");

jQuery.validator.addMethod("igualA", function(value, element, param) {
	var target = $(param);
	if (this.settings.onfocusout) {
		target.unbind(".validate-equalTo").bind("blur.validate-equalTo",
				function() {
					$(element).valid();
				});
	}

	return value.toUpperCase() === target.val().toUpperCase();

}, jQuery.format("Repetir el mismo valor de nuevo"));

jQuery.validator.addMethod("diferenteA", function(value, element, param) {
	  var target = $(param);
	  if (value) {
		  return value.toUpperCase() != target.val().toUpperCase();
	  }  else {
		  return this.optional(element);
	  }
}, "El valor debe ser diferente.");

jQuery.validator.addMethod("formato", function(value, element, param) {
	var re = new RegExp(param, "i");
	return this.optional(element) || re.test(value);
}, "El formato es incorrecto.");

jQuery.validator.addMethod("fecha", function(value, element) {
	var check = false;
	var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
	if (re.test(value)) {
		var adata = value.split('/');
		var gg = parseInt(adata[0], 10);
		var mm = parseInt(adata[1], 10);
		var aaaa = parseInt(adata[2], 10);
		var xdata = new Date(aaaa, mm - 1, gg);
		if ((xdata.getFullYear() === aaaa) && (xdata.getMonth() === mm - 1)
				&& (xdata.getDate() === gg)) {
			check = true;
		} else {
			check = false;
		}
	} else {
		check = false;
	}
	return this.optional(element) || check;
}, "Por favor ingrese una fecha correcta.");

jQuery.validator.addMethod("fechaHora", function(value, element) {
	var check = false;
	var re = /^\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}:\d{1,2}$/;
	if (re.test(value)) {
		
		var obj = value.split(' ');
		var date = obj[0].split('/');
		var dd = parseInt(date[0], 10);
		var mm = parseInt(date[1], 10);
		var aaaa = parseInt(date[2], 10);
		var time = obj[1].split(':');
		var hh = parseInt(time[0], 10);
		var mn = parseInt(time[1], 10);
		
		var xdata = new Date(aaaa, mm - 1, dd);
		xdata.setHours(hh, mn, 0);
		
		if ((xdata.getFullYear() === aaaa) && (xdata.getMonth() === mm - 1)
		&& (xdata.getDate() === dd) && (xdata.getHours() == hh)
		&& (xdata.getMinutes() == mn)) {
			check = true;
		} else {
			check = false;
		}
	} else {
		check = false;
	}
	return this.optional(element) || check;
}, "Por favor ingrese una fecha correcta.");

jQuery.validator.addMethod("hora", function(value, element) {
	
	if (!/^\d{1,2}:\d{1,2}$/.test(value)){
		return this.optional(element) || false;
	}
    
	var parts = value.split(':');
    if (parts[0] > 23 || parts[1] > 59){
    	return this.optional(element) || false;
    }
    
    return this.optional(element) || true;
}, "Por favor ingrese una hora correcta.");

jQuery.validator.addMethod("greaterThan", function(value, element, params) {

	re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

	if (re.test(value) && re.test($(params).val())) {
		return $(element).data("DateTimePicker").getDate() >= $(params).data("DateTimePicker").getDate();
	} else {
		return true;
	}

}, 'Must be greater than {0}.');

jQuery.validator.addMethod("lessThan", function(value, element, params) {
	re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
	if (re.test(value) && re.test($(params).val())) {
		return $(element).data("DateTimePicker").getDate() <= $(params).data("DateTimePicker").getDate();
	} else {
		return true;
	}

}, 'Must be less than {0}.');

jQuery.validator.addMethod("timeGreaterThan", function(value, element, params) {
	var re = /^\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}:\d{1,2}$/;
	if (re.test(value) && re.test($(params).val())) {
		return $(element).data("DateTimePicker").getDate() >= $(params).data("DateTimePicker").getDate();
	} else {
		return true;
	}

}, 'Must be greater than {0}.');

jQuery.validator.addMethod("timeLessThan", function(value, element, params) {
	var re = /^\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}:\d{1,2}$/;
	if (re.test(value) && re.test($(params).val())) {
		return $(element).data("DateTimePicker").getDate() <= $(params).data("DateTimePicker").getDate();
	} else {
		return true;
	}

}, 'Must be less than {0}.');


jQuery.validator.addMethod("notEqual", function(value, element, param) {
	var referencia = $('#'+param).val();
	if(referencia) {
		referencia = (''+referencia).toUpperCase();
	}
	if(value) {
		value = (''+value).toUpperCase();
	}
	var esValido = value != referencia;
	
  	return this.optional(element) || esValido;
}, "Por favor especifique un valor diferente.");
// Fin - Reglas de Validacion


//Inicio mecanismo auxiliar, mapeo validacion - clase css de marcado
$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
);

//*********************************************************/
vjsrequired = {
		required : true,
		messages : {
			required : ERP.MSG_VAL_OBLIGATORIO
	}};


function esRequerido(input,flag) {
	if(flag) {
		input.rules("add", vjsrequired);
	} else {
		input.rules("remove", "required");
	}
	input.valid();
}

/*
 * Considerar que validator.addMethod, registra 
 * Registrar clases para validacion usando prefijo vjs.
 * Utilizar div con class mensajeError al mismo nivel 
 * fnInvalidHandler  Implica acci�n previa a generar enfoque
 * 					por ejemplo hacer visible contenedor de error.
 * @Deprecated
 * Se verifico el soporte de Jquery Validation en base a estilos y atributos.
 * Se utilizar� una versi�n simplificada.
 */
function vincularValidacionEstilosJs(formId, fnSubmitHandler, fnInvalidHandler) {

	

	var validadorActual = $('#' + formId).validate({
		ignore : "input[type='file']",//Aplicar esta clase cuando se desea deshabilitar la validacion dinamicamente.
		errorPlacement : function(error, element) {
			if(element.siblings("div.mensajeError").length == 0) {
				$('<div class="mensajeError" style="color:red">').insertAfter(element);
			}
			if(element.attr("type") == "file" ) {
			} else {
				error.appendTo(element.siblings("div.mensajeError"));
			}
		},
		onfocusout: function(element) {
			$(element).valid();
		},
		onkeyup: function(element) {
			$(element).valid();
		},
		invalidHandler: function(form, validator) {
			
			var errors = validator.numberOfInvalids();
		    if (errors) {
		    	if(fnInvalidHandler) {
					fnInvalidHandler(validator.errorList[0]);
				} 
		        validator.errorList[0].element.focus();
			}
	    },
		submitHandler : function(form, event) {
			event.preventDefault();
			if($(form).valid()) {
				fnSubmitHandler();
			}
		}
	});

	$('#' + formId + ' .vjsrequired').each(function(index){
		$(this).rules("add",vjsrequired);
	});
	
	$('#' + formId + ' .vjstext').each(function(index){
		$(this).rules("add",{
			texto : true,
			messages : {
			texto : "Texto requerido"
		}
		});
	});
	
	$('#' + formId + ' .vjsnumerico').each(function(index){
		$(this).rules("add",{
			regex : /^\d+$/,
			messages : {
				regex : ERP.MSG_VAL_ENTERO
			}
		});
	});
	
	
	
	/*
	 * vjsdecimal: Validaci�n decimal (Basado en regexp)
	 */
	$('#' + formId + ' .vjsdecimal').each(function(index){
		$(this).rules("add",{
			regex : ERP.PATRON_DECIMAL,
			messages : {
				regex : ERP.MSG_VAL_DECIMAL
		}
		});
	});
	
	/*
	 * vjsmaxlength: Longitud m�xima permitida (en car�cteres)
	 */
	$('#' + formId + ' *[class*=vjsmaxlength]').each(function(index){
		var maxlengthClass = $(this).attr('class').match(/vjsmaxlength[\d]+/);
		var currentMaxLenght = maxlengthClass[0].substring(12);
		$(this).rules("add",{
			maxlength : currentMaxLenght,
			messages : {
				maxlength : ERP.MSG_VAL_MAXIMO_CARACTERES
		}
		});
	});
	
	/*
	 * vjsminlength: Longitud m�nima permitida (en car�cteres)
	 */
	$('#' + formId + ' *[class*=vjsminlength]').each(function(index){
		var minlengthClass = $(this).attr('class').match(/vjsminlength[\d]+/);
		var currentMinLength = minlengthClass[0].substring(12);
		$(this).rules("add",{
			minlength : currentMinLength,
			messages : {
				minlength : ERP.MSG_VAL_MINIMO_CARACTERES
		}
		});
	});
	
	/*
	 * vjsminvalue: Valor minimo num�rico
	 */
	$('#' + formId + ' *[class*=vjsminvalue]').each(function(index){
		var minClass = $(this).attr('class').match(/vjsminvalue[\d]+/);
		var currentMin = minClass[0].substring(11);
		$(this).rules("add",{
			min : currentMin,
			messages : {
				min : "Valor M&iacute;nimo: " + currentMin
		}
		});
	});
	
	/*
	 * vjsmaxvalue: Valor minimo num�rico
	 */
	$('#' + formId + ' *[class*=vjsmaxvalue]').each(function(index){
		var maxClass = $(this).attr('class').match(/vjsmaxvalue[\d]+/);
		var currentMax = maxClass[0].substring(11);
		$(this).rules("add",{
			maximoEntero : currentMax,
			messages : {
				maximoEntero : "Valor m&aacute;ximo: " + currentMax
		}
		});
	});
	
	
	/*
	 * Valida formato de fecha simple.
	 */
    $('#' + formId + ' *[class*=vjsfecha]').each(function(index){

			$(this).rules("add",{
			fecha : $(this).val(),
			messages : {
				fecha : ERP.MSG_VAL_FECHA
		}
		});
	});
    
    /*
     * Valida formato de fecha y hora simple.
     */
    $('#' + formId + ' *[class*=vjsTimestamp]').each(function(index){
    	$(this).rules("add",{
    		fechaHora : $(this).val(),
    		messages : {
    			fechaHora : ERP.MSG_VAL_FECHA
    		}
    	});
    });
    
    
	return validadorActual;
}