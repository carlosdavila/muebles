function configurarGestionExamen(paginado) {

	$('#btnGestionExamenNuevo').click(function(event) {
		mostrarNuevoEnGestionExamen();
	});

	vincularValidacionEstilosJs("formGestionExamenBuscar",
			buscarEnGestionExamen);
	
	$("#listGestionExamen")
	.jqGrid(
			{
				mtype : "GET",
				datatype : "json",
				url : '${urlListaFiltrada}',
				height : 300,
				rowNum : 180,
				
				colNames : [ 'Id', 'CODIGO',
						'NOMBRE', 'NOTA', 'ACCIONES' ],
				colModel : [
						{
							name : 'examenId',
							index : 'examenId',
							width : 70,
							sorttype : "int",
							editable : false,
							hidden : true,
							sortable : false
						},
						{
							name : 'examenCodigo',
							index : 'examenCodigo',
							width : 220,
							editable : false,
							sortable : true
						},
						{
							name : 'examenNombre',
							index : 'examenNombre',
							width : 90,
							editable : false,
							hidden : false,
							sortable : true
						},	{
							name : 'examenNota',
							index : 'examenNota',
							width : 90,
							editable : false,
							hidden : false,
							sortable : true
						},

						{
							name : 'examenId',
							index : 'examenId',
							width : 150,
							editable : true,
							formatter : function radio(
									cellValue,
									option,
									rowObject) {

								return '<a onclick="mostrarEditarEnGestionExamen('
										+ rowObject.examenId
										+ ')"><img src="'+gPath+'/resources/img/editar.png"></a>'
										+'<span>   </span>'
										+ '<a onclick="validarEliminarEnGestionExamen('
										+ rowObject.examenId
										+ ')"><img src="'+gPath+'/resources/img/eliminar.png"></a>';

							}
						}

				],
				pager : "#plistGestionExamen",
				rowNum : paginado,
				viewrecords : true,
				grouping : false,
				gridComplete : function() {
				},
				caption : '',
				beforeRequest : function() {
					responsive_jqgrid($(".jqGrid"));
				}
			});

$("#listGestionExamen").jqGrid('navGrid',
	"#plistGestionExamen", {
		edit : false,
		add : false,
		del : false,
		search : false,
		refresh : false
	});
}
function buscarEnGestionExamen() {
	$("#listGestionExamen").jqGrid('clearGridData');
	$("#listGestionExamen").jqGrid(
			'setGridParam',
			{
				url :  gPath + '/examenController/obtenerListaFiltrada' + '?valor='
						+ $("#txtGestionExamenBuscar").val() + '&columna='
						+ $("#cboGestionExamenColumna").val()
			});
	$("#listGestionExamen").trigger('reloadGrid');

}

function mostrarNuevoEnGestionExamen() {

	var lUrl = gPath + '/examenController/mostrarNuevoExamen';
	//var titulo = "Nuevo Examen";
	//mostrarMensajeConHtmlAltura('divDialogNuevoExamen', titulo,
	//		CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);
	   $("#contentMain").load(lUrl);

}

function mostrarEditarEnGestionExamen(examenId) {

	var lUrl = gPath + '/examenController/mostrarEditarExamen?examenId='
			+ examenId;
	var titulo = "Editar Examen";
	mostrarMensajeConHtmlAltura('divDialogEditarExamen', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function validarEliminarEnGestionExamen(examenId) {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_ELIMINACION, 'Elimina Examen');
	aceptar$.click(function() {
		eliminarEnGestionExamen(examenId);
	});
};

function eliminarEnGestionExamen(examenId) {
	$
			.ajax({
				type : 'get',
				url : gPath+'/examenController/eliminar?examenId='
						+ examenId,
				success : function(mensaje) {
					  if(mensaje!=""){
						  mostrarMensajeError(mensaje);
		                 }
					buscarEnGestionExamen();
				}
			});
}