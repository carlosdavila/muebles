function configurarNuevoExamen() {

	vincularValidacionEstilosJs("formNuevoExamen",
			validarGuardarNuevoExamen);



	$("#btnNuevoExamenCancelar").click(function() {
		
		var lUrl = gPath + '/clienteController/mostrarGestionCliente';
		//$("#divDialogNuevoExamen").dialog("close");
	});
}


function validarGuardarNuevoExamen() {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Guardar Registro de Examen');
	aceptar$.click(function() {
		guardarNuevoExamen();
	});
}


function guardarNuevoExamen() {
	var form = $('#formNuevoExamen');
	$
			.ajax({
				type : form.attr('method'),
				url : form.attr('action'),
				data : form.serialize(),
				success : function(data) {
				
					$("#divDialogNuevoExamen").dialog(
							"close");
					var urlBandeja=gPath+'/preguntaController/mostrarGestionPregunta?examenId='+data;
					  $("#contentMain").load(urlBandeja);
				}
			});
}