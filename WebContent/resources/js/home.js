
function configurarMenu(){
	

	setTimeout(moverDiapositivaCabecera,2000);
	function moverDiapositivaCabecera(){
	$("#myCarouselCabecera").carousel("next");
	setTimeout(moverDiapositivaCabecera,2000);
	};
	
    $('[data-toggle="tooltip"]').tooltip(); 
	vincularValidacionEstilosJs("formListarProductos",
			mostrarProductosSliderPorBuscar);

	$('ul.dropdown-menu [data-toggle=dropdown]').on(
			'click',
			function(event) {
				event.preventDefault();
				event.stopPropagation();
				$(this).parent().siblings().removeClass(
						'open');
				$(this).parent().toggleClass('open');
			});
	$("#menu_erp")
			.on(
					'click',
					'a',
					function() {

						var idSubMenuItem = "";

						var urlBandeja = "";
						idSubMenuItem = (this.id);

						switch (idSubMenuItem) {
						case 'idLogin':
							urlBandeja = '/usuarioController/mostrarLogin';
							break;

						case 'idPoliticas':
							urlBandeja = '/mostrarPoliticas';
							break;
						case 'idNosotros':
							urlBandeja = '/mostrarNosotros';
							break;
						case 'rrr':
							urlBandeja = '/examenController/mostrarGestionExamen';
							break;
						case 'ee':
							urlBandeja = '/parametroController/mostrarGestionParametro';
							break;
						case 'idProducto':
							urlBandeja = '/modeloController/listarModeloVenta';
							break;

						case 'idConfigurar':
							urlBandeja = '/categoriaController/mostrarGestionCategoria';
							break;
						case 'idImagen':
							urlBandeja = '/';
							break;
						default:
							urlBandeja = '';
						}

						if (urlBandeja != '') {
							 
							 $('#formSalirSistema').attr('action',gPath + urlBandeja);
							 $("#txtPagina").val(1);
							 $('#formSalirSistema').submit();
							/*
							$("#contentMain").load(
									gPath + urlBandeja);
						*/
						}
					});
	setTimeout(obtenerTotal, 50);

}

	function mostrarMenuPrincipal(){
		
		urlBandeja = '/mostrarMenu';
		$("#idMenuPrincipal").load(gPath + urlBandeja);
	}


	
	function mostrarCarritoDetalle(){
	
		var urlBandeja = '/carritoDetalleController/mostrarGestionCarritoDetalle';
		$("#idPopup").load( gPath+urlBandeja);
	}
			

	
	function mostrarLogin(){
		
		var urlBandeja = '/usuarioController/mostrarLogin';
		$("#idPopup").load( gPath+urlBandeja);
	}
	
    function mostrarPaginaImagen(){
		
    	urlBandeja = '/imagen';
    	$("#contentMain").load(gPath + urlBandeja);
	}



	function mostrarProductosSlider(modeloId,valor,pagina)
	{

					var urlBandeja =  gPath+'/productoController/listarProducto';
					
					$("#txtModeloId").val(modeloId);
					$("#txtValor").val(valor);
					$("#txtPagina").val(pagina);
					
					 $('#formSalirSistema').attr('action',urlBandeja);
					 $('#formSalirSistema').submit();
					
	}
	
	
	function mostrarModelosSlider(categoriaId,pagina)
	{

					var urlBandeja =  gPath+'/modeloController/listarModeloVenta';
	
					 $("#txtPagina").val(pagina);
					 $("#txtCategoriaId").val(categoriaId);
					 $('#formSalirSistema').attr('action',urlBandeja);
					 $('#formSalirSistema').submit();
					
	}
	
	

	
	function mostrarProductosSliderPorBuscar()
	{
     var txtProducto=$("#txtBuscarProducto").val();
    
     mostrarProductosSlider(0,txtProducto,1);
	
	}


 
	function salirSistema() {
		
		$.ajax({
			type : 'post',
			url : gPath+'/usuarioController/salir',
			
			success : function(data) {
				$('#formSalirSistema').attr('action', gPath);
				$("#formSalirSistema").submit();
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				
			}
		});
	}
	
    function mostrarPaginaMantenimiento(){
		
    	urlBandeja = gPath+'/categoriaController/mostrarGestionCategoria';
    	$("#contentMain").load( urlBandeja);
	}
    
    
    function obtenerTotal(){
    	
    	$
    	.ajax({
    		type : 'post',
    		url : gPath+'/carritoDetalleController/obtenerTotal',
    		success : function(data) {
    			 
    						 $('#spanCarritoTotal').html("$"+ data);
    				         $('#spanCarritoTotal1').html("$"+ data);
    		
    		}
    	});
    	
    }
    
    


    

    
    function cargarImagenModelo(input,imagen){


    	var param = '?'+new Date().getTime();
    	if(imagen != null){
    		param += '&imagen=' + imagen;
    	}
    	
    	var lUrl  = gPath+'/productoController/cargarImagen'+param;

    	$('#'+input).css('background-image', 'url(' + lUrl + ')');
    	
    }

    
    

    function agregarCarrito(productoId,cantidad){
    	
    	$
    	.ajax({
    		type : 'post',
    		url : gPath+'/carritoDetalleController/agregar?productoId='
    				+ productoId+'&cantidad='+cantidad,
    		success : function(data) {
    			 var respuesta=data.split(',');
    		
    					if("false"==respuesta[0]){
    				swal(   'Se agrego al carrito!',
    						  '',
    						  'success'
    						)
    						 $('#spanCarritoTotal').html("$"+ respuesta[1]);
    				         $('#spanCarritoTotal1').html("$"+ respuesta[1]);
    			}else{
    				swal("Ya existe este producto en el carrito!")
    			}
    		}
    	});
    	
    }
