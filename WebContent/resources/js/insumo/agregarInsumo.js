function configurarAgregarInsumo(paginado) {
	 
	
	vincularValidacionEstilosJs("formGestionInsumoBuscar",
			buscarEnAgregarInsumo);
	$("#listGestionInsumoAgregar")
			.jqGrid(
					{
						mtype : "GET",
						datatype : "json",
						url : gPath+'/insumoController/obtenerListaFiltrada',
						height : 300,
						rowNum : 180,
						colNames : [ 'Id','NOMBRE','STOCK', 'UM',
								'P.SOLES', 'FORMULA',
								'ACCIONES' ],
						colModel : [
								{
									name : 'insumoId',
									index : 'insumoId',
									width : 70,
									sorttype : "int",
									editable : false,
									hidden : true,
									sortable : false
								},
							
								{
									name : 'insumoNombre',
									index : 'insumoNombre',
									width :150,
									editable : false,
									sortable : true
								},
								{
									name : 'insumoStock',
									index : 'insumoStock',
									width : 100,
									editable : false,
									sortable : true
								},
								{
									name : 'insumoUM',
									index : 'insumoUM',
									width : 80,
									editable : false,
									sortable : true
								},
								{
									name : 'insumoPrecioSoles',
									index : 'insumoPrecioSoles',
									width : 80,
									editable : false,
									sortable : true
								},
								{
									name : 'insumoDescFormula',
									index : 'insumoDescFormula',
									width : 80,
									editable : false,
									sortable : true
								},

								{
									name : '',
									index : '',
									width : 80,
									editable : true,
									formatter : function radio(
											cellValue,
											option,
											rowObject) {
										var cadena = "";

										cadena = '<input type="checkbox" onclick="seleccionarInsumoAproducto('
												+ option.rowId
												+ ')" id="checkAgregarInsumo_'
												+ option.rowId
												+ '" />';

										return cadena;
										
									}
								}

						],
						pager : "#plistGestionInsumoAgregar",
						rowNum : paginado,
						viewrecords : true,
						grouping : false,
						gridComplete : function() {
														
							var idsLista = $("#listGestionInsumoAgregar").jqGrid('getDataIDs');
							for (var i = 0; i < idsLista.length; i++) {
								var rowIdLista=idsLista[i];
								var rowGestionInsumo = $("#listGestionInsumoAgregar").jqGrid('getRowData',rowIdLista);
						        var idsDetalle = $("#listProductoDetalle").jqGrid('getDataIDs');
								for (var j = 0; j < idsDetalle.length; j++) {
									var rowIdDetalle=idsDetalle[j];
									var rowProductoDetalle = $("#listProductoDetalle").jqGrid('getRowData',rowIdDetalle);
								           if(rowGestionInsumo['insumoId']==rowProductoDetalle['insumoBean.insumoId']){
			                                      $("#checkAgregarInsumo_"+rowIdLista).prop('checked',true);
			                                  }
			                                  
			                              };
			                       
			                       };
						},
						caption : '',
						beforeRequest : function() {
							responsive_jqgrid($(".jqGrid"));
						}
					});

	$("#listGestionInsumoAgregar").jqGrid('navGrid',
			"#plistGestionInsumoAgregar", {
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : false
			});
};

function seleccionarInsumoAproducto(selRowId) {

	var check = $("#checkAgregarInsumo_" + selRowId).is(':checked');
	
	var myGrid = $('#listGestionInsumoAgregar');
	var insumoId = myGrid.jqGrid('getCell', selRowId, 'insumoId');
	
	var insumoNombre = myGrid.jqGrid('getCell', selRowId,
			'insumoNombre');

	var insumoStock = myGrid.jqGrid('getCell', selRowId,
	'insumoStock');
	
	var insumoPrecio = myGrid.jqGrid('getCell', selRowId,
			'insumoPrecioSoles');
	var insumoDescFormula= myGrid.jqGrid('getCell', selRowId,
	'insumoDescFormula');
	var insumoUM = myGrid.jqGrid('getCell', selRowId,
	'insumoUM');

	if (check) {
		var myData = [ {
			"insumoBean.insumoId" : insumoId,
			"insumoBean.insumoNombre" : insumoNombre,
			"insumoBean.insumoUM" : insumoUM,
			"insumoBean.insumoStock" : insumoStock,
			"productoDetallePrecio" : insumoPrecio,
			"insumoBean.insumoDescFormula" : insumoDescFormula,
			"productoDetalleCantidad" : "",
			"productoDetalleTotal" : "",
			"insumoBean.insumoId" : insumoId
		} ];
		
		var mayorId=obtenerUltimoIdGrid("listProductoDetalle");
		$("#listProductoDetalle").jqGrid('addRowData',(eval(mayorId)+1), myData[0]);
		
	}else{

		validarEliminarEnAgregarInsumo(selRowId,insumoId);
	
	}
}

function validarEliminarEnAgregarInsumo(rowId,id) {
    $("#checkAgregarInsumo_"+rowId).prop('checked',true);
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_ELIMINACION, 'Elimina Insumo');
	aceptar$.click(function() {
		 $("#checkAgregarInsumo_"+rowId).prop('checked',false);
		eliminarInsumoEnProducto(id);
     });
	
};

function buscarEnAgregarInsumo() {
	$("#listGestionInsumoAgregar").jqGrid('clearGridData');
	$("#listGestionInsumoAgregar").jqGrid(
			'setGridParam',
			{
				url :  gPath+'/insumoController/obtenerListaFiltrada'
				+ '?valor='
						+ $("#txtGestionInsumoBuscar").val() + '&columna='
						+ $("#cboGestionInsumoColumna").val()
			});
	$("#listGestionInsumoAgregar").trigger('reloadGrid');

}

