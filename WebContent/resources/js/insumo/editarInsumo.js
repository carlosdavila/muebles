function configurarEditarInsumo() {
	habilitarInsumoTipo();
	//mostrarPrecioSolesEnEditarInsumo() ;
	vincularValidacionEstilosJs("formEditarInsumo",
			validarGuardarEditarInsumo);

	$("#btnEditarInsumoCancelar").click(function() {
		$("#divDialogEditarInsumo").dialog("close");
	});
	/*
	$("#insumoPrecioDolar").change(function() {
		calcularPrecioSolesEditarInsumo();
	});
	*/
	$("#insumoTipo").change(function() {
		habilitarInsumoTipo();
	});
};

function mostrarPrecioSolesEnEditarInsumo() {
	if ($("#insumoPrecioDolar").val() != "") {
		$("#insumoPrecioSoles").attr("disabled", true);
	} else {
		$("#insumoPrecioSoles").attr("disabled", false);
	}

}


function calcularPrecioSolesEditarInsumo() {
	if ($("#insumoPrecioDolar").val() != "") {
		$("#insumoPrecioSoles").val(calcularPrecioSoles($("#insumoPrecioDolar").val(), TC_DOLAR));
		$("#insumoPrecioSoles").attr("disabled", true);
	} else {
		$("#insumoPrecioSoles").val("");
		$("#insumoPrecioSoles").attr("disabled", false);
	}

}

function validarGuardarEditarInsumo() {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Edicion Registro de Insumo');
	aceptar$.click(function() {
		guardarEditarInsumo();
	});
}
;

function guardarEditarInsumo() {
	var form = $('#formEditarInsumo');
	$.ajax({
		type : form.attr('method'),
		url : form.attr('action'),
		data : form.serialize(),
		success : function(data) {
			$("#divDialogEditarInsumo").dialog("close");
			buscarEnGestionInsumo();
		}
	});
}