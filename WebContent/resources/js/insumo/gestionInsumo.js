function configurarGestionInsumo(paginado) {
	$('#btnGestionInsumoNuevo').click(function(event) {
		mostrarNuevoEnGestionInsumo();
	});
	vincularValidacionEstilosJs("formGestionInsumoBuscar",
			buscarEnGestionInsumo);
	$("#listGestionInsumo")
			.jqGrid(
					{
						mtype : "GET",
						datatype : "json",
						url : gPath +'/insumoController/obtenerListaFiltrada',
						height : 300,
						rowNum : 180,
						colNames : [ 'Id', 'NOMBRE','UM','STOCK',
								'P.SOLES',
								'ACCIONES' ],
						colModel : [
								{
									name : 'insumoId',
									index : 'insumoId',
									width : 70,
									sorttype : "int",
									editable : false,
									hidden : true,
									sortable : false
								},
								{
									name : 'insumoNombre',
									index : 'insumoNombre',
									width : 300,
									editable : false,
									sortable : true
								},
						
								{
									name : 'insumoUM',
									index : 'insumoUM',
									width : 150,
									editable : false,
									sortable : true
								},
					        	{
									name : 'insumoStock',
									index : 'insumoStock',
									width : 120,
									editable : false,
									sortable : true,
									formatter : function radio(cellValue,
											option, rowObject) {
										 var cadena='<a  href="#" onclick="agregarStock('
												+ rowObject.insumoId
												+ ')"><span style="color:blue">'
												+ rowObject.insumoStock
												+ '</span></a>';
                                        
										return cadena;

									}
								},
						
								{
									name : 'insumoPrecioSoles',
									index : 'insumoPrecioSoles',
									width : 100,
									editable : false,
									sortable : true
								},
							

								{
									name : 'insumoId',
									index : 'insumoId',
									width : 150,
									editable : true,
									formatter : function radio(
											cellValue,
											option,
											rowObject) {

										return '<a onclick="mostrarEditarEnGestionInsumo('
												+ rowObject.insumoId
												+ ')"><img src="'+gPath+'/resources/img/editar.png"></a>'
												+ '<span>   </span>'
												+ '<a onclick="validarEliminarEnGestionInsumo('
												+ rowObject.insumoId
												+ ')"><img src="'+gPath+'/resources/img/eliminar.png"></a>';

									}
								}

						],
					
						pager : "#plistGestionInsumo",
						rowNum : paginado,
						viewrecords : true,
						
						grouping : false,
						gridComplete : function() {
						},
						caption : '',
						beforeRequest : function() {
							responsive_jqgrid($(".jqGrid"));
						}
					});

	$("#listGestionInsumo").jqGrid('navGrid',
			"#plistGestionInsumo", {
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : false
			});
};






function buscarEnGestionInsumo() {
	jQuery("#listGestionInsumo").jqGrid('clearGridData');
	jQuery("#listGestionInsumo").jqGrid(
			'setGridParam',
			{
				url :  gPath +'/insumoController/obtenerListaFiltrada' + '?valor='
						+ $("#txtGestionInsumoBuscar").val() + '&columna='
						+ $("#cboGestionInsumoColumna").val()
			});
	jQuery("#listGestionInsumo").trigger('reloadGrid');

}

function mostrarNuevoEnGestionInsumo() {

	var lUrl = gPath + '/insumoController/mostrarNuevoInsumo';
	var titulo = "Nuevo Insumo";
	mostrarMensajeConHtmlAltura('divDialogNuevoInsumo', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function mostrarEditarEnGestionInsumo(insumoId) {

	var lUrl = gPath + '/insumoController/mostrarEditarInsumo?insumoId='
			+ insumoId;
	var titulo = "Editar Insumo";
	mostrarMensajeConHtmlAltura('divDialogEditarInsumo', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function validarEliminarEnGestionInsumo(insumoId) {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_ELIMINACION, 'Elimina Insumo');
	aceptar$.click(function() {
		eliminarEnGestionInsumo(insumoId);
	});
};

function eliminarEnGestionInsumo(insumoId) {
	$.ajax({
		type : 'get',
		url : gPath + '/insumoController/eliminar?insumoId='
				+ insumoId,
		success : function(mensaje) {
			  if(mensaje!=""){
				  mostrarMensajeError(mensaje);
                 }
			buscarEnGestionInsumo();
		}
	});
}


function agregarStock(insumoId){
	
	var lUrl = gPath + '/insumoController/mostrarAgregarStock?insumoId='
	+ insumoId;
var titulo = "";
mostrarMensajeConHtmlAltura('divDialogAgregarStock', titulo,
	CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);
	
}




function habilitarInsumoTipo() {
	if ($("#insumoTipo").val() == "2") {

		$("#divPlaca").show();
	} else {
		$("#divPlaca").hide();
	}
	
	if ($("#insumoTipo").val() == "4") {

		$("#divCantidadPlaca").show();
	} else {
		$("#divCantidadPlaca").hide();
	}

}

