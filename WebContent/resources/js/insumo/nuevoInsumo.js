function configurarNuevoInsumo() {
	vincularValidacionEstilosJs("formNuevoInsumo", validarGuardarNuevoInsumo);

	$("#btnNuevoInsumoCancelar").click(function() {
		$("#divDialogNuevoInsumo").dialog("close");
	});
/*
	$("#insumoPrecioDolar").change(function() {
		calcularPrecioSolesNuevoInsumo();
	});
	*/
	$("#insumoTipo").change(function() {
		habilitarInsumoTipo();
	});
	

};



/*
function calcularPrecioSolesNuevoInsumo() {
	if ($("#insumoPrecioDolar").val() != "") {
		$("#insumoPrecioSoles").val(calcularPrecioSoles($("#insumoPrecioDolar").val(), TC_DOLAR));
		$("#insumoPrecioSoles").attr("disabled", true);
	} else {
		$("#insumoPrecioSoles").val("");
		$("#insumoPrecioSoles").attr("disabled", false);
	}

}
*/
function validarGuardarNuevoInsumo() {
	var aceptar$ = mostrarMensajeConfirmacion(ERP.MSG_CONFIRMACION_GRABADO,
			'Guardar Registro de Insumo');
	aceptar$.click(function() {
		guardarNuevoInsumo();
	});
};

function guardarNuevoInsumo() {
	var form = $('#formNuevoInsumo');
	$.ajax({
		type : form.attr('method'),
		url : form.attr('action'),
		data : form.serialize(),
		success : function(data) {
			$("#divDialogNuevoInsumo").dialog("close");
			buscarEnGestionInsumo();
		}
	});
};