/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ES (Spanish; Español)
 */
(function ($) {
	$.extend($.validator.messages, {
		required: "Obligatorio.",
		remote: "Por favor, rellena este campo.",
		email: "Direcci\u00F3n inv\u00E1lido",
		url: "Por favor, escribe una URL v\u00E1lida.",
		date: "Fecha inv\u00E1lida.",
		dateISO: "Por favor, escribe una fecha (ISO) v\u00E1lida.",
		number: "N\u00FAmero inv\u00E1lido.",
		digits: "Ingresar s\u00F3lo d\u00EDgitos.",
		creditcard: "Por favor, escribe un n\u00FAmero de tarjeta v\u00E1lido.",
		equalTo: "Repetir el mismo valor de nuevo.",
		accept: "Por favor, escribe un valor con una extensi\u00F3n aceptada.",
		maxlength: $.validator.format("No escribir m\u00E1s de {0} caracteres."),
		minlength: $.validator.format("No escribir menos de {0} caracteres."),
		rangelength: $.validator.format("Escribir un valor entre {0} y {1} caracteres."),
		range: $.validator.format("Escribir un valor entre {0} y {1}."),
		max: $.validator.format("Escribir un valor menor o igual a {0}."),
		min: $.validator.format("Escribir un valor mayor o igual a {0}."),
		alphanumeric:"Letras, n\u00FAmeros y subgui\u00F3n son permitidos."
	});
}(jQuery));