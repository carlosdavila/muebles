function configurarEditarModelo() {

	
	vincularValidacionEstilosJs("formEditarModelo",
			validarGuardarEditarModelo);



	$("#btnEditarModeloCancelar").click(function() {
		$("#divDialogEditarCliente").dialog("close");
	});
}


function guardarEditarModelo() {

	var form = $('#formEditarModelo');
	$.ajax({
		type : form.attr('method'),
		url :gPath+'/modeloController/editar',
		data : form.serialize(),
		success : function(data) {
			
			$("#divDialogEditarModelo").dialog("close");
		
			
			buscarEnGestionModelo();
		},
		   error: function(xhr, status, error){
			   document.getElementById("mensajeErrorModelo").style.visibility = 'visible';
	        }
	});
}

function validarGuardarEditarModelo() {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Edicion Registro de Modelo');
	aceptar$.click(function() {
		guardarEditarModelo();
	});
}

