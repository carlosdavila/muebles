


function mostrarNuevoEnGestionModelo() {

	var lUrl = gPath + '/modeloController/mostrarNuevaModelo';
	var titulo = "Nuevo Modelo";
	mostrarMensajeConHtmlAltura('divDialogNuevaModelo', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}


function mostrarEditarEnGestionModelo(modeloId) {

	var lUrl = gPath + '/modeloController/mostrarEditarModelo?modeloId='
			+ modeloId;
	var titulo = "Editar Modelo";
	mostrarMensajeConHtmlAltura('divDialogEditarModelo', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function buscarEnGestionModelo() {

	
	
	$("#listGestionModelo").jqGrid('clearGridData');
	$("#listGestionModelo").jqGrid(
			'setGridParam',
			{
				url :  gPath + '/modeloController/obtenerListaFiltrada'  
			});
	$("#listGestionModelo").trigger('reloadGrid');


}


function validarEliminarEnGestionModelo(modeloId) {
	
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_ELIMINACION, 'Elimina Modelo');
	aceptar$.click(function() {
		eliminarEnGestionModelo(modeloId);
	});
};

function eliminarEnGestionModelo(modeloId) {
	$
			.ajax({
				type : 'get',
				url : gPath+'/modeloController/eliminar?modeloId='
						+ modeloId,
				success : function(mensaje) {
				
					  if(mensaje!=""){
						  mostrarMensajeError(mensaje);
		                 }
					buscarEnGestionModelo();
				}
			});
}





function modeloMoverFilas(categoriaId) {
	var flag=	isDiferenteOrdenModelos(categoriaId);
   
if(eval(flag)){
	var idModelos=obtenerIdsModelosOrdenas(idModelos);

	   $("#spin").show();
	
	$.ajax({
				type : 'get',
				url : gPath+'/modeloController/mover?idDiferentes='
						+ idModelos,
				success : function() {
					   $("#spin").hide();
					buscarEnGestionModelo();
				}
			});
}
	
}



function isDiferenteOrdenModelos(categoriaId){
   
    var flag=false;
    var idTable="#tableModelo_"+categoriaId+"_t";
	var ids = jQuery(idTable).jqGrid('getDataIDs');
		
		for (var i = 0; i < ids.length; i++) 
		{
			 
			  var rowId = ids[i];
			  var rowInicial=idsModeloInicial[i];
			  
			  var rowData = jQuery(idTable).jqGrid ('getRowData', rowId);
			  var rowDataInicial = jQuery(idTable).jqGrid ('getRowData', rowInicial);
				if(rowDataInicial.modeloId!= rowData.modeloId){
					flag=true;
				}
			
		}

	return flag;
	
}


function obtenerIdsModelosOrdenas(categoriaId){
	 var idModelos= new Array();

	var idTable="#tableModelo_"+categoriaId+"_t";
    
	var ids = jQuery(idTable).jqGrid('getDataIDs');
		
		for (var i = 0; i < ids.length; i++) 
		{
			 
			  var rowId = ids[i];
			  var rowInicial=idsCategoriaInicial[i];
			  var rowData = jQuery(idTable).jqGrid ('getRowData', rowId);
			  idModelos[i]=rowData.modeloId;
			
			
			
		}
return idModelos;
	
	
}




