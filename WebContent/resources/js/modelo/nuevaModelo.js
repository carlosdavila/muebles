function configurarNuevaModelo() {

	vincularValidacionEstilosJs("formNuevaModelo",
			validarGuardarNuevaModelo);


	$("#btnNuevaModeloCancelar").click(function() {
		$("#divDialogNuevaModelo").dialog("close");
	});
}


function validarGuardarNuevaModelo() {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Guardar Registro de modelo');
	aceptar$.click(function() {
		guardarNuevaModelo();
	});
}


function guardarNuevaModelo() {
	
	var categoriaId= $('#categoriaBean\\.categoriaId').val();
	
	var form = $('#formNuevaModelo');
	$
			.ajax({
				type : form.attr('method'),
				url : gPath+'/modeloController/guardar',
				data : form.serialize(),
				success : function(data) {
				
					$("#divDialogNuevaModelo").dialog(
							"close");
					buscarEnGestionModelo(categoriaId);
				},
			   error: function(xhr, status, error){
				   document.getElementById("mensajeErrorModelo").style.visibility = 'visible';
				
		        }
			});
}