function configurarEditarParametro() {
	vincularValidacionEstilosJs("formEditarParametro",
			validarGuardarEditarParametro);

	$("#btnEditarParametroCancelar").click(function() {
		$("#divDialogEditarParametro").dialog("close");
	});
}
function validarGuardarEditarParametro() {
	var aceptar$ = mostrarMensajeConfirmacion(ERP.MSG_CONFIRMACION_GRABADO,
			'Edicion Registro de Parametro');
	aceptar$.click(function() {
		guardarEditarParametro();
	});
};

function guardarEditarParametro() {
	var form = $('#formEditarParametro');
	$.ajax({
		type : form.attr('method'),
		url : form.attr('action'),
		data : form.serialize(),
		success : function(data) {
			$("#divDialogEditarParametro").dialog("close");
			buscarEnGestionParametro();
		}
	});
}