
function configurarGestionParametro(){
	vincularValidacionEstilosJs("formGestionParametroBuscar",
			buscarEnGestionParametro);
	$("#listGestionParametro")
			.jqGrid(
					{
						mtype : "GET",
						datatype : "json",
						url : gPath + '/parametroController/obtenerListaFiltrada',
						height : 300,
						rowNum : 180,
						colNames : [ 'Id', 'TIPO',
								'NOMBRE', 'ACCIONES' ],
						colModel : [
								{
									name : 'parametroId',
									index : 'parametroId',
									width : 70,
									sorttype : "int",
									editable : false,
									hidden : true,
									sortable : false
								},
								{
									name : 'parametroTipo',
									index : 'parametroTipo',
									width : 220,
									editable : false,
									sortable : false
								},
								{
									name : 'parametroNombre',
									index : 'parametroNombre',
									width : 90,
									editable : false,
									hidden : false,
									sortable : false
								},
								{
									name : 'parametroId',
									index : 'parametroId',
									width : 150,
									editable : true,
									sortable : false,
									formatter : function radio(
											cellValue,
											option,
											rowObject) {

										return '<a title="Editar" onclick="mostrarEditarEnGestionParametro('
												+ rowObject.parametroId
												+ ')"><img src="'+gPath+'/resources/img/editar.png"></a>';

									}
								}

						],
						pager : "#plistGestionParametro",
						rowNum : 1000,
						viewrecords : true,
						grouping : false,
						gridComplete : function() {
						},
						caption : '',
						beforeRequest : function() {
							responsive_jqgrid($(".jqGrid"));
						}
					});

	
}


function buscarEnGestionParametro() {
	
	$("#listGestionParametro").jqGrid('clearGridData');
	$("#listGestionParametro").jqGrid(
			'setGridParam',
			{
				url : gPath + '/parametroController/obtenerListaFiltrada'
			});
	$("#listGestionParametro").trigger('reloadGrid');

}



function mostrarEditarEnGestionParametro(parametroId) {

	var lUrl = gPath + '/parametroController/mostrarEditarParametro?parametroId='
			+ parametroId;
	var titulo = "Editar Parametro";
	mostrarMensajeConHtmlAltura('divDialogEditarParametro', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}


