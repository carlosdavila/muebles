function configurarGestionPedido(paginado) {

	mostrarFiltrosGestionPedido();

	$('#cboGestionPedidoColumna').click(function(event) {
		mostrarFiltrosGestionPedido();
	});

	$('#fechaInicio').datetimepicker({
		pickTime : false
	});
	$('#fechaFinal').datetimepicker({
		pickTime : false
	});
	$('#btnGestionPedidoNuevo').click(function(event) {
		mostrarNuevoEnGestionPedido();
	});
	vincularValidacionEstilosJs("formGestionPedidoBuscar",
			buscarEnGestionPedido);
	$("#listGestionPedido")
			.jqGrid(
					{
						mtype : "GET",
						datatype : "json",
						url : gPath + '/pedidoController/obtenerListaFiltrada',
						height : 300,
						rowNum : 180,
						colNames : [ 'Id', 'PEDIDO', 'CLIENTE', 'NRO ORDEN',
								'FECHA ORDEN','NRO FAC', 'FECHA FAC', 'NRO GUIA','FECHA GUIA',
								'ACCIONES' ],
						colModel : [
								{
									name : 'pedidoId',
									index : 'pedidoId',
									width : 70,
									sorttype : "int",
									editable : false,
									hidden : true,
									sortable : false
								},
								{
									name : 'pedidoNombre',
									index : 'pedidoNombre',
									width : 200,
									editable : false,
									sortable : true
								},

								{
									name : 'clienteBean.clienteNombre',
									index : 'clienteBean.clienteNombre',
									width : 150,
									editable : false,
									sortable : true
								},
								{
									name : 'pedidoNroOrden',
									index : 'pedidoNroOrden',
									width : 120,
									editable : false,
									sortable : true,
									formatter : function radio(cellValue,
											option, rowObject) {
										 var cadena="";
                                        if(rowObject.pedidoOrdenArchivo!=""){
                                          cadena='<a  href="#" onclick="exportarOrden('
												+ rowObject.pedidoId
												+ ')"><span style="color:blue">'
												+ rowObject.pedidoNroOrden
												+ '</span></a>';
                                        }else{
                                        	   cadena='<a >'
   												+ rowObject.pedidoNroOrden
   												+ '</a>';
                                        }
										return cadena;

									}
								},
								{
									name : 'pedidoFechaOrdenFormat',
									index : 'pedidoFechaOrdenFormat',
									width : 130,
									editable : false,
									sortable : true
								},
								{
									name : 'pedidoNroFactura',
									index : 'pedidoNroFactura',
									width : 100,
									editable : false,
									sortable : true,
									formatter : function radio(cellValue,
											option, rowObject) {

										return '<a  href="#" onclick="exportarFactura('
												+ rowObject.pedidoId
												+ ')"><span style="color:blue">'
												+ rowObject.pedidoNroFactura
												+ '</span></a>';

									}
								},
								{
									name : 'pedidoFechaFacFormat',
									index : 'pedidoFechaFacFormat',
									width : 100,
									editable : false,
									sortable : true
								},
								
								
								{
									name : 'pedidoNroGuia',
									index : 'pedidoNroGuia',
									width : 100,
									editable : false,
									sortable : true,
									formatter : function radio(cellValue,
											option, rowObject) {

										return '<a  href="#" onclick="exportarGuia('
												+ rowObject.pedidoId
												+ ')"><span style="color:blue">'
												+ rowObject.pedidoNroGuia
												+ '</span></a>';

									}
								},
								{
									name : 'pedidoFechaGuiaFormat',
									index : 'pedidoFechaGuiaFormat',
									width : 100,
									editable : false,
									sortable : true
								},
								{
									name : 'pedidoId',
									index : 'pedidoId',
									width : 100,
									editable : false,
									formatter : function radio(cellValue,
											option, rowObject) {

										return '<a onclick="mostrarEditarEnGestionPedido('
												+ rowObject.pedidoId
												+ ')"><img src="'
												+ gPath
												+ '/resources/img/editar.png"></a>'
												+ '<span>   </span>'
												+ '<a onclick="validarEliminarEnGestionPedido('
												+ rowObject.pedidoId
												+ ')"><img src="'
												+ gPath
												+ '/resources/img/eliminar.png"></a>';

									}
								}

						],
						pager : "#plistGestionPedido",
						rowNum : paginado,
						viewrecords : true,
						grouping : false,
						gridComplete : function() {
						},
						caption : '',
						beforeRequest : function() {
							responsive_jqgrid($(".jqGrid"));
						},
						subGrid : true,
						subGridRowExpanded : function(subgrid_id, row_id) {
							var myGrid = $('#listGestionPedido');
							var pedidoId = myGrid.jqGrid('getCell', row_id,
									'pedidoId');

							var subgrid_table_id;
							subgrid_table_id = subgrid_id + "_t";
							$("#" + subgrid_id).html(
									"<table id='" + subgrid_table_id
											+ "' class='scroll'></table>");
							$("#" + subgrid_table_id)
									.jqGrid(
											{

												mtype : "GET",
												datatype : "json",

												url : gPath
														+ '/pedidoDetalleController/obtenerListaFiltrada?pedidoId='
														+ pedidoId,
												height : 80,
												rowNum : 180,

												colNames : [ 'Id', 'PRODUCTO',
														'CANTIDAD', 'P. VENTA',
														'FECHA' ],
												colModel : [
														{
															name : 'productoBean.productoId',
															index : 'productoBean.productoId',
															width : 0,
															sorttype : "int",
															editable : false,
															hidden : true,
															sortable : false
														},
														{
															name : 'productoBean.productoNombre',
															index : 'productoBean.productoNombre',
															width : 200,
															editable : false,
															sortable : false
														},
														{
															name : 'productoBean.productoCantidad',
															index : 'productoBean.productoCantidad',
															width : 80,
															editable : false,
															sortable : false
														},
														{
															name : 'productoBean.productoPrecioVenta',
															index : 'productoBean.productoPrecioVenta',
															width : 120,
															editable : false,
															sortable : false
														},
														{
															name : 'productoBean.productoFechaRegFormat',
															index : 'productoBean.productoFechaRegFormat',
															width : 120,
															editable : true,
															sortable : false
														}

												],

												rowNum : '',
												viewrecords : false,
												grouping : false,
												gridComplete : function() {

												}

											});

						}
					});

	$("#listGestionPedido").jqGrid('navGrid', "#plistGestionPedido", {
		edit : false,
		add : false,
		del : false,
		search : false,
		refresh : false
	});
}

function mostrarFiltrosGestionPedido() {
	if ($('#cboGestionPedidoColumna').val() == "pedidoFechaOrden" ||
			$('#cboGestionPedidoColumna').val() == "pedidoFechaFactura" || $('#cboGestionPedidoColumna').val() == "pedidoFechaGuia" ) {
		$("#divValorGestionPedido").hide();
		$("#divFechaInicialGestionPedido").show();
		$("#divFechaFinalGestionPedido").show();
	} else {
		$("#divValorGestionPedido").show();
		$("#divFechaInicialGestionPedido").hide();
		$("#divFechaFinalGestionPedido").hide();
		
		
	}
}

function buscarEnGestionPedido() {

	$("#listGestionPedido").jqGrid('clearGridData');
	$("#listGestionPedido").jqGrid(
			'setGridParam',
			{
				url : gPath + '/pedidoController/obtenerListaFiltrada'
						+ '?valor=' + $("#txtGestionPedidoBuscar").val()
						+ '&columna=' + $("#cboGestionPedidoColumna").val()
						+ '&fechaInicio=' + $("#fechaInicio").val()
						+ '&fechaFinal=' + $("#fechaFinal").val()
			});
	$("#listGestionPedido").trigger('reloadGrid');

}

function mostrarNuevoEnGestionPedido() {

	var lUrl = gPath + '/pedidoController/mostrarNuevoPedido';
	var titulo = "";
	mostrarMensajeConHtmlInicial('divDialogNuevoPedido', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, 80, lUrl);

}

function mostrarEditarEnGestionPedido(pedidoId) {

	var lUrl = gPath + '/pedidoController/mostrarEditarPedido?pedidoId='
			+ pedidoId;
	var titulo = "Editar Pedido";
	mostrarMensajeConHtmlInicial('divDialogEditarPedido', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, 80, lUrl);

}

function validarEliminarEnGestionPedido(pedidoId) {
	var aceptar$ = mostrarMensajeConfirmacion(ERP.MSG_CONFIRMACION_ELIMINACION,
			'Elimina Producto');
	aceptar$.click(function() {
		eliminarEnGestionPedido(pedidoId);
	});
};

function eliminarEnGestionPedido(pedidoId) {
	$.ajax({
		type : 'get',
		url : gPath + '/pedidoController/eliminar?pedidoId=' + pedidoId,
		success : function(mensaje) {
			  if(mensaje!=""){
				  mostrarMensajeError(mensaje);
                 }
			buscarEnGestionPedido();
		}
	});
}

function exportarFactura(pedidoId) {
	$("#pedidoIdExportarFactura").val(pedidoId);

	$('#formExportarfactura').attr('action',
			gPath + '/pedidoController/exportarFactura');
	$('#formExportarfactura').submit();

}

function exportarGuia(pedidoId) {
	
	$("#pedidoIdExportarGuia").val(pedidoId);

	$('#formExportarGuia').attr('action',
			gPath + '/pedidoController/exportarGuia');
	$('#formExportarGuia').submit();
	alert(pedidoId);

}

function exportarOrden(pedidoId) {
$("#pedidoIdExportarOrden").val(pedidoId);

	$('#formExportarOrden').attr('action',
			gPath + '/pedidoController/exportarOrden');
	$('#formExportarOrden').submit();

}


function  mostrarDescargarArchivo(){
	
	if ($('#pedidoNroOrden').val() != "") {
		$("#fileInputButton").show();
	} else{
		$("#fileInputButton").hide();
	};

}


// funciones para nuevo y editar producto

function mostrarAgregarProductoEnPedido() {
	lastsel = -1;
	var lUrl = gPath + '/productoController/mostrarAgregarProducto';
	var titulo = "";
	mostrarMensajeConHtmlAltura('divDialogEditarInsumo', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function eliminarProductoEnPedido(productoId) {

	var ids = $("#listPedidoDetalle").jqGrid('getDataIDs');

	for (var j = 0; j < ids.length; j++) {
		var rowId = ids[j];
		var rowData = $("#listPedidoDetalle").jqGrid('getRowData', rowId);

		if (productoId == rowData['productoBean.productoId']) {
			$('#listPedidoDetalle').jqGrid('delRowData', rowId);

		}
	}
	calcularValoresEnPedido();
}

function validarEliminarEnDetallePedidoPorRowId(rowId) {
	var rowData = $("#listPedidoDetalle").jqGrid('getRowData', rowId);
	var aceptar$ = mostrarMensajeConfirmacion(ERP.MSG_CONFIRMACION_ELIMINACION,
			'Elimina Producto');
	aceptar$.click(function() {
		eliminarProductoEnPedido(rowData["productoBean.productoId"]);

	});

};

function validarEliminarEnDetallePedido(productoId) {

	var aceptar$ = mostrarMensajeConfirmacion(ERP.MSG_CONFIRMACION_ELIMINACION,
			'Elimina Producto');
	aceptar$.click(function() {
		eliminarProductoEnPedido(productoId);

	});

};

function calcularValoresEnPedido() {
	sumarPrecioEnPedido();
	calcularPrecioBrutoEnPedido();
}

function sumarPrecioEnPedido() {
	var precioVenta = 0;
	var ids = $("#listPedidoDetalle").jqGrid('getDataIDs');
	for (var j = 0; j < ids.length; j++) {
		var rowId = ids[j];
		var rowData = $("#listPedidoDetalle").jqGrid('getRowData', rowId);
		precioVenta = eval(precioVenta)
				+ eval(rowData['productoBean.productoPrecioVenta']);
	}
	$("#pedidoPrecioNeto").val(eval(precioVenta).toFixed(2));

}


function calcularPrecioBrutoEnPedido() {
	var precioNeto = $("#pedidoPrecioNeto").val();
	var igv = $("#pedidoIgv").val();
	var precioBruto = eval(eval(precioNeto) * ((100 - eval(igv)) / 100)).toFixed(2);
	$("#pedidoPrecioBruto").val(precioBruto);
	 $("#pedidoPrecioIgv").val(eval(eval(precioNeto)-eval(precioBruto)).toFixed(2));
}


function obtenerGrillaDetallePedido() {
	var arrayProductoId = new Array();
	var ids = $("#listPedidoDetalle").jqGrid('getDataIDs');
	for (var j = 0; j < ids.length; j++) {
		var rowId = ids[j];
		var rowData = $("#listPedidoDetalle").jqGrid('getRowData', rowId);
		arrayProductoId.push(rowData['productoBean.productoId']);
	}

	$("#listProductoId").val(arrayProductoId);
}

// fin de funciones de nuevo y editar pedido

