function configurarNuevoPedido() {
	$('#nodoArchivo').ajaxfileupload({
		'action' : gPath + '/pedidoController/insertarArchivo',
		'onComplete' : function(file) {
           $("#pedidoOrdenArchivo").val(file);
		},
		'onStart' : function() {

		}
	});
	vincularValidacionEstilosJs("formNuevoPedido", validarGuardarNuevoPedido);
	mostrarDescargarArchivo();
	$("#pedidoNroOrden").keyup(function() {
		 mostrarDescargarArchivo();
	});
	
	$("#btnBuscarClienteEnPedido").click(function() {
		mostrarBuscarClienteEnPedido();
	});
	$("#btnAgregarProductoEnPedido").click(function() {
		mostrarAgregarProductoEnPedido();
	});
	$("#btnCancelarNuevoPedido").click(function() {
		$("#divDialogNuevoPedido").dialog("close");
	});
	$('#pedidoFechaOrdenRequest').datetimepicker({
		pickTime : false
	});

	$('#pedidoFechaFacturaRequest').datetimepicker({
		pickTime : false
	});
	

	$('#pedidoFechaGuiaRequest').datetimepicker({
		pickTime : false
	});
	
	$("#listPedidoDetalle")
			.jqGrid(
					{
						mtype : "GET",
						datatype : "json",
						url : gPath
								+ '/pedidoDetalleController/obtenerListaFiltrada',
						height : 80,
						rowNum : 180,
						colNames : [ 'Id', 'PRODUCTO', 'CANTIDAD', 'PRECIO',
								'ACCION' ],
						colModel : [
								{
									name : 'productoBean.productoId',
									index : 'productoBean.productoId',
									width : 0,
									sorttype : "int",
									editable : false,
									hidden : true,
									sortable : false
								},
								{
									name : 'productoBean.productoNombre',
									index : 'productoBean.productoNombre',
									width : 150,
									editable : false,
									sortable : false
								},
								{
									name : 'productoBean.productoCantidad',
									index : 'productoBean.productoCantidad',
									width : 100,
									editable : false,
									sortable : false
								},
								{
									name : 'productoBean.productoPrecioVenta',
									index : 'productoBean.productoPrecioVenta',
									width : 100,
									editable : false,
									sortable : false
								},
								{
									name : '',
									index : '',
									width : 80,
									editable : true,
									sortable : false,
									formatter : function(cellvalue, options,
											rowObject) {

										return '<a onclick="validarEliminarEnDetallePedido('
												+ rowObject["productoBean.productoId"]
												+ ')"><img src="'
												+ gPath
												+ '/resources/img/eliminar.png"></a>';

									}
								}

						],

						viewrecords : false,

						gridComplete : function() {

						},
						onSelectRow : function(id) {

						},
						caption : '',
						beforeRequest : function() {
							responsive_jqgrid($(".jqGrid"));
						}
					});

}

function mostrarBuscarClienteEnPedido() {

	var lUrl = gPath + '/clienteController/mostrarBuscarCliente';
	var titulo = "Buscar Cliente";
	mostrarMensajeConHtmlAltura('divDialogBuscarCliente', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

};

function validarGuardarNuevoPedido() {
	var aceptar$ = mostrarMensajeConfirmacion(ERP.MSG_CONFIRMACION_GRABADO,
			'Guardar Registro de Pedido');
	aceptar$.click(function() {
		guardarNuevoPedido();
	});
};

function guardarNuevoPedido() {
	obtenerGrillaDetallePedido();
	var form = $('#formNuevoPedido');
	$.ajax({
		type : form.attr('method'),
		url : form.attr('action'),
		data : form.serialize(),
		success : function(data) {
			$("#divDialogNuevoPedido").dialog("close");
			buscarEnGestionPedido();
		}
	});
}
