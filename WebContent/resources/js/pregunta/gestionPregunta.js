function configurarGestionPregunta() {

	$('#btnGestionPreguntaNuevo').click(function(event) {
		mostrarNuevoEnGestionPregunta();
	});

	vincularValidacionEstilosJs("formGestionPreguntaBuscar",
			buscarEnGestionPregunta);
	
	$("#listGestionPregunta")
	.jqGrid(
			{
				mtype : "GET",
				datatype : "json",
				url : gPath+'/pregutaController/obtenerListaFiltrada?examenId='+examenId,
				height : 300,
				rowNum : 180,
				
				colNames : [ 'Id', 'DESCRIPCION',
						'ESTADO', 'ACCIONES' ],
				colModel : [
						{
							name : 'preguntaId',
							index : 'preguntaId',
							width : 70,
							sorttype : "int",
							editable : false,
							hidden : true,
							sortable : false
						},
						{
							name : 'preguntaDescripcion',
							index : 'preguntaDescripcion',
							width : 220,
							editable : false,
							sortable : true
						},
						{
							name : 'preguntaEstado',
							index : 'preguntaEstado',
							width : 90,
							editable : false,
							hidden : false,
							sortable : true
						},	{
							name : 'examenNota',
							index : 'examenNota',
							width : 90,
							editable : false,
							hidden : false,
							sortable : true
						},

						{
							name : 'preguntaId',
							index : 'preguntaId',
							width : 150,
							editable : true,
							formatter : function radio(
									cellValue,
									option,
									rowObject) {

								return '<a onclick="mostrarEditarEnGestionPregunta('
										+ rowObject.preguntaId
										+ ')"><img src="'+gPath+'/resources/img/editar.png"></a>'
										+'<span>   </span>'
										+ '<a onclick="validarEliminarEnGestionPregunta('
										+ rowObject.preguntaId
										+ ')"><img src="'+gPath+'/resources/img/eliminar.png"></a>';

							}
						}

				],
				pager : "#plistGestionPregunta",
				rowNum : paginado,
				viewrecords : true,
				grouping : false,
				gridComplete : function() {
				},
				caption : '',
				beforeRequest : function() {
					responsive_jqgrid($(".jqGrid"));
				}
			});

$("#listGestionPregunta").jqGrid('navGrid',
	"#plistGestionPregunta", {
		edit : false,
		add : false,
		del : false,
		search : false,
		refresh : false
	});
}
/*
function buscarEnGestionPregunta() {
	$("#listGestionPregunta").jqGrid('clearGridData');
	$("#listGestionPregunta").jqGrid(
			'setGridParam',
			{
				url :  gPath + '/preguntaController/obtenerListaFiltrada' + '?valor='
						+ $("#txtGestionExamenBuscar").val() + '&columna='
						+ $("#cboGestionExamenColumna").val()
			});
	$("#listGestionExamen").trigger('reloadGrid');

}
*/
function mostrarNuevoEnGestionPregunta() {

	var lUrl = gPath + '/preguntaController/mostrarNuevoPregunta';
	var titulo = "Nueva Pregunta";
	mostrarMensajeConHtmlAltura('divDialogNuevoPregunta', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);
}

function mostrarEditarEnGestionPregunta(preguntaId) {

	var lUrl = gPath + '/preguntaController/mostrarEditarPregunta?preguntaId='
			+ preguntaId;
	var titulo = "Editar Pregunta";
	mostrarMensajeConHtmlAltura('divDialogEditarPregunta', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function validarEliminarEnGestionPregunta(preguntaId) {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_ELIMINACION, 'Elimina Pregunta');
	aceptar$.click(function() {
		eliminarEnGestionPregunta(preguntaId);
	});
};

function eliminarEnGestionPregunta(preguntaId) {
	$
			.ajax({
				type : 'get',
				url : gPath+'/preguntaController/eliminar?preguntaId='
						+ preguntaId,
				success : function(mensaje) {
					  if(mensaje!=""){
						  mostrarMensajeError(mensaje);
		                 }
					buscarEnGestionPregunta();
				}
			});
}