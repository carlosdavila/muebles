function configurarBuscarProducto(paginado) {

	vincularValidacionEstilosJs("formBuscarProducto", buscarEnBuscarProducto);

	$("#listBuscarProducto")
			.jqGrid(
					{
						mtype : "GET",
						datatype : "json",
						url : gPath + '/productoController/obtenerListaFiltrada',
						height : 300,
						rowNum : 180,
						colNames : [ 'Id', 'NOMBRE', 'TIPO DOC', 'NRO DOC',
								'TELEFONO', 'DIRECCION', 'ACCION' ],
						colModel : [
								{
									name : 'productoId',
									index : 'productoId',
									width : 70,
									sorttype : "int",
									editable : false,
									hidden : true,
									sortable : false
								},
								{
									name : 'productoNombre',
									index : 'productoNombre',
									width : 150,
									editable : false,
									sortable : true
								},
								{
									name : 'tipoDocBean.tipoDocDesc',
									index : 'tipoDocBean.tipoDocDesc',
									width : 90,
									editable : false,
									hidden : false,
									sortable : true
								},

								{
									name : 'productoNroDoc',
									index : 'productoNroDoc',
									width : 100,
									editable : false,
									hidden : false,
									sortable : true
								},

								{
									name : 'productoTelefono',
									index : 'productoTelefono',
									width : 150,
									editable : false,
									hidden : true,
									sortable : true
								},
								{
									name : 'productoDireccion',
									index : 'productoDireccion',
									width : 150,
									editable : false,
									sortable : true
								},
								{
									name : '',
									index : '',
									width : 80,
									editable : true,
									formatter : function radio(cellValue,
											option, rowObject) {
										var cadena = "";

										cadena = '<input type="radio" onclick="enviarProductoAproducto('
												+ option.rowId
												+ ')" name="radio" id="checkAgregarProducto_'

												+ '" />';

										return cadena;

									}
								}

						],
						pager : "#plistGestionProducto",
						rowNum : paginado,
						viewrecords : true,
						grouping : false,
						gridComplete : function() {
						},
						caption : '',
						beforeRequest : function() {
							responsive_jqgrid($(".jqGrid"));
						}
					});

	$("#listBuscarProducto").jqGrid('navGrid', "#plistBuscarProducto", {
		edit : false,
		add : false,
		del : false,
		search : false,
		refresh : false
	});
}

function buscarEnBuscarProducto() {
	$("#listBuscarProducto").jqGrid('clearGridData');
	$("#listBuscarProducto").jqGrid(
			'setGridParam',
			{
				url : gPath + '/productoController/obtenerListaFiltrada'
						+ '?valor=' + $("#txtValorBuscarProducto").val()
						+ '&columna=' + $("#cboColumnaBuscarProducto").val()
			});
	$("#listBuscarProducto").trigger('reloadGrid');

}

function enviarProductoAproducto(rowId) {

	var myGrid = $('#listBuscarProducto');
	var productoId = myGrid.jqGrid('getCell', rowId, 'productoId');
    var productoNombre = myGrid.jqGrid('getCell', rowId, 'productoNombre');
	var tipoDoc = myGrid.jqGrid('getCell', rowId, 'tipoDocBean.tipoDocDesc');
	var nroDoc = myGrid.jqGrid('getCell', rowId, 'productoNroDoc');
	var direccion = myGrid.jqGrid('getCell', rowId, 'productoDireccion');
	var telefono = myGrid.jqGrid('getCell', rowId, 'productoTelefono');
	$("#txtProductoIdEnPedido").val(productoId);
	$("#txtProductoNombreEnPedido").val(productoNombre);
	$("#txtTipoDocEnPedido").val(tipoDoc);
	$("#txtNroDocEnPedido").val(nroDoc);
	$("#txtDireccionEnPedido").val(direccion);
	$("#txtTelefonoEnPedido").val(telefono);
}
