function configurarEditarProducto() {

	changeTipoDocEnGestionProducto();
	vincularValidacionEstilosJs("formEditarProducto",
			validarGuardarEditarProducto);



	$("#btnEditarProductoCancelar").click(function() {
		$("#divDialogEditarProducto").dialog("close");
	});
}

function changeTipoDocEnGestionProducto() {
	if ($("#cboEditarProductoTipoDoc").val() != "") {
		$("#txtEditarProductoNroDoc").attr("disabled", false);
	} else {
		$("#txtEditarProductoNroDoc").val("");
		$("#txtEditarProductoNroDoc").attr("disabled", true);
	}
}

function guardarEditarProducto() {
	var modeloId= $('#modeloBean\\.modeloId').val();
	var form = $('#formEditarProducto');
	$.ajax({
		type : form.attr('method'),
		url : gPath+'/productoController/editar',
		data : form.serialize(),
		success : function(data) {
			$("#divDialogEditarProducto").dialog("close");
			buscarEnGestionProducto(modeloId);
		}
	});
}

function validarGuardarEditarProducto() {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Edicion Registro de Producto');
	aceptar$.click(function() {
		guardarEditarProducto();
	});
}


