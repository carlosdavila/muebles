
function buscarEnGestionProducto(modeloId) {
	var idTable="#tableProducto_"+modeloId+"_t";
	$(idTable).jqGrid('clearGridData');
	$(idTable).jqGrid(
			'setGridParam',
			{
				url :  gPath + '/productoController/obtenerListaFiltrada' + '?modeloId='
						+ modeloId
			});
	$(idTable).trigger('reloadGrid');

}

function mostrarNuevoEnGestionProducto(modeloId) {

	var lUrl = gPath + '/productoController/mostrarNuevoProducto?modeloId='+modeloId;
	var titulo = "Nuevo Producto";
	mostrarMensajeConHtmlAltura('divDialogNuevoProducto', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function mostrarEditarEnGestionProducto(productoId) {

	var lUrl = gPath + '/productoController/mostrarEditarProducto?productoId='
			+ productoId;
	var titulo = "Editar Producto";
	mostrarMensajeConHtmlAltura('divDialogEditarProducto', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function validarEliminarEnGestionProducto(productoId,modeloId) {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_ELIMINACION, 'Elimina Producto');
	aceptar$.click(function() {
		eliminarEnGestionProducto(productoId,modeloId);
	});
};

function eliminarEnGestionProducto(productoId,modeloId) {
	$
			.ajax({
				type : 'get',
				url : gPath+'/productoController/eliminar?productoId='
						+ productoId+'&modeloId='+modeloId,
				success : function(mensaje) {
					  if(mensaje!=""){
						  mostrarMensajeError(mensaje);
		                 }
					buscarEnGestionProducto(modeloId);
				}
			});
}