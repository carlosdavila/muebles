function configurarNuevoProducto() {

	vincularValidacionEstilosJs("formNuevoProducto",
			validarGuardarNuevoProducto);



	$("#btnNuevoProductoCancelar").click(function() {
		$("#divDialogNuevoProducto").dialog("close");
	});
}


function validarGuardarNuevoProducto() {
	
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Guardar Registro de Producto');
	aceptar$.click(function() {
		guardarNuevoProducto();
	});
}


function guardarNuevoProducto() {
	
	var modeloId= $('#modeloBean\\.modeloId').val();
	var form = $('#formNuevoProducto');
	$
			.ajax({
				type : form.attr('method'),
				url :gPath+'/productoController/guardar',
				data : form.serialize(),
				success : function(data) {
			
					$("#divDialogNuevoProducto").dialog(
							"close");
					buscarEnGestionProducto(modeloId);
				}
			});
}