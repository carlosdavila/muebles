function configurarEditarProveedor() {

	changeTipoDocEnGestionProveedor();
	vincularValidacionEstilosJs("formEditarProveedor",
			validarGuardarEditarProveedor);

	$("#cboEditarProveedorTipoDoc").change(function() {
		changeTipoDocEnGestionProveedor();
	});

	$("#btnEditarProveedorCancelar").click(function() {
		$("#divDialogEditarProveedor").dialog("close");
	});
}

function changeTipoDocEnGestionProveedor() {
	if ($("#cboEditarProveedorTipoDoc").val() != "") {
		$("#txtEditarProveedorNroDoc").attr("disabled", false);
	} else {
		$("#txtEditarProveedorNroDoc").val("");
		$("#txtEditarProveedorNroDoc").attr("disabled", true);
	}
}

function guardarEditarProveedor() {
	var form = $('#formEditarProveedor');
	$.ajax({
		type : form.attr('method'),
		url : form.attr('action'),
		data : form.serialize(),
		success : function(data) {
			$("#divDialogEditarProveedor").dialog("close");
			buscarEnGestionProveedor();
		}
	});
}

function validarGuardarEditarCliente() {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Edicion Registro de Proveedor');
	aceptar$.click(function() {
		guardarEditarCliente();
	});
}


