function configurarGestionProveedor(paginado) {

	$('#btnGestionProveedorNuevo').click(function(event) {
		mostrarNuevoEnGestionProveedor();
	});

	vincularValidacionEstilosJs("formGestionProveedorBuscar",
			buscarEnGestionProveedor);
	
	$("#listGestionProveedor")
	.jqGrid(
			{
				mtype : "GET",
				datatype : "json",
				url : '${urlListaFiltrada}',
				height : 300,
				rowNum : 180,
				
				colNames : [ 'Id', 'NOMBRE',
						'TIPO DOC', 'NRO DOC',
						'TELEFONO', 'EMAIL',
						'DIRECCION', 'ACCIONES' ],
				colModel : [
						{
							name : 'proveedorId',
							index : 'proveedorId',
							width : 70,
							sorttype : "int",
							editable : false,
							hidden : true,
							sortable : false
						},
						{
							name : 'proveedorNombre',
							index : 'proveedorNombre',
							width : 220,
							editable : false,
							sortable : true
						},
						{
							name : 'tipoDocBean.tipoDocDesc',
							index : 'tipoDocBean.tipoDocDesc',
							width : 90,
							editable : false,
							hidden : false,
							sortable : true
						},

						{
							name : 'proveedorNroDoc',
							index : 'proveedorNroDoc',
							width : 100,
							editable : false,
							hidden : false,
							sortable : true
						},
						{
							name : 'proveedorTelefono',
							index : 'proveedorTelefono',
							width : 100,
							editable : false,
							sortable : true
						},
						{
							name : 'proveedorEmail',
							index : 'proveedorEmail',
							width : 150,
							editable : false,
							sortable : true
						},

						{
							name : 'proveedorDireccion',
							index : 'proveedorDireccion',
							width : 150,
							editable : false,
							sortable : true
						},

						{
							name : 'proveedorId',
							index : 'proveedorId',
							width : 150,
							editable : true,
							formatter : function radio(
									cellValue,
									option,
									rowObject) {

								return '<a onclick="mostrarEditarEnGestionProveedor('
										+ rowObject.proveedorId
										+ ')"><img src="'+gPath+'/resources/img/editar.png"></a>'
										+'<span>   </span>'
										+ '<a onclick="validarEliminarEnGestionProveedor('
										+ rowObject.proveedorId
										+ ')"><img src="'+gPath+'/resources/img/eliminar.png"></a>';

							}
						}

				],
				pager : "#plistGestionProveedor",
				rowNum : paginado,
				viewrecords : true,
				grouping : false,
				gridComplete : function() {
				},
				caption : '',
				beforeRequest : function() {
					responsive_jqgrid($(".jqGrid"));
				}
			});

$("#listGestionProveedor").jqGrid('navGrid',
	"#plistGestionProveedor", {
		edit : false,
		add : false,
		del : false,
		search : false,
		refresh : false
	});
}
function buscarEnGestionProveedor() {
	$("#listGestionProveedor").jqGrid('clearGridData');
	$("#listGestionProveedor").jqGrid(
			'setGridParam',
			{
				url :  gPath + '/proveedorController/obtenerListaFiltrada' + '?valor='
						+ $("#txtGestionProveedorBuscar").val() + '&columna='
						+ $("#cboGestionProveedorColumna").val()
			});
	$("#listGestionProveedor").trigger('reloadGrid');

}

function mostrarNuevoEnGestionProveedor() {

	var lUrl = gPath + '/proveedorController/mostrarNuevoProveedor';
	var titulo = "Nuevo Proveedor";
	mostrarMensajeConHtmlAltura('divDialogNuevoProveedor', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function mostrarEditarEnGestionProveedor(proveedorId) {

	var lUrl = gPath + '/proveedorController/mostrarEditarProveedor?proveedorId='
			+ proveedorId;
	var titulo = "Editar Proveedor";
	mostrarMensajeConHtmlAltura('divDialogEditarProveedor', titulo,
			CONSTANTE.MENSAJE.POPUPCHICO.TAMANO, lUrl);

}

function validarEliminarEnGestionProveedor(proveedorId) {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_ELIMINACION, 'Elimina Proveedor');
	aceptar$.click(function() {
		eliminarEnGestionProveedor(proveedorId);
	});
};

function eliminarEnGestionProveedor(proveedorId) {
	$
			.ajax({
				type : 'get',
				url : gPath+'/proveedorController/eliminar?proveedorId='
						+ proveedorId,
				success : function(mensaje) {
					  if(mensaje!=""){
						  mostrarMensajeError(mensaje);
		                 }
					buscarEnGestionProveedor();
				}
			});
}