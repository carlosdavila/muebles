function configurarNuevoProveedor() {

	vincularValidacionEstilosJs("formNuevoProveedor",
			validarGuardarNuevoProveedor);

	$("#cboNuevoProveedorTipoDoc")
			.change(
					function() {
						if ($("#cboNuevoProveedorTipoDoc")
								.val() != "") {
							$("#txtNuevoProveedorNroDoc")
									.attr("disabled", false);
						} else {
							$("#txtNuevoProveedorNroDoc")
									.val("");
							$("#txtNuevoProveedorNroDoc")
									.attr("disabled", true);
						}
					});

	$("#btnNuevoProveedorCancelar").click(function() {
		$("#divDialogNuevoProveedor").dialog("close");
	});
}


function validarGuardarNuevoProveedor() {
	var aceptar$ = mostrarMensajeConfirmacion(
			ERP.MSG_CONFIRMACION_GRABADO,
			'Guardar Registro de Proveedor');
	aceptar$.click(function() {
		guardarNuevoProveedor();
	});
}


function guardarNuevoProveedor() {
	var form = $('#formNuevoProveedor');
	$
			.ajax({
				type : form.attr('method'),
				url : form.attr('action'),
				data : form.serialize(),
				success : function(data) {
					$("#divDialogNuevoProveedor").dialog(
							"close");
					buscarEnGestionProveedor();
				}
			});
}