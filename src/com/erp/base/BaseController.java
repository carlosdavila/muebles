package com.erp.base;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.erp.categoria.dao.CategoriaDao;
import com.erp.categoria.model.CategoriaBean;
import com.erp.comun.util.ErpConstante;
import com.erp.usuario.model.UsuarioBean;

import frameworkfrivas.controller.ComunController;


public class BaseController extends ComunController{
	
	@Autowired
	private CategoriaDao categoriaDao;

	
	
	private static final Logger LOG = Logger.getLogger(BaseController.class);
	
	
	public UsuarioBean obtenerUsuarioSession(HttpSession session){
		UsuarioBean usuario= new UsuarioBean();
		if(session.getAttribute(ErpConstante.USUARIO_SESSION)!=null){
		usuario=(UsuarioBean) session.getAttribute(ErpConstante.USUARIO_SESSION);
		}
		return usuario;
	}
	
	@SuppressWarnings("unchecked")
	public List<CategoriaBean> obtenerCategoria(HttpSession session){
		List<CategoriaBean> listaCategoria= new ArrayList<CategoriaBean>();
		if(session.getAttribute(ErpConstante.SESSION_CATEGORIA)!=null){
			listaCategoria=(List<CategoriaBean>) session.getAttribute(ErpConstante.SESSION_CATEGORIA);
		}
		
		if(listaCategoria.size()==0){
			listaCategoria=cargarListaCategoria();
			session.setAttribute(ErpConstante.SESSION_CATEGORIA, listaCategoria);
		}
		
		return listaCategoria;
	}
	

	@SuppressWarnings("unchecked")
	private List<CategoriaBean> cargarListaCategoria() {
		List<CategoriaBean> listaCategoria= new ArrayList<CategoriaBean>();
		try {

			listaCategoria = (List<CategoriaBean>) categoriaDao.obtenerListaTotalOrdena("categoriaOrden", "asc");

		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}
		
		return listaCategoria;
	}
}
