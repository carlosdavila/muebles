package com.erp.carrito.dao;

import com.erp.carrito.model.CarritoBean;


import frameworkfrivas.dao.ComunDao;

public interface CarritoDao extends ComunDao<CarritoBean, Integer> {

}
