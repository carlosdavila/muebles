package com.erp.carrito.detalle.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.erp.base.BaseController;
import com.erp.carrito.detalle.model.CarritoDetalleBean;
import com.erp.carrito.detalle.service.CarritoDetalleService;
import com.erp.carrito.model.CarritoBean;
import com.erp.comun.util.ErpConstante;
import com.erp.usuario.model.UsuarioBean;

import frameworkfrivas.exception.ControllerException;
import frameworkfrivas.util.Utilitario;

@Controller
@RequestMapping("carritoDetalleController")
public class CarritoDetalleController extends BaseController {

	private static final Logger LOG = Logger.getLogger(CarritoDetalleController.class);
	@Autowired
	private CarritoDetalleService carritoDetalleService;

	@RequestMapping(value = "/mostrarGestionCarritoDetalle", method = RequestMethod.GET)
	public ModelAndView mostrarGestionCarritoDetalle(HttpSession session) {
		
		return new ModelAndView("carrito/detalle/gestionCarritoDetalle");
	
	}

	

	
	@RequestMapping(value = "/obtenerTotal", method = RequestMethod.POST)
	@ResponseBody
	public String obtenerTotal(HttpSession session) throws ControllerException {
	
		String totalEnvio="";
		try {
			
			List<CarritoDetalleBean> listaCarritoDetalle = obtenerListaDetalleCarrito(session);

			BigDecimal totalFinal=new BigDecimal("0.0");
			
			for (CarritoDetalleBean carritoDetalleBean : listaCarritoDetalle) {
			
			totalFinal=totalFinal.add(new BigDecimal( carritoDetalleBean.getCarritoDetalleTotal()));
			
			}
			totalFinal=totalFinal.setScale(2, RoundingMode.HALF_EVEN);
			totalEnvio=totalFinal.toString();
	
		} catch (Exception e) {
			LOG.debug("Error obtenerTotal " + e);
			throw new ControllerException("Error obtenerTotal", e);
		}
	
		return totalEnvio;
	}

	
	
	@RequestMapping(value = "/agregar", method = RequestMethod.POST)
	@ResponseBody
	public String agregarProducto(HttpSession session, Integer productoId, Integer cantidad) throws ControllerException {
		Boolean encontroProducto=false;
		String totalEnvio="";
		try {
			
			List<CarritoDetalleBean> listaCarritoDetalle = obtenerListaDetalleCarrito(session);
			for (CarritoDetalleBean carritoDetalleBean : listaCarritoDetalle) {
				if(carritoDetalleBean.getProductoBean().getProductoId().compareTo(productoId)==0){
			        encontroProducto=true;
					break;
				}
				
				
			}
			
			if(!encontroProducto){
			carritoDetalleService.agregarProducto(listaCarritoDetalle, productoId, cantidad);
			}
			session.setAttribute(ErpConstante.SESSION_CARRITO, listaCarritoDetalle);
		
			totalEnvio=obtenerTotal( session);
			
			
			
		} catch (Exception e) {
			LOG.debug("Error obtenerListaSession " + e);
			throw new ControllerException("Error obtenerListaSession", e);
		}
	
		return encontroProducto+","+totalEnvio;
	}

	@RequestMapping(value = "/obtenerListaSession", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerListaSession(HttpSession session) throws ControllerException {

		String json = "";
		try {
			List<CarritoDetalleBean> listaCarritoDetalle = obtenerListaDetalleCarrito(session);
			int i = 0;
			for (CarritoDetalleBean carritoDetalleBean : listaCarritoDetalle) {
				carritoDetalleBean.setCarritoDetalleId(i);
				carritoDetalleService.calcularTotalDetalleCarrito(carritoDetalleBean);
				i++;
			}
			session.setAttribute(ErpConstante.SESSION_CARRITO, listaCarritoDetalle);
			CarritoBean carritoBean = new CarritoBean();
			carritoBean.setListaCarritoDetalleBean(listaCarritoDetalle);

			json = Utilitario.formatearJsonString(listaCarritoDetalle);

		} catch (Exception e) {
			LOG.debug("Error obtenerListaSession " + e);
			throw new ControllerException("Error obtenerListaSession", e);
		}

		return "{" + " \"data\" : " + json + "}";
	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.POST)
	@ResponseBody
	public void eliminar(HttpSession session, Integer carritoDetalleId) throws ControllerException {
		try {
			List<CarritoDetalleBean> listaCarritoDetalle = obtenerListaDetalleCarrito(session);
			carritoDetalleService.eliminarProducto(listaCarritoDetalle, carritoDetalleId);
			session.setAttribute(ErpConstante.SESSION_CARRITO, listaCarritoDetalle);
		} catch (Exception e) {
			LOG.debug("Error eliminar " + e);
			throw new ControllerException("Error eliminar", e);
		}

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/enviarCorreo", method = RequestMethod.POST)
	@ResponseBody
	public void enviarCorreo(HttpSession session) throws ControllerException {
		try {
			List<CarritoDetalleBean> listaCarritoDetalle = (List<CarritoDetalleBean>) session
					.getAttribute(ErpConstante.SESSION_CARRITO);
			UsuarioBean usuario = obtenerUsuarioSession(session);
			//carritoDetalleService.enviarCorreo(listaCarritoDetalle, usuario);
			session.setAttribute(ErpConstante.SESSION_CARRITO, new ArrayList<CarritoDetalleBean>() );
		} catch (Exception e) {
			LOG.debug("Error enviarCorreo " + e);
			throw new ControllerException("Error enviarCorreo", e);
		}
	}

	@SuppressWarnings("unchecked")
	private List<CarritoDetalleBean> obtenerListaDetalleCarrito(HttpSession session) {
		List<CarritoDetalleBean> listaCarritoDetalle = null;
		listaCarritoDetalle = (List<CarritoDetalleBean>) session.getAttribute(ErpConstante.SESSION_CARRITO);
		if (listaCarritoDetalle == null) {
			listaCarritoDetalle = new ArrayList<CarritoDetalleBean>();
		}

		return listaCarritoDetalle;
	}
	
	
	@RequestMapping(value = "/cambiarCantidad", method = RequestMethod.POST)
	@ResponseBody
	public void cambiarCantidad(HttpSession session, Integer carritoDetalleId,Integer cantidad) throws ControllerException {
		try {
			List<CarritoDetalleBean> listaCarritoDetalle = obtenerListaDetalleCarrito(session);
			for (CarritoDetalleBean carritoDetalleBean : listaCarritoDetalle) {
				if(carritoDetalleId.compareTo(carritoDetalleBean.getCarritoDetalleId())==0){
					carritoDetalleBean.setCarritoDetalleCantidad(cantidad);
					carritoDetalleService.calcularTotalDetalleCarrito(carritoDetalleBean);
					break;
				}
			}
			session.setAttribute(ErpConstante.SESSION_CARRITO, listaCarritoDetalle);
		} catch (Exception e) {
			LOG.debug("Error eliminar " + e);
			throw new ControllerException("Error eliminar", e);
		}

	}

}
