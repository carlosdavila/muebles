package com.erp.carrito.detalle.dao;

import com.erp.carrito.detalle.model.CarritoDetalleBean;

import frameworkfrivas.dao.ComunDao;

public interface CarritoDetalleDao extends ComunDao<CarritoDetalleBean, Integer> {

	
}
