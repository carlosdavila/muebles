package com.erp.carrito.detalle.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.erp.carrito.model.CarritoBean;
import com.erp.producto.model.ProductoBean;

@Entity
@Table(name="t_carrito_detalle")
@Proxy(lazy=true)
public class CarritoDetalleBean {
	
	@Id
	@GeneratedValue
	@Column(name="carrito_detalle_id")
	private Integer carritoDetalleId;
	
	
	@ManyToOne
	@JoinColumn(name = "carrito_id")
	private CarritoBean carritoBean;
	
	@ManyToOne
	@JoinColumn(name = "producto_id")
	private ProductoBean productoBean;
	
	@Column(name="carrito_detalle_precio")
	private Double carritoDetallePrecio;
	
	@Column (name="carrito_detalle_cantidad")
	private int carritoDetalleCantidad;
	
	@Column(name="carrito_detalle_total")
	private Double carritoDetalleTotal;

	
	public Integer getCarritoDetalleId() {
		return carritoDetalleId;
	}

	public void setCarritoDetalleId(Integer carritoDetalleId) {
		this.carritoDetalleId = carritoDetalleId;
	}

	public CarritoBean getCarritoBean() {
		return carritoBean;
	}

	public void setCarritoBean(CarritoBean carritoBean) {
		this.carritoBean = carritoBean;
	}

	public ProductoBean getProductoBean() {
		return productoBean;
	}

	public void setProductoBean(ProductoBean productoBean) {
		this.productoBean = productoBean;
	}

	public Double getCarritoDetallePrecio() {
		return carritoDetallePrecio;
	}

	public void setCarritoDetallePrecio(Double carritoDetallePrecio) {
		this.carritoDetallePrecio = carritoDetallePrecio;
	}



	public Double getCarritoDetalleTotal() {
		return carritoDetalleTotal;
	}

	public void setCarritoDetalleTotal(Double carritoDetalleTotal) {
		this.carritoDetalleTotal = carritoDetalleTotal;
	}

	public int getCarritoDetalleCantidad() {
		return carritoDetalleCantidad;
	}

	public void setCarritoDetalleCantidad(int carritoDetalleCantidad) {
		this.carritoDetalleCantidad = carritoDetalleCantidad;
	}
	




}
