package com.erp.carrito.detalle.service;

import java.util.List;

import com.erp.carrito.detalle.model.CarritoDetalleBean;
import com.erp.usuario.model.UsuarioBean;

import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.service.ComunService;

public interface CarritoDetalleService  extends ComunService<CarritoDetalleBean, Integer>{
 
	public void agregarProducto(List<CarritoDetalleBean> listaDetalleCarrito ,Integer productoId, int cantidad) throws BusinessException;
	public void eliminarProducto(List<CarritoDetalleBean> listaDetalleCarrito ,Integer carritoDetalleId) throws BusinessException;
	public void enviarCorreo(List<CarritoDetalleBean> listaCarritoDetalle, UsuarioBean usuario) throws BusinessException ;
	
	public void calcularTotalDetalleCarrito(CarritoDetalleBean carritoDetalleBean);
}
