package com.erp.carrito.detalle.service;

import java.math.BigDecimal;

import java.math.RoundingMode;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.carrito.detalle.model.CarritoDetalleBean;
import com.erp.comun.util.ErpConstante;
import com.erp.producto.dao.ProductoDao;
import com.erp.producto.model.ProductoBean;
import com.erp.usuario.model.UsuarioBean;

import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.exception.DAOException;
import frameworkfrivas.service.ComunServiceImpl;

@Service
public class CarritoDetalleServiceImpl extends ComunServiceImpl<CarritoDetalleBean, Integer>
		implements CarritoDetalleService {

	private static final Logger LOG = Logger.getLogger(CarritoDetalleServiceImpl.class);

	@Autowired
	private ProductoDao productoDao;
	@Autowired
	private Properties erpConfig;

	@Override
	public void agregarProducto(List<CarritoDetalleBean> listaDetalleCarrito, Integer productoId, int cantidad)
			throws BusinessException {
		try {

			ProductoBean productoBean = productoDao.obtener(productoId);
			//productoBean.setProductoDescripcion("dddddd ddd d  dd  dd dddd ddddd");
		
			CarritoDetalleBean carritoDetalleBean = new CarritoDetalleBean();
			carritoDetalleBean.setCarritoDetalleCantidad(cantidad);
			carritoDetalleBean.setProductoBean(productoBean);

			carritoDetalleBean.setCarritoDetallePrecio(productoBean.getProductoPreVenta());
			calcularTotalDetalleCarrito( carritoDetalleBean);

			listaDetalleCarrito.add(carritoDetalleBean);
		} catch (DAOException e) {
			LOG.debug("Error agregarProducto " + e);
			throw new BusinessException(e);
		}

	}

	public void eliminarProducto(List<CarritoDetalleBean> listaDetalleCarrito, Integer carritoDetalleId)
			throws BusinessException {
		try {

			for (CarritoDetalleBean carritoDetalleBean : listaDetalleCarrito) {
				if (carritoDetalleId.compareTo(carritoDetalleBean.getCarritoDetalleId()) == 0) {
					listaDetalleCarrito.remove(carritoDetalleBean);
					break;
				}
			}
		} catch (Exception e) {
			LOG.debug("Error eliminarProducto " + e);
			throw new BusinessException(e);
		}

	}

	public void enviarCorreo(List<CarritoDetalleBean> listaCarritoDetalle, UsuarioBean usuario) throws BusinessException {
		try {
			String mensaje = "<h2>Hola " + usuario.getUsuarioNombre() + "! </h2><br>";
			mensaje =mensaje+ "<h2>Le enviamos el detalle de sus productos.</h2><br>";

			BigDecimal suma = new BigDecimal("0.0");
			StringBuilder tabla = new StringBuilder();
			
						
			tabla.append("<body style='color: #000;background: #efefef;font-family: Helvetica, Arial, sans-serif; text-align: center;'>");
			tabla.append("<div style='width: 100%;text-align: left;margin: 0 auto;padding: 2em;background: #fff;'>");
			tabla.append("<table style='width: 100%;text-align: left;border-collapse: collapse;margin: 0 0 1em 0;caption-side: top;'>");
			tabla.append("<thead><tr style='padding: 0.3em;'><th  style='width:55%'>Producto</th><th style='width:15%'>Cant.</th>");
			tabla.append("<th  style='width:15%'>Precio</th><th  style='width:15%'>Total</th></tr></thead>");
			
			tabla.append("<tbody style='border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 0.3em;'>");
		
		
			
			for (CarritoDetalleBean det : listaCarritoDetalle) {
				tabla.append("<tr>");
				tabla.append("<td style='padding: 0.3em;'>");
				tabla.append(det.getProductoBean().getProductoNombre().toUpperCase());
				tabla.append("</td>");
				tabla.append("<td style='padding: 0.3em;'>");
				tabla.append(det.getCarritoDetalleCantidad());
				tabla.append("</td>");
				tabla.append("<td style='padding: 0.3em;'>");
				tabla.append("S/."+det.getCarritoDetallePrecio());
				tabla.append("</td>");
				tabla.append("<td style='padding: 0.3em;'>");
				tabla.append(det.getCarritoDetalleTotal());
				tabla.append("</td>");
				tabla.append("</tr>");

				suma = suma.add(new BigDecimal(det.getCarritoDetalleTotal())).setScale(2, BigDecimal.ROUND_UP);
			}

			
			tabla.append("</tbody>");
			tabla.append("<tfoot style='text-align: center;color: #555;font-size: 0.8em;'>");
			tabla.append("<tr><td></td><td></td><td  style='color:red;text-align:left;padding: 0.7em;font-size:18px'>Total:</td><td  style='color:red;text-align:left;padding: 0.7em;font-size:18px'>S/.");
			tabla.append(suma);
			tabla.append("</td></tr>");
			tabla.append("<tr><td colspan='4'>Compiled in 2017 by Equipments</td></tr>");
			tabla.append("</tfoot>");
			tabla.append("</table></div>");
			
			
			mensaje = mensaje + tabla.toString();
			String userSmtp = erpConfig.getProperty(ErpConstante.CORREO_USER);
			String claveSmtp = erpConfig.getProperty(ErpConstante.CORREO_CLAVE);
			String host = erpConfig.getProperty(ErpConstante.CORREO_HOST);
			String puerto = erpConfig.getProperty(ErpConstante.CORREO_PUERTO);
			String from=erpConfig.getProperty(ErpConstante.CORREO_ENVIO);
			
			enviarCorreo(host, userSmtp, claveSmtp, puerto, from, usuario.getUsuarioUser(), "Carrito de Compras", mensaje);
			//enviarCorreo(host, userSmtp, claveSmtp, puerto, from, from, "Carrito de Compras", mensaje);
			
			//enviarcorreo(host,puerto,user, clave, usuario.getUsuarioUser(), mensaje, "Carrito de Compras");
		
		
		} catch (Exception e) {
			LOG.debug("Error enviarCorreo "+e);
			throw new  BusinessException(e);
		}

	}
	
	
	
	public void calcularTotalDetalleCarrito(CarritoDetalleBean carritoDetalleBean){
		BigDecimal precio=new BigDecimal(carritoDetalleBean.getCarritoDetallePrecio());
		BigDecimal cantidad=new BigDecimal(carritoDetalleBean.getCarritoDetalleCantidad());
		BigDecimal total=precio.multiply(cantidad);
		carritoDetalleBean.setCarritoDetalleTotal(total.setScale(2, RoundingMode.HALF_EVEN).doubleValue());
	}
	
	

}
