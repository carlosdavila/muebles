package com.erp.carrito.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.erp.carrito.detalle.model.CarritoDetalleBean;
import com.erp.usuario.model.UsuarioBean;

@Entity
@Table(name="t_carrito")
@Proxy(lazy=true)
public class CarritoBean {
	
	@Id
	@GeneratedValue
	@Column(name="carrito_id")
	private Integer carritoId;
	
	
	@ManyToOne
	@JoinColumn(name = "usuario_id")
	private UsuarioBean usuarioBean;

	
	@Column(name="carrito_total")
	private Double carritoTotal;
	
	
	@Column(name="carrito_comentario")
	private Double carritoComentario;


	transient  List<CarritoDetalleBean> listaCarritoDetalleBean;
	
	public Integer getCarritoId() {
		return carritoId;
	}


	public void setCarritoId(Integer carritoId) {
		this.carritoId = carritoId;
	}


	public UsuarioBean getUsuarioBean() {
		return usuarioBean;
	}


	public void setUsuarioBean(UsuarioBean usuarioBean) {
		this.usuarioBean = usuarioBean;
	}


	public Double getCarritoTotal() {
		return carritoTotal;
	}


	public void setCarritoTotal(Double carritoTotal) {
		this.carritoTotal = carritoTotal;
	}


	public Double getCarritoComentario() {
		return carritoComentario;
	}


	public void setCarritoComentario(Double carritoComentario) {
		this.carritoComentario = carritoComentario;
	}


	public List<CarritoDetalleBean> getListaCarritoDetalleBean() {
		return listaCarritoDetalleBean;
	}


	public void setListaCarritoDetalleBean(List<CarritoDetalleBean> listaCarritoDetalleBean) {
		this.listaCarritoDetalleBean = listaCarritoDetalleBean;
	}



}
