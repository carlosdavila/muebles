package com.erp.categoria.controller;



import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.erp.categoria.model.CategoriaBean;
import com.erp.categoria.service.CategoriaService;
import com.erp.comun.util.ErpConstante;
import com.erp.usuario.model.UsuarioBean;

import frameworkfrivas.bean.RequestJqGridBean;
import frameworkfrivas.bean.ResponseJqGridBean;
import frameworkfrivas.util.Utilitario;



@Controller
@RequestMapping("categoriaController")
public class CategoriaController {
	
	@Autowired
	private CategoriaService categoriaService;
	
	
	@Autowired
	private Properties erpConfig;

	private static final Logger LOG = Logger.getLogger(CategoriaController.class);
	

	@RequestMapping(value = "/mostrarGestionCategoria", method = RequestMethod.GET)
	public ModelAndView mostrarGestionCategoria(HttpSession session, Device device) {
		ModelAndView model = new ModelAndView("configurar");
		UsuarioBean usuarioBean= (UsuarioBean) session.getAttribute(ErpConstante.USUARIO_SESSION) ;
		model.addObject("usuarioBean",usuarioBean);
		model.addObject("isPc", device.isNormal());
		return model;
	}
	
	@RequestMapping(value = "/obtenerListaFiltradaVacio", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerListaFiltradaVacio(
			RequestJqGridBean requestJqGridBean) {

		String json = "";
	
		try {
			String[] columnasCoincidencias = {"categoriaNombre"};
			requestJqGridBean.setColumnaCoincidencia(columnasCoincidencias);
			
			requestJqGridBean.setSord("asc");
			requestJqGridBean.setSidx("categoriaOrden");
			
			json = Utilitario.formatearJsonString(new ResponseJqGridBean ());
						
		} catch (Exception e) {
			System.out.println(e.getMessage());
		    LOG.error(e.getMessage(), e);
		}
	
		return json;
	}

	@RequestMapping(value = "/obtenerListaFiltrada", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerListaFiltrada(
			RequestJqGridBean requestJqGridBean) {

		String json = "";
		/*
		List<Object> listaCategoriaBean= new ArrayList<Object>();
		CategoriaBean categoriaBean= new CategoriaBean();
		categoriaBean.setCategoriaId(1);
		categoriaBean.setCategoriaNombre("grupo1");
		categoriaBean.setCategoriaOrden(1);
		listaCategoriaBean.add(categoriaBean);
		CategoriaBean categoriaBea2= new CategoriaBean();
		categoriaBea2.setCategoriaId(2);
		categoriaBea2.setCategoriaNombre("grupo2");
		categoriaBea2.setCategoriaOrden(2);
		listaCategoriaBean.add(categoriaBea2);
		
		CategoriaBean categoriaBea5= new CategoriaBean();
		categoriaBea5.setCategoriaId(3);
		categoriaBea5.setCategoriaNombre("grupo3");
		categoriaBea5.setCategoriaOrden(3);
		listaCategoriaBean.add(categoriaBea5);
		
		ResponseJqGridBean responseJqGridBean = new ResponseJqGridBean();
		responseJqGridBean.setPage(1);
		responseJqGridBean.setRecords(2);
		responseJqGridBean.setTotal(2);
		responseJqGridBean.setRows(listaCategoriaBean);
	
		try{
			json = Utilitario.formatearJsonString(responseJqGridBean);
		}catch( Exception e){
			
		}
		*/
		try {
			String[] columnasCoincidencias = {"categoriaNombre"};
			requestJqGridBean.setColumnaCoincidencia(columnasCoincidencias);
			
			requestJqGridBean.setSord("asc");
			requestJqGridBean.setSidx("categoriaOrden");
			
			json = Utilitario.formatearJsonString(categoriaService
					.obtenerListaPaginado(requestJqGridBean));
						
		} catch (Exception e) {
			System.out.println(e.getMessage());
		    LOG.error(e.getMessage(), e);
		}
	
		return json;
	}

	@RequestMapping(value = "/mostrarBuscarCategoria", method = RequestMethod.GET)
	public ModelAndView mostrarBuscarCategoria() {
		return new ModelAndView("categoria/buscarCategoria");
	}

	
	@RequestMapping(value = "/mostrarNuevaCategoria", method = RequestMethod.GET)
	public ModelAndView mostrarNuevaCategoria() {
	ModelAndView model = new ModelAndView("categoria/nuevaCategoria");
		try {
			model.addObject("categoriaBean", new CategoriaBean());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return model;
		
	
	}

	@RequestMapping(value = "/mostrarEditarCategoria", method = RequestMethod.GET)
	public ModelAndView mostrarEditarCategoria(Integer categoriaId) {

		
		CategoriaBean categoriaBean = null;
		try {

			categoriaBean = categoriaService.obtener(categoriaId);
            
		
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		ModelAndView model = new ModelAndView("categoria/editarCategoria");
		model.addObject("categoriaBean", categoriaBean);
		return model;
	}

	@RequestMapping(value = "/guardar", method = RequestMethod.POST)
	@ResponseBody
	public void guardar(CategoriaBean categoriaBean) throws Exception {
		try {
			if(categoriaBean.getCategoriaNombre()!=null){
				categoriaBean.setCategoriaNombre(categoriaBean.getCategoriaNombre().toUpperCase());

			}
			categoriaService.guardarConOrden(categoriaBean);

		} catch (Exception e) {
			
			LOG.error(e.getMessage(), e);
			throw new Exception();
		}
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	@ResponseBody
	public void editar(CategoriaBean categoriaBean) throws Exception {
		try {
			if(categoriaBean.getCategoriaNombre()!=null){
				categoriaBean.setCategoriaNombre(categoriaBean.getCategoriaNombre().toUpperCase());

			}
			categoriaService.actualizar(categoriaBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new Exception();
		}

	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.GET)
	@ResponseBody
	public String editar(Integer categoriaId) {
		String mensajeError="";
		try {
			categoriaService.eliminar(categoriaId);

		} catch (DataIntegrityViolationException e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (ConstraintViolationException e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (Exception e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_GENERICO);
			LOG.error(e.getMessage(), e);
		}
		return mensajeError;

	}

	

	@RequestMapping(value = "/mover", method = RequestMethod.GET)
	@ResponseBody
	public void mover(String idDiferentes,String ordenes) {

		try {

		categoriaService.moverOrden(idDiferentes);
	
		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}
	
	}
	
	/*
	@RequestMapping(value = "/bajar", method = RequestMethod.GET)
	@ResponseBody
	public String bajar(Integer categoriaId) {
		String mensajeError="";
		try {

			categoriaService.bajar(categoriaId);

			
		} catch (Exception e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_GENERICO);
			LOG.error(e.getMessage(), e);
		}
		return mensajeError;

	}
	*/
	
}
