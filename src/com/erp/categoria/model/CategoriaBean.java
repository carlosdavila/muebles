package com.erp.categoria.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.erp.modelo.model.ModeloBean;

@Entity
@Table(name = "t_categoria" )
@Proxy(lazy=true)

public class CategoriaBean {
	
	@Id
	@Column(name = "categoria_id")
	private Integer categoriaId;
	
	@Column(name = "categoria_nombre")
	private String categoriaNombre;
	
	
	@Column(name = "categoria_tipo")
	private String categoriaTipo;
	
	
    @Column(name = "categoria_orden")
	private int categoriaOrden;
    

   private transient List<ModeloBean> listaModelo;
    
    
    public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getCategoriaNombre() {
		return categoriaNombre;
	}

	public void setCategoriaNombre(String categoriaNombre) {
		this.categoriaNombre = categoriaNombre;
	}


	public int getCategoriaOrden() {
		return categoriaOrden;
	}

	public void setCategoriaOrden(int categoriaOrden) {
		this.categoriaOrden = categoriaOrden;
	}

	public String getCategoriaTipo() {
		return categoriaTipo;
	}

	public void setCategoriaTipo(String categoriaTipo) {
		this.categoriaTipo = categoriaTipo;
	}

	public List<ModeloBean> getListaModelo() {
		return listaModelo;
	}

	public void setListaModelo(List<ModeloBean> listaModelo) {
		this.listaModelo = listaModelo;
	}







}
