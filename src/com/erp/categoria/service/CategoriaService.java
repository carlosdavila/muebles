package com.erp.categoria.service;

import com.erp.categoria.model.CategoriaBean;

import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.service.ComunService;

public interface CategoriaService extends ComunService<CategoriaBean, Integer> {
	/*
	public void subir(Integer categoriaId) throws BusinessException ;
	public void bajar(Integer categoriaId) throws BusinessException ;
	*/
	public void guardarConOrden(CategoriaBean categoriaBean) throws BusinessException;
	public void moverOrden(String idDiferentes) 	throws BusinessException;
}
