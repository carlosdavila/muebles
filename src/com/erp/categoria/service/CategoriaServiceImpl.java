package com.erp.categoria.service;




import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.erp.categoria.dao.CategoriaDao;
import com.erp.categoria.model.CategoriaBean;

import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.service.ComunServiceImpl;

@Service
public class CategoriaServiceImpl extends ComunServiceImpl<CategoriaBean, Integer> implements CategoriaService {
	@Autowired
	private CategoriaDao categoriaDao;
	private static final Logger LOG = Logger.getLogger(CategoriaServiceImpl.class);
	/*
	public void subir(Integer categoriaId) throws BusinessException  {
	
		try {
			
			CategoriaBean categoriaBean=obtener(categoriaId);
			
			int ordenAnterior=categoriaBean.getCategoriaOrden()-1;
			if(ordenAnterior!=0){
			CategoriaBean categoriaBeanAnterior=(CategoriaBean) categoriaDao.obtenerListaIgualAcolumnaOrdenado("categoriaOrden", ordenAnterior+"","categoriaOrden","asc").get(0);
			categoriaBeanAnterior.setCategoriaOrden(categoriaBean.getCategoriaOrden());
			categoriaBean.setCategoriaOrden(ordenAnterior);
			
			actualizar(categoriaBeanAnterior);
			actualizar(categoriaBean);
			}else{
				LOG.error("No se puede Subir de nivel");
		          throw new BusinessException("");		
			}
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
          throw new BusinessException(e);
			
		}
	

	}
*/
	/*
	@Override
	public void bajar(Integer categoriaId) throws BusinessException {
try {
			
			CategoriaBean categoriaBean=obtener(categoriaId);
			
			int ordenAnterior=categoriaBean.getCategoriaOrden()+1;
			CategoriaBean categoriaBeanAnterior=(CategoriaBean) categoriaDao.obtenerListaIgualAcolumnaOrdenado("categoriaOrden", ordenAnterior+""+"","categoriaOrden","asc").get(0);
			categoriaBeanAnterior.setCategoriaOrden(categoriaBean.getCategoriaOrden());
			categoriaBean.setCategoriaOrden(ordenAnterior);
			
			actualizar(categoriaBeanAnterior);
			actualizar(categoriaBean);

			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
          throw new BusinessException(e);
			
		}
		
	}
*/
	@Transactional
	public void guardarConOrden(CategoriaBean categoriaBean)
			throws BusinessException {
		try {
			
			int total=	categoriaDao.obtenerTotalFilas();
			categoriaBean.setCategoriaOrden(total+1);
			guardar(categoriaBean);

			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
		          throw new BusinessException(e);
			}
		
	}
	
	@Transactional
	public void moverOrden(String idDiferentes) 	throws BusinessException{

	String ids[]=idDiferentes.split(",");

		try {
            int i=1;
			for (String id : ids) {
				CategoriaBean categoriaBean= obtener(new Integer(id));
				categoriaBean.setCategoriaOrden(i);
			    actualizar(categoriaBean);
			i++;
			}
			
		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
			   throw new BusinessException(e);
		}
		

	}
	
	
	
}
