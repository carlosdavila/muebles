package com.erp.cliente.controller;

import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.erp.cliente.model.ClienteBean;
import com.erp.cliente.service.ClienteService;
import com.erp.comun.util.ErpConstante;
import com.erp.tipodoc.model.TipoDocBean;
import com.erp.tipodoc.service.TipoDocService;

import frameworkfrivas.bean.RequestJqGridBean;
import frameworkfrivas.util.Utilitario;

@Controller
@RequestMapping("clienteController")
public class ClienteController {
	@Autowired
	private ClienteService clienteService;

	@Autowired
	private TipoDocService tipoDocService;

	@Autowired
	private Properties erpConfig;

	private static final Logger LOG = Logger.getLogger(ClienteController.class);

	@RequestMapping(value = "/mostrarGestionCliente", method = RequestMethod.GET)
	public ModelAndView mostrarGestionCliente(HttpSession session) {
		return new ModelAndView("cliente/gestionCliente");
	}

	@RequestMapping(value = "/obtenerListaFiltrada", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerListaFiltrada(
			RequestJqGridBean requestJqGridBean) {

		String json = "";
		try {
			String[] campos = {"clienteNombre" ,"clienteNroDoc","clienteTelefono","clienteEmail","clienteDireccion","tipoDocBean.tipoDocDesc"};
			requestJqGridBean.setColumnaCoincidencia(campos);
			json = Utilitario.formatearJsonString(clienteService.obtenerListaPaginado(requestJqGridBean));
		} catch (Exception e) {
		System.out.println("e="+e);
			LOG.error(e.getMessage(), e);
		}
	
		return json;
	}

	@RequestMapping(value = "/mostrarBuscarCliente", method = RequestMethod.GET)
	public ModelAndView mostrarBuscarCliente() {
		return new ModelAndView("cliente/buscarCliente");
	}

	
	@RequestMapping(value = "/mostrarNuevoCliente", method = RequestMethod.GET)
	public ModelAndView mostrarNuevoCliente() {
		ModelAndView model = new ModelAndView("cliente/nuevoCliente");
		try {
			List<TipoDocBean> listaTipoDoc = tipoDocService.buscarTodo();
			model.addObject("listaTipoDoc", listaTipoDoc);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		model.addObject("clienteBean", new ClienteBean());
		return model;
	}

	@RequestMapping(value = "/mostrarEditarCliente", method = RequestMethod.GET)
	public ModelAndView mostrarEditarCliente(Integer clienteId) {

		
		ClienteBean clienteBean = null;
		List<TipoDocBean> listaTipoDoc=null;
		try {

			clienteBean = clienteService.obtener(clienteId);
            listaTipoDoc = tipoDocService.buscarTodo();
		
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		ModelAndView model = new ModelAndView("cliente/editarCliente");
		model.addObject("listaTipoDoc", listaTipoDoc);
		model.addObject("clienteBean", clienteBean);
		return model;
	}

	@RequestMapping(value = "/guardar", method = RequestMethod.POST)
	@ResponseBody
	public void guardar(ClienteBean clienteBean) {
		try {
			clienteService.guardar(clienteBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	@ResponseBody
	public void editar(ClienteBean clienteBean) {
		try {
			clienteService.actualizar(clienteBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.GET)
	@ResponseBody
	public String editar(Integer clienteId) {
		String mensajeError="";
		try {
			clienteService.eliminar(clienteId);

		} catch (DataIntegrityViolationException e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (ConstraintViolationException e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (Exception e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_GENERICO);
			LOG.error(e.getMessage(), e);
		}
		return mensajeError;

	}

}
