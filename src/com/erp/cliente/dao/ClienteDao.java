package com.erp.cliente.dao;


import com.erp.cliente.model.ClienteBean;

import frameworkfrivas.dao.ComunDao;

public interface ClienteDao extends ComunDao<ClienteBean, Integer> {

}
