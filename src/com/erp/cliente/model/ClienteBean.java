package com.erp.cliente.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.erp.tipodoc.model.TipoDocBean;

@Entity
@Table(name = "t_cliente" )
@Proxy(lazy=true)
public class ClienteBean {
	@Id
	@GeneratedValue
	@Column(name = "cliente_id")
	private Integer clienteId;
	@ManyToOne
	@JoinColumn(name = "tipo_doc_id")
	private TipoDocBean tipoDocBean;
	@Column(name = "cliente_nro_doc")
	private String clienteNroDoc;
	@Column(name = "cliente_nombre")
	private String clienteNombre;
	@Column(name = "cliente_email")
	private String clienteEmail;
	@Column(name = "cliente_telefono")
	private String clienteTelefono;
	@Column(name = "cliente_direccion")
	private String clienteDireccion;
	@Column(name = "cliente_contacto")
	private String clienteContacto;

	public Integer getClienteId() {
		return clienteId;
	}

	public void setClienteId(Integer clienteId) {
		this.clienteId = clienteId;
	}

	public TipoDocBean getTipoDocBean() {
		return tipoDocBean;
	}

	public void setTipoDocBean(TipoDocBean tipoDocBean) {
		this.tipoDocBean = tipoDocBean;
	}

	public String getClienteNroDoc() {
		return clienteNroDoc;
	}

	public void setClienteNroDoc(String clienteNroDoc) {
		this.clienteNroDoc = clienteNroDoc;
	}

	public String getClienteNombre() {
		return clienteNombre;
	}

	public void setClienteNombre(String clienteNombre) {
		this.clienteNombre = clienteNombre;
	}

	public String getClienteEmail() {
		return clienteEmail;
	}

	public void setClienteEmail(String clienteEmail) {
		this.clienteEmail = clienteEmail;
	}

	public String getClienteTelefono() {
		return clienteTelefono;
	}

	public void setClienteTelefono(String clienteTelefono) {
		this.clienteTelefono = clienteTelefono;
	}

	public String getClienteDireccion() {
		return clienteDireccion;
	}

	public void setClienteDireccion(String clienteDireccion) {
		this.clienteDireccion = clienteDireccion;
	}

	public String getClienteContacto() {
		return clienteContacto;
	}

	public void setClienteContacto(String clienteContacto) {
		this.clienteContacto = clienteContacto;
	}
	

}
