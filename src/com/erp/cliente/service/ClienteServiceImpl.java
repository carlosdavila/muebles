package com.erp.cliente.service;

import org.springframework.stereotype.Service;

import com.erp.cliente.model.ClienteBean;

import frameworkfrivas.service.ComunServiceImpl;

@Service
public class ClienteServiceImpl extends ComunServiceImpl<ClienteBean, Integer>
		implements ClienteService {

}
