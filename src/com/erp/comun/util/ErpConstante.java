package com.erp.comun.util;

public class ErpConstante {
	public static final String ERP_DESCRIPCION_SELECCIONE = "-- Seleccione --";
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String DATE_FORMAT_BD = "YYYY-MM-dd";
	public static final String KEY_PARAMETRO_TCDOLAR_ID = "parametro.tcdolar.id";
	public static final String KEY_PARAMETRO_IGV_ID = "parametro.igv.id";

	public static final String KEY_TIPODOC_DNI_ID = "tipodoc.dni.id";
	public static final String KEY_TIPODOC_RUC_ID = "tipodoc.ruc.id";

	public static final String PRIMERA_FECHA = "01/01/1000";
	public static final String ULTIMA_FECHA = "31/12/3000";
	public static final String VALOR_BLANCO = "";

	public static final String KEY_RAIZ_DIRECTORIO = "erp.directorio.archivo.raiz";

	public static final String MENSAJE_ERROR_KEY_FORANEO = "mensaje.error.keyforaneo";
	public static final String MENSAJE_ERROR_GENERICO = "mensaje.error.generico";

	public static final Integer TIPO_INSUMO_PAPEL = 2;
	public static final Integer TIPO_INSUMO_PLACA = 3;
	public static final Integer TIPO_INSUMO_IMPRENTA = 4;
	public static final Integer TIPO_INSUMO_ACABADO = 5;
	public static final Integer TIPO_INSUMO_TERMINADO = 6;
	public static final Integer TIPO_INSUMO_PERSONALIZADO = 7;
	public static final Integer TIPO_INSUMO_TERMINADO_LOMO = 8;
	public static final Integer TIPO_INSUMO_ACABADO_ESPECIAL = 9;

	public static final String PAPEL_30X30 = "30x30";
	public static final String PAPEL_45X40 = "45x40";
	public static final String PAPEL_50X55 = "50x55";
	public static final String SEPARACION_DE_MEDIDA = "X";

	public static final Integer DESCRIPCION_TIPO_INTERIOR = 1;
	public static final String DESCRIPCION_TIPO_INTERIOR_DESC = "INTERIOR";

	public static final Integer DESCRIPCION_TIPO_EXTERIOR = 2;
	public static final String DESCRIPCION_TIPO_EXTERIOR_DESC = "EXTERIOR";

	public static final Integer DESCRIPCION_TIPO_OTRO = 3;
	public static final String DESCRIPCION_TIPO_OTRO_DESC = "OTROS";
	
	public static final Integer DESCRIPCION_TIPO_SOBRE = 4;
	public static final String DESCRIPCION_TIPO_SOBRE_DESC = "SOBRE";

	public static final String SEPARACION_ITEM = "|";

	public static final String KEY_PATRON_FECHA_ARCHIVO = "atenea.nombre.archivo.patron.fecha";
   
	public static final String USUARIO_DESACTIVADO = "0";
	public static final String USUARIO_ACTIVO = "1";
	public static final String USUARIO_O_CLAVE_INCORRECTA = "2";
	public static final String USUARIO_SESSION = "usuario";
	public static final String SESSION_CARRITO = "listaCarritoDetalle";
	public static final String SESSION_CATEGORIA = "listaCategoria";
	public static final String SESSION_ADJUNTO = "listaAdjunto";
	public static final String SESSION_ADJUNTO_MODELO = "listaAdjuntoModelo";
	public static final String CORREO_USER = "correo.user";
	public static final String CORREO_CLAVE = "correo.clave";
	public static final String CORREO_HOST = "correo.host";
	public static final String CORREO_PUERTO = "correo.puerto";
	public static final String CORREO_ENVIO = "correo.envio";
	public static final String RUTA = "erp.ruta";
	public static final String USUARIO_PUBLICO = "publico";
	public static final String SESSION_MODELO = "listaModelo";
	public static final String SESSION_ESPECIFICACION = "listaEspecificacion";
	public static final String TAMANIO_IMAGEN_PRODUCTO = "tamanio.imagen.producto";
	public static final String TAMANIO_IMAGEN_MODELO = "tamanio.imagen.modelo";
	public static final int TAMANIO_GRANDE = 15;
	public static final int TAMANIO_MAX_ESPECIFICACION = 220;
}