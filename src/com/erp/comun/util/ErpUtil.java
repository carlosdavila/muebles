package com.erp.comun.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.erp.insumo.model.InsumoBean;
import com.erp.tipodoc.model.TipoDocBean;

public class ErpUtil {

	public static void agregarOpcionSeleccioneInsumo(
			List<InsumoBean> lista) {
		InsumoBean lp = new InsumoBean();
		lp.setInsumoNombre(ErpConstante.ERP_DESCRIPCION_SELECCIONE);
		lista.add(0, lp);
	}


	public static void agregarOpcionSeleccioneTipoDocBean(
			List<TipoDocBean> lista) {
		TipoDocBean lp = new TipoDocBean();
		lp.setTipoDocDesc(ErpConstante.ERP_DESCRIPCION_SELECCIONE);
		lista.add(0, lp);
	}

    public static String obtenerMedidaPapel(String medida){
    	String result="";
    	
    BigDecimal resul1=	new BigDecimal(obtenerArea(ErpConstante.PAPEL_30X30)).divide(new BigDecimal(obtenerArea(medida)));
    BigDecimal resul2=	new BigDecimal(obtenerArea(ErpConstante.PAPEL_45X40)).divide(new BigDecimal(obtenerArea(medida)));
    BigDecimal resul3=	new BigDecimal(obtenerArea(ErpConstante.PAPEL_50X55)).divide(new BigDecimal(obtenerArea(medida)));
    BigDecimal resp=new BigDecimal("0.0");	
    if( resul1.doubleValue() < resul2.doubleValue()){
    	resp=resul1;
    	result=ErpConstante.PAPEL_30X30;
    	}else{
    		resp=resul2;	
    		result=ErpConstante.PAPEL_45X40;
    	}
    
    if( resul3.doubleValue() < resp.doubleValue()){
    	resp=resul3;
    	result=ErpConstante.PAPEL_50X55;
    	}
    
    	return result;
    }

   public static Integer obtenerArea(String medida){
	   Integer resultado=0;
	   
	   String medida1=medida.split("x")[0];
	   String medida2=medida.split("x")[1];
	   resultado=new Integer(medida1)*new Integer(medida2);
	   return resultado;
   }
   
   public static String obtenerNumeroAleatorio(){
	long tiempo= new Date().getTime();
	long aleatorio= Math.round(Math.random()*100000);
    String numeroAleatorio=aleatorio+""+tiempo;
    return numeroAleatorio;
   }

}