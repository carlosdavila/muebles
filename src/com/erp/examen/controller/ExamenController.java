package com.erp.examen.controller;

import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.erp.comun.util.ErpConstante;
import com.erp.examen.model.ExamenBean;
import com.erp.examen.service.ExamenService;

import frameworkfrivas.bean.RequestJqGridBean;
import frameworkfrivas.util.Utilitario;
@Controller
@RequestMapping("examenController")
public class ExamenController {
	@Autowired
	private ExamenService examenService;
	
	@Autowired
	private Properties erpConfig;
	
	private static final Logger LOG = Logger.getLogger(ExamenController.class);

	@RequestMapping(value = "/mostrarGestionExamen", method = RequestMethod.GET)
	public ModelAndView mostrarGestionExamen(HttpSession session) {
		return new ModelAndView("examen/gestionExamen");
	}

	@RequestMapping(value = "/obtenerListaFiltrada", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerListaFiltrada(RequestJqGridBean requestJqGridBean) {

		String json = "";
		try {

			//json = Utilitario.formatearJsonString(examenService.obtenerListaPorCoincidenciaColumnaPag(requestJqGridBean));
		json=null;
		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}

		return json;
	}

	@RequestMapping(value = "/mostrarNuevoExamen", method = RequestMethod.GET)
	public ModelAndView mostrarNuevoExamen() {
		ModelAndView model = new ModelAndView("examen/nuevoExamen");

		model.addObject("examenBean", new ExamenBean());
		return model;
	}

	@RequestMapping(value = "/mostrarEditarExamen", method = RequestMethod.GET)
	public ModelAndView mostrarEditarExamen(Integer examenId) {

		ExamenBean examenBean = null;

		try {
			examenBean = examenService.obtener(examenId);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		ModelAndView model = new ModelAndView("examen/editarExamen");
		model.addObject("examenBean", examenBean);
		return model;
	}

	@RequestMapping(value = "/guardar", method = RequestMethod.POST)
	@ResponseBody
	public Integer guardar(ExamenBean examenBean) {
		Integer idExamen=0;
		try {
			examenService.guardar(examenBean);
			idExamen=examenBean.getExamenId();
		} catch (Exception e) {
			System.out.println("retornra pagina error");
			LOG.error(e.getMessage(), e);
		}
		return idExamen;
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	@ResponseBody
	public void editar(ExamenBean examenBean) {
		try {
			examenService.actualizar(examenBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.GET)
	@ResponseBody
	public String eliminar(Integer examenId) {
		String mensajeError = "";
		try {
			examenService.eliminar(examenId);

		} catch (DataIntegrityViolationException e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (ConstraintViolationException e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (Exception e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_GENERICO);
			LOG.error(e.getMessage(), e);
		}
		return mensajeError;

	}

}
