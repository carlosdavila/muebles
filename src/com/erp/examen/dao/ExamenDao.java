package com.erp.examen.dao;

import com.erp.examen.model.ExamenBean;

import frameworkfrivas.dao.ComunDao;

public interface ExamenDao  extends ComunDao<ExamenBean, Integer> {

}
