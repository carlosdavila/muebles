package com.erp.examen.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;
@Entity
@Table(name = "t_examen")
@Proxy(lazy = true)
public class ExamenBean {
	
		@Id
		@GeneratedValue
		@Column(name = "examen_id")
		private Integer examenId;
		
		@Column(name = "examen_codigo")
		private String examenCodigo;
		
		@Column(name = "examen_nombre")
		private String examenNombre;
				
		@Column(name = "examen_nota")
		private Double examenNota;

		public Integer getExamenId() {
			return examenId;
		}

		public void setExamenId(Integer examenId) {
			this.examenId = examenId;
		}

		public String getExamenCodigo() {
			return examenCodigo;
		}

		public void setExamenCodigo(String examenCodigo) {
			this.examenCodigo = examenCodigo;
		}

		public String getExamenNombre() {
			return examenNombre;
		}

		public void setExamenNombre(String examenNombre) {
			this.examenNombre = examenNombre;
		}

		public Double getExamenNota() {
			return examenNota;
		}

		public void setExamenNota(Double examenNota) {
			this.examenNota = examenNota;
		}



}
