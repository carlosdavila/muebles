package com.erp.inicio;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.erp.base.BaseController;
import com.erp.comun.util.ErpConstante;
import com.erp.usuario.model.UsuarioBean;

/**
 * Handles requests for the application home page.
 */
@Controller
public class InicioController extends BaseController {




	private static final Logger LOG = Logger.getLogger(InicioController.class);


	@RequestMapping(value = "/")
	public ModelAndView inicio(HttpSession session, Device device, String nroActivacion, String nroRecuperarClave) {

		
		LOG.debug("Entro debug");
		LOG.error("Entro error");
		
		ModelAndView model = new ModelAndView("home");
		UsuarioBean usuarioBean= (UsuarioBean) session.getAttribute(ErpConstante.USUARIO_SESSION) ;
		model.addObject("usuarioBean",usuarioBean);
		model.addObject("isPc", device.isNormal());
		model.addObject("listaCategoria", obtenerCategoria(session));
		
		/*
		UsuarioBean usuarioBean;

		try {

			if (nroRecuperarClave != null) {
				usuarioBean = usuarioDao.obtenerUsuarioPorNroClave(nroRecuperarClave);
				if( usuarioBean!=null && usuarioBean.getUsuarioUser()!=null){
					model.addObject("mostrarCambiarClave", true);
					usuarioBean.setUsuarioNroClave(null);
				    usuarioBean.setUsuarioEstado(ErpConstante.USUARIO_ACTIVO);
					usuarioDao.actualizar(usuarioBean);
                 }
				
				session.setAttribute(ErpConstante.USUARIO_SESSION, usuarioBean);
			}

			if (nroActivacion != null) {
				usuarioBean = usuarioDao.obtenerUsuarioPorNroActivacion(nroActivacion);
					
				if (!ErpConstante.USUARIO_ACTIVO.equals(usuarioBean.getUsuarioEstado())) {
					usuarioBean.setUsuarioEstado(ErpConstante.USUARIO_ACTIVO);
					usuarioBean.setUsuarioNroActivacion(null);
					usuarioDao.actualizar(usuarioBean);
					model.addObject("mostrarMensajeUsuarioActivo", true);
				}

				session.setAttribute(ErpConstante.USUARIO_SESSION, usuarioBean);
			}

			usuarioBean = obtenerUsuarioSession(session);

			if (!ErpConstante.USUARIO_ACTIVO.equals(usuarioBean.getUsuarioEstado())) {
				model.addObject("isUsuarioActivo", false);
			}

			model.addObject(ErpConstante.USUARIO_SESSION, usuarioBean);
			
			model.addObject("isPc", device.isNormal());
			// model.addObject("listaCategoria", listaCategoria);
		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}
*/
		return model;
	}

	
	@RequestMapping(value = "/mostrarMenu")
	public ModelAndView mostrarMenu(HttpSession session, Device device) {

		ModelAndView model = new ModelAndView("menu");
		UsuarioBean usuarioBean;

		try {

			usuarioBean = obtenerUsuarioSession(session);

			if (!ErpConstante.USUARIO_ACTIVO.equals(usuarioBean.getUsuarioEstado())) {
				model.addObject("isUsuarioActivo", false);
			}

			model.addObject(ErpConstante.USUARIO_SESSION, usuarioBean);

			//List<CategoriaBean> listaCategoria = obtenerCategoria(session);


			model.addObject("isPc", device.isNormal());
		//	model.addObject("listaCategoria", listaCategoria);
		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}

		return model;
	}


	@RequestMapping(value = "/cabecera")
	public ModelAndView cabecera(HttpSession session, Device device, String nroActivacion) {

		ModelAndView model = new ModelAndView("cabecera");
		UsuarioBean usuarioBean;

		try {

			usuarioBean = obtenerUsuarioSession(session);

			if (ErpConstante.USUARIO_ACTIVO.equals(usuarioBean.getUsuarioEstado())) {

			} else {
				model.addObject("isUsuarioActivo", false);
			}

			model.addObject(ErpConstante.USUARIO_SESSION, usuarioBean);

			model.addObject("isPc", device.isNormal());

		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}

		return model;
	}


	@RequestMapping(value = "/imagen")
	public ModelAndView imagen() {

		return new ModelAndView("imagenes");
	}
	
	
	@RequestMapping(value = "/mostrarPoliticas")
	public ModelAndView mostrarPoliticas(HttpSession session, Device device) {

		ModelAndView model = new ModelAndView("politicas");
		UsuarioBean usuarioBean;

		try {

			usuarioBean = obtenerUsuarioSession(session);

	

			model.addObject(ErpConstante.USUARIO_SESSION, usuarioBean);

			model.addObject("isPc", device.isNormal());

		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}

		return model;
	}
	
	
	@RequestMapping(value = "/mostrarNosotros")
	public ModelAndView mostrarNosotros(HttpSession session, Device device) {

		ModelAndView model = new ModelAndView("nosotros");
		UsuarioBean usuarioBean;

		try {

			usuarioBean = obtenerUsuarioSession(session);

	

			model.addObject(ErpConstante.USUARIO_SESSION, usuarioBean);

			model.addObject("isPc", device.isNormal());

		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}

		return model;
	}
	

}
