package com.erp.insumo.controller;

import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.erp.base.BaseController;
import com.erp.comun.util.ErpConstante;
import com.erp.insumo.model.InsumoBean;
import com.erp.insumo.service.InsumoService;

import frameworkfrivas.bean.RequestJqGridBean;
import frameworkfrivas.util.Utilitario;

@Controller
@RequestMapping("insumoController")
public class InsumoController extends BaseController {
	@Autowired
	private InsumoService insumoService;

	@Autowired
	private Properties erpConfig;
	@Autowired
	
	private static final Logger LOG = Logger.getLogger(InsumoController.class);

	@RequestMapping(value = "/mostrarGestionInsumo", method = RequestMethod.GET)
	public ModelAndView mostrarGestionInsumo(HttpSession session) {

		ModelAndView model = new ModelAndView("insumo/gestionInsumo");

		return model;
	}

	@RequestMapping(value = "/mostrarAgregarInsumo", method = RequestMethod.GET)
	public ModelAndView mostrarAgregarInsumo(HttpSession session) {
		return new ModelAndView("insumo/agregarInsumo");
	}


	@RequestMapping(value = "/obtenerListaFiltrada", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerListaFiltrada(RequestJqGridBean requestJqGridBean) {

		String json = "";
		try {

			json = Utilitario.formatearJsonString(insumoService
					.obtenerListaInsumoPorColumna(requestJqGridBean));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return json;
	}

	@RequestMapping(value = "/mostrarNuevoInsumo", method = RequestMethod.GET)
	public ModelAndView mostrarNuevoCliente() {
		ModelAndView model = new ModelAndView("insumo/nuevoInsumo");
		
		model.addObject("insumoBean", new InsumoBean());

		return model;
	}

	@RequestMapping(value = "/mostrarEditarInsumo", method = RequestMethod.GET)
	public ModelAndView mostrarEditarInsumo(Integer insumoId) {

		ModelAndView model = new ModelAndView("insumo/editarInsumo");
	
		InsumoBean insumoBean = null;

		try {

			insumoBean = insumoService.obtenerInsumo(insumoId);
	
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		model.addObject("insumoBean", insumoBean);

		return model;
	}

	@RequestMapping(value = "/mostrarAgregarStock", method = RequestMethod.GET)
	public ModelAndView mostrarAgregarStock(Integer insumoId) {

		ModelAndView model = new ModelAndView("insumo/agregarStock");
		InsumoBean insumoBean = null;
		try {

			insumoBean = insumoService.obtenerInsumo(insumoId);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		model.addObject("insumoBean", insumoBean);
		return model;
	}

		

	@RequestMapping(value = "/guardar", method = RequestMethod.POST)
	@ResponseBody
	public void guardar(InsumoBean insumoBean) {
		try {


			insumoService.guardar(insumoBean);

		} catch (Exception e) {
			System.out.println(e);
			LOG.error(e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	@ResponseBody
	public void editar(InsumoBean insumoBean) {
		try {

			insumoService.actualizar(insumoBean);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}

	@RequestMapping(value = "/editarStock", method = RequestMethod.POST)
	@ResponseBody
	public void editarStock(InsumoBean insumoBean) {
		try {
			insumoService.editarStock(insumoBean);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}



	@RequestMapping(value = "/eliminar", method = RequestMethod.GET)
	@ResponseBody
	public String eliminar(Integer insumoId) {
		String mensajeError = "";
		try {
			insumoService.eliminar(insumoId);
		} catch (DataIntegrityViolationException e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (ConstraintViolationException e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (Exception e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_GENERICO);
			LOG.error(e.getMessage(), e);
		}
		return mensajeError;

	}
}
