package com.erp.insumo.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.erp.insumo.model.InsumoBean;

import frameworkfrivas.dao.ComunDao;
import frameworkfrivas.exception.DAOException;

public interface InsumoDao extends ComunDao<InsumoBean, Integer> {
	
	public List<InsumoBean> obtenerListaInsumoPorTipo(Integer tipo)
			throws DAOException ;
	@Transactional
	public List<InsumoBean> obtenerListaInsumoPorTipoPapel(Integer medida1,Integer medida2)
			throws DAOException ;
	@Transactional
	public List<InsumoBean> obtenerListaInsumoAgrupadoPorNombrePapel()
			throws DAOException;
	public List<InsumoBean> obtenerListaInsumoPorNombrePapel(String nombrePapel)
			throws DAOException ;
}
