package com.erp.insumo.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.erp.comun.util.ErpConstante;
import com.erp.insumo.model.InsumoBean;

import frameworkfrivas.dao.ComunDaoImpl;
import frameworkfrivas.exception.DAOException;

public class InsumoDaoImpl extends ComunDaoImpl<InsumoBean, Integer> implements
		InsumoDao {
	private static final Logger LOG = Logger.getLogger(InsumoDaoImpl.class);

	public InsumoDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	
	@Transactional
	public List<InsumoBean> obtenerListaInsumoPorTipo(Integer tipo)
			throws DAOException {
		List<InsumoBean> list = null;
		try {

			Query query = getCurrentSession().createQuery(
					" from " + getDomainClass().getName() + " where insumoTipo="
							+  tipo );

			list = query.list();

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new DAOException(e);
		}

		return list;
	}
	
	@Transactional
	public List<InsumoBean> obtenerListaInsumoPorTipoPapel(Integer medida1,Integer medida2)
			throws DAOException {
		List<InsumoBean> list = null;
		try {

			Query query = getCurrentSession().createQuery(
					" from " + getDomainClass().getName() + " where insumoTipo="
							+  ErpConstante.TIPO_INSUMO_PAPEL+" and insumoMedida1="+medida1+" and insumoMedida2="+ medida2);

			list = query.list();

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new DAOException(e);
		}

		return list;
	}

	@Transactional
	public List<InsumoBean> obtenerListaInsumoAgrupadoPorNombrePapel()
			throws DAOException {
		List<InsumoBean> list = new ArrayList<InsumoBean>();
		try {

			 String SqlQuery = "select insumoId,insumoTipo,insumoNombre "
			 		+ "from  "+ getDomainClass().getName() + " group by insumoNombre having insumoTipo="
							+  ErpConstante.TIPO_INSUMO_PAPEL;  

			Query query = getCurrentSession().createQuery(SqlQuery);  

			 for(Iterator<?> it=query.iterate();it.hasNext();)  
			  {  
			   Object[] row = (Object[]) it.next();  

			   InsumoBean insumoBean= new InsumoBean();
			   insumoBean.setInsumoId(new Integer(row[0].toString()));
			   insumoBean.setInsumoNombre(row[2].toString());
			   list.add(insumoBean);
			  } 
			
	
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new DAOException(e);
		}

		return list;
	}
	
	
	@Transactional
	public List<InsumoBean> obtenerListaInsumoPorNombrePapel(String nombrePapel)
			throws DAOException {
		List<InsumoBean> list = null;
		try {

			Query query = getCurrentSession().createQuery(
					" from " + getDomainClass().getName() + " where insumoTipo="
							+  ErpConstante.TIPO_INSUMO_PAPEL+" and insumoNombre='"+nombrePapel+"'");

			list = query.list();

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new DAOException(e);
		}

		return list;
	}
	
	
	
	
	
	
	


}
