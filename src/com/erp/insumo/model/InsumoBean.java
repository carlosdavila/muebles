package com.erp.insumo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "t_insumo")
@Proxy(lazy = true)
public class InsumoBean {
	@Id
	@GeneratedValue
	@Column(name = "insumo_id")
	private Integer insumoId;

	@Column(name = "insumo_codigo")
	private String insumoCodigo;

	@Column(name = "insumo_Nombre")
	private String insumoNombre;
	
	@Column(name = "insumo_UM")
	private String insumoUM;

	@Column(name = "insumo_stock")
	private Integer insumoStock;
	
	@Column(name = "insumo_medida")
	private String insumoMedida;
	
	@Column(name = "insumo_Precio_Compra")
	private Double insumoPrecioCompra;
	
	@Column(name = "insumo_Precio_Venta")
	private Double insumoPrecioVenta;
	
	@Column(name = "insumo_ubicacion")
	private Integer insumoUbicacion;

	public Integer getInsumoId() {
		return insumoId;
	}

	public void setInsumoId(Integer insumoId) {
		this.insumoId = insumoId;
	}

	public String getInsumoCodigo() {
		return insumoCodigo;
	}

	public void setInsumoCodigo(String insumoCodigo) {
		this.insumoCodigo = insumoCodigo;
	}

	public String getInsumoNombre() {
		return insumoNombre;
	}

	public void setInsumoNombre(String insumoNombre) {
		this.insumoNombre = insumoNombre;
	}

	public String getInsumoUM() {
		return insumoUM;
	}

	public void setInsumoUM(String insumoUM) {
		this.insumoUM = insumoUM;
	}

	public Integer getInsumoStock() {
		return insumoStock;
	}

	public void setInsumoStock(Integer insumoStock) {
		this.insumoStock = insumoStock;
	}

	public String getInsumoMedida() {
		return insumoMedida;
	}

	public void setInsumoMedida(String insumoMedida) {
		this.insumoMedida = insumoMedida;
	}

	public Double getInsumoPrecioCompra() {
		return insumoPrecioCompra;
	}

	public void setInsumoPrecioCompra(Double insumoPrecioCompra) {
		this.insumoPrecioCompra = insumoPrecioCompra;
	}

	public Double getInsumoPrecioVenta() {
		return insumoPrecioVenta;
	}

	public void setInsumoPrecioVenta(Double insumoPrecioVenta) {
		this.insumoPrecioVenta = insumoPrecioVenta;
	}

	public Integer getInsumoUbicacion() {
		return insumoUbicacion;
	}

	public void setInsumoUbicacion(Integer insumoUbicacion) {
		this.insumoUbicacion = insumoUbicacion;
	}


	
}
