package com.erp.insumo.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class InsumoCalculo {
	private int tipoCalculo;
	private Integer cantidad;
	private Double vf1;
	private Double vf2;
	private Double vf3;

	public InsumoCalculo(int tipoCalculo) {
		super();
		this.tipoCalculo = tipoCalculo;
	}

	public InsumoCalculo(Double vf1, Double vf2, Double vf3, int tipoCalculo) {

		this.vf1 = vf1;
		this.vf2 = vf2;
		this.vf3 = vf3;
		this.tipoCalculo = tipoCalculo;
	}

	public InsumoCalculo(Integer cantidad, Double vf1, Double vf2, Double vf3,
			int tipoCalculo) {
		this.cantidad = cantidad;
		this.vf1 = vf1;
		this.vf2 = vf2;
		this.vf3 = vf3;
		this.tipoCalculo = tipoCalculo;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getVf1() {
		return vf1;
	}

	public void setVf1(Double vf1) {
		this.vf1 = vf1;
	}

	public Double getVf2() {
		return vf2;
	}

	public void setVf2(Double vf2) {
		this.vf2 = vf2;
	}

	public Double getVf3() {
		return vf3;
	}

	public void setVf3(Double vf3) {
		this.vf3 = vf3;
	}

	public int getTipoCalculo() {
		return tipoCalculo;
	}

	public void setTipoCalculo(int tipoCalculo) {
		this.tipoCalculo = tipoCalculo;
	}

	public Double getCalculo() {
		Double resultado = 0.0;
		if (tipoCalculo == 1) {
			resultado = getCalculo1();
		} else if (tipoCalculo == 2) {
			resultado = getCalculo2();
		}

		return resultado;
	}

	public String getNombreCalculo() {
		String resultado = "";
		if (tipoCalculo == 1) {
			resultado = getNombreCalculo1();
		} else if (tipoCalculo == 2) {
			resultado = getNombreCalculo2();
		}

		return resultado;
	}

	public String getDescripcionCalculo() {
		String resultado = "";
		if (tipoCalculo == 1) {
			resultado = getDescripcionCalculo1();
		} else if (tipoCalculo == 2) {
			resultado = getDescripcionCalculo2();
		}

		return resultado;
	}

	private Double getCalculo1() {
		Double resultado = 0.0;

		if (cantidad != null && vf1 != null && vf2 != null && vf3 != null) {
			BigDecimal resul = new BigDecimal("0.0");
			BigDecimal cantidadInicial = new BigDecimal(cantidad);
			BigDecimal vf1Inicial = new BigDecimal(vf1);
			BigDecimal vf2Inicial = new BigDecimal(vf2);
			BigDecimal vf3Inicial = new BigDecimal(vf3);

			resul = cantidadInicial.multiply(vf1Inicial).multiply(vf2Inicial)
					.divide(vf3Inicial, 2, RoundingMode.HALF_UP);
			resultado = resul.doubleValue();
		}
		return resultado;
	}

	private String getNombreCalculo1() {
		return " ((Cantidad*v1)*v2)/v3";
	}

	private String getDescripcionCalculo1() {
		String cadena = "";
		if (vf1 != null && vf2 != null && vf3 != null) {
			cadena = " ((Cantidad*" + vf1 + ")*" + vf2 + ")/" + vf3;
		}

		return cadena;
	}

	private Double getCalculo2() {
		Double resultado = 0.0;

		if (cantidad != null && vf1 != null && vf2 != null) {
			BigDecimal resul = new BigDecimal("0.0");
			BigDecimal cantidadInicial = new BigDecimal(cantidad);
			BigDecimal vf1Inicial = new BigDecimal(vf1);
			BigDecimal vf2Inicial = new BigDecimal(vf2);

			resul = cantidadInicial.multiply(vf1Inicial).multiply(vf2Inicial);
			resultado = resul.setScale(2, RoundingMode.HALF_UP).doubleValue();
		}
		return resultado;
	}

	private String getNombreCalculo2() {
		return " (Cantidad*v1)*v2";
	}

	private String getDescripcionCalculo2() {

		String cadena = "";
		if (vf1 != null && vf2 != null && vf3 != null) {
			cadena = " (Cantidad*" + vf1 + ")*" + vf2;
		}
		return cadena;

	}

}
