package com.erp.insumo.model;

public class TipoInsumoBean {

	private Integer insumoTipo;
	private String descripcionTipo;
	public Integer getInsumoTipo() {
		return insumoTipo;
	}
	public void setInsumoTipo(Integer insumoTipo) {
		this.insumoTipo = insumoTipo;
	}
	public String getDescripcionTipo() {
		return descripcionTipo;
	}
	public void setDescripcionTipo(String descripcionTipo) {
		this.descripcionTipo = descripcionTipo;
	}

	
}
