package com.erp.insumo.service;

import com.erp.insumo.model.InsumoBean;

import frameworkfrivas.bean.RequestJqGridBean;
import frameworkfrivas.bean.ResponseJqGridBean;
import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.service.ComunService;

public interface InsumoService extends ComunService<InsumoBean, Integer> {

	public ResponseJqGridBean obtenerListaInsumoPorColumna(RequestJqGridBean requestJqGridBean)
			throws BusinessException;

	public InsumoBean obtenerInsumo(Integer insumoId) throws BusinessException;
	
	public void editarStock (InsumoBean insumoBean) throws BusinessException;
	/*
	public List<InsumoCalculo> obtenerListaCalculo() throws BusinessException;
	
	public Double calcularInsumo(Integer insumoId,Integer cantidad) throws BusinessException;
	
	public List<TipoInsumoBean>  getListaTipoInsumo();
	public List<InsumoBean> obtenerListaInsumoPorTipo(Integer tipo)
			 throws BusinessException;
	
	
	public ResponseJqGridBean obtenerListaImportantes(Integer insumoPapel,Integer insumoImprenta,Integer insumoAcabado)
			 throws BusinessException;
	
	public List<InsumoBean> obtenerListaInsumoAgrupadoPorNombrePapel()
			throws BusinessException;
	
	
	public List<InsumoBean> obtenerListaInsumoPorNombrePapel(String nombrePapel)
			throws BusinessException; 
	
	*/
}
