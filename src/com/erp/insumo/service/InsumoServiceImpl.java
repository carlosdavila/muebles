package com.erp.insumo.service;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.comun.util.ErpConstante;
import com.erp.insumo.dao.InsumoDao;
import com.erp.insumo.model.InsumoBean;
import com.erp.parametro.model.ParametroBean;
import com.erp.parametro.service.ParametroService;

import frameworkfrivas.bean.RequestJqGridBean;
import frameworkfrivas.bean.ResponseJqGridBean;
import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.service.ComunServiceImpl;

@Service
public class InsumoServiceImpl extends ComunServiceImpl<InsumoBean, Integer>
		implements InsumoService {
	private static final Logger LOG = Logger.getLogger(InsumoServiceImpl.class);

	@Autowired
	private Properties erpConfig;
	@Autowired
	private ParametroService parametroService;
	
	@Autowired
	private InsumoDao insumoDao;

	public ResponseJqGridBean obtenerListaInsumoPorColumna(
			RequestJqGridBean requestJqGridBean) throws BusinessException {
		ResponseJqGridBean jqgrid = new ResponseJqGridBean();
		try {
			ParametroBean parametroBean = parametroService
					.obtener(new Integer(erpConfig
							.getProperty(ErpConstante.KEY_PARAMETRO_TCDOLAR_ID)));

			jqgrid = null;
			for (Object object : jqgrid.getRows()) {
				InsumoBean insumo = (InsumoBean) object;
			//	insumo.setTc(new Double(parametroBean.getParametroValor()));
			}

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new BusinessException(e);
		}

		return jqgrid;
	}

	@Override
	public InsumoBean obtenerInsumo(Integer insumoId) throws BusinessException {
		ParametroBean parametroBean = parametroService.obtener(new Integer(
				erpConfig.getProperty(ErpConstante.KEY_PARAMETRO_TCDOLAR_ID)));
		InsumoBean insumoBean = new InsumoBean();

		try {
			insumoBean = obtener(insumoId);
		//	insumoBean.setTc(new Double(parametroBean.getParametroValor()));
		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
			throw new BusinessException(e);
		}

		return insumoBean;
	}

	@Override
	public void editarStock(InsumoBean insumoBean) throws BusinessException {
		InsumoBean insumoBeanInicial = new InsumoBean();
		try {
			insumoBeanInicial = obtener(insumoBean.getInsumoId());
			insumoBeanInicial.setInsumoStock(insumoBean.getInsumoStock());
			actualizar(insumoBeanInicial);
		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
			throw new BusinessException(e);
		}
	}
	/*
	@Override
	public List<InsumoCalculo> obtenerListaCalculo() throws BusinessException {
		List<InsumoCalculo> listaCalculo = new ArrayList<InsumoCalculo>();
		for (int i = 1; i < 3; i++) {
			listaCalculo.add(new InsumoCalculo(i));
		}
		return listaCalculo;
	}

	@Override
	public Double calcularInsumo(Integer insumoId, Integer cantidad)
			throws BusinessException {
		Double respuesta=0.0;
		InsumoBean insumoBean = new InsumoBean();
		try {
			insumoBean = obtener(insumoId);
			Double vf1=insumoBean.getInsumovf1();
			Double vf2=insumoBean.getInsumovf2();
			Double vf3=insumoBean.getInsumovf3();
			int tipoCalculo=insumoBean.getInsumoFormula();
			InsumoCalculo insumoCalculo= new InsumoCalculo(cantidad, vf1, vf2, vf3, tipoCalculo);
			respuesta=insumoCalculo.getCalculo();
		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
			throw new BusinessException(e);
		}
		return respuesta;
	}

	@Override
	public List<TipoInsumoBean> getListaTipoInsumo() {
		List<TipoInsumoBean> listTipoInsumo = new ArrayList<TipoInsumoBean>();
		
		TipoInsumoBean tipoInsumoBean= new TipoInsumoBean();
		tipoInsumoBean.setInsumoTipo(1);
		tipoInsumoBean.setDescripcionTipo("OTROS");
		listTipoInsumo.add(tipoInsumoBean);
		
		 tipoInsumoBean= new TipoInsumoBean();
		tipoInsumoBean.setInsumoTipo(ErpConstante.TIPO_INSUMO_PAPEL);
		tipoInsumoBean.setDescripcionTipo("PAPEL");
		listTipoInsumo.add(tipoInsumoBean);
		
		 tipoInsumoBean= new TipoInsumoBean();
		tipoInsumoBean.setInsumoTipo(ErpConstante.TIPO_INSUMO_PLACA);
		tipoInsumoBean.setDescripcionTipo("PLACA");
		listTipoInsumo.add(tipoInsumoBean);
		
		tipoInsumoBean= new TipoInsumoBean();
		tipoInsumoBean.setInsumoTipo(ErpConstante.TIPO_INSUMO_IMPRENTA);
		tipoInsumoBean.setDescripcionTipo("IMPRENTA");
		listTipoInsumo.add(tipoInsumoBean);
		
		
		tipoInsumoBean= new TipoInsumoBean();
		tipoInsumoBean.setInsumoTipo(ErpConstante.TIPO_INSUMO_ACABADO);
		tipoInsumoBean.setDescripcionTipo("ACABADO GENERAL");
		listTipoInsumo.add(tipoInsumoBean);
		
		tipoInsumoBean= new TipoInsumoBean();
		tipoInsumoBean.setInsumoTipo(ErpConstante.TIPO_INSUMO_ACABADO_ESPECIAL);
		tipoInsumoBean.setDescripcionTipo("ACABADO ESPECIAL");
		listTipoInsumo.add(tipoInsumoBean);
		
		
		tipoInsumoBean= new TipoInsumoBean();
		tipoInsumoBean.setInsumoTipo(ErpConstante.TIPO_INSUMO_TERMINADO);
		tipoInsumoBean.setDescripcionTipo("TERMINADO");
		listTipoInsumo.add(tipoInsumoBean);
		
		tipoInsumoBean= new TipoInsumoBean();
		tipoInsumoBean.setInsumoTipo(ErpConstante.TIPO_INSUMO_TERMINADO_LOMO);
		tipoInsumoBean.setDescripcionTipo("TERMINADO LOMO");
		listTipoInsumo.add(tipoInsumoBean);
		
		
		tipoInsumoBean= new TipoInsumoBean();
		tipoInsumoBean.setInsumoTipo(ErpConstante.TIPO_INSUMO_PERSONALIZADO);
		tipoInsumoBean.setDescripcionTipo("PERZONALIZADO");
		listTipoInsumo.add(tipoInsumoBean);
		
		return listTipoInsumo;
	}

	@Override
	public List<InsumoBean> obtenerListaInsumoPorTipo(Integer tipo)
			throws BusinessException {
		List<InsumoBean>  listaInsumo= null;
		try {
			listaInsumo= insumoDao.obtenerListaInsumoPorTipo(tipo);
		} catch (DAOException e) {
		System.out.println(e);
		}
		return listaInsumo;
	}

	@Override
	public  ResponseJqGridBean obtenerListaImportantes(Integer insumoPapel,
			Integer insumoImprenta, Integer insumoAcabado)
			throws BusinessException {

		
		ResponseJqGridBean jqgrid = new ResponseJqGridBean();
		
		
		try {
			InsumoBean beanPapel = insumoDao.obtener(insumoPapel);
			jqgrid.getRows().add(beanPapel);
			
			InsumoBean beanPlaca = insumoDao.obtener(beanPapel.getInsumoPlaca());
			jqgrid.getRows().add(beanPlaca);
			
		
			
			InsumoBean beanAcabado = insumoDao.obtener(insumoAcabado);
			jqgrid.getRows().add(beanAcabado);
			
		} catch (DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return jqgrid;
	}

	@Override
	public List<InsumoBean> obtenerListaInsumoAgrupadoPorNombrePapel()
			throws BusinessException {
		
		List<InsumoBean> list=null;
		try {
		list=insumoDao.obtenerListaInsumoAgrupadoPorNombrePapel();
		
		} catch (DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<InsumoBean> obtenerListaInsumoPorNombrePapel(String nombrePapel)
			throws BusinessException {
		List<InsumoBean> list=null;
		try {
		list=insumoDao.obtenerListaInsumoPorNombrePapel(nombrePapel);
		
		} catch (DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}


	*/
}