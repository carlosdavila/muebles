package com.erp.modelo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.erp.base.BaseController;
import com.erp.comun.util.ErpConstante;
import com.erp.modelo.model.ModeloBean;
import com.erp.modelo.service.ModeloService;
import com.erp.usuario.model.UsuarioBean;

import frameworkfrivas.bean.AdjuntoBean;
import frameworkfrivas.bean.RequestJqGridBean;
import frameworkfrivas.bean.ResponseJqGridBean;
import frameworkfrivas.util.Utilitario;

@Controller
@RequestMapping("modeloController")
public class ModeloController extends BaseController {

	@Autowired
	private ModeloService modeloService;

	@Autowired
	private Properties erpConfig;

	private static final Logger LOG = Logger.getLogger(ModeloController.class);

	@RequestMapping(value = "/obtenerListaFiltrada", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerListaFiltrada(RequestJqGridBean requestJqGridBean) {

		String json = "";
		try {

			requestJqGridBean.setSord("asc");
			requestJqGridBean.setSidx("categoriaBean.categoriaNombre");
			json = Utilitario.formatearJsonString(modeloService.obtenerListaPaginado(requestJqGridBean));

		} catch (Exception e) {
			System.out.println(e.getMessage());
			LOG.error(e.getMessage(), e);
		}

		return json;
	}

	@RequestMapping(value = "/mostrarGestionModelo", method = RequestMethod.GET)
	public ModelAndView mostrarGestionmodelo(HttpSession session) {
		return new ModelAndView("modelo/gestionModelo");
	}

	@RequestMapping(value = "/mostrarBuscarModelo", method = RequestMethod.GET)
	public ModelAndView mostrarBuscarModelo() {
		return new ModelAndView("modelo/buscarModelo");
	}

	@RequestMapping(value = "/listarModeloVenta", method = RequestMethod.GET)
	public ModelAndView listarModeloVenta(HttpSession session, Device device, Integer pagina, Integer categoriaId) {

		ModelAndView model = new ModelAndView("modelo");
		try {

			RequestJqGridBean requestJqGridBean = new RequestJqGridBean();
			
			String[] columnaEquivalente = { "categoriaBean.categoriaId"};
			String[] columnaValorEquivalente = { categoriaId.toString()};
			
			requestJqGridBean.setColumnaEquivalente(columnaEquivalente);
			requestJqGridBean.setValorEquivalente(columnaValorEquivalente);
			// requestJqGridBean.setPage(pagina);
			// requestJqGridBean.setRows(6);
			requestJqGridBean.setSidx("modeloNombre");
			requestJqGridBean.setSord("asc");

			ResponseJqGridBean responseJqGridBean = modeloService.obtenerListaPaginado(requestJqGridBean);
			List<Object> listaObjModelo = responseJqGridBean.getRows();

			List<ModeloBean> listaModelo = new ArrayList<ModeloBean>();

			for (Object object : listaObjModelo) {

				ModeloBean modeloBean = (ModeloBean) object;
				modeloBean.setModeloNombre(modeloBean.getModeloNombre().toUpperCase());
				listaModelo.add(modeloBean);
			}

			model.addObject("listaModelo", listaModelo);
			model.addObject("total", responseJqGridBean.getTotal());
			model.addObject("pagina", responseJqGridBean.getPage());
			String ruta = erpConfig.getProperty(ErpConstante.KEY_RAIZ_DIRECTORIO);
			model.addObject("categoriaId", categoriaId);
			model.addObject("ruta", ruta);
			model.addObject("isPc", device.isNormal());
			UsuarioBean usuarioBean = (UsuarioBean) session.getAttribute(ErpConstante.USUARIO_SESSION);
			model.addObject("usuarioBean", usuarioBean);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return model;
	}

	@RequestMapping(value = "/mostrarNuevaModelo", method = RequestMethod.GET)
	public ModelAndView mostrarNuevaModelo(HttpSession session) {

		session.setAttribute(ErpConstante.SESSION_ADJUNTO_MODELO, new ArrayList<AdjuntoBean>());
		ModelAndView model = new ModelAndView("modelo/nuevaModelo");
		try {
			ModeloBean modeloBean = new ModeloBean();
			model.addObject("listaCategoria", obtenerCategoria(session));
			model.addObject("modeloBean", modeloBean);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return model;

	}

	@RequestMapping(value = "/mostrarEditarModelo", method = RequestMethod.GET)
	public ModelAndView mostrarEditarModelo(HttpSession session, Integer modeloId) {

		ModelAndView model = new ModelAndView("modelo/editarModelo");
		ModeloBean modeloBean = null;
		String json = "";
		try {

			modeloBean = modeloService.obtener(modeloId);
			List<AdjuntoBean> listaAdjunto = new ArrayList<AdjuntoBean>();
			if (modeloBean.getModeloImagen() != null) {
				AdjuntoBean adjuntoBean = new AdjuntoBean();
				adjuntoBean.setNombre(modeloBean.getModeloImagen());
				listaAdjunto.add(adjuntoBean);
			}
			if (modeloBean.getModeloImagen1() != null) {
				AdjuntoBean adjuntoBean2 = new AdjuntoBean();
				adjuntoBean2.setNombre(modeloBean.getModeloImagen1());
				listaAdjunto.add(adjuntoBean2);
			}
			session.setAttribute(ErpConstante.SESSION_ADJUNTO_MODELO, listaAdjunto);
			json = Utilitario.formatearJsonString(listaAdjunto);
			model.addObject("modeloBean", modeloBean);
			model.addObject("json", json);
			String ruta = erpConfig.getProperty(ErpConstante.KEY_RAIZ_DIRECTORIO);
			model.addObject("ruta", ruta);
			model.addObject("listaCategoria", obtenerCategoria(session));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return model;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/guardar", method = RequestMethod.POST)
	@ResponseBody
	public void guardar(HttpSession session, ModeloBean modeloBean) throws Exception {
		try {

			List<AdjuntoBean> listaAdjunto = (List<AdjuntoBean>) session
					.getAttribute(ErpConstante.SESSION_ADJUNTO_MODELO);
			modeloService.guardarImagen(modeloBean, listaAdjunto);
			session.setAttribute(ErpConstante.SESSION_ADJUNTO_MODELO, listaAdjunto);

			if (modeloBean.getModeloNombre() != null) {
				modeloBean.setModeloNombre(modeloBean.getModeloNombre().toUpperCase());

			}
			modeloService.guardar(modeloBean);

		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
			throw new Exception();
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	@ResponseBody
	public void editar(HttpSession session, ModeloBean modeloBean) throws Exception {
		try {

			List<AdjuntoBean> listaAdjunto = (List<AdjuntoBean>) session
					.getAttribute(ErpConstante.SESSION_ADJUNTO_MODELO);
			modeloService.guardarImagen(modeloBean, listaAdjunto);
			session.setAttribute(ErpConstante.SESSION_ADJUNTO_MODELO, listaAdjunto);

			if (modeloBean.getModeloNombre() != null) {
				modeloBean.setModeloNombre(modeloBean.getModeloNombre().toUpperCase());

			}
			modeloService.actualizar(modeloBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new Exception();
		}

	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.GET)
	@ResponseBody
	public String editar(Integer modeloId) {
		String mensajeError = "";
		try {
			modeloService.eliminar(modeloId);

		} catch (DataIntegrityViolationException e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (ConstraintViolationException e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (Exception e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_GENERICO);
			LOG.error(e.getMessage(), e);
		}
		return mensajeError;

	}


	@RequestMapping(value = "/mover", method = RequestMethod.GET)
	@ResponseBody
	public void mover(String idDiferentes) {

		try {

			modeloService.moverOrden(idDiferentes);

		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}

	}


	@RequestMapping(value = "/insertarAdjunto", method = RequestMethod.POST)
	@ResponseBody
	public String insertarAdjunto(HttpSession session, @RequestParam("file") MultipartFile file,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		String json = "";
		try {
			if (file.getSize() <= Long.parseLong(erpConfig.getProperty(ErpConstante.TAMANIO_IMAGEN_MODELO))) {
				String ruta = erpConfig.getProperty(ErpConstante.KEY_RAIZ_DIRECTORIO);
/*
				List<AdjuntoBean> listaAdjunto = (List<AdjuntoBean>) session
						.getAttribute(ErpConstante.SESSION_ADJUNTO_MODELO);
				if (listaAdjunto == null) {
					listaAdjunto = new ArrayList<AdjuntoBean>();
				}
				*/
				List<AdjuntoBean> listaAdjunto = new ArrayList<AdjuntoBean>();
				AdjuntoBean adjuntoBean = new AdjuntoBean();
				adjuntoBean.setNombre("modelo_" + new Date().getTime() + ".jpg");

				adjuntoBean.setRuta(ruta);
				insertarAdjunto(file, adjuntoBean, request, response);
				listaAdjunto.add(adjuntoBean);
				session.setAttribute(ErpConstante.SESSION_ADJUNTO_MODELO, listaAdjunto);
				json = Utilitario.formatearJsonString(listaAdjunto);
			} else {
				LOG.debug("El peso de la imagen es mayor a 400KB");
				throw new Exception("El peso de la imagen es mayor a 400KB");
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new Exception();
		}
		return json;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/eliminarImagen", method = RequestMethod.GET)
	@ResponseBody
	public String eliminarImagen(HttpSession session, Integer numeroImagen) throws Exception {
		String json = "";
		try {
			Log.debug("numeroImagen=" + numeroImagen);
			if (numeroImagen != null) {

				int numeroImg=numeroImagen.intValue();
				
				List<AdjuntoBean> listaAdjunto = (List<AdjuntoBean>) session
						.getAttribute(ErpConstante.SESSION_ADJUNTO_MODELO);

				if (numeroImg == 0) {

					listaAdjunto.remove(numeroImg);

					List<AdjuntoBean> listaAdjuntoNuevo = new ArrayList<AdjuntoBean>();
					AdjuntoBean adjuntoBean = new AdjuntoBean();
					adjuntoBean.setNombre("default.jpg");
					listaAdjuntoNuevo.add(adjuntoBean);

					//listaAdjuntoNuevo.add(listaAdjunto.get(0));
					session.setAttribute(ErpConstante.SESSION_ADJUNTO_MODELO, listaAdjuntoNuevo);
					json = Utilitario.formatearJsonString(listaAdjuntoNuevo);

				} else {
					if (listaAdjunto != null) {
						listaAdjunto.remove(numeroImg);
					}
					session.setAttribute(ErpConstante.SESSION_ADJUNTO_MODELO, listaAdjunto);
					json = Utilitario.formatearJsonString(listaAdjunto);
				}

			}

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return json;
	}

}
