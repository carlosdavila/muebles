package com.erp.modelo.dao;

import com.erp.modelo.model.ModeloBean;

import frameworkfrivas.dao.ComunDao;

public interface ModeloDao extends ComunDao<ModeloBean, Integer> {

}