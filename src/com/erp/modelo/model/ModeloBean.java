package com.erp.modelo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.erp.categoria.model.CategoriaBean;

@Entity
@Table(name = "t_modelo" )
@Proxy(lazy=true)
public class ModeloBean {

	@Id
	@GeneratedValue
	@Column(name = "modelo_id")
	private Integer modeloId;
	
	
	@ManyToOne
	@JoinColumn(name = "categoria_id")
	private CategoriaBean categoriaBean;
	
	
	@Column(name = "modelo_nombre")
	private String modeloNombre;

	@Column(name = "modelo_tipo")
	private String modeloTipo;

	
	@Column(name = "modelo_orden")
    private int modeloOrden;
	
	@Column(name="modelo_imagen")
    private String modeloImagen;
	
	@Column(name="modelo_imagen1")
    private String modeloImagen1;
	
	
	@Column(name="modelo_imagen_portada")
    private String modeloImagenPortada;
	
	public Integer getModeloId() {
		return modeloId;
	}

	public void setModeloId(Integer modeloId) {
		this.modeloId = modeloId;
	}

	public String getModeloNombre() {
		return modeloNombre;
	}

	public void setModeloNombre(String modeloNombre) {
		this.modeloNombre = modeloNombre;
	}



	public String getModeloTipo() {
		return modeloTipo;
	}

	public void setModeloTipo(String modeloTipo) {
		this.modeloTipo = modeloTipo;
	}

	public int getModeloOrden() {
		return modeloOrden;
	}

	public void setModeloOrden(int modeloOrden) {
		this.modeloOrden = modeloOrden;
	}

	public String getModeloImagen() {
		return modeloImagen;
	}

	public void setModeloImagen(String modeloImagen) {
		this.modeloImagen = modeloImagen;
	}

	public String getModeloImagenPortada() {
		return modeloImagenPortada;
	}

	public void setModeloImagenPortada(String modeloImagenPortada) {
		this.modeloImagenPortada = modeloImagenPortada;
	}

	public String getModeloImagen1() {
		return modeloImagen1;
	}

	public void setModeloImagen1(String modeloImagen2) {
		this.modeloImagen1 = modeloImagen2;
	}

	public CategoriaBean getCategoriaBean() {
		return categoriaBean;
	}

	public void setCategoriaBean(CategoriaBean categoriaBean) {
		this.categoriaBean = categoriaBean;
	}
	
	
	
	
}