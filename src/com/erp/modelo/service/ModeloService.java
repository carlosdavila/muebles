package com.erp.modelo.service;

import java.util.List;

import com.erp.modelo.model.ModeloBean;

import frameworkfrivas.bean.AdjuntoBean;
import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.service.ComunService;

public interface ModeloService extends ComunService<ModeloBean, Integer> {
//	public void subir(Integer categoriaId) throws BusinessException ;
//	public void bajar(Integer categoriaId) throws BusinessException ;
//	public void guardarConOrden(ModeloBean categoriaBean) throws BusinessException;
	public void moverOrden(String idDiferentes) throws BusinessException;
	
	public void guardarImagen(ModeloBean modeloBean, List<AdjuntoBean> listaAdjunto) ;
}
