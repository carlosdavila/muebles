package com.erp.modelo.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.erp.modelo.model.ModeloBean;

import frameworkfrivas.bean.AdjuntoBean;
import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.service.ComunServiceImpl;

@Service

public class ModeloServiceImpl extends ComunServiceImpl<ModeloBean, Integer>
		implements ModeloService {

	private static final Logger LOG = Logger.getLogger(ModeloServiceImpl.class);

	/*
	public void subir(Integer modeloId) throws BusinessException  {
	
		try {
			
			ModeloBean modeloBean=obtener(modeloId);
			
			int ordenAnterior=modeloBean.getModeloOrden()-1;
			if(ordenAnterior!=0){
			ModeloBean modeloBeanAnterior=(ModeloBean) modeloDao.obtenerListaIgualAcolumnaOrdenado("modeloOrden", ordenAnterior+"","modeloOrden","asc").get(0);
			modeloBeanAnterior.setModeloOrden(modeloBean.getModeloOrden());
			modeloBean.setModeloOrden(ordenAnterior);
			
			actualizar(modeloBeanAnterior);
			actualizar(modeloBean);
			}else{
				LOG.error("No se puede Subir de nivel");
		          throw new BusinessException("");		
			}
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
          throw new BusinessException(e);
			
		}
	

	}
*/
	/*
	@Override
	public void bajar(Integer modeloId) throws BusinessException {
try {
			
			ModeloBean modeloBean=obtener(modeloId);
			
			int ordenAnterior=modeloBean.getModeloOrden()+1;
			ModeloBean modeloBeanAnterior=(ModeloBean) modeloDao.obtenerListaIgualAcolumnaOrdenado("modeloOrden", ordenAnterior+"","modeloOrden","asc").get(0);
			modeloBeanAnterior.setModeloOrden(modeloBean.getModeloOrden());
			modeloBean.setModeloOrden(ordenAnterior);
			
			actualizar(modeloBeanAnterior);
			actualizar(modeloBean);

			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
          throw new BusinessException(e);
			
		}
		
	*/
/*
	@Override
	public void guardarConOrden(ModeloBean modeloBean)
			throws BusinessException {
		try {
			CategoriaBean categoriaBean=categoriaDao.obtener(modeloBean.getCategoriaBean().getCategoriaId());
			modeloBean.setCategoriaBean(categoriaBean);
			int total=	modeloDao.obtenerTotalFilasIgualAcolumna("categoriaBean.categoriaId", modeloBean.getCategoriaBean().getCategoriaId().toString());
			
			
			modeloBean.setModeloOrden(total+1);
			guardar(modeloBean);

			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
		          throw new BusinessException(e);
			}
		
	}
	*/
	
	@Transactional
	public void moverOrden(String idDiferentes) 	throws BusinessException{

	String ids[]=idDiferentes.split(",");

		try {
            int i=1;
			for (String id : ids) {
				ModeloBean modeloBean= obtener(new Integer(id));
				modeloBean.setModeloOrden(i);
			    actualizar(modeloBean);
				
			    i++;
			}
			
		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
			   throw new BusinessException(e);
		}
		

	}
	
	
	
	
	public void guardarImagen(ModeloBean modeloBean, List<AdjuntoBean> listaAdjunto) {
		modeloBean.setModeloImagen(null);
		modeloBean.setModeloImagen1(null);
		if (listaAdjunto != null) {
			if (listaAdjunto.size() > 0) {
				modeloBean.setModeloImagen(listaAdjunto.get(0).getNombre());
			}
			if (listaAdjunto.size() > 1) {
				modeloBean.setModeloImagen1(listaAdjunto.get(1).getNombre());
			}
		}
	}
	
	
	
}

