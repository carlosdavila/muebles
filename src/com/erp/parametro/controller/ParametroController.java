package com.erp.parametro.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.erp.parametro.model.ParametroBean;
import com.erp.parametro.service.ParametroService;

import frameworkfrivas.util.Utilitario;

@Controller
@RequestMapping("parametroController")
public class ParametroController {
	
	@Autowired
	private ParametroService parametroService;

	private static final Logger LOG = Logger
			.getLogger(ParametroController.class);

	@RequestMapping(value = "/mostrarGestionParametro", method = RequestMethod.GET)
	public ModelAndView mostrarGestionParametro(HttpSession session) {
		return new ModelAndView("parametro/gestionParametro");
	}

	@RequestMapping(value = "/obtenerListaFiltrada", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerListaFiltrada() {

		String json = "";
		try {

			json = Utilitario.formatearJsonString(parametroService
					.buscarTodo());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return json;
	}

	@RequestMapping(value = "/mostrarEditarParametro", method = RequestMethod.GET)
	public ModelAndView mostrarEditarCliente(Integer parametroId) {
		ModelAndView model = new ModelAndView("parametro/editarParametro");
		ParametroBean parametroBean = null;
		try {

			parametroBean = parametroService.obtener(parametroId);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		model.addObject("parametroBean", parametroBean);
		return model;
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	@ResponseBody
	public void editar(ParametroBean parametroBean) {
		try {
			parametroService.actualizar(parametroBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}

}
