package com.erp.parametro.dao;

import org.hibernate.SessionFactory;

import com.erp.parametro.model.ParametroBean;

import frameworkfrivas.dao.ComunDaoImpl;

public class ParametroDaoImpl extends ComunDaoImpl<ParametroBean, Integer>
		implements ParametroDao {

	public ParametroDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);

	}

}
