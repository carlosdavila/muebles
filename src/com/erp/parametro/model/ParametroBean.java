package com.erp.parametro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_parametro")
public class ParametroBean {
	@Id
	@Column(name = "parametro_id")
	private Integer parametroId;
	@Column(name = "parametro_nombre")
	private String parametroNombre;
	@Column(name = "parametro_tipo")
	private String parametroTipo;

	public Integer getParametroId() {
		return parametroId;
	}

	public void setParametroId(Integer parametroId) {
		this.parametroId = parametroId;
	}

	public String getParametroNombre() {
		return parametroNombre;
	}

	public void setParametroNombre(String parametroNombre) {
		this.parametroNombre = parametroNombre;
	}

	public String getParametroTipo() {
		return parametroTipo;
	}

	public void setParametroTipo(String parametroTipo) {
		this.parametroTipo = parametroTipo;
	}

}
