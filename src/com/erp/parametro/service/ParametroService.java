package com.erp.parametro.service;

import com.erp.parametro.model.ParametroBean;

import frameworkfrivas.service.ComunService;

public interface ParametroService extends ComunService<ParametroBean, Integer> {

}
