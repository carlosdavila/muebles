package com.erp.parametro.service;

import org.springframework.stereotype.Service;

import com.erp.parametro.model.ParametroBean;

import frameworkfrivas.service.ComunServiceImpl;
@Service
public class ParametroServiceImpl extends ComunServiceImpl<ParametroBean, Integer>
implements ParametroService {

}
