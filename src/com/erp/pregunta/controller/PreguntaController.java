package com.erp.pregunta.controller;

import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.erp.comun.util.ErpConstante;
import com.erp.examen.model.ExamenBean;
import com.erp.examen.service.ExamenService;
import com.erp.pregunta.model.PreguntaBean;
import com.erp.pregunta.service.PreguntaService;

import frameworkfrivas.bean.RequestJqGridBean;

@Controller
@RequestMapping("preguntaController")
public class PreguntaController {
	@Autowired
	private PreguntaService preguntaService;
	@Autowired
	private ExamenService examenService;
	@Autowired
	private Properties erpConfig;
	
	private static final Logger LOG = Logger.getLogger(PreguntaController.class);

	@RequestMapping(value = "/mostrarGestionPregunta", method = RequestMethod.GET)
	public ModelAndView mostrarGestionPregunta(HttpSession session,Integer examenId) {
		System.out.println("examenid="+examenId);
		ExamenBean examenBean= examenService.obtener(examenId);
		ModelAndView model = new ModelAndView("pregunta/gestionPregunta");
        model.addObject("examenBean", examenBean);
		return model;
	}

	@RequestMapping(value = "/obtenerListaFiltrada", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerListaFiltrada(RequestJqGridBean requestJqGridBean) {

		String json = "";
		try {

			json =null;
		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}

		return json;
	}

	@RequestMapping(value = "/mostrarNuevoPregunta", method = RequestMethod.GET)
	public ModelAndView mostrarNuevoPregunta() {
		ModelAndView model = new ModelAndView("pregunta/nuevoPregunta");
        model.addObject("preguntaBean", new PreguntaBean());
		return model;
	}

	@RequestMapping(value = "/mostrarEditarPregunta", method = RequestMethod.GET)
	public ModelAndView mostrarEditarPregunta(Integer preguntaId) {

		PreguntaBean preguntaBean = null;

		try {
			preguntaBean = preguntaService.obtener(preguntaId);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		ModelAndView model = new ModelAndView("pregunta/editarPregunta");
		model.addObject("preguntaBean", preguntaBean);
		return model;
	}

	@RequestMapping(value = "/guardar", method = RequestMethod.POST)
	@ResponseBody
	public void guardar(PreguntaBean preguntaBean) {
		try {
			preguntaService.guardar(preguntaBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	@ResponseBody
	public void editar(PreguntaBean preguntaBean) {
		try {
			preguntaService.actualizar(preguntaBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.GET)
	@ResponseBody
	public String eliminar(Integer preguntaId) {
		String mensajeError = "";
		try {
			preguntaService.eliminar(preguntaId);

		} catch (DataIntegrityViolationException e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (ConstraintViolationException e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (Exception e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_GENERICO);
			LOG.error(e.getMessage(), e);
		}
		return mensajeError;

	}

}
