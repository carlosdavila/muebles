package com.erp.pregunta.dao;

import com.erp.pregunta.model.PreguntaBean;

import frameworkfrivas.dao.ComunDao;

public interface PreguntaDao   extends ComunDao<PreguntaBean, Integer> {

}
