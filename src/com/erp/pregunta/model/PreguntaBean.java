package com.erp.pregunta.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "t_pregunta" )
@Proxy(lazy=true)
public class PreguntaBean {

	@Id
	@GeneratedValue
	@Column(name = "pregunta_id")
	private Integer preguntaId;
	
	@Column(name = "examen_id")
	private Integer examenId;
	
	@Column(name = "pregunta_descripcion")
	private String preguntaDescripcion;
	
	@Column(name = "pregunta_tipo")
	private String preguntaTipo;
	
	@Column(name = "pregunta_nro_alternativas")
	private Integer preguntaNroAlternativas;
	
	@Column(name = "pregunta_estado")
	private String preguntaEstado ;

	public Integer getPreguntaId() {
		return preguntaId;
	}

	public void setPreguntaId(Integer preguntaId) {
		this.preguntaId = preguntaId;
	}

	public Integer getExamenId() {
		return examenId;
	}

	public void setExamenId(Integer examenId) {
		this.examenId = examenId;
	}

	public String getPreguntaDescripcion() {
		return preguntaDescripcion;
	}

	public void setPreguntaDescripcion(String preguntaDescripcion) {
		this.preguntaDescripcion = preguntaDescripcion;
	}

	public String getPreguntaTipo() {
		return preguntaTipo;
	}

	public void setPreguntaTipo(String preguntaTipo) {
		this.preguntaTipo = preguntaTipo;
	}

	public Integer getPreguntaNroAlternativas() {
		return preguntaNroAlternativas;
	}

	public void setPreguntaNroAlternativas(Integer preguntaNroAlternativas) {
		this.preguntaNroAlternativas = preguntaNroAlternativas;
	}

	public String getPreguntaEstado() {
		return preguntaEstado;
	}

	public void setPreguntaEstado(String preguntaEstado) {
		this.preguntaEstado = preguntaEstado;
	}
	
	
}
