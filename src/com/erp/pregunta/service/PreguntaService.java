package com.erp.pregunta.service;

import com.erp.pregunta.model.PreguntaBean;

import frameworkfrivas.service.ComunService;

public interface PreguntaService  extends ComunService<PreguntaBean, Integer> {

}
