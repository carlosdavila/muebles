package com.erp.pregunta.service;

import org.springframework.stereotype.Service;

import com.erp.pregunta.model.PreguntaBean;

import frameworkfrivas.service.ComunServiceImpl;

@Service
public class PreguntaServiceImpl  extends ComunServiceImpl<PreguntaBean, Integer>
implements PreguntaService {

}
