package com.erp.producto.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.erp.comun.util.ErpConstante;
import com.erp.modelo.model.ModeloBean;
import com.erp.modelo.service.ModeloService;
import com.erp.producto.especificacion.dao.ProductoEspecificacionDao;
import com.erp.producto.especificacion.model.ProductoEspecificacionBean;
import com.erp.producto.model.ProductoBean;
import com.erp.producto.service.ProductoService;
import com.erp.usuario.model.UsuarioBean;

import frameworkfrivas.bean.AdjuntoBean;
import frameworkfrivas.bean.RequestJqGridBean;
import frameworkfrivas.bean.ResponseJqGridBean;
import frameworkfrivas.controller.ComunController;
import frameworkfrivas.util.Utilitario;

@Controller
@RequestMapping("productoController")
public class ProductoController extends ComunController {

	@Autowired
	private ProductoService productoService;

	@Autowired
	private ModeloService modeloService;

	@Autowired
	private ProductoEspecificacionDao productoEspecificacionDao;

	@Autowired
	private Properties erpConfig;

	private static final Logger LOG = Logger.getLogger(ProductoController.class);



	@RequestMapping(value = "/listarProducto", method = RequestMethod.GET)
	public ModelAndView listarProducto(HttpSession session, Device device, String valor, Integer modeloId,
			Integer pagina) {

		ModelAndView model;

		if (device.isNormal()) {
			model = new ModelAndView("producto");
		} else {
			model = new ModelAndView("productoCel");
		}

		try {
			RequestJqGridBean requestJqGridBean = new RequestJqGridBean();

			if (modeloId != null && modeloId.compareTo(0) != 0) {

				ModeloBean modeloBean = modeloService.obtener(modeloId);
				model.addObject("modeloBean", modeloBean);
				
				String[] columnaEquivalente = { "modeloBean.modeloId"};
				String[] columnaValorEquivalente = { modeloId.toString()};
				
				requestJqGridBean.setColumnaEquivalente(columnaEquivalente);
				requestJqGridBean.setValorEquivalente(columnaValorEquivalente);
				requestJqGridBean.setPage(pagina);
				requestJqGridBean.setRows(6);
				requestJqGridBean.setSidx("productoNombre");
				requestJqGridBean.setSord("asc");
			} else {
				String[] columnaCoincidencia = { "productoNombre", "productoDescripcion" };
				requestJqGridBean.setColumnaCoincidencia(columnaCoincidencia);
				requestJqGridBean.setValorCoincidencia(valor);
				requestJqGridBean.setPage(pagina);
				requestJqGridBean.setRows(6);
				requestJqGridBean.setSidx("productoNombre");
				requestJqGridBean.setSord("asc");
			}

			ResponseJqGridBean responseJqGridBean = productoService.obtenerListaPaginado(requestJqGridBean);
			List<?> listaObj = responseJqGridBean.getRows();
			List<ProductoBean> listaProducto = new ArrayList<ProductoBean>();
			for (Object obj : listaObj) {
				ProductoBean productoBean = (ProductoBean) obj;
               
				productoBean.setProductoEspecificacionDesc1(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion1()));
				productoBean.setProductoEspecificacionDesc2(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion2()));
				productoBean.setProductoEspecificacionDesc3(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion3()));
				productoBean.setProductoEspecificacionDesc4(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion4()));
				productoBean.setProductoEspecificacionDesc5(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion5()));
				productoBean.setProductoEspecificacionDesc6(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion6()));
				productoBean.setProductoEspecificacionDesc7(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion7()));
				productoBean.setProductoEspecificacionDesc8(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion8()));
				productoBean.setProductoEspecificacionDesc9(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion9()));
				productoBean.setProductoEspecificacionDesc10(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion10()));
				productoBean.setProductoEspecificacionDesc11(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion11()));
				productoBean.setProductoEspecificacionDesc12(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion12()));
				productoBean.setProductoEspecificacionDesc13(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion13()));
				productoBean.setProductoEspecificacionDesc14(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion14()));
				productoBean.setProductoEspecificacionDesc15(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion15()));
				productoBean.setProductoEspecificacionDesc16(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion16()));
				productoBean.setProductoEspecificacionDesc17(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion17()));
				productoBean.setProductoEspecificacionDesc18(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion18()));
				productoBean.setProductoEspecificacionDesc19(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion19()));
				productoBean.setProductoEspecificacionDesc20(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion20()));

				productoBean.setProductoEspecificacionDesc21(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion21()));

				productoBean.setProductoEspecificacionDesc22(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion22()));

				productoBean.setProductoEspecificacionDesc23(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion23()));

				productoBean.setProductoEspecificacionDesc24(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion24()));

				productoBean.setProductoEspecificacionDesc25(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion25()));

				productoBean.setProductoEspecificacionDesc26(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion26()));

				productoBean.setProductoEspecificacionDesc27(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion27()));

				productoBean.setProductoEspecificacionDesc28(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion28()));

				productoBean.setProductoEspecificacionDesc29(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion29()));

				productoBean.setProductoEspecificacionDesc30(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion30()));

				productoBean.setProductoEspecificacionDesc31(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion31()));

				productoBean.setProductoEspecificacionDesc32(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion32()));

				productoBean.setProductoEspecificacionDesc33(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion33()));

				productoBean.setProductoEspecificacionDesc34(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion34()));

				productoBean.setProductoEspecificacionDesc35(
						obtenerEspecificacion(session, productoBean.getProductoEspecificacion35()));

				listaProducto.add(productoBean);

			}

			model.addObject("listaProducto", listaProducto);
			model.addObject("total", responseJqGridBean.getTotal());
			model.addObject("pagina", responseJqGridBean.getPage());

			String ruta = erpConfig.getProperty(ErpConstante.KEY_RAIZ_DIRECTORIO);
			model.addObject("ruta", ruta);

		} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}

		model.addObject("isPc", device.isNormal());

		model.addObject("modeloId", modeloId);
		model.addObject("valor", valor);
		UsuarioBean usuarioBean = (UsuarioBean) session.getAttribute(ErpConstante.USUARIO_SESSION);
		model.addObject("usuarioBean", usuarioBean);

		return model;
	}

	@RequestMapping(value = "/obtenerListaFiltrada", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerListaFiltrada(Integer modeloId, RequestJqGridBean requestJqGridBean) {

		String json = "";
		try {
			String[] columnaEquivalente = { "modeloBean.modeloId"};
			String[] columnaValorEquivalente = { modeloId.toString()};
			requestJqGridBean.setColumnaEquivalente(columnaEquivalente);
			requestJqGridBean.setValorEquivalente(columnaValorEquivalente);
			requestJqGridBean.setSord("asc");
			requestJqGridBean.setSidx("productoOrden");
			json = Utilitario.formatearJsonString(productoService.obtenerListaPaginado(requestJqGridBean));

		} catch (Exception e) {
			System.out.println("e=" + e);
			LOG.error(e.getMessage(), e);
		}

		return json;
	}

	@RequestMapping(value = "/mostrarBuscarProducto", method = RequestMethod.GET)
	public ModelAndView mostrarBuscarProducto() {
		return new ModelAndView("producto/buscarProducto");
	}

	@RequestMapping(value = "/mostrarNuevoProducto", method = RequestMethod.GET)
	public ModelAndView mostrarNuevoProducto(HttpSession session, Integer modeloId) {
		ModelAndView model = new ModelAndView("producto/nuevoProducto");
		try {
			session.setAttribute(ErpConstante.SESSION_ADJUNTO, new ArrayList<AdjuntoBean>());
			ProductoBean productoBean = new ProductoBean();
			ModeloBean modeloBean = new ModeloBean();
			modeloBean.setModeloId(modeloId);
			productoBean.setModeloBean(modeloBean);
			productoBean.setModeloBean(modeloBean);
			model.addObject("productoBean", productoBean);
			model.addObject("listaEspecificacion", obtenerEspecificacion(session));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return model;
	}

	@RequestMapping(value = "/mostrarEditarProducto", method = RequestMethod.GET)
	public ModelAndView mostrarEditarProducto(HttpSession session, Integer productoId) {

		ModelAndView model = new ModelAndView("producto/editarProducto");
		String json = "";
		try {

			ProductoBean productoBean = productoService.obtener(productoId);
			List<AdjuntoBean> listaAdjunto = new ArrayList<AdjuntoBean>();
			if (productoBean.getProductoImagen() != null) {
				AdjuntoBean adjuntoBean = new AdjuntoBean();
				adjuntoBean.setNombre(productoBean.getProductoImagen());
				listaAdjunto.add(adjuntoBean);
			}

			if (productoBean.getProductoImagen1() != null) {
				AdjuntoBean adjuntoBean1 = new AdjuntoBean();
				adjuntoBean1.setNombre(productoBean.getProductoImagen1());
				listaAdjunto.add(adjuntoBean1);
			}

			if (productoBean.getProductoImagen2() != null) {
				AdjuntoBean adjuntoBean2 = new AdjuntoBean();
				adjuntoBean2.setNombre(productoBean.getProductoImagen2());
				listaAdjunto.add(adjuntoBean2);
			}

			if (productoBean.getProductoImagen3() != null) {
				AdjuntoBean adjuntoBean3 = new AdjuntoBean();
				adjuntoBean3.setNombre(productoBean.getProductoImagen3());
				listaAdjunto.add(adjuntoBean3);
			}

			if (productoBean.getProductoImagen4() != null) {
				AdjuntoBean adjuntoBean4 = new AdjuntoBean();
				adjuntoBean4.setNombre(productoBean.getProductoImagen4());
				listaAdjunto.add(adjuntoBean4);
			}

			session.setAttribute(ErpConstante.SESSION_ADJUNTO, listaAdjunto);
			json = Utilitario.formatearJsonString(listaAdjunto);
			model.addObject("productoBean", productoBean);
			model.addObject("json", json);
			model.addObject("listaEspecificacion", obtenerEspecificacion(session));
			String ruta = erpConfig.getProperty(ErpConstante.KEY_RAIZ_DIRECTORIO);
			model.addObject("listaEspecificacion", obtenerEspecificacion(session));
			model.addObject("ruta", ruta);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return model;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insertarAdjunto", method = RequestMethod.POST)
	@ResponseBody
	public String insertarAdjunto(HttpSession session, @RequestParam("file") MultipartFile file,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		String json = "";
		try {

			if (file.getSize() <= Long.parseLong(erpConfig.getProperty(ErpConstante.TAMANIO_IMAGEN_PRODUCTO))) {

				String ruta = erpConfig.getProperty(ErpConstante.KEY_RAIZ_DIRECTORIO);

				List<AdjuntoBean> listaAdjunto = (List<AdjuntoBean>) session.getAttribute(ErpConstante.SESSION_ADJUNTO);
				if (listaAdjunto == null) {
					listaAdjunto = new ArrayList<AdjuntoBean>();
				}

				AdjuntoBean adjuntoBean = new AdjuntoBean();
				adjuntoBean.setNombre("producto_" + new Date().getTime() + ".jpg");
				adjuntoBean.setRuta(ruta);
				insertarAdjunto(file, adjuntoBean, request, response);
				listaAdjunto.add(adjuntoBean);
				session.setAttribute(ErpConstante.SESSION_ADJUNTO, listaAdjunto);
				json = Utilitario.formatearJsonString(listaAdjunto);
			} else {
				LOG.debug("El peso de la imagen es mayor a 200KB");
				throw new Exception("El peso de la imagen es mayor a 200KB");
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new Exception();
		}
		return json;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	@ResponseBody
	public void editar(HttpSession session, ProductoBean productoBean) {
		try {

			List<AdjuntoBean> listaAdjunto = (List<AdjuntoBean>) session.getAttribute(ErpConstante.SESSION_ADJUNTO);
			productoService.guardarImagen(productoBean, listaAdjunto);

			productoService.actualizar(productoBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.GET)
	@ResponseBody
	public String eliminar(Integer productoId) {
		String mensajeError = "";
		try {
			productoService.eliminar(productoId);

		} catch (DataIntegrityViolationException e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);

			LOG.error(e.getMessage(), e);

		} catch (ConstraintViolationException e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (Exception e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_GENERICO);
			LOG.error(e.getMessage(), e);
		}
		return mensajeError;

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/guardar", method = RequestMethod.POST)
	@ResponseBody
	public void guardar(HttpSession session, ProductoBean productoBean) {
		try {
			List<AdjuntoBean> listaAdjunto = (List<AdjuntoBean>) session.getAttribute(ErpConstante.SESSION_ADJUNTO);
			productoService.guardarImagen(productoBean, listaAdjunto);
			session.setAttribute(ErpConstante.SESSION_ADJUNTO, listaAdjunto);

			productoService.guardarConOrden(productoBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/cargarImagen", method = RequestMethod.GET)
	public void cargarImagenProducto(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "imagen", required = false) String imagen) {

		try {
			String ruta = erpConfig.getProperty(ErpConstante.KEY_RAIZ_DIRECTORIO);
			if (StringUtils.isNotBlank(imagen)) {
				descargarArchivo(response, ruta + imagen);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/eliminarImagen", method = RequestMethod.GET)
	@ResponseBody
	public String eliminarImagen(HttpSession session, Integer numeroImagen) throws Exception {
		String json = "";
		try {
			Log.debug("numeroImagen=" + numeroImagen);
			if (numeroImagen != null) {
				int numeroImg = numeroImagen.intValue();
				List<AdjuntoBean> listaAdjunto = (List<AdjuntoBean>) session.getAttribute(ErpConstante.SESSION_ADJUNTO);
				if (listaAdjunto != null) {
					listaAdjunto.remove(numeroImg);
				}

				session.setAttribute(ErpConstante.SESSION_ADJUNTO, listaAdjunto);
				json = Utilitario.formatearJsonString(listaAdjunto);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return json;
	}

	@SuppressWarnings("unchecked")
	private List<ProductoEspecificacionBean> obtenerEspecificacion(HttpSession session) {

		List<ProductoEspecificacionBean> listaEspecificacion = (List<ProductoEspecificacionBean>) session
				.getAttribute(ErpConstante.SESSION_ESPECIFICACION);

		try {
			if (listaEspecificacion == null) {
				listaEspecificacion = (List<ProductoEspecificacionBean>) productoEspecificacionDao.buscarTodo();
				ProductoEspecificacionBean productoEspecificacionBean = new ProductoEspecificacionBean();
				productoEspecificacionBean.setProductoEspecificacionId(null);
				productoEspecificacionBean.setProductoEspecificacionNombre("--Seleccione--");
				listaEspecificacion.add(productoEspecificacionBean);
				session.setAttribute(ErpConstante.SESSION_ESPECIFICACION, listaEspecificacion);

			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return listaEspecificacion;
	}

	private String obtenerEspecificacion(HttpSession session, Integer idEspecificacion) {
		String especificacionNombre = null;
		List<ProductoEspecificacionBean> listaEspecificacion = obtenerEspecificacion(session);
		if (idEspecificacion != null) {
			for (ProductoEspecificacionBean productoEspecificacionBean : listaEspecificacion) {
				if (productoEspecificacionBean.getProductoEspecificacionId() != null
						&& idEspecificacion.compareTo(productoEspecificacionBean.getProductoEspecificacionId()) == 0) {
					especificacionNombre = productoEspecificacionBean.getProductoEspecificacionNombre();
					break;
				}
			}
		}

		return especificacionNombre;

	}

}
