package com.erp.producto.dao;


import com.erp.producto.model.ProductoBean;

import frameworkfrivas.dao.ComunDao;

public interface ProductoDao extends ComunDao<ProductoBean, Integer> {

}
