package com.erp.producto.especificacion.dao;

import com.erp.producto.especificacion.model.ProductoEspecificacionBean;

import frameworkfrivas.dao.ComunDao;

public interface ProductoEspecificacionDao  extends ComunDao<ProductoEspecificacionBean, Integer> {

}
