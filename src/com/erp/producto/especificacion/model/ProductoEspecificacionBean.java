package com.erp.producto.especificacion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name="t_producto_especificacion")
@Proxy(lazy=true)
public class ProductoEspecificacionBean {
	
	@Id
	@GeneratedValue
	@Column(name="producto_especificacion_id")
	private Integer productoEspecificacionId;

	@Column(name="producto_especificacion_nombre")
	private String  productoEspecificacionNombre;
	

	public Integer getProductoEspecificacionId() {
		return productoEspecificacionId;
	}

	public void setProductoEspecificacionId(Integer productoEspecificacionId) {
		this.productoEspecificacionId = productoEspecificacionId;
	}

	public String getProductoEspecificacionNombre() {
		return productoEspecificacionNombre;
	}

	public void setProductoEspecificacionNombre(String productoEspecificacionNombre) {
		this.productoEspecificacionNombre = productoEspecificacionNombre;
	}



	
}
