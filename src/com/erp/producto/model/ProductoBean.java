package com.erp.producto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.erp.comun.util.ErpConstante;
import com.erp.modelo.model.ModeloBean;

@Entity
@Table(name = "t_producto")
@Proxy(lazy = true)
public class ProductoBean {

	@Id
	@GeneratedValue
	@Column(name = "producto_id")
	private Integer productoId;

	@Column(name = "producto_nombre")
	private String productoNombre;

	@Column(name = "producto_descripcion")
	private String productoDescripcion;

	@Column(name = "producto_pre_compra")
	private Double productoPreCompra;

	@Column(name = "producto_pre_venta")
	private Double productoPreVenta;

	@Column(name = "producto_stock")
	private Integer productoStock;

	@ManyToOne
	@JoinColumn(name = "modelo_id")
	private ModeloBean modeloBean;

	@Column(name = "producto_orden")
	private int productoOrden;

	@Column(name = "producto_tipo")
	private String productoTipo;

	@Column(name = "producto_imagen")
	private String productoImagen;

	@Column(name = "producto_imagen1")
	private String productoImagen1;

	@Column(name = "producto_imagen2")
	private String productoImagen2;

	@Column(name = "producto_imagen3")
	private String productoImagen3;

	@Column(name = "producto_imagen4")
	private String productoImagen4;

	@Column(name = "producto_especificacion1")
	private Integer productoEspecificacion1;

	@Column(name = "producto_especificacion_valor1", columnDefinition = "varchar(700)")
	private String productoEspecificacionValor1;

	transient String productoEspecificacionDesc1;

	@Column(name = "producto_especificacion2")
	private Integer productoEspecificacion2;

	@Column(name = "producto_especificacion_valor2", length = 700)
	private String productoEspecificacionValor2;

	transient String productoEspecificacionDesc2;

	@Column(name = "producto_especificacion3")
	private Integer productoEspecificacion3;

	@Column(name = "producto_especificacion_valor3", length = 700)
	private String productoEspecificacionValor3;

	transient String productoEspecificacionDesc3;

	@Column(name = "producto_especificacion4")
	private Integer productoEspecificacion4;

	@Column(name = "producto_especificacion_valor4", length = 700)
	private String productoEspecificacionValor4;

	transient String productoEspecificacionDesc4;

	@Column(name = "producto_especificacion5")
	private Integer productoEspecificacion5;

	@Column(name = "producto_especificacion_valor5", length = 700)
	private String productoEspecificacionValor5;

	transient String productoEspecificacionDesc5;

	@Column(name = "producto_especificacion6")
	private Integer productoEspecificacion6;

	@Column(name = "producto_especificacion_valor6", length = 700)
	private String productoEspecificacionValor6;

	transient String productoEspecificacionDesc6;

	@Column(name = "producto_especificacion7")
	private Integer productoEspecificacion7;

	@Column(name = "producto_especificacion_valor7", length = 700)
	private String productoEspecificacionValor7;

	transient String productoEspecificacionDesc7;

	@Column(name = "producto_especificacion8")
	private Integer productoEspecificacion8;

	@Column(name = "producto_especificacion_valor8", length = 700)
	private String productoEspecificacionValor8;

	transient String productoEspecificacionDesc8;

	@Column(name = "producto_especificacion9")
	private Integer productoEspecificacion9;

	@Column(name = "producto_especificacion_valor9", length = 700)
	private String productoEspecificacionValor9;

	transient String productoEspecificacionDesc9;

	@Column(name = "producto_especificacion10")
	private Integer productoEspecificacion10;

	@Column(name = "producto_especificacion_valor10", length = 700)
	private String productoEspecificacionValor10;

	transient String productoEspecificacionDesc10;

	@Column(name = "producto_especificacion11")
	private Integer productoEspecificacion11;

	@Column(name = "producto_especificacion_valor11", length = 700)
	private String productoEspecificacionValor11;

	transient String productoEspecificacionDesc11;

	@Column(name = "producto_especificacion12")
	private Integer productoEspecificacion12;

	@Column(name = "producto_especificacion_valor12", length = 700)
	private String productoEspecificacionValor12;

	transient String productoEspecificacionDesc12;

	@Column(name = "producto_especificacion13")
	private Integer productoEspecificacion13;

	@Column(name = "producto_especificacion_valor13", length = 700)
	private String productoEspecificacionValor13;

	transient String productoEspecificacionDesc13;

	@Column(name = "producto_especificacion14")
	private Integer productoEspecificacion14;

	@Column(name = "producto_especificacion_valor14", length = 700)
	private String productoEspecificacionValor14;

	transient String productoEspecificacionDesc14;

	@Column(name = "producto_especificacion15")
	private Integer productoEspecificacion15;

	@Column(name = "producto_especificacion_valor15", length = 700)
	private String productoEspecificacionValor15;

	transient String productoEspecificacionDesc15;

	@Column(name = "producto_especificacion16")
	private Integer productoEspecificacion16;

	@Column(name = "producto_especificacion_valor16", length = 700)
	private String productoEspecificacionValor16;

	transient String productoEspecificacionDesc16;

	@Column(name = "producto_especificacion17")
	private Integer productoEspecificacion17;

	@Column(name = "producto_especificacion_valor17", length = 700)
	private String productoEspecificacionValor17;

	transient String productoEspecificacionDesc17;

	@Column(name = "producto_especificacion18")
	private Integer productoEspecificacion18;

	@Column(name = "producto_especificacion_valor18", length = 700)
	private String productoEspecificacionValor18;

	transient String productoEspecificacionDesc18;

	@Column(name = "producto_especificacion19")
	private Integer productoEspecificacion19;

	@Column(name = "producto_especificacion_valor19", length = 700)
	private String productoEspecificacionValor19;

	transient String productoEspecificacionDesc19;

	@Column(name = "producto_especificacion20")
	private Integer productoEspecificacion20;

	@Column(name = "producto_especificacion_valor20", length = 700)
	private String productoEspecificacionValor20;

	transient String productoEspecificacionDesc20;

	@Column(name = "producto_especificacion21")
	private Integer productoEspecificacion21;
	@Column(name = "producto_especificacion_valor21", length = 700)
	private String productoEspecificacionValor21;
	transient String productoEspecificacionDesc21;

	@Column(name = "producto_especificacion22")
	private Integer productoEspecificacion22;
	@Column(name = "producto_especificacion_valor22", length = 700)
	private String productoEspecificacionValor22;
	transient String productoEspecificacionDesc22;

	@Column(name = "producto_especificacion23")
	private Integer productoEspecificacion23;
	@Column(name = "producto_especificacion_valor23", length = 700)
	private String productoEspecificacionValor23;
	transient String productoEspecificacionDesc23;

	@Column(name = "producto_especificacion24")
	private Integer productoEspecificacion24;
	@Column(name = "producto_especificacion_valor24", length = 700)
	private String productoEspecificacionValor24;
	transient String productoEspecificacionDesc24;

	@Column(name = "producto_especificacion25")
	private Integer productoEspecificacion25;
	@Column(name = "producto_especificacion_valor25", length = 700)
	private String productoEspecificacionValor25;
	transient String productoEspecificacionDesc25;

	@Column(name = "producto_especificacion26")
	private Integer productoEspecificacion26;
	@Column(name = "producto_especificacion_valor26", length = 700)
	private String productoEspecificacionValor26;
	transient String productoEspecificacionDesc26;

	@Column(name = "producto_especificacion27")
	private Integer productoEspecificacion27;
	@Column(name = "producto_especificacion_valor27", length = 700)
	private String productoEspecificacionValor27;
	transient String productoEspecificacionDesc27;

	@Column(name = "producto_especificacion28")
	private Integer productoEspecificacion28;
	@Column(name = "producto_especificacion_valor28", length = 700)
	private String productoEspecificacionValor28;
	transient String productoEspecificacionDesc28;

	@Column(name = "producto_especificacion29")
	private Integer productoEspecificacion29;
	@Column(name = "producto_especificacion_valor29", length = 700)
	private String productoEspecificacionValor29;
	transient String productoEspecificacionDesc29;

	@Column(name = "producto_especificacion30")
	private Integer productoEspecificacion30;
	@Column(name = "producto_especificacion_valor30", length = 700)
	private String productoEspecificacionValor30;
	transient String productoEspecificacionDesc30;

	@Column(name = "producto_especificacion31")
	private Integer productoEspecificacion31;
	@Column(name = "producto_especificacion_valor31", length = 700)
	private String productoEspecificacionValor31;
	transient String productoEspecificacionDesc31;

	@Column(name = "producto_especificacion32")
	private Integer productoEspecificacion32;
	@Column(name = "producto_especificacion_valor32", length = 700)
	private String productoEspecificacionValor32;
	transient String productoEspecificacionDesc32;

	@Column(name = "producto_especificacion33")
	private Integer productoEspecificacion33;
	@Column(name = "producto_especificacion_valor33", length = 700)
	private String productoEspecificacionValor33;
	transient String productoEspecificacionDesc33;

	@Column(name = "producto_especificacion34")
	private Integer productoEspecificacion34;
	@Column(name = "producto_especificacion_valor34", length = 700)
	private String productoEspecificacionValor34;
	transient String productoEspecificacionDesc34;

	@Column(name = "producto_especificacion35")
	private Integer productoEspecificacion35;
	@Column(name = "producto_especificacion_valor35", length = 700)
	private String productoEspecificacionValor35;
	transient String productoEspecificacionDesc35;


	public Integer getProductoId() {
		return productoId;
	}

	public void setProductoId(Integer productoId) {
		this.productoId = productoId;
	}

	public String getProductoNombre() {
		return productoNombre;
	}

	public void setProductoNombre(String productoNombre) {
		this.productoNombre = productoNombre;
	}

	public Double getProductoPreCompra() {
		return productoPreCompra;
	}

	public void setProductoPreCompra(Double productoPreCompra) {
		this.productoPreCompra = productoPreCompra;
	}

	public Double getProductoPreVenta() {
		return productoPreVenta;
	}

	public void setProductoPreVenta(Double productoPreVenta) {
		this.productoPreVenta = productoPreVenta;
	}

	public Integer getProductoStock() {
		return productoStock;
	}

	public void setProductoStock(Integer productoStock) {
		this.productoStock = productoStock;
	}

	public ModeloBean getModeloBean() {
		return modeloBean;
	}

	public void setModeloBean(ModeloBean modeloBean) {
		this.modeloBean = modeloBean;
	}

	public int getProductoOrden() {
		return productoOrden;
	}

	public void setProductoOrden(int productoOrden) {
		this.productoOrden = productoOrden;
	}

	public String getProductoTipo() {
		return productoTipo;
	}

	public void setProductoTipo(String productoTipo) {
		this.productoTipo = productoTipo;
	}

	public String getProductoDescripcion() {
		return productoDescripcion;
	}

	public void setProductoDescripcion(String productoDescripcion) {
		this.productoDescripcion = productoDescripcion;
	}

	public String getProductoImagen() {
		return productoImagen;
	}

	public void setProductoImagen(String productoImagen) {
		this.productoImagen = productoImagen;
	}

	public String getProductoImagen1() {
		return productoImagen1;
	}

	public void setProductoImagen1(String productoImagen1) {
		this.productoImagen1 = productoImagen1;
	}

	public String getProductoImagen2() {
		return productoImagen2;
	}

	public void setProductoImagen2(String productoImagen2) {
		this.productoImagen2 = productoImagen2;
	}

	public String getProductoImagen3() {
		return productoImagen3;
	}

	public void setProductoImagen3(String productoImagen3) {
		this.productoImagen3 = productoImagen3;
	}

	public String getProductoImagen4() {
		return productoImagen4;
	}

	public void setProductoImagen4(String productoImagen4) {
		this.productoImagen4 = productoImagen4;
	}

	public Integer getProductoEspecificacion1() {
		return productoEspecificacion1;
	}

	public void setProductoEspecificacion1(Integer productoEspecificacion1) {
		this.productoEspecificacion1 = productoEspecificacion1;
	}

	public String getProductoEspecificacionValor1() {
		return productoEspecificacionValor1;
	}

	public void setProductoEspecificacionValor1(
			String productoEspecificacionValor1) {
		this.productoEspecificacionValor1 = productoEspecificacionValor1;
	}

	public Integer getProductoEspecificacion2() {
		return productoEspecificacion2;
	}

	public void setProductoEspecificacion2(Integer productoEspecificacion2) {
		this.productoEspecificacion2 = productoEspecificacion2;
	}

	public String getProductoEspecificacionValor2() {
		return productoEspecificacionValor2;
	}

	public void setProductoEspecificacionValor2(
			String productoEspecificacionValor2) {
		this.productoEspecificacionValor2 = productoEspecificacionValor2;
	}

	public Integer getProductoEspecificacion3() {
		return productoEspecificacion3;
	}

	public void setProductoEspecificacion3(Integer productoEspecificacion3) {
		this.productoEspecificacion3 = productoEspecificacion3;
	}

	public String getProductoEspecificacionValor3() {
		return productoEspecificacionValor3;
	}

	public void setProductoEspecificacionValor3(
			String productoEspecificacionValor3) {
		this.productoEspecificacionValor3 = productoEspecificacionValor3;
	}

	public Integer getProductoEspecificacion4() {
		return productoEspecificacion4;
	}

	public void setProductoEspecificacion4(Integer productoEspecificacion4) {
		this.productoEspecificacion4 = productoEspecificacion4;
	}

	public String getProductoEspecificacionValor4() {
		return productoEspecificacionValor4;
	}

	public void setProductoEspecificacionValor4(
			String productoEspecificacionValor4) {
		this.productoEspecificacionValor4 = productoEspecificacionValor4;
	}

	public Integer getProductoEspecificacion5() {
		return productoEspecificacion5;
	}

	public void setProductoEspecificacion5(Integer productoEspecificacion5) {
		this.productoEspecificacion5 = productoEspecificacion5;
	}

	public String getProductoEspecificacionValor5() {
		return productoEspecificacionValor5;
	}

	public void setProductoEspecificacionValor5(
			String productoEspecificacionValor5) {
		this.productoEspecificacionValor5 = productoEspecificacionValor5;
	}

	public Integer getProductoEspecificacion6() {
		return productoEspecificacion6;
	}

	public void setProductoEspecificacion6(Integer productoEspecificacion6) {
		this.productoEspecificacion6 = productoEspecificacion6;
	}

	public String getProductoEspecificacionValor6() {
		return productoEspecificacionValor6;
	}

	public void setProductoEspecificacionValor6(
			String productoEspecificacionValor6) {
		this.productoEspecificacionValor6 = productoEspecificacionValor6;
	}

	public Integer getProductoEspecificacion7() {
		return productoEspecificacion7;
	}

	public void setProductoEspecificacion7(Integer productoEspecificacion7) {
		this.productoEspecificacion7 = productoEspecificacion7;
	}

	public String getProductoEspecificacionValor7() {
		return productoEspecificacionValor7;
	}

	public void setProductoEspecificacionValor7(
			String productoEspecificacionValor7) {
		this.productoEspecificacionValor7 = productoEspecificacionValor7;
	}

	public Integer getProductoEspecificacion8() {
		return productoEspecificacion8;
	}

	public void setProductoEspecificacion8(Integer productoEspecificacion8) {
		this.productoEspecificacion8 = productoEspecificacion8;
	}

	public String getProductoEspecificacionValor8() {
		return productoEspecificacionValor8;
	}

	public void setProductoEspecificacionValor8(
			String productoEspecificacionValor8) {
		this.productoEspecificacionValor8 = productoEspecificacionValor8;
	}

	public Integer getProductoEspecificacion9() {
		return productoEspecificacion9;
	}

	public void setProductoEspecificacion9(Integer productoEspecificacion9) {
		this.productoEspecificacion9 = productoEspecificacion9;
	}

	public String getProductoEspecificacionValor9() {
		return productoEspecificacionValor9;
	}

	public void setProductoEspecificacionValor9(
			String productoEspecificacionValor9) {
		this.productoEspecificacionValor9 = productoEspecificacionValor9;
	}

	public Integer getProductoEspecificacion10() {
		return productoEspecificacion10;
	}

	public void setProductoEspecificacion10(Integer productoEspecificacion10) {
		this.productoEspecificacion10 = productoEspecificacion10;
	}

	public String getProductoEspecificacionValor10() {
		return productoEspecificacionValor10;
	}

	public void setProductoEspecificacionValor10(
			String productoEspecificacionValor10) {
		this.productoEspecificacionValor10 = productoEspecificacionValor10;
	}

	public Integer getProductoEspecificacion11() {
		return productoEspecificacion11;
	}

	public void setProductoEspecificacion11(Integer productoEspecificacion11) {
		this.productoEspecificacion11 = productoEspecificacion11;
	}

	public String getProductoEspecificacionValor11() {
		return productoEspecificacionValor11;
	}

	public void setProductoEspecificacionValor11(
			String productoEspecificacionValor11) {
		this.productoEspecificacionValor11 = productoEspecificacionValor11;
	}

	public Integer getProductoEspecificacion12() {
		return productoEspecificacion12;
	}

	public void setProductoEspecificacion12(Integer productoEspecificacion12) {
		this.productoEspecificacion12 = productoEspecificacion12;
	}

	public String getProductoEspecificacionValor12() {
		return productoEspecificacionValor12;
	}

	public void setProductoEspecificacionValor12(
			String productoEspecificacionValor12) {
		this.productoEspecificacionValor12 = productoEspecificacionValor12;
	}

	public Integer getProductoEspecificacion13() {
		return productoEspecificacion13;
	}

	public void setProductoEspecificacion13(Integer productoEspecificacion13) {
		this.productoEspecificacion13 = productoEspecificacion13;
	}

	public String getProductoEspecificacionValor13() {
		return productoEspecificacionValor13;
	}

	public void setProductoEspecificacionValor13(
			String productoEspecificacionValor13) {
		this.productoEspecificacionValor13 = productoEspecificacionValor13;
	}

	public Integer getProductoEspecificacion14() {
		return productoEspecificacion14;
	}

	public void setProductoEspecificacion14(Integer productoEspecificacion14) {
		this.productoEspecificacion14 = productoEspecificacion14;
	}

	public String getProductoEspecificacionValor14() {
		return productoEspecificacionValor14;
	}

	public void setProductoEspecificacionValor14(
			String productoEspecificacionValor14) {
		this.productoEspecificacionValor14 = productoEspecificacionValor14;
	}

	public Integer getProductoEspecificacion15() {
		return productoEspecificacion15;
	}

	public void setProductoEspecificacion15(Integer productoEspecificacion15) {
		this.productoEspecificacion15 = productoEspecificacion15;
	}

	public String getProductoEspecificacionValor15() {
		return productoEspecificacionValor15;
	}

	public void setProductoEspecificacionValor15(
			String productoEspecificacionValor15) {
		this.productoEspecificacionValor15 = productoEspecificacionValor15;
	}

	public Integer getProductoEspecificacion16() {
		return productoEspecificacion16;
	}

	public void setProductoEspecificacion16(Integer productoEspecificacion16) {
		this.productoEspecificacion16 = productoEspecificacion16;
	}

	public String getProductoEspecificacionValor16() {
		return productoEspecificacionValor16;
	}

	public void setProductoEspecificacionValor16(
			String productoEspecificacionValor16) {
		this.productoEspecificacionValor16 = productoEspecificacionValor16;
	}

	public Integer getProductoEspecificacion17() {
		return productoEspecificacion17;
	}

	public void setProductoEspecificacion17(Integer productoEspecificacion17) {
		this.productoEspecificacion17 = productoEspecificacion17;
	}

	public String getProductoEspecificacionValor17() {
		return productoEspecificacionValor17;
	}

	public void setProductoEspecificacionValor17(
			String productoEspecificacionValor17) {
		this.productoEspecificacionValor17 = productoEspecificacionValor17;
	}

	public Integer getProductoEspecificacion18() {
		return productoEspecificacion18;
	}

	public void setProductoEspecificacion18(Integer productoEspecificacion18) {
		this.productoEspecificacion18 = productoEspecificacion18;
	}

	public String getProductoEspecificacionValor18() {
		return productoEspecificacionValor18;
	}

	public void setProductoEspecificacionValor18(
			String productoEspecificacionValor18) {
		this.productoEspecificacionValor18 = productoEspecificacionValor18;
	}

	public Integer getProductoEspecificacion19() {
		return productoEspecificacion19;
	}

	public void setProductoEspecificacion19(Integer productoEspecificacion19) {
		this.productoEspecificacion19 = productoEspecificacion19;
	}

	public String getProductoEspecificacionValor19() {
		return productoEspecificacionValor19;
	}

	public void setProductoEspecificacionValor19(
			String productoEspecificacionValor19) {
		this.productoEspecificacionValor19 = productoEspecificacionValor19;
	}

	public Integer getProductoEspecificacion20() {
		return productoEspecificacion20;
	}

	public void setProductoEspecificacion20(Integer productoEspecificacion20) {
		this.productoEspecificacion20 = productoEspecificacion20;
	}

	public String getProductoEspecificacionValor20() {
		return productoEspecificacionValor20;
	}

	public void setProductoEspecificacionValor20(
			String productoEspecificacionValor20) {
		this.productoEspecificacionValor20 = productoEspecificacionValor20;
	}

	public String getProductoEspecificacionDesc1() {
		return productoEspecificacionDesc1;
	}

	public void setProductoEspecificacionDesc1(
			String productoEspecificacionDesc1) {
		this.productoEspecificacionDesc1 = productoEspecificacionDesc1;
	}

	public String getProductoEspecificacionDesc2() {
		return productoEspecificacionDesc2;
	}

	public void setProductoEspecificacionDesc2(
			String productoEspecificacionDesc2) {
		this.productoEspecificacionDesc2 = productoEspecificacionDesc2;
	}

	public String getProductoEspecificacionDesc3() {
		return productoEspecificacionDesc3;
	}

	public void setProductoEspecificacionDesc3(
			String productoEspecificacionDesc3) {
		this.productoEspecificacionDesc3 = productoEspecificacionDesc3;
	}

	public String getProductoEspecificacionDesc4() {
		return productoEspecificacionDesc4;
	}

	public void setProductoEspecificacionDesc4(
			String productoEspecificacionDesc4) {
		this.productoEspecificacionDesc4 = productoEspecificacionDesc4;
	}

	public String getProductoEspecificacionDesc5() {
		return productoEspecificacionDesc5;
	}

	public void setProductoEspecificacionDesc5(
			String productoEspecificacionDesc5) {
		this.productoEspecificacionDesc5 = productoEspecificacionDesc5;
	}

	public String getProductoEspecificacionDesc6() {
		return productoEspecificacionDesc6;
	}

	public void setProductoEspecificacionDesc6(
			String productoEspecificacionDesc6) {
		this.productoEspecificacionDesc6 = productoEspecificacionDesc6;
	}

	public String getProductoEspecificacionDesc7() {
		return productoEspecificacionDesc7;
	}

	public void setProductoEspecificacionDesc7(
			String productoEspecificacionDesc7) {
		this.productoEspecificacionDesc7 = productoEspecificacionDesc7;
	}

	public String getProductoEspecificacionDesc8() {
		return productoEspecificacionDesc8;
	}

	public void setProductoEspecificacionDesc8(
			String productoEspecificacionDesc8) {
		this.productoEspecificacionDesc8 = productoEspecificacionDesc8;
	}

	public String getProductoEspecificacionDesc9() {
		return productoEspecificacionDesc9;
	}

	public void setProductoEspecificacionDesc9(
			String productoEspecificacionDesc9) {
		this.productoEspecificacionDesc9 = productoEspecificacionDesc9;
	}

	public String getProductoEspecificacionDesc10() {
		return productoEspecificacionDesc10;
	}

	public void setProductoEspecificacionDesc10(
			String productoEspecificacionDesc10) {
		this.productoEspecificacionDesc10 = productoEspecificacionDesc10;
	}

	public String getProductoEspecificacionDesc11() {
		return productoEspecificacionDesc11;
	}

	public void setProductoEspecificacionDesc11(
			String productoEspecificacionDesc11) {
		this.productoEspecificacionDesc11 = productoEspecificacionDesc11;
	}

	public String getProductoEspecificacionDesc12() {
		return productoEspecificacionDesc12;
	}

	public void setProductoEspecificacionDesc12(
			String productoEspecificacionDesc12) {
		this.productoEspecificacionDesc12 = productoEspecificacionDesc12;
	}

	public String getProductoEspecificacionDesc13() {
		return productoEspecificacionDesc13;
	}

	public void setProductoEspecificacionDesc13(
			String productoEspecificacionDesc13) {
		this.productoEspecificacionDesc13 = productoEspecificacionDesc13;
	}

	public String getProductoEspecificacionDesc14() {
		return productoEspecificacionDesc14;
	}

	public void setProductoEspecificacionDesc14(
			String productoEspecificacionDesc14) {
		this.productoEspecificacionDesc14 = productoEspecificacionDesc14;
	}

	public String getProductoEspecificacionDesc15() {
		return productoEspecificacionDesc15;
	}

	public void setProductoEspecificacionDesc15(
			String productoEspecificacionDesc15) {
		this.productoEspecificacionDesc15 = productoEspecificacionDesc15;
	}

	public String getProductoEspecificacionDesc16() {
		return productoEspecificacionDesc16;
	}

	public void setProductoEspecificacionDesc16(
			String productoEspecificacionDesc16) {
		this.productoEspecificacionDesc16 = productoEspecificacionDesc16;
	}

	public String getProductoEspecificacionDesc17() {
		return productoEspecificacionDesc17;
	}

	public void setProductoEspecificacionDesc17(
			String productoEspecificacionDesc17) {
		this.productoEspecificacionDesc17 = productoEspecificacionDesc17;
	}

	public String getProductoEspecificacionDesc18() {
		return productoEspecificacionDesc18;
	}

	public void setProductoEspecificacionDesc18(
			String productoEspecificacionDesc18) {
		this.productoEspecificacionDesc18 = productoEspecificacionDesc18;
	}

	public String getProductoEspecificacionDesc19() {
		return productoEspecificacionDesc19;
	}

	public void setProductoEspecificacionDesc19(
			String productoEspecificacionDesc19) {
		this.productoEspecificacionDesc19 = productoEspecificacionDesc19;
	}

	public String getProductoEspecificacionDesc20() {
		return productoEspecificacionDesc20;
	}

	public void setProductoEspecificacionDesc20(
			String productoEspecificacionDesc20) {
		this.productoEspecificacionDesc20 = productoEspecificacionDesc20;
	}

	public Integer getProductoEspecificacion21() {
		return productoEspecificacion21;
	}

	public void setProductoEspecificacion21(Integer productoEspecificacion21) {
		this.productoEspecificacion21 = productoEspecificacion21;
	}

	public String getProductoEspecificacionValor21() {
		return productoEspecificacionValor21;
	}

	public void setProductoEspecificacionValor21(
			String productoEspecificacionValor21) {
		this.productoEspecificacionValor21 = productoEspecificacionValor21;
	}

	public String getProductoEspecificacionDesc21() {
		return productoEspecificacionDesc21;
	}

	public void setProductoEspecificacionDesc21(
			String productoEspecificacionDesc21) {
		this.productoEspecificacionDesc21 = productoEspecificacionDesc21;
	}

	public Integer getProductoEspecificacion22() {
		return productoEspecificacion22;
	}

	public void setProductoEspecificacion22(Integer productoEspecificacion22) {
		this.productoEspecificacion22 = productoEspecificacion22;
	}

	public String getProductoEspecificacionValor22() {
		return productoEspecificacionValor22;
	}

	public void setProductoEspecificacionValor22(
			String productoEspecificacionValor22) {
		this.productoEspecificacionValor22 = productoEspecificacionValor22;
	}

	public String getProductoEspecificacionDesc22() {
		return productoEspecificacionDesc22;
	}

	public void setProductoEspecificacionDesc22(
			String productoEspecificacionDesc22) {
		this.productoEspecificacionDesc22 = productoEspecificacionDesc22;
	}

	public Integer getProductoEspecificacion23() {
		return productoEspecificacion23;
	}

	public void setProductoEspecificacion23(Integer productoEspecificacion23) {
		this.productoEspecificacion23 = productoEspecificacion23;
	}

	public String getProductoEspecificacionValor23() {
		return productoEspecificacionValor23;
	}

	public void setProductoEspecificacionValor23(
			String productoEspecificacionValor23) {
		this.productoEspecificacionValor23 = productoEspecificacionValor23;
	}

	public String getProductoEspecificacionDesc23() {
		return productoEspecificacionDesc23;
	}

	public void setProductoEspecificacionDesc23(
			String productoEspecificacionDesc23) {
		this.productoEspecificacionDesc23 = productoEspecificacionDesc23;
	}

	public Integer getProductoEspecificacion24() {
		return productoEspecificacion24;
	}

	public void setProductoEspecificacion24(Integer productoEspecificacion24) {
		this.productoEspecificacion24 = productoEspecificacion24;
	}

	public String getProductoEspecificacionValor24() {
		return productoEspecificacionValor24;
	}

	public void setProductoEspecificacionValor24(
			String productoEspecificacionValor24) {
		this.productoEspecificacionValor24 = productoEspecificacionValor24;
	}

	public String getProductoEspecificacionDesc24() {
		return productoEspecificacionDesc24;
	}

	public void setProductoEspecificacionDesc24(
			String productoEspecificacionDesc24) {
		this.productoEspecificacionDesc24 = productoEspecificacionDesc24;
	}

	public Integer getProductoEspecificacion25() {
		return productoEspecificacion25;
	}

	public void setProductoEspecificacion25(Integer productoEspecificacion25) {
		this.productoEspecificacion25 = productoEspecificacion25;
	}

	public String getProductoEspecificacionValor25() {
		return productoEspecificacionValor25;
	}

	public void setProductoEspecificacionValor25(
			String productoEspecificacionValor25) {
		this.productoEspecificacionValor25 = productoEspecificacionValor25;
	}

	public String getProductoEspecificacionDesc25() {
		return productoEspecificacionDesc25;
	}

	public void setProductoEspecificacionDesc25(
			String productoEspecificacionDesc25) {
		this.productoEspecificacionDesc25 = productoEspecificacionDesc25;
	}

	public Integer getProductoEspecificacion26() {
		return productoEspecificacion26;
	}

	public void setProductoEspecificacion26(Integer productoEspecificacion26) {
		this.productoEspecificacion26 = productoEspecificacion26;
	}

	public String getProductoEspecificacionValor26() {
		return productoEspecificacionValor26;
	}

	public void setProductoEspecificacionValor26(
			String productoEspecificacionValor26) {
		this.productoEspecificacionValor26 = productoEspecificacionValor26;
	}

	public String getProductoEspecificacionDesc26() {
		return productoEspecificacionDesc26;
	}

	public void setProductoEspecificacionDesc26(
			String productoEspecificacionDesc26) {
		this.productoEspecificacionDesc26 = productoEspecificacionDesc26;
	}

	public Integer getProductoEspecificacion27() {
		return productoEspecificacion27;
	}

	public void setProductoEspecificacion27(Integer productoEspecificacion27) {
		this.productoEspecificacion27 = productoEspecificacion27;
	}

	public String getProductoEspecificacionValor27() {
		return productoEspecificacionValor27;
	}

	public void setProductoEspecificacionValor27(
			String productoEspecificacionValor27) {
		this.productoEspecificacionValor27 = productoEspecificacionValor27;
	}

	public String getProductoEspecificacionDesc27() {
		return productoEspecificacionDesc27;
	}

	public void setProductoEspecificacionDesc27(
			String productoEspecificacionDesc27) {
		this.productoEspecificacionDesc27 = productoEspecificacionDesc27;
	}

	public Integer getProductoEspecificacion28() {
		return productoEspecificacion28;
	}

	public void setProductoEspecificacion28(Integer productoEspecificacion28) {
		this.productoEspecificacion28 = productoEspecificacion28;
	}

	public String getProductoEspecificacionValor28() {
		return productoEspecificacionValor28;
	}

	public void setProductoEspecificacionValor28(
			String productoEspecificacionValor28) {
		this.productoEspecificacionValor28 = productoEspecificacionValor28;
	}

	public String getProductoEspecificacionDesc28() {
		return productoEspecificacionDesc28;
	}

	public void setProductoEspecificacionDesc28(
			String productoEspecificacionDesc28) {
		this.productoEspecificacionDesc28 = productoEspecificacionDesc28;
	}

	public Integer getProductoEspecificacion29() {
		return productoEspecificacion29;
	}

	public void setProductoEspecificacion29(Integer productoEspecificacion29) {
		this.productoEspecificacion29 = productoEspecificacion29;
	}

	public String getProductoEspecificacionValor29() {
		return productoEspecificacionValor29;
	}

	public void setProductoEspecificacionValor29(
			String productoEspecificacionValor29) {
		this.productoEspecificacionValor29 = productoEspecificacionValor29;
	}

	public String getProductoEspecificacionDesc29() {
		return productoEspecificacionDesc29;
	}

	public void setProductoEspecificacionDesc29(
			String productoEspecificacionDesc29) {
		this.productoEspecificacionDesc29 = productoEspecificacionDesc29;
	}

	public Integer getProductoEspecificacion30() {
		return productoEspecificacion30;
	}

	public void setProductoEspecificacion30(Integer productoEspecificacion30) {
		this.productoEspecificacion30 = productoEspecificacion30;
	}

	public String getProductoEspecificacionValor30() {
		return productoEspecificacionValor30;
	}

	public void setProductoEspecificacionValor30(
			String productoEspecificacionValor30) {
		this.productoEspecificacionValor30 = productoEspecificacionValor30;
	}

	public String getProductoEspecificacionDesc30() {
		return productoEspecificacionDesc30;
	}

	public void setProductoEspecificacionDesc30(
			String productoEspecificacionDesc30) {
		this.productoEspecificacionDesc30 = productoEspecificacionDesc30;
	}

	public Integer getProductoEspecificacion31() {
		return productoEspecificacion31;
	}

	public void setProductoEspecificacion31(Integer productoEspecificacion31) {
		this.productoEspecificacion31 = productoEspecificacion31;
	}

	public String getProductoEspecificacionValor31() {
		return productoEspecificacionValor31;
	}

	public void setProductoEspecificacionValor31(
			String productoEspecificacionValor31) {
		this.productoEspecificacionValor31 = productoEspecificacionValor31;
	}

	public String getProductoEspecificacionDesc31() {
		return productoEspecificacionDesc31;
	}

	public void setProductoEspecificacionDesc31(
			String productoEspecificacionDesc31) {
		this.productoEspecificacionDesc31 = productoEspecificacionDesc31;
	}

	public Integer getProductoEspecificacion32() {
		return productoEspecificacion32;
	}

	public void setProductoEspecificacion32(Integer productoEspecificacion32) {
		this.productoEspecificacion32 = productoEspecificacion32;
	}

	public String getProductoEspecificacionValor32() {
		return productoEspecificacionValor32;
	}

	public void setProductoEspecificacionValor32(
			String productoEspecificacionValor32) {
		this.productoEspecificacionValor32 = productoEspecificacionValor32;
	}

	public String getProductoEspecificacionDesc32() {
		return productoEspecificacionDesc32;
	}

	public void setProductoEspecificacionDesc32(
			String productoEspecificacionDesc32) {
		this.productoEspecificacionDesc32 = productoEspecificacionDesc32;
	}

	public Integer getProductoEspecificacion33() {
		return productoEspecificacion33;
	}

	public void setProductoEspecificacion33(Integer productoEspecificacion33) {
		this.productoEspecificacion33 = productoEspecificacion33;
	}

	public String getProductoEspecificacionValor33() {
		return productoEspecificacionValor33;
	}

	public void setProductoEspecificacionValor33(
			String productoEspecificacionValor33) {
		this.productoEspecificacionValor33 = productoEspecificacionValor33;
	}

	public String getProductoEspecificacionDesc33() {
		return productoEspecificacionDesc33;
	}

	public void setProductoEspecificacionDesc33(
			String productoEspecificacionDesc33) {
		this.productoEspecificacionDesc33 = productoEspecificacionDesc33;
	}

	public Integer getProductoEspecificacion34() {
		return productoEspecificacion34;
	}

	public void setProductoEspecificacion34(Integer productoEspecificacion34) {
		this.productoEspecificacion34 = productoEspecificacion34;
	}

	public String getProductoEspecificacionValor34() {
		return productoEspecificacionValor34;
	}

	public void setProductoEspecificacionValor34(
			String productoEspecificacionValor34) {
		this.productoEspecificacionValor34 = productoEspecificacionValor34;
	}

	public String getProductoEspecificacionDesc34() {
		return productoEspecificacionDesc34;
	}

	public void setProductoEspecificacionDesc34(
			String productoEspecificacionDesc34) {
		this.productoEspecificacionDesc34 = productoEspecificacionDesc34;
	}

	public Integer getProductoEspecificacion35() {
		return productoEspecificacion35;
	}

	public void setProductoEspecificacion35(Integer productoEspecificacion35) {
		this.productoEspecificacion35 = productoEspecificacion35;
	}

	public String getProductoEspecificacionValor35() {
		return productoEspecificacionValor35;
	}

	public void setProductoEspecificacionValor35(
			String productoEspecificacionValor35) {
		this.productoEspecificacionValor35 = productoEspecificacionValor35;
	}

	public String getProductoEspecificacionDesc35() {
		return productoEspecificacionDesc35;
	}

	public void setProductoEspecificacionDesc35(
			String productoEspecificacionDesc35) {
		this.productoEspecificacionDesc35 = productoEspecificacionDesc35;
	}

	public boolean isTamanioGrande() {
		int tamanio = 0;
		if (this.productoDescripcion != null) {
			tamanio = this.productoDescripcion.length();
		}
		return tamanio > ErpConstante.TAMANIO_GRANDE;
	}

	public boolean isTamanioMuyGrandeEsp2() {
		int tamanio = 0;
		if (this.productoEspecificacionValor1 != null
				&& this.productoEspecificacionValor2 != null) {
			tamanio = this.productoEspecificacionValor1.length()
					+ this.productoEspecificacionValor2.length();
		}
		return tamanio > ErpConstante.TAMANIO_MAX_ESPECIFICACION;
	}

	public boolean isTamanioMuyGrandeEsp3() {
		int tamanio = 0;
		if (this.productoEspecificacionValor1 != null
				&& this.productoEspecificacionValor2 != null
				&& this.productoEspecificacionValor3 != null) {
			tamanio = this.productoEspecificacionValor1.length()
					+ this.productoEspecificacionValor2.length()
					+ this.productoEspecificacionValor3.length();
		}
		return tamanio > ErpConstante.TAMANIO_MAX_ESPECIFICACION;
	}

}
