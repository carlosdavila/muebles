package com.erp.producto.service;


import java.util.List;

import com.erp.producto.model.ProductoBean;

import frameworkfrivas.bean.AdjuntoBean;
import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.service.ComunService;

public interface ProductoService extends ComunService<ProductoBean, Integer> {
	
	
	public void guardarConOrden(ProductoBean productoBean)
			throws BusinessException ;
	public ProductoBean obtenerUltimoProducto()
			throws BusinessException;
	public void guardarImagen(ProductoBean productoBean, List<AdjuntoBean> listaAdjunto) ;
}
