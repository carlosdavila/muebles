package com.erp.producto.service;



import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.erp.modelo.dao.ModeloDao;
import com.erp.modelo.model.ModeloBean;
import com.erp.producto.dao.ProductoDao;
import com.erp.producto.model.ProductoBean;

import frameworkfrivas.bean.AdjuntoBean;
import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.service.ComunServiceImpl;

@Service
public class ProductoServiceImpl extends ComunServiceImpl<ProductoBean, Integer> implements ProductoService {
	@Autowired
	private ProductoDao productoDao;
	
	@Autowired
	private ModeloDao modeloDao;
	
	private static final Logger LOG = Logger.getLogger(ProductoServiceImpl.class);
	
	
	@Transactional
	public void guardarConOrden(ProductoBean productoBean)
			throws BusinessException {
		try {
		ModeloBean modeloBean=	modeloDao.obtener(productoBean.getModeloBean().getModeloId());
		productoBean.setModeloBean(modeloBean);	
		String[] columnaEquivalente = { "modeloBean.modeloId"};
		String[] columnaValorEquivalente = {productoBean.getModeloBean().getModeloId().toString()};
		
		int total=	productoDao.obtenerTotalFilasIgualAcolumnaYporCoincidenciaAlgunasColumnas(null,
				null,columnaEquivalente, columnaValorEquivalente, null,
				null, null);
				
		productoBean.setProductoOrden(total+1);
			guardar(productoBean);

			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
		          throw new BusinessException(e);
			}
		
	}
	
	
	@Transactional
	public ProductoBean obtenerUltimoProducto()
			throws BusinessException {
		ProductoBean productoBean= new ProductoBean();
		try {
		      productoBean= (ProductoBean) productoDao.obtenerListaTotalOrdena("productoId", "desc").get(0);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
		          throw new BusinessException(e);
			}
		return productoBean;
	}
	
	
	public void guardarImagen(ProductoBean productoBean, List<AdjuntoBean> listaAdjunto) {
		productoBean.setProductoImagen(null);
		productoBean.setProductoImagen1(null);
		productoBean.setProductoImagen2(null);
		productoBean.setProductoImagen3(null);
		productoBean.setProductoImagen4(null);
		
		
		
		if (listaAdjunto != null) {
			if (listaAdjunto.size() > 0) {
				productoBean.setProductoImagen(listaAdjunto.get(0).getNombre());
			}
			if (listaAdjunto.size() > 1) {
				productoBean.setProductoImagen1(listaAdjunto.get(1).getNombre());
			}
			if (listaAdjunto.size() > 2) {
				productoBean.setProductoImagen2(listaAdjunto.get(2).getNombre());
			}
			if (listaAdjunto.size() > 3) {
				productoBean.setProductoImagen3(listaAdjunto.get(3).getNombre());
			}
			if (listaAdjunto.size() > 4) {
				productoBean.setProductoImagen4(listaAdjunto.get(4).getNombre());
			}
		}
	}

	
}
