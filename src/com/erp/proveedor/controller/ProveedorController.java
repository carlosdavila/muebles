package com.erp.proveedor.controller;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.erp.comun.util.ErpConstante;
import com.erp.proveedor.model.ProveedorBean;
import com.erp.proveedor.service.ProveedorService;
import com.erp.tipodoc.model.TipoDocBean;
import com.erp.tipodoc.service.TipoDocService;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import frameworkfrivas.bean.RequestJqGridBean;
import frameworkfrivas.util.Utilitario;

@Controller
@RequestMapping("proveedorController")
public class ProveedorController {
	@Autowired
	private ProveedorService proveedorService;

	@Autowired
	private TipoDocService tipoDocService;
	
	@Autowired
	private Properties erpConfig;

	private static final Logger LOG = Logger.getLogger(ProveedorController.class);

	@RequestMapping(value = "/mostrarGestionProveedor", method = RequestMethod.GET)
	public ModelAndView mostrarGestionProveedor(HttpSession session) {
		return new ModelAndView("proveedor/gestionProveedor");
	}
	
	
	@RequestMapping(value = "/obtenerBusqueda", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerBusqueda(String proveedor) {
		String json="";
		

  try {

	json = Utilitario.formatearJsonString(	proveedorService.obtenerBusqueda(proveedor));
} catch (JsonGenerationException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (JsonMappingException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
		System.out.println("proveedor="+proveedor);
		return json;
	}
	
	@RequestMapping(value = "/obtenerListaFiltrada", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerListaFiltrada(
			RequestJqGridBean requestJqGridBean) {

		String json = "";
		try {

			json = null;
		} catch (Exception e) {
		
			LOG.error(e.getMessage(), e);
		}
	
		return json;
	}
	
	@RequestMapping(value = "/mostrarBuscarProveedor", method = RequestMethod.GET)
	public ModelAndView mostrarBuscarProveedor() {
		return new ModelAndView("proveedor/buscarProveedor");
	}
	
	@RequestMapping(value = "/mostrarNuevoProveedor", method = RequestMethod.GET)
	public ModelAndView mostrarNuevoProveedor() {
		ModelAndView model = new ModelAndView("proveedor/nuevoProveedor");
		try {
			List<TipoDocBean> listaTipoDoc = tipoDocService.buscarTodo();
			model.addObject("listaTipoDoc", listaTipoDoc);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		model.addObject("proveedorBean", new ProveedorBean());
		return model;
	}
	
	@RequestMapping(value = "/mostrarEditarProveedor", method = RequestMethod.GET)
	public ModelAndView mostrarEditarProveedor(Integer proveedorId) {
		
		ProveedorBean proveedorBean = null;
		List<TipoDocBean> listaTipoDoc=null;
		try {

			proveedorBean = proveedorService.obtener(proveedorId);
            listaTipoDoc = tipoDocService.buscarTodo();
		
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		ModelAndView model = new ModelAndView("proveedor/editarProveedor");
		model.addObject("listaTipoDoc", listaTipoDoc);
		model.addObject("proveedorBean", proveedorBean);
		return model;
	}

	@RequestMapping(value = "/guardar", method = RequestMethod.POST)
	@ResponseBody
	public void guardar(ProveedorBean proveedorBean) {
		try {
			proveedorService.guardar(proveedorBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	@ResponseBody
	public void editar(ProveedorBean proveedorBean) {
		try {
			proveedorService.actualizar(proveedorBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.GET)
	@ResponseBody
	public String editar(Integer proveedorId) {
		String mensajeError="";
		try {
			proveedorService.eliminar(proveedorId);

		} catch (DataIntegrityViolationException e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (ConstraintViolationException e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (Exception e) {

			mensajeError = erpConfig
					.getProperty(ErpConstante.MENSAJE_ERROR_GENERICO);
			LOG.error(e.getMessage(), e);
		}
		return mensajeError;

	}
	
}
