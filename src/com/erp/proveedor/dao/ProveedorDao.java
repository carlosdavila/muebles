package com.erp.proveedor.dao;

import java.util.List;

import com.erp.proveedor.model.ProveedorBean;

import frameworkfrivas.dao.ComunDao;
import frameworkfrivas.exception.DAOException;

public interface ProveedorDao extends ComunDao<ProveedorBean, Integer> {
	public List<ProveedorBean> obtenerBusqueda(
			String nombre) throws DAOException ;
}
