package com.erp.proveedor.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.erp.proveedor.model.ProveedorBean;

import frameworkfrivas.dao.ComunDaoImpl;
import frameworkfrivas.exception.DAOException;

public class ProveedorDaoImpl  extends ComunDaoImpl<ProveedorBean, Integer>
implements ProveedorDao {
	private static final Logger LOG = Logger
			.getLogger(ProveedorDaoImpl.class);
	public ProveedorDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}

	
	
	
	public List<ProveedorBean> obtenerBusqueda(
			String nombre) throws DAOException {
		List<ProveedorBean> listaProveedor = new ArrayList<ProveedorBean>();
		try {
System.out.println(" from " + getDomainClass().getName()
							+ " where proveedorNombre like %'" + nombre+"'%"
							+ " order by proveedorNombre  asc");
			Query query = getCurrentSession().createQuery(
					" from " + getDomainClass().getName()
							+ " where proveedorNombre like :nombre "+
							 " order by proveedorNombre  asc");

			listaProveedor =  query.setParameter("nombre", nombre + "%").list();

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new DAOException(e);
		}

		return listaProveedor;
	}

	
}
