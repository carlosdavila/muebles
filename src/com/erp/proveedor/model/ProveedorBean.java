package com.erp.proveedor.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.erp.tipodoc.model.TipoDocBean;

@Entity
@Table(name = "t_proveedor" )
@Proxy(lazy=true)
public class ProveedorBean {
	@Id
	@GeneratedValue
	@Column(name = "proveedor_id")
	private Integer proveedorId;
	@ManyToOne
	@JoinColumn(name = "tipo_doc_id")
	private TipoDocBean tipoDocBean;
	@Column(name = "proveedor_nro_doc")
	private String proveedorNroDoc;
	@Column(name = "proveedor_nombre")
	private String proveedorNombre;
	@Column(name = "proveedor_email")
	private String proveedorEmail;
	@Column(name = "proveedor_telefono")
	private String proveedorTelefono;
	@Column(name = "rpoveedor_direccion")
	private String proveedorDireccion;
	@Column(name = "proveedor_contacto")
	private String proveedorContacto;
	public Integer getProveedorId() {
		return proveedorId;
	}
	public void setProveedorId(Integer proveedorId) {
		this.proveedorId = proveedorId;
	}
	public TipoDocBean getTipoDocBean() {
		return tipoDocBean;
	}
	public void setTipoDocBean(TipoDocBean tipoDocBean) {
		this.tipoDocBean = tipoDocBean;
	}
	public String getProveedorNroDoc() {
		return proveedorNroDoc;
	}
	public void setProveedorNroDoc(String proveedorNroDoc) {
		this.proveedorNroDoc = proveedorNroDoc;
	}
	public String getProveedorNombre() {
		return proveedorNombre;
	}
	public void setProveedorNombre(String proveedorNombre) {
		this.proveedorNombre = proveedorNombre;
	}
	public String getProveedorEmail() {
		return proveedorEmail;
	}
	public void setProveedorEmail(String proveedorEmail) {
		this.proveedorEmail = proveedorEmail;
	}
	public String getProveedorTelefono() {
		return proveedorTelefono;
	}
	public void setProveedorTelefono(String proveedorTelefono) {
		this.proveedorTelefono = proveedorTelefono;
	}
	public String getProveedorDireccion() {
		return proveedorDireccion;
	}
	public void setProveedorDireccion(String proveedorDireccion) {
		this.proveedorDireccion = proveedorDireccion;
	}
	public String getProveedorContacto() {
		return proveedorContacto;
	}
	public void setProveedorContacto(String proveedorContacto) {
		this.proveedorContacto = proveedorContacto;
	}

}
