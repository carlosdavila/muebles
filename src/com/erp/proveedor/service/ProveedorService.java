package com.erp.proveedor.service;

import java.util.List;

import com.erp.proveedor.model.ProveedorBean;

import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.service.ComunService;

public interface ProveedorService extends ComunService<ProveedorBean, Integer> {

	public List<ProveedorBean> obtenerBusqueda(
			String nombre) throws BusinessException ;
}
