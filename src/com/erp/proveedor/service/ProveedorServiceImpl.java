package com.erp.proveedor.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.proveedor.dao.ProveedorDao;
import com.erp.proveedor.model.ProveedorBean;

import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.service.ComunServiceImpl;

@Service
public class ProveedorServiceImpl extends ComunServiceImpl<ProveedorBean, Integer>
implements ProveedorService {
	private static final Logger LOG = Logger
			.getLogger(ProveedorServiceImpl.class);
	@Autowired
	private ProveedorDao ProveedorDao;
	
	@Override
	public List<ProveedorBean> obtenerBusqueda(String nombre) throws BusinessException {
		List<ProveedorBean> listaproveedor= new ArrayList<ProveedorBean>();	
		try{
		listaproveedor= ProveedorDao.obtenerBusqueda(nombre);
		}catch(Exception e){
			LOG.error("Error obtenerBusqueda");
			throw new BusinessException(e);
		}
		
		return listaproveedor;
	}

}
