package com.erp.respuesta.controller;

import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.erp.comun.util.ErpConstante;
import com.erp.pregunta.controller.PreguntaController;
import com.erp.respuesta.model.RespuestaBean;
import com.erp.respuesta.service.RespuestaService;

import frameworkfrivas.bean.RequestJqGridBean;
import frameworkfrivas.util.Utilitario;

@Controller
@RequestMapping("respuestaController")
public class ControllerRespuesta {
	@Autowired
	private RespuestaService respuestaService;
	
	@Autowired
	private Properties erpConfig;
	
	private static final Logger LOG = Logger.getLogger(PreguntaController.class);

	@RequestMapping(value = "/mostrarGestionRespuesta", method = RequestMethod.GET)
	public ModelAndView mostrarGestionRespuesta(HttpSession session) {
		return new ModelAndView("respuesta/gestionRespuesta");
	}

	@RequestMapping(value = "/obtenerListaFiltrada", method = RequestMethod.GET)
	@ResponseBody
	public String obtenerListaFiltrada(RequestJqGridBean requestJqGridBean) {

		String json = "";
		try {

			json = null;
			} catch (Exception e) {

			LOG.error(e.getMessage(), e);
		}

		return json;
	}

	@RequestMapping(value = "/mostrarNuevoRespuesta", method = RequestMethod.GET)
	public ModelAndView mostrarNuevoRespuesta() {
		ModelAndView model = new ModelAndView("pregunta/nuevoRespuesta");

		model.addObject("respuestaBean", new RespuestaBean());
		return model;
	}

	@RequestMapping(value = "/mostrarEditarRespuesta", method = RequestMethod.GET)
	public ModelAndView mostrarEditarRespuesta(Integer respuestaId) {

		RespuestaBean respuestaBean = null;

		try {
			respuestaBean = respuestaService.obtener(respuestaId);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		ModelAndView model = new ModelAndView("pregunta/editarPregunta");
		model.addObject("preguntaBean", respuestaBean);
		return model;
	}

	@RequestMapping(value = "/guardar", method = RequestMethod.POST)
	@ResponseBody
	public void guardar(RespuestaBean respuestaBean) {
		try {
			respuestaService.guardar(respuestaBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	@ResponseBody
	public void editar(RespuestaBean respuestaBean) {
		try {
			respuestaService.actualizar(respuestaBean);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.GET)
	@ResponseBody
	public String eliminar(Integer respuestaId) {
		String mensajeError = "";
		try {
			respuestaService.eliminar(respuestaId);

		} catch (DataIntegrityViolationException e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (ConstraintViolationException e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_KEY_FORANEO);
			LOG.error(e.getMessage(), e);

		} catch (Exception e) {

			mensajeError = erpConfig.getProperty(ErpConstante.MENSAJE_ERROR_GENERICO);
			LOG.error(e.getMessage(), e);
		}
		return mensajeError;

	}

}
