package com.erp.respuesta.dao;

import com.erp.respuesta.model.RespuestaBean;

import frameworkfrivas.dao.ComunDao;

public interface RespuestaDao   extends ComunDao<RespuestaBean, Integer> {

}
