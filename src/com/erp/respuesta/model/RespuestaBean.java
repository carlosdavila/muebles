package com.erp.respuesta.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "t_respuesta" )
@Proxy(lazy=true)
public class RespuestaBean {
	
	@Id
	@GeneratedValue
	@Column(name = "respuesta_id")
	private Integer respuestaId;
	
	@Column(name = "pregunta_id")
	private Integer preguntaId;
	
	@Column(name = "respuesta_descripcion")
	private String respuestaDescripcion;

	@Column(name = "respuesta_correcta")
	private Boolean respuestaCorrecta;

	public Integer getRespuestaId() {
		return respuestaId;
	}

	public void setRespuestaId(Integer respuestaId) {
		this.respuestaId = respuestaId;
	}

	public Integer getPreguntaId() {
		return preguntaId;
	}

	public void setPreguntaId(Integer preguntaId) {
		this.preguntaId = preguntaId;
	}

	public String getRespuestaDescripcion() {
		return respuestaDescripcion;
	}

	public void setRespuestaDescripcion(String respuestaDescripcion) {
		this.respuestaDescripcion = respuestaDescripcion;
	}

	public Boolean getRespuestaCorrecta() {
		return respuestaCorrecta;
	}

	public void setRespuestaCorrecta(Boolean respuestaCorrecta) {
		this.respuestaCorrecta = respuestaCorrecta;
	}


}
