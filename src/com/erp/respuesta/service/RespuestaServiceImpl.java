package com.erp.respuesta.service;

import org.springframework.stereotype.Service;

import com.erp.respuesta.model.RespuestaBean;

import frameworkfrivas.service.ComunServiceImpl;

@Service
public class RespuestaServiceImpl   extends ComunServiceImpl<RespuestaBean, Integer>
implements RespuestaService {

}
