package com.erp.seccion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "t_seccion" )
@Proxy(lazy=true)
public class SeccionBean {
	@Id
	@GeneratedValue
	@Column(name = "seccion_id")
	private Integer seccionId;
	
    @Column(name = "seccion_codigo")
	private String seccionCodigo;
    
    @Column(name = "seccion_nombre")
	private String seccionNombre;
    
    @Column(name = "seccion_turno")
	private Integer seccionTurno;
    
    @Column(name = "seccion_nivel")
	private Integer seccionNivel;
    
    @Column(name = "seccion_anio")
	private Integer seccionAnio;
    


	public Integer getSeccionId() {
		return seccionId;
	}

	public void setSeccionId(Integer seccionId) {
		this.seccionId = seccionId;
	}

	public String getSeccionCodigo() {
		return seccionCodigo;
	}

	public void setSeccionCodigo(String seccionCodigo) {
		this.seccionCodigo = seccionCodigo;
	}

	public String getSeccionNombre() {
		return seccionNombre;
	}

	public void setSeccionNombre(String seccionNombre) {
		this.seccionNombre = seccionNombre;
	}

	public Integer getSeccionTurno() {
		return seccionTurno;
	}

	public void setSeccionTurno(Integer seccionTurno) {
		this.seccionTurno = seccionTurno;
	}

	public Integer getSeccionAnio() {
		return seccionAnio;
	}

	public void setSeccionAnio(Integer seccionAnio) {
		this.seccionAnio = seccionAnio;
	}

	public Integer getSeccionNivel() {
		return seccionNivel;
	}

	public void setSeccionNivel(Integer seccionNivel) {
		this.seccionNivel = seccionNivel;
	}
     

}
