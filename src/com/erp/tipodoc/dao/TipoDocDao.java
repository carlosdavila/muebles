package com.erp.tipodoc.dao;

import com.erp.tipodoc.model.TipoDocBean;

import frameworkfrivas.dao.ComunDao;

public interface TipoDocDao  extends ComunDao<TipoDocBean, Integer>{

}
