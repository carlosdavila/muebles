package com.erp.tipodoc.dao;

import org.hibernate.SessionFactory;

import com.erp.tipodoc.model.TipoDocBean;

import frameworkfrivas.dao.ComunDaoImpl;

public class TipoDocDaoImpl extends ComunDaoImpl<TipoDocBean, Integer>
		implements TipoDocDao {

	public TipoDocDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);

	}

}
