package com.erp.tipodoc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_tipo_doc")
public class TipoDocBean {
	@Id
	@Column(name = "tipo_doc_id")
	private Integer tipoDocId;
	@Column(name = "tipo_doc_desc")
	private String tipoDocDesc;

	public Integer getTipoDocId() {
		return tipoDocId;
	}

	public void setTipoDocId(Integer tipoDocId) {
		this.tipoDocId = tipoDocId;
	}

	public String getTipoDocDesc() {
		return tipoDocDesc;
	}

	public void setTipoDocDesc(String tipoDocDesc) {
		this.tipoDocDesc = tipoDocDesc;
	}

}
