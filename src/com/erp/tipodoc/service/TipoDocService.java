package com.erp.tipodoc.service;

import java.util.List;

import com.erp.tipodoc.model.TipoDocBean;

import frameworkfrivas.exception.BusinessException;

public interface TipoDocService {

	public List<TipoDocBean> buscarTodo() throws BusinessException ;
	
	
}
