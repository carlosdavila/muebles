package com.erp.tipodoc.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.comun.util.ErpUtil;
import com.erp.tipodoc.dao.TipoDocDao;
import com.erp.tipodoc.model.TipoDocBean;

import frameworkfrivas.exception.BusinessException;

@Service
public class TipoDocServiceImpl implements TipoDocService {
	@Autowired
	private TipoDocDao tipoDocDao;
	private static final Logger LOG = Logger
			.getLogger(TipoDocServiceImpl.class);

	@SuppressWarnings("unchecked")
	public List<TipoDocBean> buscarTodo() throws BusinessException {
		List<TipoDocBean> listaTipoDoc = new ArrayList<TipoDocBean>();
		try {
		 listaTipoDoc = (List<TipoDocBean>) tipoDocDao
					.buscarTodo();
			ErpUtil.agregarOpcionSeleccioneTipoDocBean(listaTipoDoc);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new BusinessException(e);
		}

		return listaTipoDoc;
	}
}
