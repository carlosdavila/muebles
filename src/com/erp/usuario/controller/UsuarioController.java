package com.erp.usuario.controller;

import java.util.List;


import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.erp.base.BaseController;
import com.erp.comun.util.ErpConstante;
import com.erp.usuario.model.UsuarioBean;
import com.erp.usuario.service.UsuarioService;

import frameworkfrivas.bean.RequestJqGridBean;
import frameworkfrivas.exception.ControllerException;

@Controller
@RequestMapping("usuarioController")
public class UsuarioController extends BaseController{
	
	@Autowired
	private UsuarioService usuarioService;
	

	
	private static final Logger LOG = Logger.getLogger(UsuarioController.class);
	
	
	
	
	
	@RequestMapping(value = "/mostrarLogin", method = RequestMethod.GET)
	public ModelAndView mostrarLogin(HttpSession session) throws ControllerException {
		ModelAndView model=new ModelAndView("usuario/login");
		model.addObject("usuario", new UsuarioBean());
		return model;
		
	}
	
	
	@RequestMapping(value = "/mostrarBienvenido", method = RequestMethod.GET)
	public ModelAndView mostrarBienvenido(HttpSession session) throws ControllerException {
		
		UsuarioBean usuarioBean = obtenerUsuarioSession(session);
		ModelAndView model=new ModelAndView("usuario/bienvenido");
		model.addObject("usuario",usuarioBean);
		return model;
		
	}
	 
	@RequestMapping(value = "/mostrarRecuperarClave", method = RequestMethod.GET)
	public ModelAndView recuperarClave(HttpSession session) throws ControllerException {
		ModelAndView model=new ModelAndView("usuario/recuperarClave");
		model.addObject("usuario", new UsuarioBean());
		return model;
		
	}
	
	@RequestMapping(value = "/mostrarCambiarClave", method = RequestMethod.GET)
	public ModelAndView mostrarCambiarClave(HttpSession session) throws ControllerException {
		ModelAndView model=new ModelAndView("usuario/cambiarClave");
		model.addObject("usuario", new UsuarioBean());
		return model;
		
	}
	
	@RequestMapping(value = "/mostrarNuevoUsuario", method = RequestMethod.GET)
	public ModelAndView mostrarNuevoUsuario(HttpSession session) throws ControllerException {
		ModelAndView model=new ModelAndView("usuario/nuevoUsuario");
		model.addObject("usuario", new UsuarioBean());
		return model;
		
	}
	
	
	@RequestMapping(value = "/validarLogin", method = RequestMethod.POST)
	@ResponseBody
	public String validarLogin(HttpSession session,UsuarioBean usuario) throws ControllerException {
       String resultado=ErpConstante.USUARIO_DESACTIVADO;
       
       
       
		try { 
			RequestJqGridBean requestJqGridBean= new RequestJqGridBean();
			
			String[] columnaEquivalente = { "usuario_user","usuario_clave"};
			String[] columnaValorEquivalente = { usuario.getUsuarioUser().toLowerCase(), usuario.getUsuarioClave()};
			requestJqGridBean.setColumnaEquivalente(columnaEquivalente);
			requestJqGridBean.setValorEquivalente(columnaValorEquivalente);
			requestJqGridBean.setSord("asc");
			requestJqGridBean.setSidx("usuario_user");
			
			UsuarioBean user=null;
	
			List<Object> lista = usuarioService.obtenerListaTotal(requestJqGridBean).getRows();
			
			if(lista!=null && !lista.isEmpty()){
				user=(UsuarioBean) lista.get(0);
			}
			
            
			if (user != null) {
				if(ErpConstante.USUARIO_ACTIVO.equals(user.getUsuarioEstado())){
				session.setAttribute(ErpConstante.USUARIO_SESSION, user);
				resultado=ErpConstante.USUARIO_ACTIVO;
				}else{
					resultado=ErpConstante.USUARIO_DESACTIVADO;	
				}
             }else{
            	 resultado=ErpConstante.USUARIO_O_CLAVE_INCORRECTA;
             }
           
		} catch (Exception e) {
			LOG.debug("Error validarLogin " + e);
			throw new ControllerException("Error validarLogin", e);
		}
		return resultado;
	}
	

	/*
	@RequestMapping(value = "/guardar", method = RequestMethod.POST)
	@ResponseBody
	public String guardarConNumeroActivacion(UsuarioBean usuarioBean) throws ControllerException {
		String res="0";
				
		try {
			UsuarioBean usuarioBD =usuarioDao.obtenerUsuarioPorEmail(usuarioBean);
			if(usuarioBD==null){
				usuarioService.guardarConNumeroActivacion(usuarioBean);
			}else{
				res="1";
			}
			
		} catch (Exception e) {
			LOG.debug("Error guardarConNumeroActivacion " + e);
			throw new ControllerException("Error guardarConNumeroActivacion", e);
		}
		return res;
	}
	*/
	
	/*
	@RequestMapping(value = "/activarUsuario", method = RequestMethod.GET)
	public ModelAndView activarUsuario(HttpSession session,String nroActivacion)  throws ControllerException{
		try{
	    UsuarioBean usuarioBean=usuarioDao.obtenerUsuarioPorNroActivacion(nroActivacion);
		if(ErpConstante.USUARIO_ACTIVO.equals(usuarioBean.getUsuarioEstado())){
			
		}
		}catch(Exception e){
			LOG.debug("Error activarUsuario " + e);
			throw new ControllerException("Error activarUsuario", e);
		}
		ModelAndView model=new ModelAndView("usuario/login");
		model.addObject("usuario", new UsuarioBean());
		return model;
		
	}
	
	*/
	/*
	@RequestMapping(value = "/recuperarClave", method = RequestMethod.POST)
	@ResponseBody
	public void recuperarClave(UsuarioBean usuarioBean) throws ControllerException {
		try {
			
			usuarioService.enviarLinkRecuperarClave(usuarioBean);

		} catch (Exception e) {
			LOG.debug("Error recuperarClave " + e);
			throw new ControllerException("Error recuperarClave", e);
		}
	}
	*/
	
	@RequestMapping(value = "/cambiarClave", method = RequestMethod.POST)
	@ResponseBody
	public void cambiarClave(HttpSession session, UsuarioBean usuarioBean) throws ControllerException {
		try {
			
			UsuarioBean usuarioSession= obtenerUsuarioSession(session);
			usuarioSession.setUsuarioClave(usuarioBean.getUsuarioClaveNueva());
			usuarioService.actualizar(usuarioSession);

		} catch (Exception e) {
			LOG.debug("Error cambiarClave " + e);
			throw new ControllerException("Error cambiarClave", e);
		}
	}
	
	@RequestMapping(value = "/salir", method = RequestMethod.POST)
	@ResponseBody
	public void salir(HttpSession session) throws ControllerException {
		try {
			session.setAttribute(ErpConstante.SESSION_ADJUNTO,null);
			session.setAttribute(ErpConstante.SESSION_ADJUNTO_MODELO,null);
			  session.setAttribute(ErpConstante.USUARIO_SESSION,null);
			  session.setAttribute(ErpConstante.SESSION_CARRITO,null);
			  session.setAttribute(ErpConstante.SESSION_CATEGORIA,null);
			  session.setAttribute(ErpConstante.SESSION_MODELO,null);
			  session.setAttribute(ErpConstante.SESSION_ESPECIFICACION,null);
			  
			  session.invalidate();
		} catch (Exception e) {
			LOG.debug("Error salir " + e);
			throw new ControllerException("Error salir", e);
		}
	}
}
