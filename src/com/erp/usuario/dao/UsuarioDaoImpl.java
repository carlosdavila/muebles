package com.erp.usuario.dao;

import org.hibernate.SessionFactory;


import com.erp.usuario.model.UsuarioBean;

import frameworkfrivas.dao.ComunDaoImpl;

public class UsuarioDaoImpl extends ComunDaoImpl<UsuarioBean, Integer>
		implements UsuarioDao {



	public UsuarioDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
/*
	@Transactional
	public UsuarioBean obtenerUsuarioPorLogin(UsuarioBean usuarioBean)
			throws DAOException {

		UsuarioBean usuario = null;
		try {
			
		 getCurrentSession().getTransaction().begin(); 
			usuario = (UsuarioBean) getCurrentSession()
					.createCriteria(UsuarioBean.class)
					.add(Restrictions.eq("usuarioUser",
							usuarioBean.getUsuarioUser().toLowerCase()))
					.add(Restrictions.eq("usuarioClave",
							usuarioBean.getUsuarioClave())).uniqueResult();
			 getCurrentSession().getTransaction().commit();
		} catch (Exception e) {
			 getCurrentSession().getTransaction().rollback();
			LOG.error(e.getMessage(), e);
			throw new DAOException(e);
		}
		
		
		return usuario;

	}
	
	@Transactional
	public UsuarioBean obtenerUsuarioPorNroActivacion(String usuarioNroActivacion)
			throws DAOException {

		UsuarioBean usuario = null;
		try {
			
		 getCurrentSession().getTransaction().begin(); 
			usuario = (UsuarioBean) getCurrentSession()
					.createCriteria(UsuarioBean.class)
					.add(Restrictions.eq("usuarioNroActivacion",
							usuarioNroActivacion)).uniqueResult();
			 getCurrentSession().getTransaction().commit();
		} catch (Exception e) {
			 getCurrentSession().getTransaction().rollback();
			LOG.error(e.getMessage(), e);
			throw new DAOException(e);
		}
		return usuario;

	}
	
	
	
	
	@Transactional
	public UsuarioBean obtenerUsuarioPorEmail(UsuarioBean usuarioBean)
			throws DAOException {

		UsuarioBean usuario = null;
		try {
			
		 getCurrentSession().getTransaction().begin(); 
			usuario = (UsuarioBean) getCurrentSession()
					.createCriteria(UsuarioBean.class)
					.add(Restrictions.eq("usuarioUser",
							usuarioBean.getUsuarioUser().toLowerCase())).uniqueResult();
			 getCurrentSession().getTransaction().commit();
		} catch (Exception e) {
			 getCurrentSession().getTransaction().rollback();
			LOG.error(e.getMessage(), e);
			throw new DAOException(e);
		}
		return usuario;

	}
	
	
	
	@Transactional
	public UsuarioBean obtenerUsuarioPorNroClave(String usuarioNroClave)
			throws DAOException {

		UsuarioBean usuario = null;
		try {
			
		 getCurrentSession().getTransaction().begin(); 
			usuario = (UsuarioBean) getCurrentSession()
					.createCriteria(UsuarioBean.class)
					.add(Restrictions.eq("usuarioNroClave",
							usuarioNroClave)).uniqueResult();
			 getCurrentSession().getTransaction().commit();
		} catch (Exception e) {
			 getCurrentSession().getTransaction().rollback();
			LOG.error(e.getMessage(), e);
			throw new DAOException(e);
		}
		return usuario;

	}
*/
}
