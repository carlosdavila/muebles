package com.erp.usuario.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_usuario")
public class UsuarioBean {

	@Id
	@GeneratedValue
	@Column(name = "usuario_id")
	private Integer usuarioId;

	@Column(name = "usuario_user")
	private String usuarioUser;

	@Column(name = "usuario_clave")
	private String usuarioClave;

	@Column(name = "usuario_estado")
	private String usuarioEstado;
	
	@Column(name = "usuario_perfil")
	private String usuarioPerfil;
	
	@Column(name = "usuario_nombre")
	private String usuarioNombre;
	
	@Column(name = "usuario_nro_activacion")
	private String usuarioNroActivacion;
	
	@Column(name = "usuario_nro_clave")
	private String usuarioNroClave;

	transient String usuarioClaveNueva;
	
	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getUsuarioUser() {
		return usuarioUser;
	}

	public void setUsuarioUser(String usuarioUser) {
		this.usuarioUser = usuarioUser;
	}

	public String getUsuarioClave() {
		return usuarioClave;
	}

	public void setUsuarioClave(String usuarioClave) {
		this.usuarioClave = usuarioClave;
	}

	public String getUsuarioEstado() {
		return usuarioEstado;
	}

	public void setUsuarioEstado(String usuarioEstado) {
		this.usuarioEstado = usuarioEstado;
	}

	public String getUsuarioPerfil() {
		return usuarioPerfil;
	}

	public void setUsuarioPerfil(String usuarioPerfil) {
		this.usuarioPerfil = usuarioPerfil;
	}

	public String getUsuarioNombre() {
		return usuarioNombre;
	}

	public void setUsuarioNombre(String usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}

	public String getUsuarioNroActivacion() {
		return usuarioNroActivacion;
	}

	public void setUsuarioNroActivacion(String usuarioNroActivacion) {
		this.usuarioNroActivacion = usuarioNroActivacion;
	}

	public String getUsuarioClaveNueva() {
		return usuarioClaveNueva;
	}

	public void setUsuarioClaveNueva(String usuarioClaveNueva) {
		this.usuarioClaveNueva = usuarioClaveNueva;
	}

	public String getUsuarioNroClave() {
		return usuarioNroClave;
	}

	public void setUsuarioNroClave(String usuarioNroClave) {
		this.usuarioNroClave = usuarioNroClave;
	}

	
	
}
