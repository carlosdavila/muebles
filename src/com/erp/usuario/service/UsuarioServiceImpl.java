package com.erp.usuario.service;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.comun.util.ErpConstante;
import com.erp.comun.util.ErpUtil;
import com.erp.usuario.model.UsuarioBean;


import frameworkfrivas.exception.BusinessException;
import frameworkfrivas.service.ComunServiceImpl;
@Service
public class UsuarioServiceImpl extends ComunServiceImpl<UsuarioBean, Integer> implements UsuarioService {

	private static final Logger LOG = Logger.getLogger(UsuarioServiceImpl.class);
	@Autowired
	private Properties erpConfig;
	

	public void guardarConNumeroActivacion(UsuarioBean usuarioBean) throws BusinessException {
		try {
			usuarioBean.setUsuarioNroActivacion(ErpUtil.obtenerNumeroAleatorio());
			enviarLinkConfirmacion( usuarioBean);
			usuarioBean.setUsuarioUser(usuarioBean.getUsuarioUser().toLowerCase());
			usuarioBean.setUsuarioPerfil(ErpConstante.USUARIO_PUBLICO);
			guardar(usuarioBean);
		} catch (Exception e) {
			LOG.debug(e);
			throw new BusinessException(e);
		}
	}

	/*
	public void enviarLinkRecuperarClave(UsuarioBean usuario) throws BusinessException {
		try {
			
			UsuarioBean usuarioBean= usuarioDao.obtenerUsuarioPorEmail(usuario);
			usuarioBean.setUsuarioNroClave(ErpUtil.obtenerNumeroAleatorio());
			usuarioDao.actualizar(usuarioBean);
			String ruta=erpConfig.getProperty(ErpConstante.RUTA);
			
			String link=ruta+"?nroRecuperarClave="+usuarioBean.getUsuarioNroClave();
			String mensaje = "<h2>Hola " + usuarioBean.getUsuarioNombre() + "! </h2><br>";
			mensaje =mensaje+ "<h2>Por favor dar click al Link  para restablecer su clave.</h2><br>";
			 
			 mensaje=mensaje+"<a href='"+link+"'><font color='red'>"+link+"</font></a>";

		
		
        	String userSmtp = erpConfig.getProperty(ErpConstante.CORREO_USER);
			String claveSmtp = erpConfig.getProperty(ErpConstante.CORREO_CLAVE);
			String host = erpConfig.getProperty(ErpConstante.CORREO_HOST);
			String puerto = erpConfig.getProperty(ErpConstante.CORREO_PUERTO);
			String from=erpConfig.getProperty(ErpConstante.CORREO_ENVIO);
			
			enviarCorreo(host, userSmtp, claveSmtp, puerto, from, usuarioBean.getUsuarioUser().toLowerCase(), "Correo de Recuperar Clave", mensaje);
			//enviarcorreo(host,puerto,user, clave, usuario.getUsuarioUser(), mensaje, "Carrito de Compras");
		
		
		
		
		
		} catch (Exception e) {
			LOG.debug(e);
			throw new BusinessException(e);
		}
	}
	*/
	private void enviarLinkConfirmacion(UsuarioBean usuarioBean) throws BusinessException {
		try {
			String ruta=erpConfig.getProperty(ErpConstante.RUTA);
			String link=ruta+"?nroActivacion="+usuarioBean.getUsuarioNroActivacion();
			String mensaje = "<h2>Hola " + usuarioBean.getUsuarioNombre() + "! </h2><br>";
			mensaje =mensaje+ "<h2>Bienvenido a Equipments.Por favor dar click al Link  para activar su usuario.</h2><br>";
			 mensaje=mensaje+ "<a href='"+link+"'><font color='red'>"+link+"</font></a>";
			/*
			String user = erpConfig.getProperty(ErpConstante.CORREO_USER);
			String clave = erpConfig.getProperty(ErpConstante.CORREO_CLAVE);
		
			String correoEnvio=erpConfig.getProperty(ErpConstante.CORREO_ENVIO);
		
			
            enviarcorreo2("fernando.rivas.fuentes@gmail.com", clave, usuarioBean.getUsuarioUser(), mensaje, "Correo de Activacion");
		*/
		
		
        	String userSmtp = erpConfig.getProperty(ErpConstante.CORREO_USER);
			String claveSmtp = erpConfig.getProperty(ErpConstante.CORREO_CLAVE);
			String host = erpConfig.getProperty(ErpConstante.CORREO_HOST);
			String puerto = erpConfig.getProperty(ErpConstante.CORREO_PUERTO);
			String from=erpConfig.getProperty(ErpConstante.CORREO_ENVIO);
			
			enviarCorreo(host, userSmtp, claveSmtp, puerto, from, usuarioBean.getUsuarioUser().toLowerCase(), "Correo de Activacion", mensaje);
			//enviarcorreo(host,puerto,user, clave, usuario.getUsuarioUser(), mensaje, "Carrito de Compras");
		
		
		
		
		
		} catch (Exception e) {
			LOG.debug(e);
			throw new BusinessException(e);
		}
	}
	
	

    public boolean enviarcorreo2(String de, String clave,String para,String mensaje,String asunto) throws BusinessException{
		 boolean enviado = false;
		 try{

		 String host="smtp.gmail.com";
	
		 Properties prop=System.getProperties();

		 prop.put("mail.smtp.starttls.enable","true");
		 prop.put("mail.smtp.host",host);
		 prop.put("mail.smtp.user",de);
		 prop.put("mail.smtp.password",clave);
		 prop.put("mail.smtp.port","465");
		 prop.put("mail.smtp.socketFactory.port", "465");
		 prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		 prop.put("mail.smtp.auth","true");

		 Session session=Session.getDefaultInstance(prop,null);
		 MimeMessage message= new MimeMessage(session);

		 message.setFrom(new InternetAddress(de));

		 message.setRecipient(Message.RecipientType.TO, new InternetAddress(para));
		 message.setSubject(asunto);
		// message.setText(mensaje,"text/html");
		
		 
		 BodyPart text = new MimeBodyPart( ) ; 
		  text.setContent(mensaje, "text/html" ) ; 
		  MimeMultipart multiPart = new MimeMultipart( ) ;
         multiPart.addBodyPart(text ) ; 
         message.setContent(multiPart);
		 
		

		 Transport transport=session.getTransport("smtp");
		 transport.connect(host,de,clave);

		 transport.sendMessage(message, message.getAllRecipients());
		 transport.close();
		 enviado=true;
	

		 }catch (Exception e){
			 LOG.error(e.getMessage(), e);
				throw new BusinessException(e);

		 }
		 return enviado;

		 }

		 

}
